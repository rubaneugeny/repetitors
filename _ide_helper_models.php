<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App{
/**
 * App\KoefPrint
 *
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|KoefPrint newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|KoefPrint newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|KoefPrint query()
 */
	class KoefPrint extends \Eloquent {}
}

namespace App{
/**
 * App\LernGeo
 *
 * @method static \Illuminate\Database\Eloquent\Builder|LernGeo newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LernGeo newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LernGeo query()
 */
	class LernGeo extends \Eloquent {}
}

namespace App\Models{
/**
 * AdminsReviews
 *
 * @mixin Eloquent
 * @property int $id_admins
 * @property string $name
 * @property string|null $phone Телефон
 * @property string|null $email E-mail
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Admins newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Admins newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Admins query()
 * @method static \Illuminate\Database\Eloquent\Builder|Admins whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admins whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admins whereIdAdmins($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admins whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admins wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admins whereUpdatedAt($value)
 */
	class Admins extends \Eloquent {}
}

namespace App\Models{
/**
 * AdminsReviews
 *
 * @mixin Eloquent
 * @property int $id_admins_reviews
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|AdminsReviews newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AdminsReviews newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AdminsReviews query()
 * @method static \Illuminate\Database\Eloquent\Builder|AdminsReviews whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdminsReviews whereIdAdminsReviews($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdminsReviews whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdminsReviews whereUpdatedAt($value)
 */
	class AdminsReviews extends \Eloquent {}
}

namespace App\Models{
/**
 * Blog
 *
 * @mixin Eloquent
 * @property int $id_blog
 * @property int $column Разделение на колонки
 * @property string $date
 * @property string $slug
 * @property string $breadcrumbs Хлебные крошки
 * @property string $header
 * @property string $title
 * @property string $text
 * @property string|null $photo
 * @property string|null $description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Blog newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Blog newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Blog query()
 * @method static \Illuminate\Database\Eloquent\Builder|Blog whereBreadcrumbs($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Blog whereColumn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Blog whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Blog whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Blog whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Blog whereHeader($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Blog whereIdBlog($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Blog wherePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Blog whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Blog whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Blog whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Blog whereUpdatedAt($value)
 */
	class Blog extends \Eloquent {}
}

namespace App\Models{
/**
 * BreadcrumbsCategories
 *
 * @mixin Eloquent
 * @property int $id_breadcrumbs_categories
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|BreadcrumbsCategories newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BreadcrumbsCategories newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BreadcrumbsCategories query()
 * @method static \Illuminate\Database\Eloquent\Builder|BreadcrumbsCategories whereIdBreadcrumbsCategories($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BreadcrumbsCategories whereName($value)
 */
	class BreadcrumbsCategories extends \Eloquent {}
}

namespace App\Models{
/**
 * BreadcrumbsTemplates
 *
 * @mixin Eloquent
 * @property int $id_breadcrumbs_templates
 * @property int $id_breadcrumbs_categories
 * @property int $type Тип: 1 - репетитор, 2 - автоинструктор
 * @property string $last
 * @property string $items
 * @property int $page
 * @method static \Illuminate\Database\Eloquent\Builder|BreadcrumbsTemplates newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BreadcrumbsTemplates newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BreadcrumbsTemplates query()
 * @method static \Illuminate\Database\Eloquent\Builder|BreadcrumbsTemplates whereIdBreadcrumbsCategories($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BreadcrumbsTemplates whereIdBreadcrumbsTemplates($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BreadcrumbsTemplates whereItems($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BreadcrumbsTemplates whereLast($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BreadcrumbsTemplates wherePage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BreadcrumbsTemplates whereType($value)
 */
	class BreadcrumbsTemplates extends \Eloquent {}
}

namespace App\Models{
/**
 * Cars
 *
 * @mixin Eloquent
 * @property int $id_cars
 * @property int $id_cars_manufactures
 * @property string $slug
 * @property string $name
 * @property string $name_eng
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\CarsManufactures|null $manufacturer
 * @method static \Illuminate\Database\Eloquent\Builder|Cars newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Cars newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Cars query()
 * @method static \Illuminate\Database\Eloquent\Builder|Cars whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Cars whereIdCars($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Cars whereIdCarsManufactures($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Cars whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Cars whereNameEng($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Cars whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Cars whereUpdatedAt($value)
 */
	class Cars extends \Eloquent {}
}

namespace App\Models{
/**
 * Cars Manufactures
 *
 * @mixin Eloquent
 * @property int $id_cars_manufactures
 * @property string $name
 * @property string $name_eng
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|CarsManufactures newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CarsManufactures newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CarsManufactures query()
 * @method static \Illuminate\Database\Eloquent\Builder|CarsManufactures whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CarsManufactures whereIdCarsManufactures($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CarsManufactures whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CarsManufactures whereNameEng($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CarsManufactures whereUpdatedAt($value)
 */
	class CarsManufactures extends \Eloquent {}
}

namespace App\Models{
/**
 * Cities
 *
 * @mixin Eloquent
 * @property int $id_cities
 * @property string $slug
 * @property string $name
 * @property string $in_the_direction
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Cities newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Cities newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Cities query()
 * @method static \Illuminate\Database\Eloquent\Builder|Cities whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Cities whereIdCities($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Cities whereInTheDirection($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Cities whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Cities whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Cities whereUpdatedAt($value)
 */
	class Cities extends \Eloquent {}
}

namespace App\Models{
/**
 * CommentsAppendTextTemplates
 *
 * @mixin Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|CommentsAppendTextTemplates newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CommentsAppendTextTemplates newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CommentsAppendTextTemplates query()
 */
	class CommentsAppendTextTemplates extends \Eloquent {}
}

namespace App\Models{
/**
 * CommentsPrependTextCategories
 *
 * @mixin Eloquent
 * @property int $id_comments_prepend_text_categories
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|CommentsPrependTextCategories newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CommentsPrependTextCategories newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CommentsPrependTextCategories query()
 * @method static \Illuminate\Database\Eloquent\Builder|CommentsPrependTextCategories whereIdCommentsPrependTextCategories($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CommentsPrependTextCategories whereName($value)
 */
	class CommentsPrependTextCategories extends \Eloquent {}
}

namespace App\Models{
/**
 * CommentsPrependTextTemplates
 *
 * @mixin Eloquent
 * @property int $id_comments_prepend_text_templates
 * @property int $id_comments_prepend_text_categories
 * @property int|null $id_courses
 * @property string|null $exclude_courses
 * @property string $template
 * @property int $page
 * @property int $row_index
 * @method static \Illuminate\Database\Eloquent\Builder|CommentsPrependTextTemplates newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CommentsPrependTextTemplates newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CommentsPrependTextTemplates query()
 * @method static \Illuminate\Database\Eloquent\Builder|CommentsPrependTextTemplates whereExcludeCourses($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CommentsPrependTextTemplates whereIdCommentsPrependTextCategories($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CommentsPrependTextTemplates whereIdCommentsPrependTextTemplates($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CommentsPrependTextTemplates whereIdCourses($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CommentsPrependTextTemplates wherePage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CommentsPrependTextTemplates whereRowIndex($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CommentsPrependTextTemplates whereTemplate($value)
 */
	class CommentsPrependTextTemplates extends \Eloquent {}
}

namespace App\Models{
/**
 * Countries
 *
 * @mixin Eloquent
 * @property int $id_countries
 * @property string $name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CountriesCities[] $cities
 * @property-read int|null $cities_count
 * @method static \Illuminate\Database\Eloquent\Builder|Countries newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Countries newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Countries query()
 * @method static \Illuminate\Database\Eloquent\Builder|Countries whereIdCountries($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Countries whereName($value)
 */
	class Countries extends \Eloquent {}
}

namespace App\Models{
/**
 * CountriesCities
 *
 * @mixin Eloquent
 * @property int $id_countries_cities
 * @property int $id_countries
 * @property string $name
 * @property string $url
 * @method static \Illuminate\Database\Eloquent\Builder|CountriesCities newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CountriesCities newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CountriesCities query()
 * @method static \Illuminate\Database\Eloquent\Builder|CountriesCities whereIdCountries($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CountriesCities whereIdCountriesCities($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CountriesCities whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CountriesCities whereUrl($value)
 */
	class CountriesCities extends \Eloquent {}
}

namespace App\Models{
/**
 * Courses
 *
 * @mixin Eloquent
 * @property int $id_courses
 * @property int|null $id_courses_categories ??? Основная категория, непонятное разделение и нет точного ответа на это
 * @property int|null $id_courses_categories_second ??? Вторая категория для фильтрации, непонятное разделение и нет точного ответа на это
 * @property int $id_courses_types Тип предмета
 * @property int|null $id_title_categories Шаблон заголовка предмета для генерации Title, Description
 * @property int|null $id_total_categories Шаблон значения Всего преподавателей на странице, при отсутствии не показывается данная запись
 * @property int|null $id_prepend_text_categories Шаблон текста после Всего преподавателей на странице
 * @property int|null $id_photo_text_categories Шаблон текста в img alt преподавателя
 * @property int|null $id_comments_prepend_text_categories Шаблон текста перед комментарием
 * @property int|null $id_pages_append_text_categories Шаблон текста после списка номеров страниц
 * @property int|null $id_parent_course Указывается основной предмет для объединения в группу с подкатегориями. Используется для генерации title
 * @property int|null $id_courses_second Второй предмет для выборки например биология и химия
 * @property int|null $id_courses_third Третий предмет для выборки
 * @property string $name Название курса
 * @property string $slug
 * @property string|null $slug_second
 * @property string|null $recommend_name Имя в рекомендованных ссылках, шаблонах
 * @property string|null $recommend_name_second Имя в рекомендованных ссылках, шаблонах
 * @property string|null $recommend_name_third Имя в рекомендованных ссылках
 * @property string|null $link4 ???
 * @property string|null $link5 ???
 * @property string|null $link6 ???
 * @property string|null $recommend_name_seventh Используется для автозамены в шаблонах
 * @property string|null $recommend_name_eighth Используется для автозамены в шаблонах
 * @property string|null $recommend_name_nineth Шаблон mini_text
 * @property int $has_review Есть ли отзывы к предмету
 * @property int $class_menu Включить позицию поиска по классам в фильтре поиска
 * @property int $skype Возможно ли проводить онлайн занятие
 * @property int $malo ???
 * @property int $spbgu Флаг для фильтра на странице other_predmet_mgu.php
 * @property string|null $multiplier_duration Коэффициент, изначально рассчитанный для каждого предмета на основании средней длительности занятий по данному предмету, используется для расчета цены при заявке
 * @property int|null $koef_kol Количество репетиторов c aktive 2,3,5,6,8 по предметам начальный момент введения коэффициентов рассчитанное, на основании него прописывается необходимый коэффициент на странице расчета итогового коэффициента
 * @property int|null $price_min ???
 * @property int|null $price_max ???
 * @property float|null $koef_ind_sr ???
 * @property string|null $cities Используется для страницы other_predmet_gorod.php для выборки предметов по конкретному городу
 * @property string|null $metro Используется для страницы other_predmet_metro.php для выборки предметов по конкретному метро
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read Courses|null $parentCourse
 * @property-read Courses|null $secondCourse
 * @property-read \App\Models\Seo|null $seo
 * @method static \Illuminate\Database\Eloquent\Builder|Courses newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Courses newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Courses query()
 * @method static \Illuminate\Database\Eloquent\Builder|Courses whereCities($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Courses whereClassMenu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Courses whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Courses whereHasReview($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Courses whereIdCommentsPrependTextCategories($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Courses whereIdCourses($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Courses whereIdCoursesCategories($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Courses whereIdCoursesCategoriesSecond($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Courses whereIdCoursesSecond($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Courses whereIdCoursesThird($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Courses whereIdCoursesTypes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Courses whereIdPagesAppendTextCategories($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Courses whereIdParentCourse($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Courses whereIdPhotoTextCategories($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Courses whereIdPrependTextCategories($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Courses whereIdTitleCategories($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Courses whereIdTotalCategories($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Courses whereKoefIndSr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Courses whereKoefKol($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Courses whereLink4($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Courses whereLink5($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Courses whereLink6($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Courses whereMalo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Courses whereMetro($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Courses whereMultiplierDuration($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Courses whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Courses wherePriceMax($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Courses wherePriceMin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Courses whereRecommendName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Courses whereRecommendNameEighth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Courses whereRecommendNameNineth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Courses whereRecommendNameSecond($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Courses whereRecommendNameSeventh($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Courses whereRecommendNameThird($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Courses whereSkype($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Courses whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Courses whereSlugSecond($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Courses whereSpbgu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Courses whereUpdatedAt($value)
 */
	class Courses extends \Eloquent {}
}

namespace App\Models{
/**
 * CoursesCategories
 *
 * @mixin Eloquent
 * @property int $id_courses_categories
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|CoursesCategories newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CoursesCategories newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CoursesCategories query()
 * @method static \Illuminate\Database\Eloquent\Builder|CoursesCategories whereIdCoursesCategories($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CoursesCategories whereName($value)
 */
	class CoursesCategories extends \Eloquent {}
}

namespace App\Models{
/**
 * CoursesCategoriesSecond
 *
 * @mixin Eloquent
 * @property int $id_courses_categories_second
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|CoursesCategoriesSecond newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CoursesCategoriesSecond newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CoursesCategoriesSecond query()
 * @method static \Illuminate\Database\Eloquent\Builder|CoursesCategoriesSecond whereIdCoursesCategoriesSecond($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CoursesCategoriesSecond whereName($value)
 */
	class CoursesCategoriesSecond extends \Eloquent {}
}

namespace App\Models{
/**
 * CoursesSubcourses
 *
 * @mixin Eloquent
 * @property int $id_courses
 * @property int $id_users
 * @method static \Illuminate\Database\Eloquent\Builder|CoursesSubcourses newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CoursesSubcourses newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CoursesSubcourses query()
 * @method static \Illuminate\Database\Eloquent\Builder|CoursesSubcourses whereIdCourses($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CoursesSubcourses whereIdUsers($value)
 */
	class CoursesSubcourses extends \Eloquent {}
}

namespace App\Models{
/**
 * CoursesTypes
 *
 * @mixin Eloquent
 * @property int $id_courses_types
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|CoursesTypes newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CoursesTypes newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CoursesTypes query()
 * @method static \Illuminate\Database\Eloquent\Builder|CoursesTypes whereIdCoursesTypes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CoursesTypes whereName($value)
 */
	class CoursesTypes extends \Eloquent {}
}

namespace App\Models{
/**
 * Districts
 *
 * @mixin Eloquent
 * @property int $id_districts
 * @property string $slug
 * @property string $name
 * @property string $in_the_direction
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Districts newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Districts newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Districts query()
 * @method static \Illuminate\Database\Eloquent\Builder|Districts whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Districts whereIdDistricts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Districts whereInTheDirection($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Districts whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Districts whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Districts whereUpdatedAt($value)
 */
	class Districts extends \Eloquent {}
}

namespace App\Models{
/**
 * Districts
 *
 * @mixin Eloquent
 * @property int $id_favourites
 * @property int $id_users
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Favourites newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Favourites newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Favourites query()
 * @method static \Illuminate\Database\Eloquent\Builder|Favourites whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Favourites whereIdFavourites($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Favourites whereIdUsers($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Favourites whereUpdatedAt($value)
 */
	class Favourites extends \Eloquent {}
}

namespace App\Models{
/**
 * Headers
 *
 * @mixin Eloquent
 * @property int $id_headers
 * @property string $slug
 * @property string $title
 * @method static \Illuminate\Database\Eloquent\Builder|Headers newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Headers newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Headers query()
 * @method static \Illuminate\Database\Eloquent\Builder|Headers whereIdHeaders($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Headers whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Headers whereTitle($value)
 */
	class Headers extends \Eloquent {}
}

namespace App\Models{
/**
 * LinksCategories
 *
 * @mixin Eloquent
 * @property int $id_links_categories
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|LinksCategories newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LinksCategories newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LinksCategories query()
 * @method static \Illuminate\Database\Eloquent\Builder|LinksCategories whereIdLinksCategories($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LinksCategories whereName($value)
 */
	class LinksCategories extends \Eloquent {}
}

namespace App\Models{
/**
 * LinksTemplates
 *
 * @mixin Eloquent
 * @property int $id_links_templates
 * @property int $id_links_categories
 * @property string $template
 * @property int $page
 * @method static \Illuminate\Database\Eloquent\Builder|LinksTemplates newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LinksTemplates newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LinksTemplates query()
 * @method static \Illuminate\Database\Eloquent\Builder|LinksTemplates whereIdLinksCategories($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LinksTemplates whereIdLinksTemplates($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LinksTemplates wherePage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LinksTemplates whereTemplate($value)
 */
	class LinksTemplates extends \Eloquent {}
}

namespace App\Models{
/**
 * Metro
 *
 * @mixin Eloquent
 * @property int $id_metro
 * @property int $id_metro_branches
 * @property string $slug
 * @property string $name
 * @property string $in_the_direction
 * @property int $ao ???
 * @property int $rating Используется в рекомендованных ссылках
 * @property string $nearest_stations Ближайшие станции
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\MetroBranches|null $branch
 * @method static \Illuminate\Database\Eloquent\Builder|Metro newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Metro newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Metro query()
 * @method static \Illuminate\Database\Eloquent\Builder|Metro whereAo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Metro whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Metro whereIdMetro($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Metro whereIdMetroBranches($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Metro whereInTheDirection($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Metro whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Metro whereNearestStations($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Metro whereRating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Metro whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Metro whereUpdatedAt($value)
 */
	class Metro extends \Eloquent {}
}

namespace App\Models{
/**
 * MetroBranches
 *
 * @mixin Eloquent
 * @property int $id_metro_branches
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|MetroBranches newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MetroBranches newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MetroBranches query()
 * @method static \Illuminate\Database\Eloquent\Builder|MetroBranches whereIdMetroBranches($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MetroBranches whereName($value)
 */
	class MetroBranches extends \Eloquent {}
}

namespace App\Models{
/**
 * MetroBranches
 *
 * @mixin Eloquent
 * @property int $id_multipliers
 * @property string $multiplier
 * @property string $date
 * @method static \Illuminate\Database\Eloquent\Builder|Multipliers newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Multipliers newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Multipliers query()
 * @method static \Illuminate\Database\Eloquent\Builder|Multipliers whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Multipliers whereIdMultipliers($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Multipliers whereMultiplier($value)
 */
	class Multipliers extends \Eloquent {}
}

namespace App\Models{
/**
 * News
 *
 * @mixin Eloquent
 * @property int $id_news
 * @property string $date
 * @property string $name
 * @property string $title
 * @property string $text
 * @property string|null $photo
 * @property string|null $description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|News newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|News newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|News query()
 * @method static \Illuminate\Database\Eloquent\Builder|News whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|News whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|News whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|News whereIdNews($value)
 * @method static \Illuminate\Database\Eloquent\Builder|News whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|News wherePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|News whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|News whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|News whereUpdatedAt($value)
 */
	class News extends \Eloquent {}
}

namespace App\Models{
/**
 * NewsUsers
 *
 * @mixin Eloquent
 * @property int $id_news_users
 * @property string $date
 * @property string $title
 * @property string $text
 * @property int $for_car_instructors Новость для автоинструкторов
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|NewsUsers newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|NewsUsers newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|NewsUsers query()
 * @method static \Illuminate\Database\Eloquent\Builder|NewsUsers whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsUsers whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsUsers whereForCarInstructors($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsUsers whereIdNewsUsers($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsUsers whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsUsers whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsUsers whereUpdatedAt($value)
 */
	class NewsUsers extends \Eloquent {}
}

namespace App\Models{
/**
 * Orders
 *
 * @mixin Eloquent
 * @property int $id_orders
 * @property int $id_orders_statuses Статус заявки
 * @property int $id_orders_admin_statuses Статус заявки для администратора
 * @property int|null $id_orders_types Тип заявки
 * @property int|null $id_admins Администратор заявки
 * @property string $date Дата заказа
 * @property string $name Имя заказчика
 * @property string|null $phone Должно быть NOT NULL, но старые записи без указания телефона
 * @property string|null $email
 * @property int|null $id_cities Город
 * @property int|null $id_metro Метро
 * @property int|null $id_courses Предмет
 * @property int $id_orders_locations Место проведения занятия
 * @property int|null $id_users Репетитор, который взял заказ
 * @property int|null $id_users_referrer Реферальный ID преподавателя, за которого начислится бонусная программа
 * @property string|null $object Цель занятия
 * @property string|null $additional_information Дополнительная информация
 * @property int $duration Продолжительность занятия в минутах
 * @property string|null $comment Комментарий от администратора
 * @property string|null $first_lesson_date Дата первого занятия
 * @property string $price_limit Желаемая стоимость занятия от заказчика
 * @property string $price_per_user Сколько должен заплатить преподаватель, если у него было менее 5 занятий
 * @property string $how_much_paid Сколько оплатил преподаватель из выставленной суммы
 * @property string $total_price Общая стоимость заказа
 * @property string|null $kogda ???
 * @property string|null $god ???
 * @property string|null $mes ???
 * @property string|null $den ???
 * @property string|null $data_od ???
 * @property int $ps1 Счетчик кол-ва смс с письмом 1
 * @property int $ps2 Счетчик кол-ва смс с письмом 2
 * @property int $ps3 Счетчик кол-ва смс с письмом 3
 * @property int $p1 Счетчик кол-ва писем с письмом 1
 * @property int $p2 Счетчик кол-ва писем с письмом 2
 * @property int $p3 Счетчик кол-ва писем с письмом 3
 * @property string|null $calculation Детальный расчет цены с формулами и суммами
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Courses|null $course
 * @property-read \App\Models\Reviews|null $review
 * @property-read \App\Models\Users|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|Orders newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Orders newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Orders query()
 * @method static \Illuminate\Database\Eloquent\Builder|Orders whereAdditionalInformation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Orders whereCalculation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Orders whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Orders whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Orders whereDataOd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Orders whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Orders whereDen($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Orders whereDuration($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Orders whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Orders whereFirstLessonDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Orders whereGod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Orders whereHowMuchPaid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Orders whereIdAdmins($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Orders whereIdCities($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Orders whereIdCourses($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Orders whereIdMetro($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Orders whereIdOrders($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Orders whereIdOrdersAdminStatuses($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Orders whereIdOrdersLocations($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Orders whereIdOrdersStatuses($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Orders whereIdOrdersTypes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Orders whereIdUsers($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Orders whereIdUsersReferrer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Orders whereKogda($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Orders whereMes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Orders whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Orders whereObject($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Orders whereP1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Orders whereP2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Orders whereP3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Orders wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Orders wherePriceLimit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Orders wherePricePerUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Orders wherePs1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Orders wherePs2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Orders wherePs3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Orders whereTotalPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Orders whereUpdatedAt($value)
 */
	class Orders extends \Eloquent {}
}

namespace App\Models{
/**
 * OrdersAdminStatuses
 *
 * @mixin Eloquent
 * @property int $id_orders_admin_statuses
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|OrdersAdminStatuses newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OrdersAdminStatuses newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OrdersAdminStatuses query()
 * @method static \Illuminate\Database\Eloquent\Builder|OrdersAdminStatuses whereIdOrdersAdminStatuses($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrdersAdminStatuses whereName($value)
 */
	class OrdersAdminStatuses extends \Eloquent {}
}

namespace App\Models{
/**
 * OrdersLocations
 *
 * @mixin Eloquent
 * @property int $id_orders_locations
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|OrdersLocations newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OrdersLocations newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OrdersLocations query()
 * @method static \Illuminate\Database\Eloquent\Builder|OrdersLocations whereIdOrdersLocations($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrdersLocations whereName($value)
 */
	class OrdersLocations extends \Eloquent {}
}

namespace App\Models{
/**
 * OrdersStatuses
 *
 * @mixin Eloquent
 * @property int $id_orders_statuses
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|OrdersStatuses newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OrdersStatuses newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OrdersStatuses query()
 * @method static \Illuminate\Database\Eloquent\Builder|OrdersStatuses whereIdOrdersStatuses($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrdersStatuses whereName($value)
 */
	class OrdersStatuses extends \Eloquent {}
}

namespace App\Models{
/**
 * OrdersTypes
 *
 * @mixin Eloquent
 * @property int $id_orders_types
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|OrdersTypes newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OrdersTypes newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OrdersTypes query()
 * @method static \Illuminate\Database\Eloquent\Builder|OrdersTypes whereIdOrdersTypes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrdersTypes whereName($value)
 */
	class OrdersTypes extends \Eloquent {}
}

namespace App\Models{
/**
 * PagesAppendTextCategories
 *
 * @mixin Eloquent
 * @property int $id_pages_append_text_categories
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|PagesAppendTextCategories newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PagesAppendTextCategories newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PagesAppendTextCategories query()
 * @method static \Illuminate\Database\Eloquent\Builder|PagesAppendTextCategories whereIdPagesAppendTextCategories($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PagesAppendTextCategories whereName($value)
 */
	class PagesAppendTextCategories extends \Eloquent {}
}

namespace App\Models{
/**
 * PagesAppendTextTemplates
 *
 * @mixin Eloquent
 * @property int $id_pages_append_text_templates
 * @property int $id_pages_append_text_categories
 * @property int|null $id_courses_categories
 * @property int|null $id_courses
 * @property string $template Основной шаблон
 * @property int $page Страница, где будет показываться шаблон
 * @property int $split Если установлен флаг используется функция splitSentence либо cutUpToSentence, должен быть указан text_limit
 * @property int $seo Если установлен флаг, то вначале проверяется поле text из таблицы seo, потом уже берется шаблон
 * @property int|null $text_limit Ограничение выводимого текста
 * @property int $repetitors_count_limit Количество репетиторов, меньше которого будет показываться второй шаблон, 0 - без ограничений
 * @property string $class CSS класс, который будет добавлен в тег p
 * @method static \Illuminate\Database\Eloquent\Builder|PagesAppendTextTemplates newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PagesAppendTextTemplates newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PagesAppendTextTemplates query()
 * @method static \Illuminate\Database\Eloquent\Builder|PagesAppendTextTemplates whereClass($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PagesAppendTextTemplates whereIdCourses($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PagesAppendTextTemplates whereIdCoursesCategories($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PagesAppendTextTemplates whereIdPagesAppendTextCategories($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PagesAppendTextTemplates whereIdPagesAppendTextTemplates($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PagesAppendTextTemplates wherePage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PagesAppendTextTemplates whereRepetitorsCountLimit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PagesAppendTextTemplates whereSeo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PagesAppendTextTemplates whereSplit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PagesAppendTextTemplates whereTemplate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PagesAppendTextTemplates whereTextLimit($value)
 */
	class PagesAppendTextTemplates extends \Eloquent {}
}

namespace App\Models{
/**
 * Phonebook
 *
 * @mixin Eloquent
 * @property int $id_phonebook
 * @property string $phone
 * @property int $id_users
 * @property string|null $last_name
 * @property string|null $first_name
 * @property string|null $middle_name
 * @property string|null $email
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Phonebook newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Phonebook newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Phonebook query()
 * @method static \Illuminate\Database\Eloquent\Builder|Phonebook whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Phonebook whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Phonebook whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Phonebook whereIdPhonebook($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Phonebook whereIdUsers($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Phonebook whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Phonebook whereMiddleName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Phonebook wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Phonebook whereUpdatedAt($value)
 */
	class Phonebook extends \Eloquent {}
}

namespace App\Models{
/**
 * PhotoTextCategories
 *
 * @mixin Eloquent
 * @property int $id_photo_text_categories
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|PhotoTextCategories newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PhotoTextCategories newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PhotoTextCategories query()
 * @method static \Illuminate\Database\Eloquent\Builder|PhotoTextCategories whereIdPhotoTextCategories($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PhotoTextCategories whereName($value)
 */
	class PhotoTextCategories extends \Eloquent {}
}

namespace App\Models{
/**
 * PhotoTextTemplates
 *
 * @mixin Eloquent
 * @property int $id_photo_text_templates
 * @property int $id_photo_text_categories
 * @property int|null $id_courses_categories_second
 * @property int|null $id_courses
 * @property string $template
 * @property int $page
 * @property int $row_index Номер строки при выборке
 * @method static \Illuminate\Database\Eloquent\Builder|PhotoTextTemplates newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PhotoTextTemplates newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PhotoTextTemplates query()
 * @method static \Illuminate\Database\Eloquent\Builder|PhotoTextTemplates whereIdCourses($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PhotoTextTemplates whereIdCoursesCategoriesSecond($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PhotoTextTemplates whereIdPhotoTextCategories($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PhotoTextTemplates whereIdPhotoTextTemplates($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PhotoTextTemplates wherePage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PhotoTextTemplates whereRowIndex($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PhotoTextTemplates whereTemplate($value)
 */
	class PhotoTextTemplates extends \Eloquent {}
}

namespace App\Models{
/**
 * PrependTextCategories
 *
 * @mixin Eloquent
 * @property int $id_prepend_text_categories
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|PrependTextCategories newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PrependTextCategories newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PrependTextCategories query()
 * @method static \Illuminate\Database\Eloquent\Builder|PrependTextCategories whereIdPrependTextCategories($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PrependTextCategories whereName($value)
 */
	class PrependTextCategories extends \Eloquent {}
}

namespace App\Models{
/**
 * PrependTextTemplates
 *
 * @mixin Eloquent
 * @property int $id_prepend_text_templates
 * @property int $id_prepend_text_categories
 * @property string $template Основной шаблон
 * @property int $page Основная страница
 * @property int $wrap Установлен флаг, то оборачивается текст в шаблон prepend-text
 * @property int $split Если установлен флаг используется функция splitSentence либо cutUpToSentence, должен быть указан text_limit
 * @property int|null $text_limit Ограничение длины текста для переноса
 * @property string $class CSS класс для тега p
 * @method static \Illuminate\Database\Eloquent\Builder|PrependTextTemplates newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PrependTextTemplates newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PrependTextTemplates query()
 * @method static \Illuminate\Database\Eloquent\Builder|PrependTextTemplates whereClass($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PrependTextTemplates whereIdPrependTextCategories($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PrependTextTemplates whereIdPrependTextTemplates($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PrependTextTemplates wherePage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PrependTextTemplates whereSplit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PrependTextTemplates whereTemplate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PrependTextTemplates whereTextLimit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PrependTextTemplates whereWrap($value)
 */
	class PrependTextTemplates extends \Eloquent {}
}

namespace App\Models{
/**
 * Ratings
 *
 * @mixin Eloquent
 * @property int $id_users
 * @property int $id_courses Используется для фильтрации подкатегорий, например репетиторы эксперты ЕГЭ и другие.
 * @property int $rating
 * @method static \Illuminate\Database\Eloquent\Builder|Ratings newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Ratings newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Ratings query()
 * @method static \Illuminate\Database\Eloquent\Builder|Ratings whereIdCourses($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ratings whereIdUsers($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ratings whereRating($value)
 */
	class Ratings extends \Eloquent {}
}

namespace App\Models{
/**
 * Reviews
 *
 * @mixin Eloquent
 * @property int $id_reviews
 * @property int $id_reviews_statuses Статус отзыва 1 - Новый, 2 - Подтвержденный, 3 - Отклоненный
 * @property int|null $id_orders
 * @property int $id_users
 * @property int|null $id_admins_reviews
 * @property string $date
 * @property string $name
 * @property string|null $phone
 * @property string $comment
 * @property string|null $response Ответ репетитора на отзыв, возможно заменим на доп. таблицу с сообщениями
 * @property int $rating
 * @property int $is_deleted
 * @property int|null $new ???
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Courses|null $course
 * @property-read \App\Models\Orders|null $order
 * @property-read \App\Models\ReviewsStatuses|null $status
 * @property-read \App\Models\Users|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|Reviews newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Reviews newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Reviews query()
 * @method static \Illuminate\Database\Eloquent\Builder|Reviews whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Reviews whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Reviews whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Reviews whereIdAdminsReviews($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Reviews whereIdOrders($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Reviews whereIdReviews($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Reviews whereIdReviewsStatuses($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Reviews whereIdUsers($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Reviews whereIsDeleted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Reviews whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Reviews whereNew($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Reviews wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Reviews whereRating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Reviews whereResponse($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Reviews whereUpdatedAt($value)
 */
	class Reviews extends \Eloquent {}
}

namespace App\Models{
/**
 * ReviewsStatuses
 *
 * @mixin Eloquent
 * @property int $id_reviews_statuses
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|ReviewsStatuses newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ReviewsStatuses newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ReviewsStatuses query()
 * @method static \Illuminate\Database\Eloquent\Builder|ReviewsStatuses whereIdReviewsStatuses($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReviewsStatuses whereName($value)
 */
	class ReviewsStatuses extends \Eloquent {}
}

namespace App\Models{
/**
 * ReviewsTeachers
 *
 * @mixin Eloquent
 * @property int $id_reviews_teachers
 * @property int $id_users
 * @property int|null $id_courses ??? Непонятно, нужно ли оставлять это поле или удалять, посмотреть после админки
 * @property int $is_approved Подтвержден ли отзыв
 * @property string $comment
 * @property int $id_lern_temp
 * @property int $active_temp
 * @property-read \App\Models\Users|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|ReviewsTeachers newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ReviewsTeachers newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ReviewsTeachers query()
 * @method static \Illuminate\Database\Eloquent\Builder|ReviewsTeachers whereActiveTemp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReviewsTeachers whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReviewsTeachers whereIdCourses($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReviewsTeachers whereIdLernTemp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReviewsTeachers whereIdReviewsTeachers($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReviewsTeachers whereIdUsers($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReviewsTeachers whereIsApproved($value)
 */
	class ReviewsTeachers extends \Eloquent {}
}

namespace App\Models{
/**
 * Seo
 *
 * @mixin Eloquent
 * @property int $id_seo
 * @property string|null $emoji Иконка в title
 * @property string|null $title Заголовок страницы
 * @property string|null $h1 Заголовок страницы
 * @property string|null $total_count Текст перед общим количеством записей преподавателей на странице
 * @property string|null $prepend_text Текст после всего репетиторов на странице
 * @property string|null $text Текст в конце страницы после номеров страниц
 * @property string|null $link ???
 * @property string|null $descript ???
 * @property string|null $k1 ???
 * @property string|null $k2 ???
 * @property string|null $k3 ???
 * @property string|null $k4 ???
 * @property string|null $k5 ???
 * @property string|null $k6 ???
 * @property string|null $k7 ???
 * @property string|null $k8 ???
 * @property string|null $k9 ???
 * @property string|null $k10 ???
 * @method static \Illuminate\Database\Eloquent\Builder|Seo newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Seo newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Seo query()
 * @method static \Illuminate\Database\Eloquent\Builder|Seo whereDescript($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Seo whereEmoji($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Seo whereH1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Seo whereIdSeo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Seo whereK1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Seo whereK10($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Seo whereK2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Seo whereK3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Seo whereK4($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Seo whereK5($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Seo whereK6($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Seo whereK7($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Seo whereK8($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Seo whereK9($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Seo whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Seo wherePrependText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Seo whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Seo whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Seo whereTotalCount($value)
 */
	class Seo extends \Eloquent {}
}

namespace App\Models{
/**
 * Sms
 *
 * @mixin Eloquent
 * @property int $id_sms
 * @property string $id
 * @property string $date
 * @property string $phone Телефон получателя
 * @property string $message Сообщение
 * @property int|null $id_admins
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Sms newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Sms newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Sms query()
 * @method static \Illuminate\Database\Eloquent\Builder|Sms whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Sms whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Sms whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Sms whereIdAdmins($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Sms whereIdSms($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Sms whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Sms wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Sms whereUpdatedAt($value)
 */
	class Sms extends \Eloquent {}
}

namespace App\Models{
/**
 * TitleCategories
 *
 * @mixin Eloquent
 * @property int $id_title_categories
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|TitleCategories newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TitleCategories newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TitleCategories query()
 * @method static \Illuminate\Database\Eloquent\Builder|TitleCategories whereIdTitleCategories($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TitleCategories whereName($value)
 */
	class TitleCategories extends \Eloquent {}
}

namespace App\Models{
/**
 * TitleTemplates
 *
 * @mixin Eloquent
 * @property int $id_title_templates
 * @property int $id_title_categories Категория заголовка
 * @property int|null $id_courses_categories Категория предмета основная
 * @property int|null $id_courses_categories_second Категория предмета дополнительная
 * @property int|null $id_courses_types Тип предмета
 * @property int|null $id_parent_course Основной предмет для группировки
 * @property int|null $id_courses Предмет
 * @property int $page Номер страницы, на которой будет размещен, 0 - все страницы
 * @property string $title
 * @property string|null $description
 * @method static \Illuminate\Database\Eloquent\Builder|TitleTemplates newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TitleTemplates newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TitleTemplates query()
 * @method static \Illuminate\Database\Eloquent\Builder|TitleTemplates whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TitleTemplates whereIdCourses($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TitleTemplates whereIdCoursesCategories($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TitleTemplates whereIdCoursesCategoriesSecond($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TitleTemplates whereIdCoursesTypes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TitleTemplates whereIdParentCourse($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TitleTemplates whereIdTitleCategories($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TitleTemplates whereIdTitleTemplates($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TitleTemplates wherePage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TitleTemplates whereTitle($value)
 */
	class TitleTemplates extends \Eloquent {}
}

namespace App\Models{
/**
 * TotalCategories
 *
 * @mixin Eloquent
 * @property int $id_total_categories
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|TotalCategories newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TotalCategories newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TotalCategories query()
 * @method static \Illuminate\Database\Eloquent\Builder|TotalCategories whereIdTotalCategories($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TotalCategories whereName($value)
 */
	class TotalCategories extends \Eloquent {}
}

namespace App\Models{
/**
 * TotalTemplates
 *
 * @mixin Eloquent
 * @property int $id_total_templates
 * @property int $id_total_categories Категория заголовка
 * @property string|null $template
 * @property int $page
 * @method static \Illuminate\Database\Eloquent\Builder|TotalTemplates newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TotalTemplates newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TotalTemplates query()
 * @method static \Illuminate\Database\Eloquent\Builder|TotalTemplates whereIdTotalCategories($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TotalTemplates whereIdTotalTemplates($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TotalTemplates wherePage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TotalTemplates whereTemplate($value)
 */
	class TotalTemplates extends \Eloquent {}
}

namespace App\Models{
/**
 * Universities
 *
 * @mixin Eloquent
 * @property int $id_universities
 * @property string $slug
 * @property string $name
 * @property string $short_name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Universities newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Universities newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Universities query()
 * @method static \Illuminate\Database\Eloquent\Builder|Universities whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Universities whereIdUniversities($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Universities whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Universities whereShortName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Universities whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Universities whereUpdatedAt($value)
 */
	class Universities extends \Eloquent {}
}

namespace App\Models{
/**
 * Users
 *
 * @mixin Eloquent
 * @property int $id_users
 * @property string|null $slug
 * @property string $login Логин
 * @property string $password Пароль
 * @property string|null $email E-mail
 * @property int $sex Пол: 0 - мужчина, 1 - женщина
 * @property string|null $last_name Фамилия
 * @property string|null $first_name Имя
 * @property string|null $middle_name Отчество
 * @property string|null $phone Телефон
 * @property string|null $landline_phone Городской телефон
 * @property int|null $year_of_birth Год рождения
 * @property string|null $education Образование
 * @property int|null $work_experience Стаж преподавателя / Стаж обучения вождению
 * @property string|null $work_experience_info Стаж вождения
 * @property int|null $graduation_year Год окончания учебного заведения
 * @property int|null $id_courses Основной предмет
 * @property int|null $id_courses_second Дополнительный предмет
 * @property int|null $id_courses_third Дополнительный предмет
 * @property int|null $id_courses_fourth Дополнительный предмет
 * @property int|null $id_cities Основной город
 * @property int|null $id_cities_second Дополнительный город
 * @property int|null $id_cities_third Дополнительный город
 * @property int|null $id_cities_fourth Дополнительный город
 * @property string|null $metro_list Список станций метро куда выезжает преподаватель
 * @property int|null $id_metro Основная станция метро
 * @property int|null $id_metro_second Дополнительная станция метро
 * @property int|null $id_districts Район
 * @property int|null $id_users_statuses Статус преподавателя
 * @property int $id_users_active_statuses Статус преподавателя для администратора
 * @property int $is_car_instructor Флаг, является ли преподаватель автоинструктором
 * @property int|null $id_cars Первый автомобиль
 * @property int|null $id_cars_second Второй автомобиль
 * @property int $teachers_home Принимает ли преподаватель дома
 * @property int $students_home Выезжает ли преподаватель к ученику
 * @property int $first_lesson_free Первое занятие бесплатно?
 * @property int $show_last_name Показать/Скрыть фамилию преподавателя
 * @property int $skype Флаг, обучение происходит по skype
 * @property string|null $reg_date Дата регистрации
 * @property string|null $comment Комментарий
 * @property string|null $additional_information Дополнительная информация
 * @property string $price_per_hour Стоимость занятия в час
 * @property string|null $services_and_prices Услуги и цены, отображается в карточке преподавателя
 * @property int $orders_count Количество заявок
 * @property float $reviews_rating Рейтинг преподавателя по отзывам
 * @property int $reviews_positive_count Кол-во положительных отзывов
 * @property int $reviews_neutral_count Кол-во нейтральных отзывов
 * @property int $reviews_negative_count Кол-во негативных отзывов
 * @property int|null $id_universities Университет
 * @property string|null $komdoma ??? Районы проведения занятий / Куда выезжает преподаватель
 * @property string|null $photo Основная фотография преподавателя
 * @property int $rating Рейтинг преподавателя, идет сортировка анкет по нему
 * @property int $rating_address Флаг, что добавили рейтинг за адрес
 * @property string|null $location Населенный пункт, показывается когда rating_address
 * @property string|null $street Улица, показывается когда rating_address
 * @property string|null $house_number Номер дома, показывается когда rating_address
 * @property string|null $about ???
 * @property int|null $last_visit ???
 * @property string|null $last_visit_time ???
 * @property string|null $balans ???
 * @property int $gorod ???
 * @property int|null $id_courses_native ID предмета, по которому преподаватель является носителем языка, так же данный предмет должен быть у него в одной из 4х категорий предметов
 * @property int|null $oferta ???
 * @property string|null $avto1 ??? Авто 1
 * @property int|null $nami1 ??? Дублирующие педали, не нашел где используется при выводе
 * @property string|null $photo1 ???
 * @property string|null $avto2 ??? Авто 2
 * @property int|null $nami2 ??? Дублирующие педали, не нашел где используется при выводе
 * @property string|null $photo2 ???
 * @property string|null $photo3 ???
 * @property string|null $photo4 ???
 * @property int|null $new_comment ???
 * @property int|null $free ???
 * @property int $show_video Флаг, отображать ли видео в карточке репетитора
 * @property int|null $videootzyv ???
 * @property int $linka ???
 * @property int|null $doci ???
 * @property int|null $live ???
 * @property int|null $tip ???
 * @property int|null $svoe ??? Могу предоставить музыкальный инструмент на первое время уроков
 * @property int|null $dubl ??? Вторая анкета (отметьте, если уже делалась анкета ранее - по другим дисциплинам) id_users
 * @property int|null $edit_fltr ???
 * @property int|null $edit_fltr_c ???
 * @property int $rating_last_name Флаг, что добавили рейтинг за фамилию
 * @property int|null $edit_anketa ???
 * @property int|null $edit_anketa_c ???
 * @property int|null $hide_anketa ???
 * @property int|null $hide_anketa_c ???
 * @property int|null $hide_anketa_s ???
 * @property int|null $hide_anketa_m ???
 * @property string|null $login_skype ???
 * @property int $show_schedule Флаг, показывать ли расписание в карточке репетитора
 * @property string|null $schedule_last_update_date Дата последнего изменения расписания
 * @property string|null $multiplier_personal Индивидуальный коэффициент, используется для рассчета стоимости заказа
 * @property int $is_deleted Удален ли пользователь?
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Cars|null $car
 * @property-read \App\Models\Cars|null $carSecond
 * @property-read \App\Models\Cities|null $city
 * @property-read \App\Models\Cities|null $cityFourth
 * @property-read \App\Models\Cities|null $citySecond
 * @property-read \App\Models\Cities|null $cityThird
 * @property-read \App\Models\Courses|null $course
 * @property-read \App\Models\Courses|null $courseFourth
 * @property-read \App\Models\Courses|null $courseSecond
 * @property-read \App\Models\Courses|null $courseThird
 * @property-read \App\Models\Districts|null $district
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\UsersDocuments[] $documents
 * @property-read int|null $documents_count
 * @property-read \App\Models\Metro|null $metro
 * @property-read \App\Models\Metro|null $metroSecond
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \App\Models\UsersStatuses|null $status
 * @property-read \App\Models\Universities|null $university
 * @property-read \App\Models\UsersActiveStatuses|null $usersActiveStatuses
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\UsersVideo[] $video
 * @property-read int|null $video_count
 * @method static \Illuminate\Database\Eloquent\Builder|Users newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Users newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Users query()
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereAbout($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereAdditionalInformation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereAvto1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereAvto2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereBalans($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereDoci($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereDubl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereEditAnketa($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereEditAnketaC($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereEditFltr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereEditFltrC($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereEducation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereFirstLessonFree($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereFree($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereGorod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereGraduationYear($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereHideAnketa($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereHideAnketaC($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereHideAnketaM($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereHideAnketaS($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereHouseNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereIdCars($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereIdCarsSecond($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereIdCities($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereIdCitiesFourth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereIdCitiesSecond($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereIdCitiesThird($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereIdCourses($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereIdCoursesFourth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereIdCoursesNative($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereIdCoursesSecond($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereIdCoursesThird($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereIdDistricts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereIdMetro($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereIdMetroSecond($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereIdUniversities($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereIdUsers($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereIdUsersActiveStatuses($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereIdUsersStatuses($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereIsCarInstructor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereIsDeleted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereKomdoma($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereLandlinePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereLastVisit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereLastVisitTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereLinka($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereLive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereLocation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereLogin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereLoginSkype($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereMetroList($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereMiddleName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereMultiplierPersonal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereNami1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereNami2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereNewComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereOferta($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereOrdersCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users wherePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users wherePhoto1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users wherePhoto2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users wherePhoto3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users wherePhoto4($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users wherePricePerHour($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereRating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereRatingAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereRatingLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereRegDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereReviewsNegativeCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereReviewsNeutralCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereReviewsPositiveCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereReviewsRating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereScheduleLastUpdateDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereServicesAndPrices($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereSex($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereShowLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereShowSchedule($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereShowVideo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereSkype($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereStreet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereStudentsHome($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereSvoe($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereTeachersHome($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereTip($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereVideootzyv($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereWorkExperience($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereWorkExperienceInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereYearOfBirth($value)
 */
	class Users extends \Eloquent {}
}

namespace App\Models{
/**
 * UsersActiveStatuses
 *
 * @mixin Eloquent
 * @property int $id_users_active_statuses
 * @property string $name
 * @property string $color
 * @method static \Illuminate\Database\Eloquent\Builder|UsersActiveStatuses newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UsersActiveStatuses newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UsersActiveStatuses query()
 * @method static \Illuminate\Database\Eloquent\Builder|UsersActiveStatuses whereColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersActiveStatuses whereIdUsersActiveStatuses($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersActiveStatuses whereName($value)
 */
	class UsersActiveStatuses extends \Eloquent {}
}

namespace App\Models{
/**
 * UsersDocuments
 *
 * @mixin Eloquent
 * @property int $id_users_documents
 * @property int $id_users
 * @property string|null $document
 * @method static \Illuminate\Database\Eloquent\Builder|UsersDocuments newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UsersDocuments newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UsersDocuments query()
 * @method static \Illuminate\Database\Eloquent\Builder|UsersDocuments whereDocument($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersDocuments whereIdUsers($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersDocuments whereIdUsersDocuments($value)
 */
	class UsersDocuments extends \Eloquent {}
}

namespace App\Models{
/**
 * UsersSchedule
 *
 * @mixin Eloquent
 * @property int $id_users_schedule
 * @property int $id_users_schedule_week_days
 * @property int $id_users_schedule_time
 * @property int $id_users
 * @method static \Illuminate\Database\Eloquent\Builder|UsersSchedule newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UsersSchedule newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UsersSchedule query()
 * @method static \Illuminate\Database\Eloquent\Builder|UsersSchedule whereIdUsers($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersSchedule whereIdUsersSchedule($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersSchedule whereIdUsersScheduleTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersSchedule whereIdUsersScheduleWeekDays($value)
 */
	class UsersSchedule extends \Eloquent {}
}

namespace App\Models{
/**
 * UsersScheduleTime
 *
 * @mixin Eloquent
 * @property int $id_users_schedule_time
 * @property string $start
 * @property string $end
 * @method static \Illuminate\Database\Eloquent\Builder|UsersScheduleTime newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UsersScheduleTime newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UsersScheduleTime query()
 * @method static \Illuminate\Database\Eloquent\Builder|UsersScheduleTime whereEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersScheduleTime whereIdUsersScheduleTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersScheduleTime whereStart($value)
 */
	class UsersScheduleTime extends \Eloquent {}
}

namespace App\Models{
/**
 * UsersScheduleWeekDays
 *
 * @mixin Eloquent
 * @property int $id_users_schedule_week_days
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|UsersScheduleWeekDays newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UsersScheduleWeekDays newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UsersScheduleWeekDays query()
 * @method static \Illuminate\Database\Eloquent\Builder|UsersScheduleWeekDays whereIdUsersScheduleWeekDays($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersScheduleWeekDays whereName($value)
 */
	class UsersScheduleWeekDays extends \Eloquent {}
}

namespace App\Models{
/**
 * UsersStatuses
 *
 * @mixin Eloquent
 * @property int $id_users_statuses
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|UsersStatuses newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UsersStatuses newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UsersStatuses query()
 * @method static \Illuminate\Database\Eloquent\Builder|UsersStatuses whereIdUsersStatuses($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersStatuses whereName($value)
 */
	class UsersStatuses extends \Eloquent {}
}

namespace App\Models{
/**
 * UsersVideo
 *
 * @mixin Eloquent
 * @property int $id_users_video
 * @property int $id_users
 * @property string $video
 * @property int|null $date ???
 * @property int|null $mes ???
 * @property int|null $avto ???
 * @method static \Illuminate\Database\Eloquent\Builder|UsersVideo newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UsersVideo newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UsersVideo query()
 * @method static \Illuminate\Database\Eloquent\Builder|UsersVideo whereAvto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersVideo whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersVideo whereIdUsers($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersVideo whereIdUsersVideo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersVideo whereMes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersVideo whereVideo($value)
 */
	class UsersVideo extends \Eloquent {}
}

namespace App{
/**
 * App\OtzyvAdd
 *
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|OtzyvAdd newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OtzyvAdd newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OtzyvAdd query()
 */
	class OtzyvAdd extends \Eloquent {}
}

namespace App{
/**
 * App\OtzyvDel
 *
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|OtzyvDel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OtzyvDel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OtzyvDel query()
 */
	class OtzyvDel extends \Eloquent {}
}

namespace App{
/**
 * App\OtzyvNeg
 *
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|OtzyvNeg newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OtzyvNeg newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OtzyvNeg query()
 */
	class OtzyvNeg extends \Eloquent {}
}

namespace App{
/**
 * App\OtzyvNegDel
 *
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|OtzyvNegDel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OtzyvNegDel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OtzyvNegDel query()
 */
	class OtzyvNegDel extends \Eloquent {}
}

namespace App{
/**
 * App\Seo2
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Seo2 newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Seo2 newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Seo2 query()
 */
	class Seo2 extends \Eloquent {}
}

namespace App{
/**
 * App\UsersDeleted
 *
 * @method static \Illuminate\Database\Eloquent\Builder|UsersDeleted newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UsersDeleted newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UsersDeleted query()
 */
	class UsersDeleted extends \Eloquent {}
}

