<?php

use App\Models\PrependTextCategories;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PrependTextCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('prepend_text_categories');

        Schema::create('prepend_text_categories', function (Blueprint $table) {
            $table->integer('id_prepend_text_categories', true, true)->nullable(false);
            $table->string('name')->nullable(false);
        });

        if (($f = fopen(__DIR__.'/../dumps/prepend_text_categories.csv', 'r')) !== false) {
            while (($data = fgetcsv($f, 0, "\t")) !== false) {
                $item = new PrependTextCategories();
                $item->id_prepend_text_categories = $data[0];
                $item->name = $data[1];
                $item->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prepend_text_categories');
    }
}
