<?php

use App\Models\Users;
use App\Models\UsersVideo;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UsersVideoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('users_video');

        if (Schema::hasTable('video')) {
            DB::statement('ALTER TABLE video ENGINE = InnoDB');
            DB::statement('ALTER TABLE video CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci');

            Schema::rename('video', 'users_video');
        }

        Schema::table('users_video', function (Blueprint $table) {
            $table->renameColumn('id_post', 'id_users_video');
            $table->renameColumn('id_rep', 'id_users');
            $table->text('video')->change();
            $table->integer('date')->nullable()->comment('???')->change();
            $table->integer('mes')->nullable()->comment('???')->change();
            $table->integer('avto')->nullable()->comment('???')->change();

            $table->dropIndex('id_rep');
            $table->index('id_users', 'id_users');
        });

        $videos = UsersVideo::all();

        foreach ($videos ?? [] as $video) {
            $video->video = trim($video->video);

            if (empty($video->video) || !Users::find($video->id_users)) {
                $video->delete();
            } else {
                $video->video = str_replace('https://', '//', $video->video);
                $video->save();
            }
        }

        DB::statement("ALTER TABLE `users_video` MODIFY COLUMN `id_users_video` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT");
        DB::statement("ALTER TABLE `users_video` MODIFY COLUMN `id_users` INT(10) UNSIGNED NOT NULL AFTER `id_users_video`");
        DB::statement("ALTER TABLE `users_video` MODIFY COLUMN `video` TEXT NOT NULL COLLATE 'utf8mb4_unicode_ci' AFTER `id_users`");

        Schema::table('users_video', function (Blueprint $table) {
            $table->foreign('id_users', 'users_video_users')->references('id_users')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_video');
    }
}
