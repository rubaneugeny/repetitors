<?php

use App\Models\UsersScheduleTime;
use App\Models\UsersScheduleWeekDays;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SchedulesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_schedule_time', function (Blueprint $table) {
            $table->integer('id_users_schedule_time', true, true)->nullable(false)->default(null);
            $table->time('start')->nullable(false);
            $table->time('end')->nullable(false);
        });

        if (($f = fopen(__DIR__.'/../dumps/users_schedule_time.csv', 'r')) !== false) {
            while (($data = fgetcsv($f, 0, "\t")) !== false) {
                $item = new UsersScheduleTime();
                $item->id_users_schedule_time = $data[0];
                $item->start = $data[1];
                $item->end = $data[2];
                $item->save();
            }
        }

        Schema::create('users_schedule_week_days', function (Blueprint $table) {
            $table->integer('id_users_schedule_week_days', true, true)->nullable(false)->default(null);
            $table->string('name', 2)->nullable(false);
        });

        if (($f = fopen(__DIR__.'/../dumps/users_schedule_week_days.csv', 'r')) !== false) {
            while (($data = fgetcsv($f, 0, "\t")) !== false) {
                $item = new UsersScheduleWeekDays();
                $item->id_users_schedule_week_days = $data[0];
                $item->name = $data[1];
                $item->save();
            }
        }

        Schema::create('users_schedule', function (Blueprint $table) {
            $table->integer('id_users_schedule', true, true)->nullable(false)->default(null);
            $table->integer('id_users_schedule_week_days', false, true)->nullable(false)->default(null);
            $table->integer('id_users_schedule_time', false, true)->nullable(false)->default(null);
            $table->integer('id_users', false, true)->nullable(false)->default(null);

            $table->foreign('id_users_schedule_week_days', 'users_schedule_users_schedule_week_days')->references('id_users_schedule_week_days')->on('users_schedule_week_days')->onDelete('restrict')->onUpdate('cascade');
            $table->foreign('id_users_schedule_time', 'users_schedule_users_schedule_time')->references('id_users_schedule_time')->on('users_schedule_time')->onDelete('restrict')->onUpdate('cascade');
            $table->foreign('id_users', 'users_schedule_users')->references('id_users')->on('users')->onDelete('cascade')->onUpdate('cascade');

            $table->unique(['id_users_schedule_week_days', 'id_users_schedule_time', 'id_users'], 'id_users_schedule_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
