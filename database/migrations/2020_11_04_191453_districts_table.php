<?php

use App\Models\Districts;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DistrictsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('raion')) {
            DB::statement('ALTER TABLE raion ENGINE = InnoDB');
            DB::statement('ALTER TABLE raion CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci');

            Schema::rename('raion', 'districts');
        }

        Schema::table('districts', function (Blueprint $table) {
            $table->timestamps();
            $table->renameColumn('id_raion', 'id_districts');
            $table->renameColumn('url', 'slug');
            $table->renameColumn('in', 'in_the_direction');
            $table->dropColumn('unikum');
        });

        $districts = Districts::all();

        foreach ($districts ?? [] as $district) {
            $district->slug = trim($district->slug);
            $district->name = trim($district->name);
            $district->in_the_direction = trim($district->in_the_direction);
            $district->save();
        }

        DB::statement("ALTER TABLE `districts` MODIFY COLUMN `id_districts` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT");
        DB::statement("ALTER TABLE `districts` MODIFY COLUMN `slug` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_unicode_ci' AFTER `id_districts`");
        DB::statement("ALTER TABLE `districts` MODIFY COLUMN `name` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_unicode_ci' AFTER `slug`");
        DB::statement("ALTER TABLE `districts` MODIFY COLUMN `in_the_direction` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_unicode_ci' AFTER `name`");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('districts');
    }
}
