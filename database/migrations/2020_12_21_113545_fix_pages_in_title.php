<?php

use App\Models\TitleTemplates;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FixPagesInTitle extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $templates = TitleTemplates::where('title', 'like', '%{PAGE_START}%')->orWhere('description', 'like', '%{PAGE_START}%')->get();

        foreach ($templates as $template) {
            $template->title = str_replace('{PAGE_START}', '{FROM}', $template->title);
            $template->title = str_replace('{PAGE_END}', '{TO}', $template->title);

            $template->description = str_replace('{PAGE_START}', '{FROM}', $template->description);
            $template->description = str_replace('{PAGE_END}', '{TO}', $template->description);

            $template->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
