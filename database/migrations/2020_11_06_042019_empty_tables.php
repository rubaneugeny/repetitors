<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EmptyTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('mes');
        Schema::dropIfExists('gryffindor_worklist');
        Schema::dropIfExists('perenos');
        Schema::dropIfExists('perepodbor');

        Schema::dropIfExists('gorodfour');
        Schema::dropIfExists('gorodtri');
        Schema::dropIfExists('gorodtwo');

        Schema::dropIfExists('lerntwo');
        Schema::dropIfExists('lerntri');
        Schema::dropIfExists('lernfour');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
