<?php

use App\Models\UsersStatuses;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UsersStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_statuses', function (Blueprint $table) {
            $table->integer('id_users_statuses', true, true)->nullable(false)->default(null);
            $table->string('name', 50)->nullable(false);
        });

        if (($f = fopen(__DIR__ . '/../dumps/users_statuses.csv', 'r')) !== false) {
            while (($data = fgetcsv($f, 0, "\t")) !== false) {
                $status = new UsersStatuses();
                $status->id_users_statuses = $data[0];
                $status->name = $data[1];
                $status->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_statuses');
    }
}
