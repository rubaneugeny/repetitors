<?php

use App\Models\Multipliers;
use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MultipliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('multipliers');

        if (Schema::hasTable('koef')) {
            DB::statement('ALTER TABLE `koef` ENGINE = InnoDB');
            DB::statement('ALTER TABLE `koef` CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci');

            Schema::rename('koef', 'multipliers');
        }

        Schema::table('multipliers', function (Blueprint $table) {
            $table->renameColumn('date', 'old_date');
            $table->renameColumn('koef', 'multiplier');
        });

        Schema::table('multipliers', function (Blueprint $table) {
            $table->dateTime('date')->nullable(false)->after('multiplier');
            $table->integer('id_multipliers', true, true)->nullable(false)->first();
        });

        $mulptipliers = Multipliers::all();

        foreach ($mulptipliers ?? [] as $multiplier) {
            $multiplier->date = Carbon::createFromTimestamp($multiplier->old_date)->format('Y-m-d H:i:s');
            $multiplier->save();
        }

        Schema::table('multipliers', function (Blueprint $table) {
            $table->dropColumn('old_date');
        });

        DB::statement("ALTER TABLE `multipliers` MODIFY COLUMN `id_multipliers` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT");
        DB::statement("ALTER TABLE `multipliers` MODIFY COLUMN `multiplier` DECIMAL(10,2) UNSIGNED NOT NULL AFTER `id_multipliers`");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('multipliers');
    }
}
