<?php

use App\Models\UsersActiveStatuses;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UsersActiveStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('users_active_statuses', function (Blueprint $table) {
            $table->integer('id_users_active_statuses', true, true)->nullable(false);
            $table->string('name')->nullable(false);
            $table->string('color', 10)->nullable(false);
        });

        if (($f = fopen(__DIR__.'/../dumps/users_active_statuses.csv', 'r')) !== false) {
            while (($data = fgetcsv($f, 0, "\t")) !== false) {
                $status = new UsersActiveStatuses();
                $status->id_users_active_statuses = $data[0];
                $status->name = $data[1];
                $status->color = $data[2];
                $status->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
