<?php

use App\Models\Courses;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Courses2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE `courses` MODIFY COLUMN `koef_ind_sr` DOUBLE NULL COMMENT '???'");

        Schema::table('courses', function (Blueprint $table) {
            $table->dropColumn('dance');
            $table->dropColumn('koef_rep');

            $table->foreign('id_courses_types', 'courses_courses_types')->references('id_courses_types')->on('courses_types')->onDelete('restrict')->onUpdate('cascade');
        });

        $courses = Courses::all();

        foreach ($courses ?? [] as $course) {
            $course->name = trim($course->name);
            $course->slug = trim($course->slug);

            if (!empty($course->slug_second)) {
                $course->slug_second = trim($course->slug_second);
            }

            if (!empty($course->recommend_name)) {
                $course->recommend_name = trim($course->recommend_name);
            } else {
                $course->recommend_name = null;
            }

            if (!empty($course->recommend_name_second)) {
                $course->recommend_name_second = trim($course->recommend_name_second);
            } else {
                $course->recommend_name_second = null;
            }

            if (!empty($course->recommend_name_third)) {
                $course->recommend_name_third = trim($course->recommend_name_third);
            } else {
                $course->recommend_name_third = null;
            }

            if (!empty($course->link4)) {
                $course->link4 = trim($course->link4);
            } else {
                $course->link4 = null;
            }

            if (!empty($course->link5)) {
                $course->link5 = trim($course->link5);
            } else {
                $course->link5 = null;
            }

            if (!empty($course->link6)) {
                $course->link6 = trim($course->link6);
            } else {
                $course->link6 = null;
            }

            if (!empty($course->recommend_name_seventh)) {
                $course->recommend_name_seventh = trim($course->recommend_name_seventh);
            } else {
                $course->recommend_name_seventh = null;
            }

            if (!empty($course->recommend_name_eighth)) {
                $course->recommend_name_eighth = trim($course->recommend_name_eighth);
            } else {
                $course->recommend_name_eighth = null;
            }

            if (!empty($course->recommend_name_nineth)) {
                $course->recommend_name_nineth = trim($course->recommend_name_nineth);
            } else {
                $course->recommend_name_nineth = null;
            }

            $course->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
