<?php

use App\Models\ReviewsStatuses;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ReviewsStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('reviews_statuses');

        Schema::create('reviews_statuses', function (Blueprint $table) {
            $table->integer('id_reviews_statuses', true, true)->nullable(false);
            $table->string('name')->nullable(false);
        });

        if (($f = fopen(__DIR__.'/../dumps/reviews_statuses.csv', 'r')) !== false) {
            while (($data = fgetcsv($f, 0, "\t")) !== false) {
                $item = new ReviewsStatuses();
                $item->id_reviews_statuses = $data[0];
                $item->name = $data[1];
                $item->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reviews_statuses');
    }
}
