<?php

use App\Models\Orders;
use App\Models\Reviews;
use App\Models\Users;
use App\Garbage\OtzyvAdd;
use App\Garbage\OtzyvDel;
use App\Garbage\OtzyvNeg;
use App\Garbage\OtzyvNegDel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ReviewsTable2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $reviews = Reviews::all();

        // Удалим дубликаты (очень медленная процедура)
        foreach ($reviews as $review) {
            echo $review->id_reviews."\r\n";
            $dups = Reviews::where(['comment' => $review->comment, 'id_users' => $review->id_users])->where('id_reviews', '!=', $review->id_reviews)->get();

            foreach($dups as $dup) {
                echo $review->comment."\r\n---\r\n".$dup->id_reviews."\r\n".$dup->comment."\r\n=========================\r\n";
                $dup->delete();
            }
        }

        // Преобразуем неправильные комментарии
        $otzyvAdd = OtzyvAdd::all();

        foreach ($otzyvAdd as $review) {
            if ($review->id_rep == 0 || !Users::find($review->id_rep)) {
                $review->delete();
            } else {
                $review->comment = trim(stripslashes(html_entity_decode($review->comment)));

                if ($review->reit == 0) {
                    $review->reit = 1;
                }

                $review->save();
            }
        }

        // Удалим дубликаты
        $otzyvAdd = OtzyvAdd::all();

        foreach ($otzyvAdd as $review) {
            $dups = OtzyvAdd::where(['comment' => $review->comment])->where('id', '!=', $review->id)->get();

            foreach($dups as $dup) {
                $dup->delete();
            }
        }

        // Удалим короткие записи
        $otzyvAdd = OtzyvAdd::all();

        foreach ($otzyvAdd as $review) {
            if (strlen($review->comment) < 10) {
                $review->delete();
            }
        }

        // Переносим отклоненные заявки в общую таблицу
        $otzyvAdd = OtzyvAdd::whereIn('status', [0, 2])->get();

        foreach ($otzyvAdd as $otzyv) {
            $count = Reviews::where(['comment' => $otzyv->comment, 'id_users' => $otzyv->id_rep])->count();

            if ($count == 0) {
                try {
                    $review = new Reviews();
                    $review->date = $otzyv->dt;
                    $review->name = $otzyv->name;
                    $review->id_users = $otzyv->id_rep;
                    $review->id_orders = $otzyv->id_post;
                    $review->phone = $otzyv->phone;
                    $review->comment = $otzyv->comment;
                    // $review->response = trim($otzyv->response);
                    $review->new = empty($otzyv->new) ? 1 : $otzyv->new;
                    $review->rating = empty($otzyv->reit) ? 4 : $otzyv->reit;

                    if ($review->rating == 0) {
                        $review->rating = 4;
                    }

                    $review->id_reviews_statuses = 3; // Отклонена

                    if ($review->save()) {
                        $otzyv->delete();
                    }
                } catch (\Exception $e) {
                    echo $e->getMessage();
                }
            }
        }

        $otzyvNeg = OtzyvNeg::all();

        foreach ($otzyvNeg as $review) {
            if ($review->id_rep == 0 || !Users::find($review->id_rep)) {
                $review->delete();
            } else {
                $review->comment = trim(stripslashes(html_entity_decode($review->comment)));
                $review->save();
            }
        }

        // Удалим негативные отзывы у которых нет заявки
        $otzyvNeg = OtzyvNeg::whereNotNull('id_zay')->get();

        foreach($otzyvNeg as $otzyv) {
            if (!Orders::find($otzyv->id_zay)) {
                $otzyv->delete();
            }
        }

        // Переносим негативные отзывы в общую таблицу
        $otzyvNeg = OtzyvNeg::all();

        foreach($otzyvNeg as $otzyv) {
            $count = Reviews::where(['comment' => $otzyv->comment, 'id_users' => $otzyv->id_rep])->count();

            if ($count == 0) {
                try {
                    $review = new Reviews();
                    $review->date = $otzyv->dt;
                    $review->name = $otzyv->name;
                    $review->id_users = $otzyv->id_rep;
                    $review->id_orders = $otzyv->id_zay;
                    $review->phone = $otzyv->phone;
                    $review->comment = $otzyv->comment;
                    $review->response = trim($otzyv->response);
                    $review->new = $otzyv->new;
                    $review->rating = empty($otzyv->reit) ? 4 : $otzyv->reit;

                    if ($review->rating == 0) {
                        $review->rating = 4;
                    }

                    $review->id_admins_reviews = $otzyv->id_admin;
                    $review->id_reviews_statuses = $otzyv->aktive;

                    if ($review->save()) {
                        $otzyv->delete();
                    }
                } catch (\Exception $e) {
                    echo $e->getMessage();
                }
            }
        }

        // Преобразуем неправильные комментарии
        $otzyvDel = OtzyvDel::all();

        foreach ($otzyvDel as $review) {
            if ($review->id_rep == 0 || !Users::find($review->id_rep)) {
                $review->delete();
            } else {
                $review->comment = trim(stripslashes(html_entity_decode($review->comment)));
                $review->save();
            }
        }

        // Удалим удаленные отзывы у которых нет заявки
        $otzyvNeg = OtzyvDel::whereNotNull('id_zay')->get();

        foreach($otzyvNeg as $otzyv) {
            if (!Orders::find($otzyv->id_zay)) {
                $otzyv->delete();
            }
        }

        // Переносим удаленные отзывы в общую таблицу
        $otzyvNeg = OtzyvDel::all();

        foreach($otzyvNeg as $otzyv) {
            $count = Reviews::where(['comment' => $otzyv->comment, 'id_users' => $otzyv->id_rep])->count();

            if ($count == 0) {
                try {
                    $review = new Reviews();
                    $review->date = $otzyv->dt;
                    $review->name = $otzyv->name;
                    $review->id_users = $otzyv->id_rep;
                    $review->id_orders = $otzyv->id_zay;
                    $review->phone = $otzyv->phone;
                    $review->comment = $otzyv->comment;
                    $review->new = $otzyv->new;
                    $review->rating = empty($otzyv->reit) ? 4 : $otzyv->reit;

                    if ($review->rating == 0) {
                        $review->rating = 4;
                    }

                    $review->id_admins_reviews = $otzyv->id_admin;
                    $review->id_reviews_statuses = 2;
                    $review->is_deleted = 1;

                    if ($review->save()) {
                        $otzyv->delete();
                    }
                } catch (\Exception $e) {
                    echo $e->getMessage();
                }
            }
        }

        // Преобразуем неправильные комментарии
        $otzyvDel = OtzyvNegDel::all();

        foreach ($otzyvDel as $review) {
            if ($review->id_rep == 0 || !Users::find($review->id_rep)) {
                $review->delete();
            } else {
                $review->comment = trim(stripslashes(html_entity_decode($review->comment)));
                $review->save();
            }
        }

        // Удалим удаленные отзывы у которых нет заявки
        $otzyvNeg = OtzyvNegDel::whereNotNull('id_zay')->get();

        foreach($otzyvNeg as $otzyv) {
            $count = Orders::where(['id_orders' => $otzyv->id_zay])->count();

            if ($count == 0) {
                $otzyv->delete();
            }
        }

        // Переносим удаленные отзывы в общую таблицу
        $otzyvNeg = OtzyvNegDel::all();

        foreach($otzyvNeg as $otzyv) {
            $count = Reviews::where(['comment' => $otzyv->comment, 'id_users' => $otzyv->id_rep])->count();

            if ($count == 0) {
                try {
                    $review = new Reviews();
                    $review->date = $otzyv->dt;
                    $review->name = $otzyv->name;
                    $review->id_users = $otzyv->id_rep;
                    $review->id_orders = $otzyv->id_zay;
                    $review->response = $otzyv->response;
                    $review->comment = $otzyv->comment;
                    $review->new = $otzyv->new;
                    $review->rating = empty($otzyv->reit) ? 4 : $otzyv->reit;

                    if ($review->rating == 0) {
                        $review->rating = 4;
                    }

                    $review->id_admins_reviews = $otzyv->id_admin;
                    $review->id_reviews_statuses = $otzyv->aktive;
                    $review->is_deleted = 1;

                    if ($review->save()) {
                        $otzyv->delete();
                    }
                } catch (\Exception $e) {
                    echo $e->getMessage();
                }
            }
        }

        /*
         * id_users - id_rep
         * comment - comment
         * name - name
         * reit - rating
         * dt - date (convert from unixtimestamp)
         * id_post - id_orders
         * phone -
         * comment_admin -
         * status -
         * code_status -
         */
        $otzyvAdd = OtzyvAdd::whereNotNull('id_post')->get();

        foreach ($otzyvAdd as $review) {
            if ($rev = Reviews::where(['id_users' => $review->id_rep, 'id_orders' => $review->id_post])->first()) {
                print_r($review->toArray());
                print_r($rev->toArray());

                // Переносим телефоны
                $rev->phone = $review->phone;
                $rev->save();

                // Удаляем
                $review->delete();
                echo '-----------------------------------------------------------------';
            }
        }

        // Чистим несуществующих репетиторов
        $usersId = Users::pluck('id_users');
        OtzyvAdd::whereNotIn('id_rep', $usersId)->delete();

        // Ищем отзывы без заказа
        $otzyvAdd = OtzyvAdd::whereNull('id_post')->get();

        foreach ($otzyvAdd as $review) {
            if ($rev = Reviews::where(['id_users' => $review->id_rep, 'comment' => $review->comment])->first()) {
                print_r($review->toArray());
                print_r($rev->toArray());

                if (!$rev->phone) {
                    $rev->phone = $review->phone;
                    $rev->save();
                }

                // Удаляем
                $review->delete();
                echo '-----------------------------------------------------------------';
            }
        }

        // Удаляем пустые комментарии
        OtzyvAdd::where(['comment' => ''])->delete();

        // Ищем отзывы по окончанию
        $otzyvAdd = OtzyvAdd::all();

        foreach ($otzyvAdd as $review) {
            $comment = trim($review->comment);
            $name = strpos($review->name,' ') > 0 ? substr($review->name, 0, strpos($review->name,' ')) : trim($review->name);

            if ($rev = Reviews::where(['id_users' => $review->id_rep])
                ->where('comment', 'like', '%'.substr($comment, -30))
                ->where('name', 'like', '%'.$name.'%')
                ->first()) {
                print_r($review->toArray());
                print_r($rev->toArray());

                if (!$rev->phone) {
                    $rev->phone = $review->phone;
                    $rev->save();
                }

                // Удаляем
                $review->delete();
                echo '-----------------------------------------------------------------';
            }
        }

        foreach ($otzyvAdd as $review) {
            $comment = trim($review->comment);
            $name = strpos($review->name,' ') > 0 ? substr($review->name, 0, strpos($review->name,' ')) : trim($review->name);

            if ($rev = Reviews::where(['id_users' => $review->id_rep])
                ->where('comment', 'like', '%'.substr($comment, -20))
                ->where('name', 'like', '%'.$name.'%')
                ->first()) {
                print_r($review->toArray());
                print_r($rev->toArray());

                if (!$rev->phone) {
                    $rev->phone = $review->phone;
                    $rev->save();
                }

                // Удаляем
                $review->delete();
                echo '-----------------------------------------------------------------';
            }
        }

        $otzyvAdd = OtzyvAdd::all();

        foreach ($otzyvAdd as $review) {
            $comment = trim($review->comment);
            $name = strpos($review->name,' ') > 0 ? substr($review->name, 0, strpos($review->name,' ')) : trim($review->name);

            if ($rev = Reviews::where(['id_users' => $review->id_rep])
                ->where('comment', 'like', '%'.substr($comment, -10))
                ->where('name', 'like', '%'.$name.'%')
                ->first()) {
                print_r($review->toArray());
                print_r($rev->toArray());

                if (!$rev->phone) {
                    $rev->phone = $review->phone;
                    $rev->save();
                }

                // Удаляем
                $review->delete();
                echo '-----------------------------------------------------------------';
            }
        }

        $otzyvAdd = OtzyvAdd::all();

        foreach ($otzyvAdd as $review) {
            $comment = trim($review->comment);
            $name = strpos($review->name,' ') > 0 ? substr($review->name, 0, strpos($review->name,' ')) : trim($review->name);

            if ($rev = Reviews::where(['id_users' => $review->id_rep])
                ->where('comment', 'like', '%'.substr($comment, -10).'%')
                ->where('name', 'like', '%'.$name.'%')
                ->first()) {
                print_r($review->toArray());
                print_r($rev->toArray());

                if (!$rev->phone) {
                    $rev->phone = $review->phone;
                    $rev->save();
                }

                // Удаляем
                $review->delete();
                echo '-----------------------------------------------------------------';
            }
        }

        $otzyvAdd = OtzyvAdd::all();

        foreach ($otzyvAdd as $review) {
            $comment = trim($review->comment);
            $name = strpos($review->name,' ') > 0 ? substr($review->name, 0, strpos($review->name,' ')) : trim($review->name);

            if ($rev = Reviews::where(['id_users' => $review->id_rep])
                ->where('comment', 'like', '%'.substr($comment, 10).'%')
                ->where('name', 'like', '%'.$name.'%')
                ->first()) {
                print_r($review->toArray());
                print_r($rev->toArray());

                if (!$rev->phone) {
                    $rev->phone = $review->phone;
                    $rev->save();
                }

                // Удаляем
                $review->delete();
                echo '-----------------------------------------------------------------';
            }
        }

        $otzyvAdd = OtzyvAdd::all();

        foreach ($otzyvAdd as $review) {
            $comment = trim($review->comment);
            $name = strpos($review->name,' ') > 0 ? substr($review->name, 0, strpos($review->name,' ')) : trim($review->name);

            if ($rev = Reviews::where(['id_users' => $review->id_rep])
                ->where('comment', 'like', '%'.substr($comment, -20).'%')
                ->where('name', 'like', '%'.$name.'%')
                ->first()) {
                print_r($review->toArray());
                print_r($rev->toArray());

                if (!$rev->phone) {
                    $rev->phone = $review->phone;
                    $rev->save();
                }

                // Удаляем
                $review->delete();
                echo '-----------------------------------------------------------------';
            }
        }

        $otzyvAdd = OtzyvAdd::all();

        foreach ($otzyvAdd as $review) {
            $comment = trim($review->comment);
            $name = strpos($review->name,' ') > 0 ? substr($review->name, 0, strpos($review->name,' ')) : trim($review->name);

            if ($rev = Reviews::where(['id_users' => $review->id_rep])
                ->where('comment', 'like', '%'.substr($comment, 20, 150).'%')
                ->where('name', 'like', '%'.$name.'%')
                ->first()) {
                print_r($review->toArray());
                print_r($rev->toArray());

                if (!$rev->phone) {
                    $rev->phone = $review->phone;
                    $rev->save();
                }

                // Удаляем
                $review->delete();
                echo '-----------------------------------------------------------------';
            }
        }

        $otzyvAdd = OtzyvAdd::all();

        foreach ($otzyvAdd as $review) {
            $comment = trim($review->comment);
            $name = strpos($review->name,' ') > 0 ? substr($review->name, 0, strpos($review->name,' ')) : trim($review->name);

            if ($rev = Reviews::where(['id_users' => $review->id_rep])
                ->where('comment', 'like', '%'.substr($comment, 20, 100).'%')
                ->where('name', 'like', '%'.$name.'%')
                ->first()) {
                print_r($review->toArray());
                print_r($rev->toArray());

                if (!$rev->phone) {
                    $rev->phone = $review->phone;
                    $rev->save();
                }

                // Удаляем
                $review->delete();
                echo '-----------------------------------------------------------------';
            }
        }

        $otzyvAdd = OtzyvAdd::all();

        foreach ($otzyvAdd as $review) {
            $comment = trim($review->comment);
            $name = strpos($review->name,' ') > 0 ? substr($review->name, 0, strpos($review->name,' ')) : trim($review->name);

            if ($rev = Reviews::where(['id_users' => $review->id_rep])
                ->where('comment', 'like', '%'.substr($comment, -50))
                ->where('name', 'like', '%'.$name.'%')
                ->first()) {
                print_r($review->toArray());
                print_r($rev->toArray());

                if (!$rev->phone) {
                    $rev->phone = $review->phone;
                    $rev->save();
                }

                // Удаляем
                $review->delete();
                echo '-----------------------------------------------------------------';
            }
        }

        $otzyvAdd = OtzyvAdd::all();

        foreach ($otzyvAdd as $review) {
            $comment = trim($review->comment);
            $name = strpos($review->name,' ') > 0 ? substr($review->name, 0, strpos($review->name,' ')) : trim($review->name);

            if ($rev = Reviews::where(['id_users' => $review->id_rep])
                ->where('comment', 'like', '%'.substr($comment, -30))
                ->where('name', 'like', '%'.$name.'%')
                ->first()) {
                print_r($review->toArray());
                print_r($rev->toArray());

                if (!$rev->phone) {
                    $rev->phone = $review->phone;
                    $rev->save();
                }

                // Удаляем
                $review->delete();
                echo '-----------------------------------------------------------------';
            }
        }

        $otzyvAdd = OtzyvAdd::all();

        foreach ($otzyvAdd as $review) {
            $comment = trim($review->comment);
            $name = strpos($review->name,' ') > 0 ? substr($review->name, 0, strpos($review->name,' ')) : trim($review->name);

            if ($rev = Reviews::where(['id_users' => $review->id_rep])
                ->where('comment', 'like', '%'.substr($comment, -10))
                ->where('name', 'like', '%'.$name.'%')
                ->first()) {
                print_r($review->toArray());
                print_r($rev->toArray());

                if (!$rev->phone) {
                    $rev->phone = $review->phone;
                    $rev->save();
                }

                // Удаляем
                $review->delete();
                echo '-----------------------------------------------------------------';
            }
        }

        $otzyvAdd = OtzyvAdd::all();

        foreach ($otzyvAdd as $review) {
            $comment = trim($review->comment);
            $name = strpos($review->name,' ') > 0 ? substr($review->name, 0, strpos($review->name,' ')) : trim($review->name);

            if ($rev = Reviews::where(['id_users' => $review->id_rep])
                ->where('comment', 'like', '%'.substr($comment, 10, 50).'%')
                ->where('name', 'like', '%'.$name.'%')
                ->first()) {
                print_r($review->toArray());
                print_r($rev->toArray());

                if (!$rev->phone) {
                    $rev->phone = $review->phone;
                    $rev->save();
                }

                // Удаляем
                $review->delete();
                echo '-----------------------------------------------------------------';
            }
        }

        $otzyvAdd = OtzyvAdd::all();

        foreach ($otzyvAdd as $review) {
            $comment = trim($review->comment);
            $name = strpos($review->name,' ') > 0 ? substr($review->name, 0, strpos($review->name,' ')) : trim($review->name);

            if ($rev = Reviews::where(['id_users' => $review->id_rep])
                ->where('comment', 'like', '%'.substr($comment, 0, 50).'%')
                ->where('name', 'like', '%'.$name.'%')
                ->first()) {
                print_r($review->toArray());
                print_r($rev->toArray());

                if (!$rev->phone) {
                    $rev->phone = $review->phone;
                    $rev->save();
                }

                // Удаляем
                $review->delete();
                echo '-----------------------------------------------------------------';
            }
        }

        $otzyvAdd = OtzyvAdd::all();

        foreach ($otzyvAdd as $review) {
            $comment = trim($review->comment);
            $name = strpos($review->name,' ') > 0 ? substr($review->name, 0, strpos($review->name,' ')) : trim($review->name);

            if ($rev = Reviews::where(['id_users' => $review->id_rep])
                ->where('comment', 'like', substr($comment, 0, 30).'%')
                ->where('name', 'like', '%'.$name.'%')
                ->first()) {
                print_r($review->toArray());
                print_r($rev->toArray());

                if (!$rev->phone) {
                    $rev->phone = $review->phone;
                    $rev->save();
                }

                // Удаляем
                $review->delete();
                echo '-----------------------------------------------------------------';
            }
        }

        $otzyvAdd = OtzyvAdd::all();

        foreach ($otzyvAdd as $review) {
            $comment = trim($review->comment);

            if ($rev = Reviews::where(['id_users' => $review->id_rep])
                ->where('comment', 'like', substr($comment, 0, 70).'%')
                ->first()) {
                print_r($review->toArray());
                print_r($rev->toArray());

                if (!$rev->phone) {
                    $rev->phone = $review->phone;
                    $rev->save();
                }

                // Удаляем
                $review->delete();
                echo '-----------------------------------------------------------------';
            }
        }

        $otzyvAdd = OtzyvAdd::all();

        foreach ($otzyvAdd as $review) {
            $comment = trim($review->comment);
            $name = strpos($review->name,' ') > 0 ? substr($review->name, 0, strpos($review->name,' ')) : trim($review->name);
            //$name = strpos($review->name,' ') > 0 ? substr($review->name, 0, strpos($review->name,' ')) : trim($review->name);

            if ($rev = Reviews::where(['id_users' => $review->id_rep])
                ->where('comment', 'like', '%'.substr($comment, 0, 50).'%')
                ->where('name', 'like', '%'.$name.'%')
                ->first()) {
                print_r($review->toArray());
                print_r($rev->toArray());

                if (!$rev->phone) {
                    $rev->phone = $review->phone;
                    $rev->save();
                }

                // Удаляем
                $review->delete();
                echo '-----------------------------------------------------------------';
            }
        }

        $otzyvAdd = OtzyvAdd::all();

        foreach ($otzyvAdd as $review) {
            $comment = trim($review->comment);
            // $name = strpos($review->name,' ') > 0 ? substr($review->name, 0, strpos($review->name,' ')) : trim($review->name);
            $name = strpos($review->name,' ') > 0 ? substr($review->name, 0, strpos($review->name,' ')) : trim($review->name);

            if ($rev = Reviews::where(['id_users' => $review->id_rep])
                ->where('comment', 'like', '%'.substr($comment, 0, 40).'%')
                ->where('name', 'like', '%'.$name.'%')
                ->first()) {
                print_r($review->toArray());
                print_r($rev->toArray());

                if (!$rev->phone) {
                    $rev->phone = $review->phone;
                    $rev->save();
                }

                // Удаляем
                $review->delete();
                echo '-----------------------------------------------------------------';
            }
        }

        $otzyvAdd = OtzyvAdd::all();

        foreach ($otzyvAdd as $review) {
            $comment = trim($review->comment);
            $pos = strlen($comment) < 2 ? 0 : strpos($comment,' ', 2);
            if ($pos !== false) {
                $comment = strlen($comment) > 40 ? trim(substr($comment, $pos, 100)) : trim($comment);
                // $name = strpos($review->name,' ') > 0 ? substr($review->name, 0, strpos($review->name,' ')) : trim($review->name);
                $name = strpos($review->name, ' ') > 0 ? substr($review->name, 0,
                    strpos($review->name, ' ')) : trim($review->name);

                if ($rev = Reviews::where(['id_users' => $review->id_rep])
                    ->where('comment', 'like', '%'.$comment.'%')
                    ->where('name', 'like', '%'.$name.'%')
                    ->first()) {
                    echo $comment."\r\n";
                    print_r($review->toArray());
                    print_r($rev->toArray());

                    if (!$rev->phone) {
                        $rev->phone = $review->phone;
                        $rev->save();
                    }

                    // Удаляем
                    $review->delete();
                    echo '-----------------------------------------------------------------'."\r\n";
                }
            }
        }

        $otzyvAdd = OtzyvAdd::all();

        foreach ($otzyvAdd as $review) {
            $comment = trim($review->comment);
            $comment = strlen($comment) > 40 ? trim(substr($comment, 0, 100)) : trim($comment);
            // $name = strpos($review->name,' ') > 0 ? substr($review->name, 0, strpos($review->name,' ')) : trim($review->name);
            $name = strpos($review->name, ',') > 0 ? substr($review->name, 0,
                strpos($review->name, ',')) : trim($review->name);

            if ($rev = Reviews::where(['id_users' => $review->id_rep])
                ->where('comment', 'like', '%'.$comment.'%')
                ->where('name', 'like', '%'.$name.'%')
                ->first()) {
                echo $comment."\r\n";
                print_r($review->toArray());
                print_r($rev->toArray());

                if (!$rev->phone) {
                    $rev->phone = $review->phone;
                    $rev->save();
                }

                // Удаляем
                $review->delete();
                echo '-----------------------------------------------------------------'."\r\n";
            }
        }

        // Переносим оставшиеся отзывы как новые для ручного аппрува
        $otzyvAdd = OtzyvAdd::all();

        foreach ($otzyvAdd as $otzyv) {
            $count = Reviews::where(['comment' => $otzyv->comment, 'id_users' => $otzyv->id_rep])->count();

            if ($count == 0) {
                try {
                    $review = new Reviews();
                    $review->date = $otzyv->dt;
                    $review->name = $otzyv->name;
                    $review->id_users = $otzyv->id_rep;
                    $review->id_orders = $otzyv->id_post;
                    $review->phone = $otzyv->phone;
                    $review->comment = $otzyv->comment;
                    $review->new = 1;
                    $review->rating = empty($otzyv->reit) ? 4 : $otzyv->reit;

                    if ($review->rating == 0) {
                        $review->rating = 4;
                    }

                    $review->id_reviews_statuses = 1; // Новая заявка

                    if ($review->save()) {
                        $otzyv->delete();
                    }
                } catch (\Exception $e) {
                    echo $e->getMessage();
                }
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
