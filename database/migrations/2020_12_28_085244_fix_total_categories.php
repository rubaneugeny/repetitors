<?php

use App\Models\TotalCategories;
use App\Models\TotalTemplates;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FixTotalCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('headers_categories', function (Blueprint $table) {
            $table->dropIndex('slug');
        });

        DB::statement("ALTER TABLE `total_templates` ADD COLUMN `id_courses_categories_second` INT(10) UNSIGNED NULL AFTER `id_total_categories`");
        DB::statement("ALTER TABLE `total_templates` ADD COLUMN `id_courses` INT(10) UNSIGNED NULL AFTER `id_courses_categories_second`");

        $category = TotalCategories::create(['name' => 'metro']);
        TotalTemplates::create([
            'id_total_categories' => $category->id_total_categories,
            'id_courses' => 1000,
            'template' => 'Хотите найти хорошего автоинструктора {IN_THE_DIRECTION}? В нашей базе их {TOTAL_COUNT}!',
            'page' => 1
        ]);
        TotalTemplates::create([
            'id_total_categories' => $category->id_total_categories,
            'id_courses' => 1001,
            'template' => 'Хотите найти хорошего автоинструктора {IN_THE_DIRECTION}? В нашей базе их {TOTAL_COUNT}!',
            'page' => 1
        ]);
        TotalTemplates::create([
            'id_total_categories' => $category->id_total_categories,
            'id_courses' => 1002,
            'template' => 'Хотите найти хорошего мотоинструктора {IN_THE_DIRECTION}? В нашей базе их {TOTAL_COUNT}!',
            'page' => 1
        ]);
        TotalTemplates::create([
            'id_total_categories' => $category->id_total_categories,
            'id_courses' => 23,
            'template' => 'Хотите найти хорошего логопеда {IN_THE_DIRECTION}? В нашей базе их {TOTAL_COUNT}!',
            'page' => 1
        ]);
        TotalTemplates::create([
            'id_total_categories' => $category->id_total_categories,
            'id_courses' => 13,
            'template' => 'Хотите найти репетитора начальных классов на м. {NAME}? В нашей базе их {TOTAL_COUNT}!',
            'page' => 1
        ]);
        TotalTemplates::create([
            'id_total_categories' => $category->id_total_categories,
            'id_courses' => 63,
            'template' => 'Хотите найти репетитора по подготовке к школе на м. {NAME}? В нашей базе их {TOTAL_COUNT}!',
            'page' => 1
        ]);

        TotalTemplates::create([
            'id_total_categories' => $category->id_total_categories,
            'id_courses_categories_second' => 5,
            'template' => 'Всего тренеров по {RECOMMEND_NAME} {IN_THE_DIRECTION}: {TOTAL_COUNT}',
            'page' => 0
        ]);
        TotalTemplates::create([
            'id_total_categories' => $category->id_total_categories,
            'id_courses_categories_second' => 7,
            'template' => 'Всего тренеров по {RECOMMEND_NAME} {IN_THE_DIRECTION}: {TOTAL_COUNT}',
            'page' => 0
        ]);

        TotalTemplates::create([
            'id_total_categories' => $category->id_total_categories,
            'id_courses_categories_second' => 4,
            'template' => 'Хотите найти репетитора по {RECOMMEND_NAME} на м. {NAME}? В нашей базе их {TOTAL_COUNT}!',
            'page' => 1
        ]);
        TotalTemplates::create([
            'id_total_categories' => $category->id_total_categories,
            'id_courses_categories_second' => 3,
            'template' => 'Хотите найти репетитора по {RECOMMEND_NAME} на м. {NAME}? В нашей базе их {TOTAL_COUNT}!',
            'page' => 1
        ]);
        TotalTemplates::create([
            'id_total_categories' => $category->id_total_categories,
            'id_courses_categories_second' => 1,
            'template' => 'Хотите найти репетитора по {RECOMMEND_NAME} на м. {NAME}? В нашей базе их {TOTAL_COUNT}!',
            'page' => 1
        ]);
        TotalTemplates::create([
            'id_total_categories' => $category->id_total_categories,
            'id_courses_categories_second' => 2,
            'template' => 'Хотите найти репетитора {RECOMMEND_NAME_SECOND} на метро {NAME}? В нашей базе их {TOTAL_COUNT}!',
            'page' => 1
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
