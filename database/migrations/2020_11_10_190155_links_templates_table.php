<?php

use App\Models\LinksTemplates;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class LinksTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('links_templates');

        Schema::create('links_templates', function (Blueprint $table) {
            $table->integer('id_links_templates', true, true)->nullable(false);
            $table->integer('id_links_categories', false, true)->nullable(false)->index('id_links_categories');
            $table->text('template')->nullable(false);
            $table->smallInteger('page', false, true)->nullable(false);

            $table->foreign('id_links_categories', 'links_templates_links_categories')
                ->references('id_links_categories')
                ->on('links_categories')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        if (($f = fopen(__DIR__.'/../dumps/links_templates.csv', 'r')) !== false) {
            while (($data = fgetcsv($f, 0, "\t")) !== false) {
                $item = new LinksTemplates();
                $item->id_links_templates = $data[0];
                $item->id_links_categories = $data[1];
                $item->template = $data[2];
                $item->page = $data[3];
                $item->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('links_templates');
    }
}
