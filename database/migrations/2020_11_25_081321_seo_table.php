<?php

use App\Models\Seo;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SeoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE seo ENGINE = InnoDB');
        DB::statement('ALTER TABLE seo CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci');

        Schema::table('seo', function (Blueprint $table) {
            $table->renameColumn('id', 'id_seo');
            $table->renameColumn('kolvo', 'total_count');
            $table->renameColumn('miniup', 'prepend_text');

            $table->dropColumn('h2');
            $table->dropColumn('d1');
            $table->dropColumn('d2');
            $table->dropColumn('d3');
            $table->dropColumn('d4');
            $table->dropColumn('d5');
            $table->dropColumn('d6');
            $table->dropColumn('d7');
            $table->dropColumn('d8');
            $table->dropColumn('d9');
            $table->dropColumn('d10');
        });

        $items = Seo::all();

        foreach ($items ?? [] as $item) {
            $item->title = trim(stripslashes(html_entity_decode($item->title)));

            if (empty($item->title)) {
                $item->title = null;
            }

            $item->h1 = trim($item->h1);

            if (empty($item->h1)) {
                $item->h1 = null;
            }

            $item->total_count = trim($item->total_count);

            if (empty($item->total_count)) {
                $item->total_count = null;
            }

            $item->text = trim(preg_replace('~^&#8194;&#8194;~', '', $item->text));

            if (empty($item->text)) {
                $item->text = null;
            }

            $item->link = trim($item->link);
            $item->descript = trim($item->descript);

            if (empty($item->descript)) {
                $item->descript = null;
            }

            $item->k1 = trim($item->k1);

            if (empty($item->k1)) {
                $item->k1 = null;
            }

            $item->k2 = trim($item->k2);

            if (empty($item->k2)) {
                $item->k2 = null;
            }

            $item->k3 = trim($item->k3);

            if (empty($item->k3)) {
                $item->k3 = null;
            }

            $item->k4 = trim($item->k4);

            if (empty($item->k4)) {
                $item->k4 = null;
            }

            $item->k5 = trim($item->k5);

            if (empty($item->k5)) {
                $item->k5 = null;
            }

            $item->k6 = trim($item->k6);

            if (empty($item->k6)) {
                $item->k6 = null;
            }

            $item->k7 = trim($item->k7);

            if (empty($item->k7)) {
                $item->k7 = null;
            }

            $item->k8 = trim($item->k8);

            if (empty($item->k8)) {
                $item->k8 = null;
            }

            $item->k9 = trim($item->k9);

            if (empty($item->k9)) {
                $item->k9 = null;
            }

            $item->k10 = trim($item->k10);

            if (empty($item->k10)) {
                $item->k10 = null;
            }

            $item->save();
        }

        DB::statement("ALTER TABLE `seo` MODIFY COLUMN `id_seo` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT");
        DB::statement("ALTER TABLE `seo` MODIFY COLUMN `emoji` VARCHAR(255) NULL COLLATE 'utf8mb4_unicode_ci' COMMENT 'Иконка в title' AFTER `id_seo`");
        DB::statement("ALTER TABLE `seo` MODIFY COLUMN `title` TEXT NULL COLLATE 'utf8mb4_unicode_ci' COMMENT 'Заголовок страницы' AFTER `emoji`");
        DB::statement("ALTER TABLE `seo` MODIFY COLUMN `h1` VARCHAR(255) NULL COLLATE 'utf8mb4_unicode_ci' COMMENT 'Заголовок страницы' AFTER `title`");
        DB::statement("ALTER TABLE `seo` MODIFY COLUMN `total_count` TEXT NULL COLLATE 'utf8mb4_unicode_ci' COMMENT 'Текст перед общим количеством записей преподавателей на странице' AFTER `h1`");
        DB::statement("ALTER TABLE `seo` MODIFY COLUMN `prepend_text` TEXT NULL COLLATE 'utf8mb4_unicode_ci' COMMENT 'Текст после всего репетиторов на странице' AFTER `total_count`");
        DB::statement("ALTER TABLE `seo` MODIFY COLUMN `text` TEXT NULL COLLATE 'utf8mb4_unicode_ci' COMMENT 'Текст в конце страницы после номеров страниц' AFTER `prepend_text`");
        DB::statement("ALTER TABLE `seo` MODIFY COLUMN `link` VARCHAR(255) NULL COLLATE 'utf8mb4_unicode_ci' COMMENT '???'");
        DB::statement("ALTER TABLE `seo` MODIFY COLUMN `descript` TEXT NULL COLLATE 'utf8mb4_unicode_ci' COMMENT '???'");
        DB::statement("ALTER TABLE `seo` MODIFY COLUMN `k1` TEXT NULL COLLATE 'utf8mb4_unicode_ci' COMMENT '???'");
        DB::statement("ALTER TABLE `seo` MODIFY COLUMN `k2` TEXT NULL COLLATE 'utf8mb4_unicode_ci' COMMENT '???'");
        DB::statement("ALTER TABLE `seo` MODIFY COLUMN `k3` TEXT NULL COLLATE 'utf8mb4_unicode_ci' COMMENT '???'");
        DB::statement("ALTER TABLE `seo` MODIFY COLUMN `k4` TEXT NULL COLLATE 'utf8mb4_unicode_ci' COMMENT '???'");
        DB::statement("ALTER TABLE `seo` MODIFY COLUMN `k5` TEXT NULL COLLATE 'utf8mb4_unicode_ci' COMMENT '???'");
        DB::statement("ALTER TABLE `seo` MODIFY COLUMN `k6` TEXT NULL COLLATE 'utf8mb4_unicode_ci' COMMENT '???'");
        DB::statement("ALTER TABLE `seo` MODIFY COLUMN `k7` TEXT NULL COLLATE 'utf8mb4_unicode_ci' COMMENT '???'");
        DB::statement("ALTER TABLE `seo` MODIFY COLUMN `k8` TEXT NULL COLLATE 'utf8mb4_unicode_ci' COMMENT '???'");
        DB::statement("ALTER TABLE `seo` MODIFY COLUMN `k9` TEXT NULL COLLATE 'utf8mb4_unicode_ci' COMMENT '???'");
        DB::statement("ALTER TABLE `seo` MODIFY COLUMN `k10` TEXT NULL COLLATE 'utf8mb4_unicode_ci' COMMENT '???'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
