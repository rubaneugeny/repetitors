<?php

use App\Models\ReviewsTeachers;
use App\Models\Users;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ReviewsTeachersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('reviews_teachers');

        if (Schema::hasTable('tutors_reviews')) {
            DB::statement('ALTER TABLE `tutors_reviews` ENGINE = InnoDB');
            DB::statement('ALTER TABLE `tutors_reviews` CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci');

            Schema::rename('tutors_reviews', 'reviews_teachers');
        }

        Schema::table('reviews_teachers', function (Blueprint $table) {
            $table->renameColumn('id', 'id_reviews_teachers');
            $table->renameColumn('user_id', 'id_users');
            $table->renameColumn('review', 'comment');
            $table->renameColumn('active', 'is_approved');
            $table->renameColumn('id_lern', 'id_courses');

//            $table->renameColumn('active_temp', 'temp_is_approved');
//            $table->dropColumn('id_lern');
//            $table->dropColumn('active_temp');
//            $table->dropColumn('id_lern_temp');
        });

        DB::statement("ALTER TABLE `reviews_teachers` MODIFY COLUMN `id_courses` INT(10) UNSIGNED NULL");

        $reviews = ReviewsTeachers::all();

        foreach ($reviews ?? [] as $review) {
            if (!Users::find($review->id_users)) {
                $review->delete();
            } elseif ($review->id_courses == 0) {
                $review->id_courses = null;
                $review->save();
            }
        }

        DB::statement("ALTER TABLE `reviews_teachers` MODIFY COLUMN `id_reviews_teachers` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT");
        DB::statement("ALTER TABLE `reviews_teachers` MODIFY COLUMN `id_users` INT(10) UNSIGNED NOT NULL AFTER `id_reviews_teachers`");
        DB::statement("ALTER TABLE `reviews_teachers` MODIFY COLUMN `id_courses` INT(10) UNSIGNED NULL COMMENT '??? Непонятно, нужно ли оставлять это поле или удалять, посмотреть после админки' AFTER `id_users`");
        DB::statement("ALTER TABLE `reviews_teachers` MODIFY COLUMN `is_approved` SMALLINT(5) NOT NULL DEFAULT 0 COMMENT 'Подтвержден ли отзыв' AFTER `id_courses`");
        DB::statement("ALTER TABLE `reviews_teachers` MODIFY COLUMN `comment` TEXT NOT NULL COLLATE 'utf8mb4_unicode_ci' AFTER `is_approved`");

        // TODO: связать с id_courses или возможно не надо, проверить после админки
        Schema::table('reviews_teachers', function (Blueprint $table) {
            $table->foreign('id_users', 'id_users')->references('id_users')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->index('id_courses', 'id_courses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reviews_teachers');
    }
}
