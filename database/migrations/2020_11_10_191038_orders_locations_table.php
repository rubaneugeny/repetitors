<?php

use App\Models\OrdersLocations;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class OrdersLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('orders_locations');

        Schema::create('orders_locations', function (Blueprint $table) {
            $table->integer('id_orders_locations', true, true)->nullable(false);
            $table->string('name', 30)->nullable(false);
        });

        if (($f = fopen(__DIR__.'/../dumps/orders_locations.csv', 'r')) !== false) {
            while (($data = fgetcsv($f, 0, "\t")) !== false) {
                $item = new OrdersLocations();
                $item->id_orders_locations = $data[0];
                $item->name = $data[1];
                $item->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders_locations');
    }
}
