<?php

use App\Models\Courses;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FixCoursesTable2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE `ratings` MODIFY COLUMN `id_courses` INT(10) UNSIGNED NOT NULL COMMENT 'Используется для фильтрации подкатегорий, например репетиторы эксперты ЕГЭ и другие.'");

        Courses::whereIn('id_courses', [
            4608, 4599, 4550, 4551, 4547, 4590, 4548, 4549, 4583, 4582, 4570, 4573, 4572, 4571, 4554, 4555, 4552, 4553,
            4557, 4558, 4556, 4580, 4581, 4564, 4565, 4563, 4592, 4579, 4681, 4650, 4543, 4544, 4538, 4588, 4587, 4589,
            4539, 4540, 4545, 4542, 4546, 4541, 4585, 4586, 4584, 4566, 4568, 4567, 4569, 4525, 4529, 4526, 4527, 4561,
            4562, 4559, 4560, 4574, 4576, 4577, 4578, 4591, 4575, 4687, 4645, 4613, 4627, 4609, 4600, 4636, 4691, 4623,
            4604, 4594, 4632, 4689, 4619, 4649, 4615, 4643, 4629, 4605, 4595, 4633, 4690, 4620, 4607, 4598, 4635, 4692,
            4622, 4647, 4615, 4642, 4610, 4601, 4637, 4693, 4624, 4648, 4617, 4644, 4612, 4639, 4695, 4626, 4603, 4593,
            4631, 4686, 4537, 4618, 4611, 4602, 4638, 4694, 4625, 4646, 4614, 4667, 4668, 4641, 4628, 4630, 4523, 4606,
            4596, 4634, 4688, 4621,
        ])->update([
            'id_title_categories' => 1,
            'id_total_categories' => 8, // miniup
            'id_prepend_text_categories' => 15, // miniup
            'id_photo_text_categories' => 1,
            'id_pages_append_text_categories' => 18
        ]);
        Courses::whereIn('id_courses', [
            4674, 4680, 4682, 4651, 4652, 4653, 4640, 4654, 4655, 4656, 4657, 4659, 4660, 4661, 4662, 4663, 4665, 4666,
            4675, 4676, 4677, 4678, 4679, 4669, 4670, 4671, 4672, 4673,
        ])->update([
            'id_title_categories' => 1,
            'id_total_categories' => 8, // miniup
            'id_prepend_text_categories' => 15, // miniup
            'id_photo_text_categories' => 1
        ]);
        Courses::whereIn('id_courses', [
            4000
        ])->update([
            'id_title_categories' => 1,
            'id_total_categories' => 6,
            'id_prepend_text_categories' => 16,
            'id_photo_text_categories' => 1
        ]);
        Courses::whereIn('id_courses', [
            4002
        ])->update([
            'id_title_categories' => 1,
            'id_total_categories' => 6,
            'id_prepend_text_categories' => 16,
            'id_photo_text_categories' => 1,
            'id_pages_append_text_categories' => 18
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
