<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UniqueColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('universities', function(Blueprint $table) {
            $table->unique('slug', 'slug');
        });

        Schema::table('metro', function(Blueprint $table) {
            $table->unique('slug', 'slug');
        });

        Schema::table('headers', function(Blueprint $table) {
            $table->unique('slug', 'slug');
        });

        Schema::table('cars', function(Blueprint $table) {
            $table->unique('slug', 'slug');
        });

        Schema::table('districts', function(Blueprint $table) {
            $table->unique('slug', 'slug');
        });

        Schema::table('blog', function(Blueprint $table) {
            $table->unique('slug', 'slug');
        });

        Schema::table('total_categories', function(Blueprint $table) {
            $table->unique('name', 'name');
        });

        Schema::table('title_categories', function(Blueprint $table) {
            $table->unique('name', 'name');
        });

        Schema::table('prepend_text_categories', function(Blueprint $table) {
            $table->unique('name', 'name');
        });

        Schema::table('photo_text_categories', function(Blueprint $table) {
            $table->unique('name', 'name');
        });

        Schema::table('pages_append_text_categories', function(Blueprint $table) {
            $table->unique('name', 'name');
        });

        Schema::table('links_categories', function(Blueprint $table) {
            $table->unique('name', 'name');
        });

        Schema::table('comments_prepend_text_categories', function(Blueprint $table) {
            $table->unique('name', 'name');
        });

        Schema::table('breadcrumbs_categories', function(Blueprint $table) {
            $table->unique('name', 'name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
