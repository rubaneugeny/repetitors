<?php

use App\Models\Courses;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSecondCourse extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE `courses` ADD COLUMN `id_courses_third` INT(10) UNSIGNED NULL COMMENT 'Третий предмет для выборки' AFTER `id_courses_second`");

        Schema::table('courses', function (Blueprint $table) {
            $table->index('id_courses_third', 'id_courses_third');
        });

        Schema::table('courses', function (Blueprint $table) {
            $table->foreign('id_courses_third', 'courses_third_courses')->references('id_courses')->on('courses')->onDelete('set null')->onUpdate('cascade');
        });

        Courses::where(['id_courses' => 4575])->update(['id_courses_second' => 14]);
        Courses::where(['id_courses' => 4574])->update(['id_courses_second' => 15]);
        Courses::where(['id_courses' => 4577])->update(['id_courses_second' => 16]);
        Courses::where(['id_courses' => 4576])->update(['id_courses_second' => 17]);
        Courses::where(['id_courses' => 4578])->update(['id_courses_second' => 19]);
        Courses::where(['id_courses' => 4591])->update(['id_courses_second' => 14, 'id_courses_third' => 15]);
        Courses::where(['id_courses' => 4550])->update(['id_courses_second' => 2]);
        Courses::where(['id_courses' => 4551])->update(['id_courses_second' => 7]);
        Courses::where(['id_courses' => 4590])->update(['id_courses_second' => 5, 'id_courses_third' => 7]);
        Courses::where(['id_courses' => 4547])->update(['id_courses_second' => 5]);
        Courses::where(['id_courses' => 4548])->update(['id_courses_second' => 9]);
        Courses::where(['id_courses' => 4549])->update(['id_courses_second' => 3]);
        Courses::where(['id_courses' => 4583])->update(['id_courses_second' => 16]);
        Courses::where(['id_courses' => 4582])->update(['id_courses_second' => 14]);
        Courses::where(['id_courses' => 4570])->update(['id_courses_second' => 7]);
        Courses::where(['id_courses' => 4572])->update(['id_courses_second' => 1]);
        Courses::where(['id_courses' => 4571])->update(['id_courses_second' => 11]);
        Courses::where(['id_courses' => 4554])->update(['id_courses_second' => 2]);
        Courses::where(['id_courses' => 4555])->update(['id_courses_second' => 7]);
        Courses::where(['id_courses' => 4552])->update(['id_courses_second' => 9]);
        Courses::where(['id_courses' => 4553])->update(['id_courses_second' => 3]);
        Courses::where(['id_courses' => 4557])->update(['id_courses_second' => 2]);
        Courses::where(['id_courses' => 4558])->update(['id_courses_second' => 7]);
        Courses::where(['id_courses' => 4556])->update(['id_courses_second' => 3]);
        Courses::where(['id_courses' => 4580])->update(['id_courses_second' => 15]);
        Courses::where(['id_courses' => 4581])->update(['id_courses_second' => 14]);
        Courses::where(['id_courses' => 4564])->update(['id_courses_second' => 2]);
        Courses::where(['id_courses' => 4565])->update(['id_courses_second' => 7]);
        Courses::where(['id_courses' => 4563])->update(['id_courses_second' => 12]);
        Courses::where(['id_courses' => 4592])->update(['id_courses_second' => 11, 'id_courses_third' => 3]);
        Courses::where(['id_courses' => 4579])->update(['id_courses_second' => 17]);
        Courses::where(['id_courses' => 4543])->update(['id_courses_second' => 2]);
        Courses::where(['id_courses' => 4544])->update(['id_courses_second' => 7]);
        Courses::where(['id_courses' => 4588])->update(['id_courses_second' => 4, 'id_courses_third' => 7]);
        Courses::where(['id_courses' => 4587])->update(['id_courses_second' => 4, 'id_courses_third' => 5]);
        Courses::where(['id_courses' => 4589])->update(['id_courses_second' => 4, 'id_courses_third' => 9]);
        Courses::where(['id_courses' => 4538])->update(['id_courses_second' => 4]);
        Courses::where(['id_courses' => 4539])->update(['id_courses_second' => 5]);
        Courses::where(['id_courses' => 4540])->update(['id_courses_second' => 9]);
        Courses::where(['id_courses' => 4545])->update(['id_courses_second' => 10]);
        Courses::where(['id_courses' => 4542])->update(['id_courses_second' => 12]);
        Courses::where(['id_courses' => 4546])->update(['id_courses_second' => 11]);
        Courses::where(['id_courses' => 4585])->update(['id_courses_second' => 3, 'id_courses_third' => 2]);
        Courses::where(['id_courses' => 4586])->update(['id_courses_second' => 3, 'id_courses_third' => 12]);
        Courses::where(['id_courses' => 4584])->update(['id_courses_second' => 3, 'id_courses_third' => 13]);
        Courses::where(['id_courses' => 4541])->update(['id_courses_second' => 3]);
        Courses::where(['id_courses' => 4523])->update(['id_courses_second' => 2]);
        Courses::where(['id_courses' => 4566])->update(['id_courses_second' => 2]);
        Courses::where(['id_courses' => 4568])->update(['id_courses_second' => 4]);
        Courses::where(['id_courses' => 4567])->update(['id_courses_second' => 5]);
        Courses::where(['id_courses' => 4569])->update(['id_courses_second' => 9]);
        Courses::where(['id_courses' => 4561])->update(['id_courses_second' => 2]);
        Courses::where(['id_courses' => 4562])->update(['id_courses_second' => 10]);
        Courses::where(['id_courses' => 4559])->update(['id_courses_second' => 12]);
        Courses::where(['id_courses' => 4560])->update(['id_courses_second' => 11]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
