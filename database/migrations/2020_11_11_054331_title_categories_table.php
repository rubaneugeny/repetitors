<?php

use App\Models\TitleCategories;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TitleCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('title_categories');

        Schema::create('title_categories', function (Blueprint $table) {
            $table->integer('id_title_categories', true, true)->nullable(false);
            $table->string('name')->nullable(false);
        });

        if (($f = fopen(__DIR__.'/../dumps/title_categories.csv', 'r')) !== false) {
            while (($data = fgetcsv($f, 0, "\t")) !== false) {
                $item = new TitleCategories();
                $item->id_title_categories = $data[0];
                $item->name = $data[1];
                $item->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('title_categories');
    }
}
