<?php

use App\Models\Phonebook;
use App\Models\Users;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PhonebookTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('telefon')) {
            DB::statement('ALTER TABLE `telefon` ENGINE = InnoDB');
            DB::statement('ALTER TABLE `telefon` CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci');

            Schema::rename('telefon', 'phonebook');
        }

        DB::statement("ALTER TABLE `phonebook` MODIFY COLUMN `rep` INT(10) UNSIGNED NOT NULL");

        Schema::table('phonebook', function (Blueprint $table) {
            $table->renameColumn('id_tel', 'id_phonebook');
            $table->renameColumn('tel', 'phone');
            $table->renameColumn('rep', 'id_users');
            $table->renameColumn('lname', 'last_name');
            $table->renameColumn('name', 'first_name');
            $table->renameColumn('oname', 'middle_name');
            $table->renameColumn('mail', 'email');

            $table->timestamps();

            $table->index('id_users', 'id_users');
        });

        DB::statement("ALTER TABLE `phonebook` MODIFY COLUMN `id_phonebook` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT");
        DB::statement("ALTER TABLE `phonebook` MODIFY COLUMN `phone` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_unicode_ci' AFTER `id_phonebook`");
        DB::statement("ALTER TABLE `phonebook` MODIFY COLUMN `id_users` INT(10) UNSIGNED NOT NULL AFTER `phone`");
        DB::statement("ALTER TABLE `phonebook` MODIFY COLUMN `last_name` VARCHAR(255) NULL COLLATE 'utf8mb4_unicode_ci' AFTER `id_users`");
        DB::statement("ALTER TABLE `phonebook` MODIFY COLUMN `first_name` VARCHAR(255) NULL COLLATE 'utf8mb4_unicode_ci' AFTER `last_name`");
        DB::statement("ALTER TABLE `phonebook` MODIFY COLUMN `middle_name` VARCHAR(255) NULL COLLATE 'utf8mb4_unicode_ci' AFTER `first_name`");
        DB::statement("ALTER TABLE `phonebook` MODIFY COLUMN `email` VARCHAR(255) NULL COLLATE 'utf8mb4_unicode_ci' AFTER `middle_name`");

        $items = Phonebook::all();

        foreach ($items ?? [] as $item) {
            $item->last_name = trim($item->last_name);
            $item->first_name = trim($item->first_name);
            $item->middle_name = trim($item->middle_name);
            $item->email = trim($item->email);

            if (empty($item->last_name)) {
                $item->last_name = null;
            }

            if (empty($item->first_name)) {
                $item->first_name = null;
            }

            if (empty($item->middle_name)) {
                $item->middle_name = null;
            }

            if (empty($item->email)) {
                $item->email = null;
            }

            if (!Users::find($item->id_users)) {
                $item->delete();
            } else {
                $item->save();
            }
        }

        Schema::table('phonebook', function (Blueprint $table) {
            $table->foreign('id_users', 'phonebook_users')->references('id_users')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('phonebook');
    }
}
