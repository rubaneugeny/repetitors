<?php

use App\Models\Users;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UsersRelations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Users::where(['id_universities' => 30])->update(['id_universities' => null]);

        Schema::table('users', function (Blueprint $table) {
            $table->foreign('id_cars', 'users_cars')->references('id_cars')->on('cars')->onDelete('set null')->onUpdate('cascade');
            $table->foreign('id_cars_second', 'users_cars_second')->references('id_cars')->on('cars')->onDelete('set null')->onUpdate('cascade');
            $table->foreign('id_cities', 'users_cities')->references('id_cities')->on('cities')->onDelete('set null')->onUpdate('cascade');
            $table->foreign('id_cities_second', 'users_cities_second')->references('id_cities')->on('cities')->onDelete('set null')->onUpdate('cascade');
            $table->foreign('id_cities_third', 'users_cities_third')->references('id_cities')->on('cities')->onDelete('set null')->onUpdate('cascade');
            $table->foreign('id_cities_fourth', 'users_cities_fourth')->references('id_cities')->on('cities')->onDelete('set null')->onUpdate('cascade');
            $table->foreign('id_courses', 'users_courses')->references('id_courses')->on('courses')->onDelete('set null')->onUpdate('cascade');
            $table->foreign('id_courses_second', 'users_courses_second')->references('id_courses')->on('courses')->onDelete('set null')->onUpdate('cascade');
            $table->foreign('id_courses_third', 'users_courses_third')->references('id_courses')->on('courses')->onDelete('set null')->onUpdate('cascade');
            $table->foreign('id_courses_fourth', 'users_courses_fourth')->references('id_courses')->on('courses')->onDelete('set null')->onUpdate('cascade');
            $table->foreign('id_districts', 'users_districts')->references('id_districts')->on('districts')->onDelete('set null')->onUpdate('cascade');
            $table->foreign('id_metro', 'users_metro')->references('id_metro')->on('metro')->onDelete('set null')->onUpdate('cascade');
            $table->foreign('id_metro_second', 'users_metro_second')->references('id_metro')->on('metro')->onDelete('set null')->onUpdate('cascade');
            $table->foreign('id_universities', 'users_universities')->references('id_universities')->on('universities')->onDelete('set null')->onUpdate('cascade');
            $table->foreign('id_users_active_statuses', 'users_users_active_statuses')->references('id_users_active_statuses')->on('users_active_statuses')->onDelete('restrict')->onUpdate('cascade');
            $table->foreign('id_users_statuses', 'users_users_statuses')->references('id_users_statuses')->on('users_statuses')->onDelete('restrict')->onUpdate('cascade');

            $table->renameColumn('video', 'show_video');
        });

        DB::statement("ALTER TABLE `users` MODIFY COLUMN `komdoma` MEDIUMTEXT NULL COLLATE 'utf8mb4_unicode_ci' COMMENT '??? Районы проведения занятий / Куда выезжает преподаватель'");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `about` MEDIUMTEXT NULL COLLATE 'utf8mb4_unicode_ci' COMMENT '???'");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `last_visit` INT(11) NULL COMMENT '???'");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `last_visit_time` MEDIUMTEXT NULL COLLATE 'utf8mb4_unicode_ci' COMMENT '???'");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `balans` MEDIUMTEXT NULL COLLATE 'utf8mb4_unicode_ci' COMMENT '???'");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `gorod` INT(11) NOT NULL COMMENT '???' DEFAULT 0");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `id_courses_native` INT(10) UNSIGNED NULL COMMENT 'ID предмета, по которому преподаватель является носителем языка, так же данный предмет должен быть у него в одной из 4х категорий предметов'");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `oferta` INT(11) NULL COMMENT '???'");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `avto1` MEDIUMTEXT NULL COLLATE 'utf8mb4_unicode_ci' COMMENT '??? Авто 1'");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `avto2` MEDIUMTEXT NULL COLLATE 'utf8mb4_unicode_ci' COMMENT '??? Авто 2'");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `nami1` INT(11) NULL COMMENT '??? Дублирующие педали, не нашел где используется при выводе'");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `nami2` INT(11) NULL COMMENT '??? Дублирующие педали, не нашел где используется при выводе'");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `photo1` MEDIUMTEXT NULL COLLATE 'utf8mb4_unicode_ci' COMMENT '???'");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `photo2` MEDIUMTEXT NULL COLLATE 'utf8mb4_unicode_ci' COMMENT '???'");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `photo3` MEDIUMTEXT NULL COLLATE 'utf8mb4_unicode_ci' COMMENT '???'");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `photo4` MEDIUMTEXT NULL COLLATE 'utf8mb4_unicode_ci' COMMENT '???'");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `new_comment` INT(11) NULL COMMENT '???' DEFAULT 1");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `free` INT(11) NULL COMMENT '???' DEFAULT 1");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `show_video` TINYINT(1) UNSIGNED NOT NULL COMMENT 'Флаг, отображать ли видео в карточке репетитора' DEFAULT 0");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `videootzyv` INT(11) NULL COMMENT '???'");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `linka` INT(11) NOT NULL COMMENT '???' DEFAULT 1");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `doci` INT(11) NULL COMMENT '???' DEFAULT 1");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `live` INT(11) NULL COMMENT '???'");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `tip` INT(11) NULL COMMENT '???'");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `svoe` INT(11) NULL COMMENT '??? Могу предоставить музыкальный инструмент на первое время уроков'");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `dubl` INT(11) NULL COMMENT '??? Вторая анкета (отметьте, если уже делалась анкета ранее - по другим дисциплинам) id_users'");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `edit_fltr` INT(11) NULL COMMENT '???' DEFAULT 0");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `edit_fltr_c` INT(11) NULL COMMENT '???' DEFAULT 0");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `rating_last_name` TINYINT(1) UNSIGNED NOT NULL COMMENT 'Флаг, что добавили рейтинг за фамилию' DEFAULT 0");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `edit_anketa` INT(11) NULL COMMENT '???' DEFAULT 0");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `edit_anketa_c` INT(11) NULL COMMENT '???' DEFAULT 0");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `hide_anketa` INT(11) NULL COMMENT '???' DEFAULT 0");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `hide_anketa_c` INT(11) NULL COMMENT '???' DEFAULT 0");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `hide_anketa_s` INT(11) NULL COMMENT '???' DEFAULT 0");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `hide_anketa_m` INT(11) NULL COMMENT '???' DEFAULT 0");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `login_skype` VARCHAR(255) NULL COMMENT '???'");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `show_schedule` TINYINT(1) UNSIGNED NOT NULL COMMENT 'Флаг, показывать ли расписание в карточке репетитора' DEFAULT 1");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `schedule_last_update_date` DATETIME NULL COMMENT 'Дата последнего изменения расписания'");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `multiplier_personal` DECIMAL(10,2) NULL COMMENT 'Индивидуальный коэффициент, используется для рассчета стоимости заказа'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
