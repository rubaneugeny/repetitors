<?php

use App\Models\TotalCategories;
use App\Models\TotalTemplates;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTotalText extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $category = TotalCategories::create(['name' => 'district']);
        TotalTemplates::create([
            'id_total_categories' => $category->id_total_categories,
            'id_courses' => 1000,
            'template' => 'Хотите найти хорошего автоинструктора {IN_THE_DIRECTION}? В нашей базе их {TOTAL_COUNT}!',
            'page' => 1
        ]);
        TotalTemplates::create([
            'id_total_categories' => $category->id_total_categories,
            'id_courses' => 1000,
            'template' => 'Всего инструкторов по вождению {IN_THE_DIRECTION} - {TOTAL_COUNT}',
            'page' => 0
        ]);
        TotalTemplates::create([
            'id_total_categories' => $category->id_total_categories,
            'id_courses' => 1001,
            'template' => 'Хотите найти хорошего автоинструктора {IN_THE_DIRECTION}? В нашей базе их {TOTAL_COUNT}!',
            'page' => 1
        ]);
        TotalTemplates::create([
            'id_total_categories' => $category->id_total_categories,
            'id_courses' => 1001,
            'template' => 'Всего инструкторов по вождению {IN_THE_DIRECTION} - {TOTAL_COUNT}',
            'page' => 0
        ]);

        TotalTemplates::create([
            'id_total_categories' => $category->id_total_categories,
            'id_courses' => 1002,
            'template' => 'Хотите найти хорошего мотоинструктора {IN_THE_DIRECTION}? В нашей базе их {TOTAL_COUNT}!',
            'page' => 1
        ]);
        TotalTemplates::create([
            'id_total_categories' => $category->id_total_categories,
            'id_courses' => 1002,
            'template' => 'Всего мотоинструкторов {IN_THE_DIRECTION} - {TOTAL_COUNT}',
            'page' => 0
        ]);

        TotalTemplates::create([
            'id_total_categories' => $category->id_total_categories,
            'id_courses' => 23,
            'template' => 'Хотите найти хорошего логопеда {IN_THE_DIRECTION}? В нашей базе их {TOTAL_COUNT}!',
            'page' => 1
        ]);
        TotalTemplates::create([
            'id_total_categories' => $category->id_total_categories,
            'id_courses' => 23,
            'template' => 'Всего логопедов {IN_THE_DIRECTION} - {TOTAL_COUNT}',
            'page' => 0
        ]);
        TotalTemplates::create([
            'id_total_categories' => $category->id_total_categories,
            'id_courses' => 13,
            'template' => 'Хотите найти репетитора начальных классов {IN_THE_DIRECTION}? В нашей базе их {TOTAL_COUNT}!',
            'page' => 1
        ]);
        TotalTemplates::create([
            'id_total_categories' => $category->id_total_categories,
            'id_courses' => 13,
            'template' => 'Всего репетиторов начальной школы {IN_THE_DIRECTION} - {TOTAL_COUNT}',
            'page' => 0
        ]);
        TotalTemplates::create([
            'id_total_categories' => $category->id_total_categories,
            'id_courses' => 63,
            'template' => 'Хотите найти репетитора по подготовке к школе {IN_THE_DIRECTION}? В нашей базе их {TOTAL_COUNT}!',
            'page' => 1
        ]);
        TotalTemplates::create([
            'id_total_categories' => $category->id_total_categories,
            'id_courses' => 63,
            'template' => 'Всего репетиторов для подготовки к школе {IN_THE_DIRECTION} - {TOTAL_COUNT}',
            'page' => 0
        ]);

        TotalTemplates::create([
            'id_total_categories' => $category->id_total_categories,
            'id_courses_categories_second' => 5,
            'template' => 'Всего тренеров по {RECOMMEND_NAME} {IN_THE_DIRECTION}: {TOTAL_COUNT}',
            'page' => 0
        ]);
        TotalTemplates::create([
            'id_total_categories' => $category->id_total_categories,
            'id_courses_categories_second' => 7,
            'template' => 'Всего тренеров по {RECOMMEND_NAME} {IN_THE_DIRECTION}: {TOTAL_COUNT}',
            'page' => 0
        ]);

        TotalTemplates::create([
            'id_total_categories' => $category->id_total_categories,
            'id_courses_categories_second' => 4,
            'template' => 'Хотите найти репетитора по {RECOMMEND_NAME} {IN_THE_DIRECTION}? В нашей базе их {TOTAL_COUNT}!',
            'page' => 1
        ]);
        TotalTemplates::create([
            'id_total_categories' => $category->id_total_categories,
            'id_courses_categories_second' => 4,
            'template' => 'Хотите найти преподавателя по {RECOMMEND_NAME} {IN_THE_DIRECTION}? В нашей базе их {TOTAL_COUNT}!',
            'page' => 2
        ]);
        TotalTemplates::create([
            'id_total_categories' => $category->id_total_categories,
            'id_courses_categories_second' => 4,
            'template' => 'Хотите найти учителя по {RECOMMEND_NAME} {IN_THE_DIRECTION}? В нашей базе их {TOTAL_COUNT}!',
            'page' => 3
        ]);
        TotalTemplates::create([
            'id_total_categories' => $category->id_total_categories,
            'id_courses_categories_second' => 4,
            'template' => 'Всего репетиторов по {RECOMMEND_NAME} {IN_THE_DIRECTION} - {TOTAL_COUNT}',
            'page' => 0
        ]);

        TotalTemplates::create([
            'id_total_categories' => $category->id_total_categories,
            'id_courses_categories_second' => 3,
            'template' => 'Хотите найти репетитора по {RECOMMEND_NAME} {IN_THE_DIRECTION}? В нашей базе их {TOTAL_COUNT}!',
            'page' => 1
        ]);
        TotalTemplates::create([
            'id_total_categories' => $category->id_total_categories,
            'id_courses_categories_second' => 3,
            'template' => 'Всего репетиторов по {RECOMMEND_NAME} {IN_THE_DIRECTION} - {TOTAL_COUNT}',
            'page' => 0
        ]);

        TotalTemplates::create([
            'id_total_categories' => $category->id_total_categories,
            'id_courses_categories_second' => 1,
            'template' => 'Хотите найти репетитора по {RECOMMEND_NAME} {IN_THE_DIRECTION}? В нашей базе их {TOTAL_COUNT}!',
            'page' => 1
        ]);
        TotalTemplates::create([
            'id_total_categories' => $category->id_total_categories,
            'id_courses_categories_second' => 1,
            'template' => 'Хотите найти преподавателя по {RECOMMEND_NAME} {IN_THE_DIRECTION}? В нашей базе их {TOTAL_COUNT}!',
            'page' => 2
        ]);
        TotalTemplates::create([
            'id_total_categories' => $category->id_total_categories,
            'id_courses_categories_second' => 1,
            'template' => 'Хотите найти учителя по {RECOMMEND_NAME} {IN_THE_DIRECTION}? В нашей базе их {TOTAL_COUNT}!',
            'page' => 3
        ]);
        TotalTemplates::create([
            'id_total_categories' => $category->id_total_categories,
            'id_courses_categories_second' => 1,
            'template' => 'Хотите найти репетитора по {RECOMMEND_NAME} {IN_THE_DIRECTION}? В нашей базе их {TOTAL_COUNT}!',
            'page' => 4
        ]);
        TotalTemplates::create([
            'id_total_categories' => $category->id_total_categories,
            'id_courses_categories_second' => 1,
            'template' => 'Хотите найти репетитора по {RECOMMEND_NAME} {IN_THE_DIRECTION}? В нашей базе их {TOTAL_COUNT}!',
            'page' => 5
        ]);
        TotalTemplates::create([
            'id_total_categories' => $category->id_total_categories,
            'id_courses_categories_second' => 1,
            'template' => 'Всего репетиторов по {RECOMMEND_NAME} {IN_THE_DIRECTION} - {TOTAL_COUNT}',
            'page' => 0
        ]);

        TotalTemplates::create([
            'id_total_categories' => $category->id_total_categories,
            'id_courses_categories_second' => 2,
            'template' => 'Хотите найти репетитора {RECOMMEND_NAME_SECOND} {IN_THE_DIRECTION}? В нашей базе их {TOTAL_COUNT}!',
            'page' => 1
        ]);
        TotalTemplates::create([
            'id_total_categories' => $category->id_total_categories,
            'id_courses_categories_second' => 2,
            'template' => 'Всего репетиторов по {RECOMMEND_NAME} {IN_THE_DIRECTION} - {TOTAL_COUNT}',
            'page' => 0
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
