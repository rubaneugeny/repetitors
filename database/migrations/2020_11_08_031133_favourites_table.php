<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FavouritesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('favorites');
        Schema::dropIfExists('favourites');

        Schema::create('favourites', function (Blueprint $table) {
            $table->string('id_favourites', 36)->nullable(false)->index('id_favourites');
            $table->integer('id_users', false, true)->nullable(false)->index('id_users');
            $table->timestamps();
        });

        Schema::table('favourites', function (Blueprint $table) {
            $table->foreign('id_users', 'favourites_users')->references('id_users')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('favourites');
    }
}
