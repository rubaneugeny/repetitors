<?php

use App\Models\Users;
use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('user')) {
            DB::statement('ALTER TABLE `user` ENGINE = InnoDB');
            DB::statement('ALTER TABLE `user` CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci');

            Schema::rename('user', 'users');
        }

        Schema::table('users', function (Blueprint $table) {
            $table->renameColumn('id_user', 'id_users');
            $table->renameColumn('seo_link', 'slug');
            $table->renameColumn('pswrd', 'password');
            $table->renameColumn('mail', 'email');
            $table->renameColumn('pol', 'sex');
            $table->renameColumn('lname', 'last_name');
            $table->renameColumn('name', 'first_name');
            $table->renameColumn('oname', 'middle_name');
            $table->renameColumn('dtime', 'landline_phone');
            $table->renameColumn('year', 'year_of_birth');
            $table->renameColumn('obrazovanie', 'education');
            $table->renameColumn('stag', 'work_experience');
            $table->renameColumn('stag_v', 'work_experience_info');
            $table->renameColumn('diplom', 'graduation_year');
            $table->renameColumn('id_vuz', 'id_universities');
            $table->renameColumn('id_lern', 'id_courses');
            $table->renameColumn('id_lerntwo', 'id_courses_second');
            $table->renameColumn('id_lerntri', 'id_courses_third');
            $table->renameColumn('id_lernfour', 'id_courses_fourth');
            $table->renameColumn('id_gorod', 'id_cities');
            $table->renameColumn('id_gorodtwo', 'id_cities_second');
            $table->renameColumn('id_gorodtri', 'id_cities_third');
            $table->renameColumn('id_gorodfour', 'id_cities_fourth');
            $table->renameColumn('id_metro2', 'id_metro_second');
            $table->renameColumn('id_raion', 'id_districts');
            $table->renameColumn('status', 'id_users_statuses');
            $table->renameColumn('aktive', 'id_users_active_statuses');
            $table->renameColumn('avto', 'is_car_instructor');
            $table->renameColumn('id_avto', 'id_cars');
            $table->renameColumn('id_avto2', 'id_cars_second');
            $table->renameColumn('doma', 'teachers_home');
            $table->renameColumn('nedoma', 'students_home');
            $table->renameColumn('proba', 'first_lesson_free');
            $table->renameColumn('familia', 'show_last_name');
            $table->renameColumn('dopinfo', 'additional_information');
            $table->renameColumn('price', 'price_per_hour');
            $table->renameColumn('post_count', 'orders_count');
            $table->renameColumn('comment_reit', 'reviews_rating');
            $table->renameColumn('comment_plus', 'reviews_positive_count');
            $table->renameColumn('comment_neitral', 'reviews_neutral_count');
            $table->renameColumn('comment_minus', 'reviews_negative_count');
            $table->renameColumn('cena', 'services_and_prices');
            $table->renameColumn('reit', 'rating');
            $table->renameColumn('reit_address', 'rating_address');
            $table->renameColumn('naspunkt', 'location');
            $table->renameColumn('ulica', 'street');
            $table->renameColumn('adres', 'house_number');
            $table->renameColumn('native', 'id_courses_native');
            $table->renameColumn('reit_familia', 'rating_last_name');
            $table->renameColumn('koef_ind', 'multiplier_personal');
            $table->renameColumn('wc_show', 'show_schedule');
            $table->renameColumn('wc_dt', 'schedule_last_update_date');

            $table->boolean('is_deleted')->nullable(false)->default(0);
            $table->dateTime('reg_date')->nullable()->after('familia')->comment('Дата регистрации');

            $table->dropColumn('comment');      // reviews_count
            $table->dropColumn('stat');         // articles_count
            $table->dropColumn('doci_count');
            $table->dropColumn('video_count');
            $table->dropColumn('to_bd');

            $table->timestamps();
        });

        Schema::table('users', function (Blueprint $table) {
            $table->renameColumn('komlern', 'comment');
        });

        $users = Users::all();

        foreach ($users ?? [] as $user) {
            $user->work_experience_info = html_entity_decode($user->work_experience_info);
            $user->comment = html_entity_decode($user->comment);
            $user->additional_information = html_entity_decode($user->additional_information);
            $user->education = html_entity_decode($user->education);
            $user->services_and_prices = html_entity_decode($user->services_and_prices);
            $user->about = html_entity_decode($user->about);

            if ($user->year_of_birth == '0') {
                $user->year_of_birth = null;
            }

            if ($user->id_courses == '0') {
                $user->id_courses = null;
            }

            if ($user->id_courses_second == '0') {
                $user->id_courses_second = null;
            }

            if ($user->id_courses_third == '0') {
                $user->id_courses_third = null;
            }

            if ($user->id_courses_fourth == '0') {
                $user->id_courses_fourth = null;
            }

            $reg_date = Carbon::createFromTimestamp($user->date_registration)->format('Y-m-d H:i:s');

            if ($user->id_users <= 3100) {
                $user->reg_date = '2009-01-01 00:00:00';
            } elseif ($user->id_users <= 24362) {
                $user->reg_date = '2010-01-01 00:00:00';
            } elseif ($user->id_users <= 29078) {
                $user->reg_date = '2012-01-01 00:00:00';
            } elseif ($user->id_users <= 36108) {
                $user->reg_date = '2013-01-01 00:00:00';
            } elseif ($user->id_users <= 42484) {
                $user->reg_date = '2014-01-01 00:00:00';
            } elseif ($user->id_users <= 46535) {
                $user->reg_date = '2015-01-01 00:00:00';
            } elseif ($user->id_users <= 50559) {
                $user->reg_date = '2016-01-01 00:00:00';
            } elseif ($user->id_users <= 54813) {
                $user->reg_date = '2017-01-01 00:00:00';
            } elseif ($user->id_users < 57371) {
                $user->reg_date = '2018-01-01 00:00:00';
            } else {
                $user->reg_date = $reg_date;
            }

            $user->photo = trim($user->photo);

            if (in_array($user->photo, ['teacher.jpg', 'teacher0.jpg'])) {
                $user->photo = null;
            }

            $user->photo = str_replace('photo/', '', $user->photo);

            if (empty($user->skype)) {
                $user->skype = 0;
            }

            if ($user->id_districts == '0') {
                $user->id_districts = null;
            }

            if ($user->id_metro == '0') {
                $user->id_metro = null;
            }

            if ($user->id_metro_second == '0') {
                $user->id_metro_second = null;
            }

            $user->first_name = trim($user->first_name);

            if (empty($user->first_name)) {
                $user->first_name = null;
            }

            $user->last_name = trim($user->last_name);

            if (empty($user->last_name)) {
                $user->last_name = null;
            }

            $user->middle_name = trim($user->middle_name);

            if (empty($user->middle_name)) {
                $user->middle_name = null;
            }

            $user->email = trim($user->email);

            if (empty($user->email)) {
                $user->email = null;
            }

            $user->slug = trim($user->slug);

            if (empty($user->slug)) {
                $user->slug = null;
            }

            if ($user->id_cities == '0') {
                $user->id_cities = null;
            }

            if ($user->id_cities_second == '0') {
                $user->id_cities_second = null;
            }

            if ($user->id_cities_third == '0') {
                $user->id_cities_third = null;
            }

            if ($user->id_cities_fourth == '0') {
                $user->id_cities_fourth = null;
            }

            if ($user->id_universities == '0') {
                $user->id_universities = null;
            }

            if ($user->id_users_active_statuses == 12) {
                $user->id_users_active_statuses = 7;
            }

            if ($user->graduation_year == '0') {
                $user->graduation_year = null;
            }

            $user->education = trim($user->education);

            if (empty($user->education)) {
                $user->education = null;
            }

            $user->additional_information = trim($user->additional_information);

            if (empty($user->additional_information)) {
                $user->additional_information = null;
            }

            if (empty($user->is_car_instructor)) {
                $user->is_car_instructor = 0;
            }

            $user->landline_phone = trim($user->landline_phone);

            if (($user->landline_phone == '0') || (empty($user->landline_phone))) {
                $user->landline_phone = null;
            }

            if (empty($user->show_last_name)) {
                $user->show_last_name = 0;
            }

            if ($user->id_cars == '0') {
                $user->id_cars = null;
            }

            if ($user->id_cars_second == '0') {
                $user->id_cars_second = null;
            }

            if ($user->id_courses_native == '0') {
                $user->id_courses_native = null;
            }

            $user->street = trim($user->street);

            if (empty($user->street)) {
                $user->street = null;
            }

            $user->house_number = trim($user->house_number);

            if (empty($user->house_number)) {
                $user->house_number = null;
            }

            $user->location = trim($user->location);

            if (empty($user->location)) {
                $user->location = null;
            }

            $user->work_experience = trim($user->work_experience);

            if ($user->work_experience == '0') {
                $user->work_experience = null;
            } elseif ($user->work_experience == '2010') {
                $user->work_experience = '10';
            }

            $user->work_experience_info = trim($user->work_experience_info);

            if ($user->work_experience_info == '0' || empty($user->work_experience_info)) {
                $user->work_experience_info = null;
            }

            $user->id_users_statuses = trim($user->id_users_statuses);

            switch ($user->id_users_statuses) {
                case 'Частный преподаватель':
                    $user->id_users_statuses = 1;
                    break;

                case 'Преподаватель ВУЗа':
                case 'Преподаватель вуза':
                    $user->id_users_statuses = 2;
                    break;

                case 'Студент':
                    $user->id_users_statuses = 3;
                    break;

                case 'Аспирант':
                    $user->id_users_statuses = 4;
                    break;

                case 'Школьный преподаватель':
                    $user->id_users_statuses = 5;
                    break;

                case '':
                    $user->id_users_statuses = null;
                    break;
            }

            $user->services_and_prices = trim($user->services_and_prices);

            if (empty($user->services_and_prices)) {
                $user->services_and_prices = null;
            }

            $user->phone = trim($user->phone);

            if (in_array($user->phone, ['+7-', '+7-0', '+7-000-000-00-00', '+7-1', '+7-111-111-11-11', ''])) {
                $user->phone = null;
            }

            $user->teachers_home = trim($user->teachers_home);

            if (empty($user->teachers_home)) {
                $user->teachers_home = 0;
            } else {
                switch ($user->teachers_home) {
                    case 'в гимназии':
                    case 'Занятия только по скайпу!':
                    case 'Нет':
                        $user->teachers_home = 0;
                        break;

                    case 'Да-2 минуты пешком от ст. метро Комендантский пр.':
                    case 'Да':
                        $user->teachers_home = 1;
                        break;
                }
            }

            $user->students_home = trim($user->students_home);

            if (empty($user->students_home)) {
                $user->students_home = 0;
            } else {
                switch ($user->students_home) {
                    case 'только Тосно':
                    case 'только Котово Поле':
                    case 'Да':
                        $user->students_home = 1;
                        break;

                    case 'Занятия только по скайпу!':
                    case 'Нет':
                        $user->students_home = 0;
                        break;
                }
            }

            $user->first_lesson_free = trim($user->first_lesson_free);

            if (empty($user->first_lesson_free)) {
                $user->first_lesson_free = 0;
            } else {
                $user->first_lesson_free = $user->first_lesson_free[0];
            }

            $user->metro_list = trim($user->metro_list);

            if (empty($user->metro_list)) {
                $user->metro_list = null;
            }

            $user->save();
        }

        if (Schema::hasColumn('users', 'promo2020')) {
            Schema::table('users', function (Blueprint $table) {
                $table->dropColumn('promo2020');
            });
        }

        if (Schema::hasColumn('users', 'promo2020_wait_email')) {
            Schema::table('users', function (Blueprint $table) {
                $table->dropColumn('promo2020_wait_email');
            });
        }

        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('date_registration');

            $table->dropIndex('id_metro2');
            $table->index('id_metro_second', 'id_metro_second');

            $table->dropIndex('id_lern');
            $table->index('id_courses', 'id_courses');

            $table->dropIndex('id_lerntwo');
            $table->index('id_courses_second', 'id_courses_second');

            $table->dropIndex('id_lerntri');
            $table->index('id_courses_third', 'id_courses_third');

            $table->dropIndex('id_lernfour');
            $table->index('id_courses_fourth', 'id_courses_fourth');

            $table->dropIndex('id_raion');
            $table->index('id_districts', 'id_districts');

            $table->dropIndex('id_gorod');
            $table->index('id_cities', 'id_cities');

            $table->dropIndex('id_gorodtwo');
            $table->index('id_cities_second', 'id_cities_second');

            $table->dropIndex('id_gorodtri');
            $table->index('id_cities_third', 'id_cities_third');

            $table->dropIndex('id_gorodfour');
            $table->index('id_cities_fourth', 'id_cities_fourth');

            $table->dropIndex('id_vuz');
            $table->index('id_universities', 'id_universities');

            $table->dropIndex('native');
            $table->index('id_courses_native', 'id_courses_native');

            $table->dropIndex('hide_anketa_s');
        });

        Users::whereIn('id_users', [39272, 36411, 34881, 41079, 36159, 42137, 42138, 42139, 42140, 42141, 42142, 42454, 34860, 34752, 39528, 39203, 39204, 43432, 43445, 35791])->delete();
        Users::where('first_name',  'last_name')->where('last_name', 'middle_name')->delete();

        DB::statement("ALTER TABLE `users` MODIFY COLUMN `id_users` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `slug` VARCHAR(255) NULL COLLATE 'utf8mb4_unicode_ci' AFTER `id_users`");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `login` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_unicode_ci' COMMENT 'Логин' AFTER `slug`");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `password` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_unicode_ci' COMMENT 'Пароль' AFTER `login`");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `email` VARCHAR(255) NULL COLLATE 'utf8mb4_unicode_ci' COMMENT 'E-mail' AFTER `password`");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `sex` TINYINT(1) UNSIGNED NOT NULL COLLATE 'utf8mb4_unicode_ci' COMMENT 'Пол: 0 - мужчина, 1 - женщина' DEFAULT 0 AFTER `email`");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `last_name` VARCHAR(255) NULL COLLATE 'utf8mb4_unicode_ci' COMMENT 'Фамилия' AFTER `sex`");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `first_name` VARCHAR(255) NULL COLLATE 'utf8mb4_unicode_ci' COMMENT 'Имя' AFTER `last_name`");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `middle_name` VARCHAR(255) NULL COLLATE 'utf8mb4_unicode_ci' COMMENT 'Отчество' AFTER `first_name`");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `phone` VARCHAR(20) NULL COLLATE 'utf8mb4_unicode_ci' COMMENT 'Телефон' AFTER `middle_name`");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `landline_phone` VARCHAR(255) NULL COLLATE 'utf8mb4_unicode_ci' COMMENT 'Городской телефон' AFTER `phone`");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `year_of_birth` SMALLINT(5) UNSIGNED NULL COMMENT 'Год рождения' AFTER `landline_phone`");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `education` TEXT NULL COLLATE 'utf8mb4_unicode_ci' COMMENT 'Образование' AFTER `year_of_birth`");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `work_experience` SMALLINT(5) UNSIGNED NULL COMMENT 'Стаж преподавателя / Стаж обучения вождению' AFTER `education`");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `work_experience_info` TEXT NULL COLLATE 'utf8mb4_unicode_ci' COMMENT 'Стаж вождения' AFTER `work_experience`");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `graduation_year` SMALLINT(5) UNSIGNED NULL COMMENT 'Год окончания учебного заведения' AFTER `work_experience_info`");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `id_universities` INT(10) UNSIGNED NULL COMMENT 'Университет' AFTER `graduation_year`");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `id_courses` INT(10) UNSIGNED NULL COMMENT 'Основной предмет' AFTER `graduation_year`");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `id_courses_second` INT(10) UNSIGNED NULL COMMENT 'Дополнительный предмет' AFTER `id_courses`");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `id_courses_third` INT(10) UNSIGNED NULL COMMENT 'Дополнительный предмет' AFTER `id_courses_second`");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `id_courses_fourth` INT(10) UNSIGNED NULL COMMENT 'Дополнительный предмет' AFTER `id_courses_third`");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `id_cities` INT(10) UNSIGNED NULL COMMENT 'Основной город' AFTER `id_courses_fourth`");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `id_cities_second` INT(10) UNSIGNED NULL COMMENT 'Дополнительный город' AFTER `id_cities`");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `id_cities_third` INT(10) UNSIGNED NULL COMMENT 'Дополнительный город' AFTER `id_cities_second`");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `id_cities_fourth` INT(10) UNSIGNED NULL COMMENT 'Дополнительный город' AFTER `id_cities_third`");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `metro_list` VARCHAR(255) NULL COLLATE 'utf8mb4_unicode_ci' COMMENT 'Список станций метро куда выезжает преподаватель' AFTER `id_cities_fourth`");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `id_metro` INT(10) UNSIGNED NULL COMMENT 'Основная станция метро' AFTER `metro_list`");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `id_metro_second` INT(10) UNSIGNED NULL COMMENT 'Дополнительная станция метро' AFTER `id_metro`");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `id_districts` INT(10) UNSIGNED NULL COMMENT 'Район' AFTER `id_metro_second`");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `id_users_statuses` INT(10) UNSIGNED NULL COMMENT 'Статус преподавателя' AFTER `id_districts`");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `id_users_active_statuses` INT(10) UNSIGNED NOT NULL COMMENT 'Статус преподавателя для администратора' AFTER `id_users_statuses`");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `is_car_instructor` TINYINT(1) UNSIGNED NOT NULL COMMENT 'Флаг, является ли преподаватель автоинструктором' DEFAULT 0 AFTER `id_users_active_statuses`");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `id_cars` INT(10) UNSIGNED NULL COMMENT 'Первый автомобиль' AFTER `is_car_instructor`");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `id_cars_second` INT(10) UNSIGNED NULL COMMENT 'Второй автомобиль' AFTER `id_cars`");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `teachers_home` TINYINT(1) UNSIGNED NOT NULL COMMENT 'Принимает ли преподаватель дома' DEFAULT 0 AFTER `id_cars_second`");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `students_home` TINYINT(1) UNSIGNED NOT NULL COMMENT 'Выезжает ли преподаватель к ученику' DEFAULT 0 AFTER `teachers_home`");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `first_lesson_free` SMALLINT(5) UNSIGNED NOT NULL COMMENT 'Первое занятие бесплатно?' DEFAULT 0 AFTER `students_home`");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `show_last_name` TINYINT(1) UNSIGNED NOT NULL COMMENT 'Показать/Скрыть фамилию преподавателя' DEFAULT 1 AFTER `first_lesson_free`");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `skype` TINYINT(1) UNSIGNED NOT NULL COMMENT 'Флаг, обучение происходит по skype' DEFAULT 0 AFTER `show_last_name`");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `reg_date` DATETIME NULL COMMENT 'Дата регистрации' AFTER `skype`");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `comment` TEXT NULL COLLATE 'utf8mb4_unicode_ci' COMMENT 'Комментарий' AFTER `reg_date`");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `additional_information` TEXT NULL COLLATE 'utf8mb4_unicode_ci' COMMENT 'Дополнительная информация' AFTER `comment`");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `price_per_hour` DECIMAL(10,0) UNSIGNED NOT NULL COMMENT 'Стоимость занятия в час' DEFAULT 0 AFTER `additional_information`");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `orders_count` SMALLINT(5) UNSIGNED NOT NULL COMMENT 'Количество заявок' DEFAULT 0 AFTER `price_per_hour`");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `reviews_rating` FLOAT UNSIGNED NOT NULL COMMENT 'Рейтинг преподавателя по отзывам' DEFAULT 0 AFTER `orders_count`");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `reviews_positive_count` SMALLINT(5) UNSIGNED NOT NULL COMMENT 'Кол-во положительных отзывов' DEFAULT 0 AFTER `reviews_rating`");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `reviews_neutral_count` SMALLINT(5) UNSIGNED NOT NULL COMMENT 'Кол-во нейтральных отзывов' DEFAULT 0 AFTER `reviews_positive_count`");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `reviews_negative_count` SMALLINT(5) UNSIGNED NOT NULL COMMENT 'Кол-во негативных отзывов' DEFAULT 0 AFTER `reviews_neutral_count`");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `services_and_prices` TEXT NULL COLLATE 'utf8mb4_unicode_ci' COMMENT 'Услуги и цены, отображается в карточке преподавателя' AFTER `price_per_hour`");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `photo` VARCHAR(255) NULL COLLATE 'utf8mb4_unicode_ci' COMMENT 'Основная фотография преподавателя'");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `rating` SMALLINT(5) NOT NULL COMMENT 'Рейтинг преподавателя, идет сортировка анкет по нему' DEFAULT 0");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `rating_address` TINYINT(1) UNSIGNED NOT NULL COMMENT 'Флаг, что добавили рейтинг за адрес' DEFAULT 0 AFTER `rating`");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `location` TEXT NULL COLLATE 'utf8mb4_unicode_ci' COMMENT 'Населенный пункт, показывается когда rating_address' AFTER `rating_address`");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `street` VARCHAR(255) NULL COLLATE 'utf8mb4_unicode_ci' COMMENT 'Улица, показывается когда rating_address' AFTER `location`");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `house_number` VARCHAR(255) NULL COLLATE 'utf8mb4_unicode_ci' COMMENT 'Номер дома, показывается когда rating_address' AFTER `street`");
        DB::statement("ALTER TABLE `users` MODIFY COLUMN `is_deleted` TINYINT(1) UNSIGNED NOT NULL COMMENT 'Удален ли пользователь?' DEFAULT 0");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
