<?php

use App\Garbage\LernGeo;
use App\Models\Courses;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCitiesToCourse extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('!_lerngeo')) {
            Schema::rename('!_lerngeo', 'lerngeo');
        }

        DB::statement("ALTER TABLE `courses` ADD COLUMN `cities` VARCHAR(255) NULL COLLATE 'utf8mb4_unicode_ci' COMMENT 'Используется для страницы other_predmet_gorod.php для выборки предметов по конкретному городу' AFTER `koef_ind_sr`");

        $geo = LernGeo::all();
        $list = range(2, 47);

        foreach ($geo as $item) {
            $cities = [];

            foreach ($list as $key) {
                if ($item['g'.$key]) {
                    array_push($cities, $key);
                }
            }

            Courses::where(['id_courses' => $item->id])->update([
                'cities' => count($cities) > 0 ? implode(',', $cities) : null,
            ]);
        }

        foreach ($list as $key) {
            Schema::table('lerngeo', function (Blueprint $table) use ($key) {
                $table->dropColumn('g'.$key);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
