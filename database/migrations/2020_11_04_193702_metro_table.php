<?php

use App\Models\Metro;
use App\Models\MetroBranches;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MetroTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('metro_branches', function (Blueprint $table) {
            $table->integer('id_metro_branches', true, true)->nullable(false)->default(null);
            $table->string('name');
        });

        if (($f = fopen(__DIR__.'/../dumps/metro_branches.csv', 'r')) !== false) {
            while (($data = fgetcsv($f, 0, "\t")) !== false) {
                $item = new MetroBranches();
                $item->id_metro_branches = $data[0];
                $item->name = $data[1];
                $item->save();
            }
        }

        DB::statement('ALTER TABLE metro ENGINE = InnoDB');
        DB::statement('ALTER TABLE metro CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci');

        Schema::table('metro', function (Blueprint $table) {
            $table->dropColumn('0');
            $table->renameColumn('in', 'in_the_direction');
            $table->renameColumn('url', 'slug');
            $table->renameColumn('vetka', 'id_metro_branches');
            $table->renameColumn('reit', 'rating');
            $table->string('nearest_stations')->collation('utf8mb4_unicode_ci')->nullable(false)->comment('Ближайшие станции')->after('20');
            $table->timestamps();
        });

        $metro = Metro::all();

        foreach ($metro ?? [] as $item) {
            $item->name = trim($item->name);
            $item->in_the_direction = trim($item->in_the_direction);
            $item->slug = trim($item->slug);

            switch ($item->id_metro_branches) {
                case 4:
                case 6:
                case 8:
                    $item->id_metro_branches = 2;
                    break;

                case 3:
                case 5:
                case 7:
                case 9:
                    $item->id_metro_branches = 3;
                    break;

                case 11:
                    $item->id_metro_branches = 5;
                    break;
            }

            $item->save();
        }

        DB::statement('UPDATE `metro` SET `7` = null, `8` = null, `9` = null WHERE `id_metro` = 2');
        DB::statement('UPDATE `metro` SET `nearest_stations` = CONCAT_WS(",", `1`,`2`,`3`,`4`,`5`,`6`,`7`,`8`,`9`,`10`,`11`,`12`,`13`,`14`,`15`,`16`,`17`,`18`,`19`,`20`)');

        Schema::table('metro', function (Blueprint $table) {
            $table->dropColumn('1');
            $table->dropColumn('2');
            $table->dropColumn('3');
            $table->dropColumn('4');
            $table->dropColumn('5');
            $table->dropColumn('6');
            $table->dropColumn('7');
            $table->dropColumn('8');
            $table->dropColumn('9');
            $table->dropColumn('10');
            $table->dropColumn('11');
            $table->dropColumn('12');
            $table->dropColumn('13');
            $table->dropColumn('14');
            $table->dropColumn('15');
            $table->dropColumn('16');
            $table->dropColumn('17');
            $table->dropColumn('18');
            $table->dropColumn('19');
            $table->dropColumn('20');
            $table->dropIndex('in');
            $table->index('id_metro_branches', 'id_metro_branches');
        });

        DB::statement("ALTER TABLE `metro` MODIFY COLUMN `id_metro` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT");
        DB::statement("ALTER TABLE `metro` MODIFY COLUMN `id_metro_branches` INT(10) UNSIGNED NOT NULL AFTER `id_metro`");
        DB::statement("ALTER TABLE `metro` MODIFY COLUMN `slug` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_unicode_ci' AFTER `id_metro_branches`");
        DB::statement("ALTER TABLE `metro` MODIFY COLUMN `name` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_unicode_ci' AFTER `slug`");
        DB::statement("ALTER TABLE `metro` MODIFY COLUMN `in_the_direction` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_unicode_ci' AFTER `name`");
        DB::statement("ALTER TABLE `metro` MODIFY COLUMN `ao` SMALLINT(5) UNSIGNED NOT NULL COMMENT '???' AFTER `in_the_direction`");
        DB::statement("ALTER TABLE `metro` MODIFY COLUMN `rating` SMALLINT(5) UNSIGNED NOT NULL COMMENT 'Используется в рекомендованных ссылках'AFTER `ao`");

        Schema::table('metro', function (Blueprint $table) {
            $table->foreign('id_metro_branches', 'id_metro_branches')->references('id_metro_branches')->on('metro_branches')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
