<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CoursesRelations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('courses', function (Blueprint $table) {
            $table->foreign('id_title_categories', 'courses_title_categories')->references('id_title_categories')->on('title_categories')->onDelete('restrict')->onUpdate('cascade');
            $table->foreign('id_total_categories', 'courses_total_categories')->references('id_total_categories')->on('total_categories')->onDelete('restrict')->onUpdate('cascade');
            $table->foreign('id_prepend_text_categories', 'courses_prepend_text_categories')->references('id_prepend_text_categories')->on('prepend_text_categories')->onDelete('restrict')->onUpdate('cascade');
            $table->foreign('id_photo_text_categories', 'courses_photo_text_categories')->references('id_photo_text_categories')->on('photo_text_categories')->onDelete('restrict')->onUpdate('cascade');
            $table->foreign('id_comments_prepend_text_categories', 'courses_comments_prepend_text_categories')->references('id_comments_prepend_text_categories')->on('comments_prepend_text_categories')->onDelete('restrict')->onUpdate('cascade');
            $table->foreign('id_pages_append_text_categories', 'courses_pages_append_text_categories')->references('id_pages_append_text_categories')->on('pages_append_text_categories')->onDelete('restrict')->onUpdate('cascade');
            $table->foreign('id_parent_course', 'courses_parent_course')->references('id_courses')->on('courses')->onDelete('set null')->onUpdate('cascade');
            $table->foreign('id_courses_second', 'courses_second_courses')->references('id_courses')->on('courses')->onDelete('set null')->onUpdate('cascade');
            $table->foreign('id_courses_categories', 'courses_courses_categories')->references('id_courses_categories')->on('courses_categories')->onDelete('restrict')->onUpdate('cascade');
            $table->foreign('id_courses_categories_second', 'courses_courses_categories_second')->references('id_courses_categories_second')->on('courses_categories_second')->onDelete('restrict')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
