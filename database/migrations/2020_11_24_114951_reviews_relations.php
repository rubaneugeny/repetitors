<?php

use App\Models\Reviews;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ReviewsRelations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::drop('otzyv_add');
        Schema::rename('otzyv_del', '!!otzyv_del');
        Schema::rename('otzyv_neg', '!!otzyv_neg');
        Schema::rename('otzyv_neg_com', '!!otzyv_neg_com');
        Schema::rename('otzyv_neg_del', '!!otzyv_neg_del');
        Schema::rename('otzyv_perenos', '!!otzyv_perenos');

        $reviews = Reviews::all();

        foreach ($reviews ?? [] as $review) {
            if ($review->id_users == 0) {
                $review->delete();
            }
        }

        Schema::table('reviews', function (Blueprint $table) {
            $table->foreign('id_reviews_statuses', 'reviews_reviews_statuses')->references('id_reviews_statuses')->on('reviews_statuses')->onDelete('restrict')->onUpdate('cascade');
            $table->foreign('id_orders', 'reviews_orders')->references('id_orders')->on('orders')->onDelete('set null')->onUpdate('cascade');
            $table->foreign('id_users', 'reviews_users')->references('id_users')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('id_admins_reviews', 'reviews_admins_reviews')->references('id_admins_reviews')->on('admins_reviews')->onDelete('set null')->onUpdate('cascade');
        });

        DB::statement("ALTER TABLE `reviews` MODIFY COLUMN `new` INT(10) NULL COMMENT '???'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
