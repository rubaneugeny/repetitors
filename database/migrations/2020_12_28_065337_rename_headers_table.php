<?php

use App\Models\HeadersCategories;
use App\Models\HeadersTemplates;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameHeadersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('breadcrumbs_templates', function (Blueprint $table) {
            $table->foreign('id_courses', 'id_courses')
                ->references('id_courses')
                ->on('courses')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::rename('headers', 'headers_categories');

        Schema::table('headers_categories', function (Blueprint $table) {
            $table->renameColumn('id_headers', 'id_headers_categories');
            $table->renameColumn('slug', 'name');
            $table->dropColumn('title');
        });

        Schema::table('headers_categories', function (Blueprint $table) {
            $table->unique('name', 'name');
        });

        Schema::create('headers_templates', function (Blueprint $table) {
            $table->integer('id_headers_templates', true, true)->nullable(false);
            $table->integer('id_headers_categories', false, true)->nullable(false)->index('id_headers_categories');
            $table->integer('id_courses_categories_second', false, true)->nullable()->index('id_courses_categories_second');
            $table->integer('id_courses', false, true)->nullable()->index('id_courses');
            $table->text('template')->nullable(false);
            $table->smallInteger('page', false, true)->nullable(false);
        });

        Schema::table('headers_templates', function (Blueprint $table) {
            $table->foreign('id_headers_categories', 'headers_templates_headers_categories')
                ->references('id_headers_categories')
                ->on('headers_categories')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('id_courses_categories_second', 'headers_templates_courses_categories_second')
                ->references('id_courses_categories_second')
                ->on('courses_categories_second')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('id_courses', 'headers_templates_courses')
                ->references('id_courses')
                ->on('courses')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        $items = [
            1 => 'Частные автонструкторы Санкт-Петербурга',
            2 => 'Инструкторы по вождению в Санкт-Петербурге',
            3 => 'Частные инструкторы по вождению (женщины)'
        ];

        foreach ($items as $key => $value) {
            $template = new HeadersTemplates();
            $template->id_headers_categories = $key;
            $template->template = $value;
            $template->page = 0;
            $template->save();
        }

        $category = HeadersCategories::create(['name' => 'metro']);
        HeadersTemplates::create([
            'id_headers_categories' => $category->id_headers_categories,
            'id_courses' => 1000,
            'template' => 'Инструкторы по вождению {IN_THE_DIRECTION}',
            'page' => 0
        ]);
        HeadersTemplates::create([
            'id_headers_categories' => $category->id_headers_categories,
            'id_courses' => 1001,
            'template' => 'Инструкторы по вождению {IN_THE_DIRECTION}',
            'page' => 0
        ]);
        HeadersTemplates::create([
            'id_headers_categories' => $category->id_headers_categories,
            'id_courses' => 1002,
            'template' => 'Мотоинструкторы {IN_THE_DIRECTION}',
            'page' => 0
        ]);
        HeadersTemplates::create([
            'id_headers_categories' => $category->id_headers_categories,
            'id_courses' => 23,
            'template' => 'Частные логопеды {IN_THE_DIRECTION}',
            'page' => 0
        ]);
        HeadersTemplates::create([
            'id_headers_categories' => $category->id_headers_categories,
            'id_courses' => 13,
            'template' => 'Репетиторы начальных классов на метро {NAME}',
            'page' => 0
        ]);
        HeadersTemplates::create([
            'id_headers_categories' => $category->id_headers_categories,
            'id_courses' => 63,
            'template' => 'Репетиторы для подготовки к школе на метро {NAME}',
            'page' => 0
        ]);
        HeadersTemplates::create([
            'id_headers_categories' => $category->id_headers_categories,
            'id_courses_categories_second' => 5,
            'template' => 'Частные тренеры по {RECOMMEND_NAME} {IN_THE_DIRECTION}',
            'page' => 0
        ]);
        HeadersTemplates::create([
            'id_headers_categories' => $category->id_headers_categories,
            'id_courses_categories_second' => 7,
            'template' => 'Частные тренеры по {RECOMMEND_NAME} {IN_THE_DIRECTION}',
            'page' => 0
        ]);

        foreach ([1, 4] as $value) {
            HeadersTemplates::create([
                'id_headers_categories' => $category->id_headers_categories,
                'id_courses_categories_second' => $value,
                'template' => 'Репетиторы по {RECOMMEND_NAME} на метро {NAME}',
                'page' => 1
            ]);
            HeadersTemplates::create([
                'id_headers_categories' => $category->id_headers_categories,
                'id_courses_categories_second' => $value,
                'template' => 'Преподаватели {RECOMMEND_NAME_SECOND} на метро {NAME}',
                'page' => 2
            ]);
            HeadersTemplates::create([
                'id_headers_categories' => $category->id_headers_categories,
                'id_courses_categories_second' => $value,
                'template' => 'Учителя {RECOMMEND_NAME_SECOND} на метро {NAME}',
                'page' => 3
            ]);
            HeadersTemplates::create([
                'id_headers_categories' => $category->id_headers_categories,
                'id_courses_categories_second' => $value,
                'template' => 'Репетиторы {RECOMMEND_NAME_SECOND} в районе метро {NAME}',
                'page' => 0
            ]);
        }

        HeadersTemplates::create([
            'id_headers_categories' => $category->id_headers_categories,
            'id_courses_categories_second' => 2,
            'template' => 'Репетиторы по {RECOMMEND_NAME} на метро {NAME}',
            'page' => 1
        ]);
        HeadersTemplates::create([
            'id_headers_categories' => $category->id_headers_categories,
            'id_courses_categories_second' => 2,
            'template' => 'Репетиторы {RECOMMEND_NAME_SECOND} в районе метро {NAME}',
            'page' => 0
        ]);

        HeadersTemplates::create([
            'id_headers_categories' => $category->id_headers_categories,
            'id_courses_categories_second' => 3,
            'template' => 'Репетиторы по {RECOMMEND_NAME} на метро {NAME}',
            'page' => 1
        ]);
        HeadersTemplates::create([
            'id_headers_categories' => $category->id_headers_categories,
            'id_courses_categories_second' => 3,
            'template' => 'Репетиторы по {RECOMMEND_NAME} в районе метро {NAME}',
            'page' => 0
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
