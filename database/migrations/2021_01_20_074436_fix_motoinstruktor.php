<?php

use App\Models\BreadcrumbsCategories;
use App\Models\BreadcrumbsTemplates;
use App\Models\Courses;
use App\Models\HeadersCategories;
use App\Models\HeadersTemplates;
use App\Models\PagesAppendTextCategories;
use App\Models\PagesAppendTextTemplates;
use App\Models\PrependTextCategories;
use App\Models\PrependTextTemplates;
use App\Models\TitleCategories;
use App\Models\TitleTemplates;
use App\Models\TotalCategories;
use App\Models\TotalTemplates;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FixMotoinstruktor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $category = TitleCategories::create(['name' => 'motoinstruktor']);
        TitleTemplates::create([
            'id_title_categories' => $category->id_title_categories,
            'page' => 1,
            'title' => 'Частные мотоинструкторы Санкт-Петербурга. &#127949;&#65039; Обучение вождению мотоцикла. Уроки, курсы вождения мотоцикла в СПб',
            'description' => 'Частные мотоинструкторы Санкт-Петербурга и ЛО. &#11088;Отзывы, цены, бесплатный и быстрый поиск на сайте!&#11088; Мотоинструкторы в разных районах СПб.'
        ]);
        TitleTemplates::create([
            'id_title_categories' => $category->id_title_categories,
            'page' => 0,
            'title' => 'Мотоинструкторы (анкеты с {FROM}-{TO}) - "СПбРепетитор"'
        ]);
        $course = Courses::find(Courses::MOTORBIKES);
        $course->id_title_categories = $category->id_title_categories;
        $course->save();

        $category = BreadcrumbsCategories::create(['name' => 'motoinstruktor']);
        BreadcrumbsTemplates::create([
            'id_breadcrumbs_categories' => $category->id_breadcrumbs_categories,
//            'id_courses' => 1002,
            'type' => 2,
            'last' => 'Мотоинструкторы',
            'page' => 0
        ]);

        $category = HeadersCategories::create(['name' => 'motoinstruktor']);
        HeadersTemplates::create([
            'id_headers_categories' => $category->id_headers_categories,
//            'id_courses' => 1002,
            'template' => 'Мотоинструкторы',
            'page' => 0
        ]);

        $category = TotalCategories::create(['name' => 'motoinstruktor']);
        TotalTemplates::create([
            'id_total_categories' => $category->id_total_categories,
//            'id_courses' => 1002,
            'template' => 'Всего частных мотоинструкторов из Санкт-Петербурга в базе: <span>{TOTAL_COUNT}</span>',
            'page' => 0
        ]);

        $category = PrependTextCategories::create(['name' => 'motoinstruktor']);
        PrependTextTemplates::create([
            'id_prepend_text_categories' => $category->id_prepend_text_categories,
//            'id_courses' => 1002,
            'wrap' => 1,
            'template' => 'Если у Вас нет времени искать самостоятельно мотоинструктора для обучения вождению, просматривая все анкеты, то Вы можете <a href="avto_postform.php">написать</a>, какой именно частный инструктор по вождению мотоцикла Вам нужен, и администратор <strong>бесплатно</strong> подберет Вам подходящие варианты.',
            'page' => 0
        ]);

        $category = PagesAppendTextCategories::create(['name' => 'motoinstruktor']);
        PagesAppendTextTemplates::create([
            'id_pages_append_text_categories' => $category->id_pages_append_text_categories,
            'template' => '<b>Где найти хорошего мотоинструктора?</b>
<br /><br />На нашем сайте Вы найдете анкеты преподавателей, приглашающих на курсы вождения мотоцикла в Санкт-Петербурге, с подробной информацией о них, и сможете выбрать наиболее подходящего Вам специалиста.
<br /><br /><b>Ищу мотоинструктора в СПб, посоветуйте кого-нибудь.</b>
<br /><br />Вы можете посмотреть отзывы о преподавателях и выбрать подходящего инструктора. Также стоит обратить внимание на опыт работы, стоимость занятий, тип мотоцикла, а также на то в каком районе Санкт-Петербурга этот преподаватель проводит свои курсы по вождению  мотоцикла.
<br /><br /><br />Если Вам требуется мотоинструктор в Санкт-Петербурге, то оставляйте заявку в разделе "Найти репетитора".<br />',
            'page' => 1,
            'split' => 0,
            'seo' => 0,
            'text_limit' => 500,
            'repetitors_count_limit' => 0,
            'class' => 's_text_info_bottom'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
