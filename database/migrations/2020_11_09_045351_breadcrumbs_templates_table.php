<?php

use App\Models\BreadcrumbsTemplates;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BreadcrumbsTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('breadcrumbs_templates');

        Schema::create('breadcrumbs_templates', function (Blueprint $table) {
            $table->integer('id_breadcrumbs_templates', true, true)->nullable(false);
            $table->integer('id_breadcrumbs_categories', false, true)->nullable(false)->index('id_breadcrumbs_categories');
            $table->integer('id_courses', false, true)->nullable(true)->index('id_courses');
            $table->smallInteger('type')->comment('Тип: 1 - репетитор, 2 - автоинструктор')->nullable(false);
            $table->string('last')->nullable(false);
            $table->longText('items')->nullable(false);
            $table->smallInteger('page')->nullable(false);

            $table->foreign('id_breadcrumbs_categories', 'breadcrumbs_categories_breadcrumbs_templates')
                ->references('id_breadcrumbs_categories')
                ->on('breadcrumbs_categories')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        if (($f = fopen(__DIR__.'/../dumps/breadcrumbs_templates.csv', 'r')) !== false) {
            while (($data = fgetcsv($f, 0, "\t")) !== false) {
                $breadcrumbs = new BreadcrumbsTemplates();
                $breadcrumbs->id_breadcrumbs_templates = $data[0];
                $breadcrumbs->id_breadcrumbs_categories = $data[1];
                $breadcrumbs->type = $data[2];
                $breadcrumbs->last = $data[3];
                $breadcrumbs->items = $data[4];
                $breadcrumbs->page = $data[5];
                $breadcrumbs->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('breadcrumbs_templates');
    }
}
