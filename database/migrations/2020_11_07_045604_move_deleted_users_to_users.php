<?php

use App\Models\Users;
use App\Garbage\UsersDeleted;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MoveDeletedUsersToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $users = UsersDeleted::all();

        foreach ($users ?? [] as $deleted) {
            if (!Users::find($deleted->id_users_deleted)) {
                $user = new Users();
                $user->is_deleted = 1;
                $user->id_users = $deleted->id_users_deleted;
                $user->slug = $deleted->slug;
                $user->login = $deleted->login;
                $user->password = $deleted->password;
                $user->email = $deleted->email;
                $user->sex = $deleted->sex;
                $user->last_name = $deleted->last_name;
                $user->first_name = $deleted->first_name;
                $user->middle_name = $deleted->middle_name;
                $user->phone = $deleted->phone;
                $user->landline_phone = $deleted->landline_phone;
                $user->year_of_birth = $deleted->year_of_birth;
                $user->education = $deleted->education;
                $user->work_experience = $deleted->work_experience;
                $user->work_experience_info = $deleted->work_experience_info;
                $user->graduation_year = $deleted->graduation_year;
                $user->id_courses = $deleted->id_courses;
                $user->id_courses_second = $deleted->id_courses_second;
                $user->id_courses_third = $deleted->id_courses_third;
                $user->id_courses_fourth = $deleted->id_courses_fourth;
                $user->id_cities = $deleted->id_cities;
                $user->id_cities_second = $deleted->id_cities_second;
                $user->id_cities_third = $deleted->id_cities_third;
                $user->id_cities_fourth = $deleted->id_cities_fourth;
                $user->metro_list = $deleted->metro_list;
                $user->id_metro = $deleted->id_metro;
                $user->id_metro_second = $deleted->id_metro_second;
                $user->id_districts = $deleted->id_districts;
                $user->id_users_statuses = $deleted->id_users_statuses;
                $user->id_users_active_statuses = $deleted->id_users_active_statuses;
                $user->is_car_instructor = $deleted->is_car_instructor;
                $user->id_cars = $deleted->id_cars;
                $user->id_cars_second = $deleted->id_cars_second;
                $user->teachers_home = $deleted->teachers_home;
                $user->students_home = $deleted->students_home;
                $user->first_lesson_free = $deleted->first_lesson_free;
                $user->show_last_name = $deleted->show_last_name;
                $user->skype = $deleted->skype;
                $user->reg_date = $deleted->reg_date;
                $user->comment = $deleted->comment;
                $user->additional_information = $deleted->additional_information;
                $user->price_per_hour = $deleted->price_per_hour;
                $user->services_and_prices = $deleted->services_and_prices;
                $user->orders_count = $deleted->orders_count;
                $user->reviews_rating = $deleted->reviews_rating;
                $user->reviews_positive_count = $deleted->reviews_positive_count;
                $user->reviews_neutral_count = $deleted->reviews_neutral_count;
                $user->reviews_negative_count = $deleted->reviews_negative_count;
                $user->id_universities = $deleted->id_universities;
                $user->komdoma = $deleted->komdoma;
                $user->photo = $deleted->photo;
                $user->rating = $deleted->rating;
                $user->rating_address = $deleted->rating_address;
                $user->location = $deleted->location;
                $user->street = $deleted->street;
                $user->house_number = $deleted->house_number;
                $user->about = $deleted->about;
                $user->last_visit = $deleted->last_visit;
                $user->last_visit_time = $deleted->last_visit_time;
                $user->balans = $deleted->balans;
                $user->gorod = $deleted->gorod;
                $user->id_courses_native = $deleted->id_courses_native;
                $user->oferta = $deleted->oferta;
                $user->avto1 = $deleted->avto1;
                $user->nami1 = $deleted->nami1;
                $user->photo1 = $deleted->photo1;
                $user->avto2 = $deleted->avto2;
                $user->nami2 = $deleted->nami2;
                $user->photo2 = $deleted->photo2;
                $user->new_comment = $deleted->new_comment;
                $user->free = $deleted->free;
                $user->video = $deleted->video;
                $user->videootzyv = $deleted->videootzyv;
                $user->linka = $deleted->linka;
                $user->doci = $deleted->doci;
                $user->live = $deleted->live;
                $user->tip = $deleted->tip;
                $user->svoe = $deleted->svoe;
                $user->dubl = $deleted->dubl;
                $user->edit_fltr = $deleted->edit_fltr;
                $user->edit_fltr_c = $deleted->edit_fltr_c;
                $user->rating_last_name = $deleted->rating_last_name;
                $user->edit_anketa = $deleted->edit_anketa;
                $user->edit_anketa_c = $deleted->edit_anketa_c;
                $user->hide_anketa_c = $deleted->hide_anketa_c;
                $user->hide_anketa_s = $deleted->hide_anketa_s;
                $user->hide_anketa_m = $deleted->hide_anketa_m;
                $user->login_skype = $deleted->login_skype;
                $user->show_schedule = $deleted->show_schedule;
                $user->schedule_last_update_date = $deleted->schedule_last_update_date;
                $user->multiplier_personal = $deleted->multiplier_personal;
                $user->photo3 = $deleted->photo3;
                $user->created_at = $deleted->created_at;
                $user->updated_at = $deleted->updated_at;
                $user->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
