<?php

use App\Models\Cars;
use App\Models\CarsManufactures;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('avto')) {
            DB::statement('ALTER TABLE `avto` ENGINE = InnoDB');
            DB::statement('ALTER TABLE `avto` CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci');

            Schema::rename('avto', 'cars');
        }

        Schema::create('cars_manufactures', function (Blueprint $table) {
            $table->integer('id_cars_manufactures', true, true);
            $table->string('name', 50)->nullable(false);
            $table->string('name_eng', 50)->nullable(false);
            $table->timestamps();
        });

        Schema::table('cars', function (Blueprint $table) {
            $table->renameColumn('id_avto', 'id_cars');
            $table->integer('id_cars_manufactures', false, true)->index('id_cars_manufactures')->nullable(false)->after('id_avto');
            $table->renameColumn('url', 'slug');
            $table->renameColumn('model', 'name');
            $table->renameColumn('modeleng', 'name_eng');
            $table->timestamps();
        });

        $cars = Cars::all();

        foreach ($cars ?? [] as $car) {
            $car->marka = strtr($car->marka, ['Ваз' => 'Лада', 'Митсубиси' => 'Мицубиси']);
            $car->markaeng = strtr($car->markaeng, ['Ваз' => 'Lada']);

            $exists = CarsManufactures::where(['name' => $car->marka])->first();

            if (!$exists) {
                $manufacturer = new CarsManufactures();
                $manufacturer->name = trim($car->marka);
                $manufacturer->name_eng = trim($car->markaeng);
                $manufacturer->save();

                $car->id_cars_manufactures = $manufacturer->id_cars_manufactures;
            } else {
                $car->id_cars_manufactures = $exists->id_cars_manufactures;
            }

            $car->save();
        }

        Schema::table('cars', function (Blueprint $table) {
            $table->dropColumn('marka');
            $table->dropColumn('markaeng');
        });

        DB::statement("ALTER TABLE `cars` MODIFY COLUMN `id_cars` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT");
        DB::statement("ALTER TABLE `cars` MODIFY COLUMN `id_cars_manufactures` INT(10) UNSIGNED NOT NULL AFTER `id_cars`");
        DB::statement("ALTER TABLE `cars` MODIFY COLUMN `slug` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_unicode_ci' AFTER `id_cars_manufactures`");
        DB::statement("ALTER TABLE `cars` MODIFY COLUMN `name` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_unicode_ci' AFTER `slug`");
        DB::statement("ALTER TABLE `cars` MODIFY COLUMN `name_eng` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_unicode_ci' AFTER `name`");

        Schema::table('cars', function (Blueprint $table) {
            $table->foreign('id_cars_manufactures', 'id_cars_manufactures')->references('id_cars_manufactures')->on('cars_manufactures')->onDelete('restrict')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cars');
        Schema::dropIfExists('cars_manufactures');
    }
}
