<?php

use App\Models\PhotoTextTemplates;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PhotoTextTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('photo_text_templates');

        Schema::create('photo_text_templates', function (Blueprint $table) {
            $table->integer('id_photo_text_templates', true, true)->nullable(false);
            $table->integer('id_photo_text_categories', false, true)->nullable(false)->index('id_photo_text_categories');
            $table->integer('id_courses_categories_second', false, true)->nullable()->index('id_courses_categories_second');
            $table->integer('id_courses', false, true)->nullable()->index('id_courses');
            $table->text('template')->nullable(false);
            $table->smallInteger('page', false, true)->nullable(false);
            $table->smallInteger('row_index', false, true)->nullable(false)->comment('Номер строки при выборке');

            $table->foreign('id_courses', 'photo_text_templates_courses')
                ->references('id_courses')
                ->on('courses')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('id_courses_categories_second', 'photo_text_templates_courses_categories_second')
                ->references('id_courses_categories_second')
                ->on('courses_categories_second')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('id_photo_text_categories', 'photo_text_templates_photo_text_categories')
                ->references('id_photo_text_categories')
                ->on('photo_text_categories')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        if (($f = fopen(__DIR__.'/../dumps/photo_text_templates.csv', 'r')) !== false) {
            while (($data = fgetcsv($f, 0, "\t")) !== false) {
                $item = new PhotoTextTemplates();
                $item->id_photo_text_templates = $data[0];
                $item->id_photo_text_categories = $data[1];
                $item->id_courses_categories_second = $data[2] != 'NULL' ? $data[2] : null;
                $item->id_courses = $data[3] != 'NULL' ? $data[3] : null;
                $item->template = $data[4];
                $item->page = $data[5];
                $item->row_index = $data[6];
                $item->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('photo_text_templates');
    }
}
