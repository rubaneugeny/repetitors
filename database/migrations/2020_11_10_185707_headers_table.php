<?php

use App\Garbage\Headers;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class HeadersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('headers');

        Schema::create('headers', function (Blueprint $table) {
            $table->integer('id_headers', true, true)->nullable(false);
            $table->string('slug')->nullable(false);
            $table->string('title')->nullable(false);
        });

        if (($f = fopen(__DIR__.'/../dumps/headers.csv', 'r')) !== false) {
            while (($data = fgetcsv($f, 0, "\t")) !== false) {
                $item = new Headers();
                $item->id_headers = $data[0];
                $item->slug = $data[1];
                $item->title = $data[2];
                $item->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('headers');
    }
}
