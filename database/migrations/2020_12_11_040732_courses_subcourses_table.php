<?php

use App\Models\Courses;
use App\Models\CoursesSubcourses;
use App\Models\Users;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CoursesSubcoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('!_user_fltr')) {
            DB::statement('ALTER TABLE `!_user_fltr` ENGINE = InnoDB');
            DB::statement('ALTER TABLE `!_user_fltr` CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci');

            Schema::rename('!_user_fltr', 'courses_subcourses');
        }

        Schema::table('courses_subcourses', function (Blueprint $table) {
            $table->renameColumn('id_fltr', 'id_courses');
            $table->renameColumn('id_user', 'id_users');
            $table->dropIndex('primary');
            $table->dropIndex('fltr_u');
        });

        Schema::table('courses_subcourses', function (Blueprint $table) {
            $table->primary(['id_users', 'id_courses']);
            $table->index('id_users', 'id_users');
            $table->index('id_courses', 'id_courses');
        });

        $subcourses = CoursesSubcourses::all();

        foreach ($subcourses ?? [] as $course) {
            if (!Users::find($course->id_users)) {
                $course->delete();
            } elseif (!Courses::find($course->id_courses)) {
                $course->delete();
            }
        }

        DB::statement("ALTER TABLE `courses_subcourses` MODIFY COLUMN `id_courses` INT(10) UNSIGNED NOT NULL ");
        DB::statement("ALTER TABLE `courses_subcourses` MODIFY COLUMN `id_users` INT(10) UNSIGNED NOT NULL AFTER `id_courses`");

        Schema::table('courses_subcourses', function (Blueprint $table) {
            $table->foreign('id_users', 'courses_subcourses_users')->references('id_users')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('id_courses', 'courses_subcourses_courses')->references('id_courses')->on('courses')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
