<?php

use App\Models\Courses;
use App\Models\TitleCategories;
use App\Models\TitleTemplates;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FixMotorbyke extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $category = TitleCategories::create(['name' => 'title_motoinstruktop']);
        TitleTemplates::create([
            'id_title_categories' => $category->id_title_categories,
            'page' => 1,
            'title' => 'Частные мотоинструкторы Санкт-Петербурга. &#127949;&#65039; Обучение вождению мотоцикла. Уроки, курсы вождения мотоцикла в СПб',
            'description' => 'Частные мотоинструкторы Санкт-Петербурга и ЛО. &#11088;Отзывы, цены, бесплатный и быстрый поиск на сайте!&#11088; Мотоинструкторы в разных районах СПб.'
        ]);
        TitleTemplates::create([
            'id_title_categories' => $category->id_title_categories,
            'page' => 0,
            'title' => 'Мотоинструкторы (анкеты с {FROM}-{TO}) - "СПбРепетитор"',
            'description' => ''
        ]);
        $course = Courses::find(Courses::MOTORBIKES);
        $course->id_title_categories = $category->id_title_categories;
        $course->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
