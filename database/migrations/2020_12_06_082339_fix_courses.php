<?php

use App\Models\Courses;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FixCourses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Courses::whereIn('id_courses', [
            169, 177, 166, 162, 159, 176, 175, 172, 173, 296, 160,
            161, 164, 174, 183, 170, 168, 167, 163, 179, 158, 157,
            165, 36, 184, 182, 181, 178, 180
        ])->update([
            'id_title_categories' => 1,
            'id_total_categories' => 8, // miniup
            'id_prepend_text_categories' => 15, // miniup
            'id_photo_text_categories' => 1,
            'id_pages_append_text_categories' => 12
        ]);
        Courses::where(['id_courses' => 148])->update([
            'id_title_categories' => 4,
            'id_total_categories' => 1,
            'id_prepend_text_categories' => 3,
            'id_photo_text_categories' => 1,
            'id_pages_append_text_categories' => 9
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
