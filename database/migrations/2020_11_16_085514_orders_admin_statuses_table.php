<?php

use App\Models\OrdersAdminStatuses;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class OrdersAdminStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders_admin_statuses', function (Blueprint $table) {
            $table->integer('id_orders_admin_statuses', true, true)->nullable(false)->default(null);
            $table->string('name');
        });

        $statuses = [
            1 => 'Новая заявка',
            2 => 'Открытый доступ',
            3 => 'Распределенная',
            4 => 'Сбор отзывов',
            5 => 'Сбор отзыва, смс и письмо НЕ отправлены',
            6 => 'Удаленый заказ',
            10 => 'Удаленный из новых',
            13 => 'Раскраска розовым',
            26 => 'Отложенная',
            27 => 'Черный список',
            28 => 'Снятая',
            29 => 'Проверка оплаты',
            31 => 'Сбор отзывов',
            32 => 'Должник',
            33 => 'Отзыв собран в текущем месяце',
            34 => 'Отказ оставлять отзыв',
            35 => 'Сбор отзыва после экзамена',
            39 => 'Оплата проверена в текущем месяце',
            41 => 'Сбор отзыва, смс и письмо отправлены',
            42 => 'Отзыв собран',
            43 => 'Сбор отзыва, письмо отправлено'
        ];

        foreach ($statuses ?? [] as $key => $value) {
            $item = new OrdersAdminStatuses();
            $item->id_orders_admin_statuses = $key;
            $item->name = $value;
            $item->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
