<?php

use App\Models\CoursesCategoriesSecond;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CoursesCategoriesSecondTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('courses_categories_second');

        Schema::create('courses_categories_second', function (Blueprint $table) {
            $table->integer('id_courses_categories_second', true, true)->nullable(false);
            $table->string('name', 50)->nullable(false);
        });

        if (($f = fopen(__DIR__.'/../dumps/courses_categories_second.csv', 'r')) !== false) {
            while (($data = fgetcsv($f, 0, "\t")) !== false) {
                $item = new CoursesCategoriesSecond();
                $item->id_courses_categories_second = $data[0];
                $item->name = $data[1];
                $item->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses_categories_second');
    }
}
