<?php

use App\Models\Ratings;
use App\Models\Users;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RatingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('ratings');

        if (Schema::hasTable('rating')) {
            DB::statement('ALTER TABLE `rating` ENGINE = InnoDB');
            DB::statement('ALTER TABLE `rating` CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci');

            Schema::rename('rating', 'ratings');
        }

        Schema::table('ratings', function (Blueprint $table) {
            $table->renameColumn('id_user', 'id_users');
            $table->renameColumn('id_lern', 'id_courses');
            $table->renameColumn('rate', 'rating');
            $table->dropIndex('primary');
        });

        Schema::table('ratings', function (Blueprint $table) {
            $table->primary(['id_users', 'id_courses']);
            $table->index('id_users', 'id_users');
            $table->index('id_courses', 'id_courses');
        });

        $ratings = Ratings::all();

        foreach ($ratings ?? [] as $rating) {
            if (!Users::find($rating->id_users)) {
                $rating->delete();
            }
        }

        DB::statement("ALTER TABLE `ratings` MODIFY COLUMN `id_users` INT(10) UNSIGNED NOT NULL");
        DB::statement("ALTER TABLE `ratings` MODIFY COLUMN `id_courses` INT(10) UNSIGNED NOT NULL AFTER `id_users`");
        DB::statement("ALTER TABLE `ratings` MODIFY COLUMN `rating` SMALLINT(5) NOT NULL DEFAULT 0 AFTER `id_courses`");

        Schema::table('ratings', function (Blueprint $table) {
            $table->foreign('id_users', 'ratings_users')->references('id_users')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ratings');
    }
}
