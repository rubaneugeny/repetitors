<?php

use App\Models\Courses;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FixCoursesSlug extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $courses = Courses::whereNotNull('id_parent_course')->get();

        foreach ($courses ?? [] as $course) {
            $parent = Courses::find($course->id_parent_course);
            $course->slug = $parent->slug.'/'.$course->slug;
            $course->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
