<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TotalTemplatesRelations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('total_templates', function (Blueprint $table) {
            $table->foreign('id_courses_categories_second', 'total_templates_courses_categories_second')
                ->references('id_courses_categories_second')
                ->on('courses_categories_second')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('id_courses', 'total_templates_courses')
                ->references('id_courses')
                ->on('courses')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
