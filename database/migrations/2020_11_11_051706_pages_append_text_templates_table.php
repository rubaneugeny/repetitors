<?php

use App\Models\PagesAppendTextTemplates;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PagesAppendTextTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('pages_append_text_templates');

        Schema::create('pages_append_text_templates', function (Blueprint $table) {
            $table->integer('id_pages_append_text_templates', true, true)->nullable(false);
            $table->integer('id_pages_append_text_categories', false, true)->nullable(false)->index('id_pages_append_text_categories');
            $table->integer('id_courses_categories', false, true)->nullable()->index('id_courses_categories');
            $table->integer('id_courses', false, true)->nullable()->index('id_courses');
            $table->text('template')->nullable(false)->comment('Основной шаблон');
            $table->smallInteger('page', false, true)->nullable(false)->comment('Страница, где будет показываться шаблон');
            $table->smallInteger('split', false, true)->nullable(false)->default(0)->comment('Если установлен флаг используется функция splitSentence либо cutUpToSentence, должен быть указан text_limit');
            $table->smallInteger('seo', false, true)->nullable(false)->default(0)->comment('Если установлен флаг, то вначале проверяется поле text из таблицы seo, потом уже берется шаблон');
            $table->smallInteger('text_limit', false, true)->nullable()->comment('Ограничение выводимого текста');
            $table->smallInteger('repetitors_count_limit', false, true)->nullable(false)->default(0)->comment('Количество репетиторов, меньше которого будет показываться второй шаблон, 0 - без ограничений');
            $table->string('class')->nullable(false)->default('s_text_info_bottom')->comment('CSS класс, который будет добавлен в тег p');

            $table->foreign('id_courses', 'pages_append_text_templates_courses')
                ->references('id_courses')
                ->on('courses')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('id_courses_categories', 'pages_append_text_templates_courses_categories')
                ->references('id_courses_categories')
                ->on('courses_categories')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('id_pages_append_text_categories', 'pages_append_text_templates_pages_append_text_categories')
                ->references('id_pages_append_text_categories')
                ->on('pages_append_text_categories')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        if (($f = fopen(__DIR__.'/../dumps/pages_append_text_templates.csv', 'r')) !== false) {
            while (($data = fgetcsv($f, 0, "\t")) !== false) {
                $item = new PagesAppendTextTemplates();
                $item->id_pages_append_text_templates = $data[0];
                $item->id_pages_append_text_categories = $data[1];
                $item->id_courses_categories = $data[2] != 'NULL' ? $data[2] : null;
                $item->id_courses = $data[3] != 'NULL' ? $data[3] : null;
                $item->template = $data[4];
                $item->page = $data[5];
                $item->split = $data[6];
                $item->seo = $data[7];
                $item->text_limit = $data[8] != 'NULL' ? $data[8] : null;
                $item->repetitors_count_limit = $data[9];
                $item->class = $data[10];
                $item->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages_append_text_templates');
    }
}
