<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\AdminsReviews;

class AdminsReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('admin_otz')) {
            DB::statement('ALTER TABLE admin_otz ENGINE = InnoDB');
            DB::statement('ALTER TABLE admin_otz CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci');

            Schema::rename('admin_otz', 'admins_reviews');
        }

        Schema::table('admins_reviews', function (Blueprint $table) {
            $table->timestamps();
            $table->renameColumn('id_admin', 'id_admins_reviews');
            $table->string('name')->collation('utf8mb4_unicode_ci')->nullable(false)->change();
        });

        $admins = AdminsReviews::all();

        foreach ($admins ?? [] as $admin) {
            $admin->name = trim($admin->name);
            $admin->save();
        }

        Schema::table('admins_reviews', function (Blueprint $table) {
            $table->integer('id_admins_reviews', true, true)->nullable(false)->default(null)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins_reviews');
    }
}
