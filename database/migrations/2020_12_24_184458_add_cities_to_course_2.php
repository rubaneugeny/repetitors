<?php

use App\Garbage\LernGeo;
use App\Models\Courses;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCitiesToCourse2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE `courses` ADD COLUMN `metro` VARCHAR(255) NULL COLLATE 'utf8mb4_unicode_ci' COMMENT 'Используется для страницы other_predmet_metro.php для выборки предметов по конкретному метро' AFTER `cities`");

        $geo = LernGeo::all();
        $list = range(2, 71);

        foreach ($geo as $item) {
            $metro = [];

            foreach ($list as $key) {
                if ($item['m'.$key]) {
                    array_push($metro, $key);
                }
            }

            Courses::where(['id_courses' => $item->id])->update([
                'metro' => count($metro) > 0 ? implode(',', $metro) : null,
            ]);
        }

        Schema::drop('lerngeo');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
