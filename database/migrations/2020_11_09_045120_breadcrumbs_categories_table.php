<?php

use App\Models\BreadcrumbsCategories;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BreadcrumbsCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('breadcrumbs_categories');

        Schema::create('breadcrumbs_categories', function (Blueprint $table) {
            $table->integer('id_breadcrumbs_categories', true, true)->nullable(false);
            $table->string('name')->nullable(false);
        });

        if (($f = fopen(__DIR__.'/../dumps/breadcrumbs_categories.csv', 'r')) !== false) {
            while (($data = fgetcsv($f, 0, "\t")) !== false) {
                $breadcrumbs = new BreadcrumbsCategories();
                $breadcrumbs->id_breadcrumbs_categories = $data[0];
                $breadcrumbs->name = $data[1];
                $breadcrumbs->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('breadcrumbs_categories');
    }
}
