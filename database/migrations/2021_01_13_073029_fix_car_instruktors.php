<?php

use App\Models\BreadcrumbsTemplates;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FixCarInstruktors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        BreadcrumbsTemplates::where(['id_breadcrumbs_templates' => 2])->update([
            'type' => null
        ]);
        BreadcrumbsTemplates::where(['id_breadcrumbs_templates' => 4])->update([
            'type' => null
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
