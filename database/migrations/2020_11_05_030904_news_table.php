<?php

use App\Models\News;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class NewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE `news` ENGINE = InnoDB');
        DB::statement('ALTER TABLE `news` CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci');

        Schema::table('news', function (Blueprint $table) {
            $table->renameColumn('date', 'year');
            $table->renameColumn('descr', 'description');
            $table->renameColumn('foto', 'photo');
            $table->renameColumn('from', 'name');
            $table->timestamps();
        });

        Schema::table('news', function (Blueprint $table) {
            $table->date('date')->nullable(false)->after('id_news');
        });

        $news = News::all();

        foreach ($news ?? [] as $item) {
            $item->date = $item->year.'-'.
                str_pad($item->mes, 2, 0, STR_PAD_LEFT).'-'.
                str_pad($item->den, 2, 0, STR_PAD_LEFT);

            $item->name = trim($item->name);

            if (empty($item->name)) {
                $item->name = 'Администрация СПбРепетитор';
            }

            $item->text = trim($item->text);
            $item->title = trim($item->title);
            $item->description = trim($item->description);

            if (empty($item->description)) {
                $item->description = null;
            }

            $item->photo = trim(str_replace('photo_news/', '', $item->photo));

            if (empty($item->photo) || ($item->photo == '0')) {
                $item->photo = null;
            }

            $item->save();
        }

        Schema::table('news', function (Blueprint $table) {
            $table->dropColumn('year');
            $table->dropColumn('mes');
            $table->dropColumn('den');
        });

        DB::statement("ALTER TABLE `news` MODIFY COLUMN `id_news` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT");
        DB::statement("ALTER TABLE `news` MODIFY COLUMN `date` DATE NOT NULL AFTER `id_news`");
        DB::statement("ALTER TABLE `news` MODIFY COLUMN `name` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_unicode_ci' AFTER `date`");
        DB::statement("ALTER TABLE `news` MODIFY COLUMN `title` TEXT NOT NULL COLLATE 'utf8mb4_unicode_ci' AFTER `name`");
        DB::statement("ALTER TABLE `news` MODIFY COLUMN `text` TEXT NOT NULL COLLATE 'utf8mb4_unicode_ci' AFTER `title`");
        DB::statement("ALTER TABLE `news` MODIFY COLUMN `photo` VARCHAR(255) NULL COLLATE 'utf8mb4_unicode_ci' AFTER `text`");
        DB::statement("ALTER TABLE `news` MODIFY COLUMN `description` TEXT NULL COLLATE 'utf8mb4_unicode_ci' AFTER `photo`");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
