<?php

use App\Models\PhotoTextCategories;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PhotoTextCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('photo_text_categories');

        Schema::create('photo_text_categories', function (Blueprint $table) {
            $table->integer('id_photo_text_categories', true, true)->nullable(false);
            $table->string('name')->nullable(false);
        });

        if (($f = fopen(__DIR__.'/../dumps/photo_text_categories.csv', 'r')) !== false) {
            while (($data = fgetcsv($f, 0, "\t")) !== false) {
                $item = new PhotoTextCategories();
                $item->id_photo_text_categories = $data[0];
                $item->name = $data[1];
                $item->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('photo_text_categories');
    }
}
