<?php

use App\Models\Courses;
use App\Models\TitleCategories;
use App\Models\TitleTemplates;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FixMkpp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $category = TitleCategories::create(['name' => 'title_mkpp']);
        TitleTemplates::create([
            'id_title_categories' => $category->id_title_categories,
            'page' => 1,
            'title' => 'Инструкторы по вождению на "механике", &#128663; автоинструкторы на МКПП в Санкт-Петербурге - "СПбРепетитор"',
            'description' => 'Частные инструкторы по вождению с МКПП в Санкт-Петербурге и Области. Недорого. &#11088;Проверенные отзывы, адекватные цены, рейтинг!&#11088; База автоинструкторов на механике.'
        ]);
        TitleTemplates::create([
            'id_title_categories' => $category->id_title_categories,
            'page' => 0,
            'title' => 'Автоинструкторы МКПП (анкеты с {FROM}-{TO}) - "СПбРепетитор"',
            'description' => 'Автоинструкторы на МКПП (анкеты с {FROM}-{TO})'
        ]);
        $course = Courses::find(Courses::MANUAL_TRANSMISSION);
        $course->id_title_categories = $category->id_title_categories;
        $course->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
