<?php

use App\Models\PrependTextTemplates;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PrependTextTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('prepend_text_templates');

        Schema::create('prepend_text_templates', function (Blueprint $table) {
            $table->integer('id_prepend_text_templates', true, true)->nullable(false);
            $table->integer('id_prepend_text_categories', false, true)->nullable(false)->index('id_prepend_text_categories');
            $table->text('template')->nullable(false)->comment('Основной шаблон');
            $table->smallInteger('page', false, true)->nullable(false)->comment('Основная страница');
            $table->boolean('wrap')->nullable(false)->default(0)->comment('Установлен флаг, то оборачивается текст в шаблон prepend-text');
            $table->boolean('split')->nullable(false)->default(0)->comment('Если установлен флаг используется функция splitSentence либо cutUpToSentence, должен быть указан text_limit');
            $table->smallInteger('text_limit', false, true)->nullable()->comment('Ограничение длины текста для переноса');
            $table->string('class')->nullable(false)->comment('CSS класс для тега p');

            $table->foreign('id_prepend_text_categories', 'prepend_text_templates_prepend_text_categories')
                ->references('id_prepend_text_categories')
                ->on('prepend_text_categories')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        if (($f = fopen(__DIR__.'/../dumps/prepend_text_templates.csv', 'r')) !== false) {
            while (($data = fgetcsv($f, 0, "|")) !== false) {
                $item = new PrependTextTemplates();
                $item->id_prepend_text_templates = $data[0];
                $item->id_prepend_text_categories = $data[1];
                $item->template = $data[2];
                $item->page = $data[3];
                $item->wrap = $data[4];
                $item->split = $data[5];
                $item->text_limit = $data[6] != 'NULL' ? $data[6] : null;
                $item->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prepend_text_templates');
    }
}
