<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameUndefinedTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $tables = ['aktiv', 'balans', 'bonus', 'del', 'koef_buffer', 'kolvo', 'kpd', 'kpd1', 'lerngeo', 'logop', 'logoped', 'paymaster', 'payments', 'post1', 'red', 'reiting', 'rep_views', 'shlak', 'temp', 'temp1', 'user1', 'usercomment', 'userpost', 'user_aktive_logs', 'user_delete_zapros', 'user_edit', 'user_fltr', 'videootzyv', 'vzyat', 'post_user_relation', 'user_calendar', 'links_sidebar'];

        foreach ($tables ?? [] as $table) {
            if (Schema::hasTable($table)) {
                Schema::rename($table, '!_'.$table);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
