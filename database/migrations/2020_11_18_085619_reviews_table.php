<?php

use App\Models\Orders;
use App\Models\Reviews;
use App\Models\Users;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('reviews');

        if (Schema::hasTable('otzyv')) {
            DB::statement('ALTER TABLE otzyv ENGINE = InnoDB');
            DB::statement('ALTER TABLE otzyv CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci');

            Schema::rename('otzyv', 'reviews');
        }

        Schema::table('reviews', function (Blueprint $table) {
            $table->renameColumn('id_post', 'id_reviews');
            $table->renameColumn('id_rep', 'id_users');
            $table->renameColumn('id_admin', 'id_admins_reviews');
            $table->renameColumn('id_zay', 'id_orders');
            $table->renameColumn('reit', 'rating');
            $table->renameColumn('date', 'date_del');
            $table->renameColumn('dt', 'date');

            $table->dropColumn('date_del');
            $table->dropColumn('mes');
            $table->dropColumn('avto');

            $table->integer('id_reviews_statuses', false, true)->nullable(false)->default(null);

            $table->timestamps();
        });

        $reviews = Reviews::all();

        foreach ($reviews as $review) {
            $review->comment = trim(stripslashes(html_entity_decode($review->comment)));
            $review->name = trim($review->name);

            if ($review->id_users == 0 || !Users::find($review->id_users)) {
                $review->id_users = null;
            }

            if ($review->rating == 0) {
                $review->rating = 4;
            }

            if ($review->id_admins_reviews === 666777) {
                $review->id_admins_reviews = null;
            }

            if ($review->id_orders == 0 || !Orders::find($review->id_orders)) {
                $review->id_orders = null;
            }

            switch ($review->new) {
                case 2:
                    $review->id_reviews_statuses = 1;
                    break;

                case 1:
                    $review->id_reviews_statuses = 2;
                    break;

                default:
                    $review->id_reviews_statuses = 1;
            }

            if (empty($review->comment)) {
                $review->delete();
            } else {
                $review->save();
            }
        }

        DB::statement("ALTER TABLE `reviews` MODIFY COLUMN `id_reviews` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT");
        DB::statement("ALTER TABLE `reviews` MODIFY COLUMN `id_reviews_statuses` INT(10) UNSIGNED NOT NULL COMMENT 'Статус отзыва 1 - Новый, 2 - Подтвержденный, 3 - Отклоненный' DEFAULT 1 AFTER `id_reviews`");
        DB::statement("ALTER TABLE `reviews` MODIFY COLUMN `id_orders` INT(10) UNSIGNED NULL AFTER `id_reviews_statuses`");
        DB::statement("ALTER TABLE `reviews` MODIFY COLUMN `id_users` INT(10) UNSIGNED NOT NULL AFTER `id_orders`");
        DB::statement("ALTER TABLE `reviews` MODIFY COLUMN `id_admins_reviews` INT(10) UNSIGNED NULL AFTER `id_users`");
        DB::statement("ALTER TABLE `reviews` MODIFY COLUMN `date` DATETIME NOT NULL AFTER `id_admins_reviews`");
        DB::statement("ALTER TABLE `reviews` MODIFY COLUMN `name` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_unicode_ci' AFTER `date`");
        DB::statement("ALTER TABLE `reviews` ADD COLUMN `phone` VARCHAR(20) NULL COLLATE 'utf8mb4_unicode_ci' AFTER `name`");
        DB::statement("ALTER TABLE `reviews` MODIFY COLUMN `comment` TEXT NOT NULL COLLATE 'utf8mb4_unicode_ci' AFTER `phone`");
        DB::statement("ALTER TABLE `reviews` ADD COLUMN `response` TEXT NULL COLLATE 'utf8mb4_unicode_ci' COMMENT 'Ответ репетитора на отзыв, возможно заменим на доп. таблицу с сообщениями' AFTER `comment`");
        DB::statement("ALTER TABLE `reviews` MODIFY COLUMN `rating` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1 AFTER `response`");
        DB::statement("ALTER TABLE `reviews` ADD COLUMN `is_deleted` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 AFTER `rating`");

        Schema::table('reviews', function (Blueprint $table) {
            $table->dropIndex('list_post');
            $table->dropIndex('id_rep');

            $table->index('id_admins_reviews', 'id_admins_reviews');
            $table->index('id_orders', 'id_orders');
            $table->index('id_users', 'id_users');
            $table->index('id_reviews_statuses', 'id_reviews_statuses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reviews');
    }
}
