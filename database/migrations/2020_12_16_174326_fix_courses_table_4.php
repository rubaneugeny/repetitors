<?php

use App\Models\Courses;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FixCoursesTable4 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Courses::whereIn('id_courses', [
            4536, 4530, 4531, 4532, 4533, 4534, 4535,
        ])->update([
            'id_title_categories' => 1,
            'id_total_categories' => 8, // miniup
            'id_prepend_text_categories' => 15, // miniup
            'id_photo_text_categories' => 1,
            'id_pages_append_text_categories' => 18
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
