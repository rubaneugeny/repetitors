<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Cities;

class CitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('gorod')) {
            DB::statement('ALTER TABLE `gorod` ENGINE = InnoDB');
            DB::statement('ALTER TABLE `gorod` CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci');

            Schema::rename('gorod', 'cities');
        }

        Schema::table('cities', function (Blueprint $table) {
            $table->dropColumn('ao');
            $table->dropColumn('from');
            $table->renameColumn('id_gorod', 'id_cities');
            $table->renameColumn('url', 'slug');
            $table->renameColumn('in', 'in_the_direction');
            $table->timestamps();
        });

        $city = new Cities();
        $city->id_cities = 1;
        $city->slug = 'sankt_peterburg';
        $city->name = 'Санкт-Петербург';
        $city->in_the_direction = 'в Санкт-Петербурге';
        $city->save();

        $cities = Cities::all();

        foreach ($cities ?? [] as $city) {
            $city->slug = trim($city->slug);
            $city->name = trim($city->name);
            $city->in_the_direction = trim($city->in_the_direction);
            $city->save();
        }

        DB::statement("ALTER TABLE `cities` MODIFY COLUMN `id_cities` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT");
        DB::statement("ALTER TABLE `cities` MODIFY COLUMN `slug` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_unicode_ci' AFTER `id_cities`");
        DB::statement("ALTER TABLE `cities` MODIFY COLUMN `name` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_unicode_ci' AFTER `slug`");
        DB::statement("ALTER TABLE `cities` MODIFY COLUMN `in_the_direction` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_unicode_ci' AFTER `name`");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cities');
    }
}
