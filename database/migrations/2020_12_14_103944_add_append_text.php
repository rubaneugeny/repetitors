<?php

use App\Models\PagesAppendTextCategories;
use App\Models\PagesAppendTextTemplates;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAppendText extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $category = PagesAppendTextCategories::create(['name' => 'seo_text']);
        PagesAppendTextTemplates::create([
            'id_pages_append_text_categories' => $category->id_pages_append_text_categories,
            'template' => '',
            'page' => 1,
            'split' => 0,
            'seo' => 1,
            'text_limit' => 500,
            'repetitors_count_limit' => 0,
            'class' => 's_text_info_bottom'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
