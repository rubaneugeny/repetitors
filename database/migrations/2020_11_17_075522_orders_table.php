<?php

use App\Garbage\KoefPrint;
use App\Models\Courses;
use App\Models\Orders;
use App\Models\Users;
use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class OrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('orders');

        if (Schema::hasTable('post')) {
            DB::statement('ALTER TABLE post ENGINE = InnoDB');
            DB::statement('ALTER TABLE post CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci');

            Schema::rename('post', 'orders');
        }

        Schema::table('orders', function (Blueprint $table) {
            $table->renameColumn('id_post', 'id_orders');
            $table->renameColumn('namelern', 'id_orders_statuses');
            $table->renameColumn('cenazakaza_type_post', 'id_orders_types');
            $table->renameColumn('mail', 'email');
            $table->renameColumn('id_gorod', 'id_cities');
            $table->renameColumn('id_lern', 'id_courses');
            $table->renameColumn('mesto', 'id_orders_locations');
            $table->renameColumn('id_rep', 'id_users');
            $table->renameColumn('cel', 'object');
            $table->renameColumn('dopinfo', 'additional_information');
            $table->renameColumn('utime', 'duration');
            $table->renameColumn('what', 'comment');
            $table->renameColumn('tel', 'first_lesson_date');
            $table->renameColumn('pprice', 'price_limit');
            $table->renameColumn('id_user', 'id_orders_admin_statuses');
            $table->renameColumn('pochta', 'id_admins');
            $table->renameColumn('close', 'how_much_paid');
            $table->renameColumn('rep', 'price_per_user');
            $table->renameColumn('cenazakaza', 'total_price');
            $table->renameColumn('id_repetitor', 'id_users_referrer');

            $table->text('calculation')->nullable()->comment('Детальный расчет цены с формулами и суммами')->after('id_repetitor');

            $table->dropColumn('otzyv_sec_code');
            $table->dropColumn('new_comment');
            $table->dropColumn('new_comment_ad');
            $table->dropColumn('s1');
            $table->dropColumn('s2');
            $table->dropColumn('s3');

            $table->timestamps();
        });

        /*
         * ps1,ps2,ps3 - это счетчики смсок с 3 видами писем
         * p1,p2,p3 - это счетчики отправки 3х видов писем на почту
         * сenazakaza - сумма, которую должен заплатить репетитор. Рассчитывается при изменении параметров заявки и при оформлении заявки на сайт
         * rep - если заполнено, то это сумма, которую должен оплатить репетитор. Используется, если было меньше 5 занятий
         * close - сумма, которую заплатил препоаватель,и  которая участвует в расчета
         * kogda - дата действия. По ней сортируются заявки в админках, например shadow1.php - тут показываются только те заявки, дата действия по которым сегодня или раньше (просрочена т.е.)
         * data_od - полезное поле, используется для понимания сколько дней заявка лежит в ОД, например в админке на странице zay.php
         */

        Orders::whereIn('id_orders', [99325, 99328, 99329, 99330, 281386])->delete();

        $items = Orders::all();

        foreach ($items ?? [] as $item) {
            $item->name = trim($item->name);

            switch ($item->id_orders_statuses) {
                case '5':
                    $item->id_orders_statuses = 7;
                    break;

                case '4':
                    $item->id_orders_statuses = 6;
                    break;

                case '29':
                    $item->id_orders_statuses = 5;
                    break;

                case '3':
                    $item->id_orders_statuses = 4;
                    break;

                case '2':
                    $item->id_orders_statuses = 3;
                    break;

                case '1':
                    $item->id_orders_statuses = 2;
                    break;

                default:
                    $item->id_orders_statuses = 1;
            }

            $item->phone = trim($item->phone);

            if (empty($item->phone)) {
                $item->phone = null;
            }

            $item->email = trim($item->email);

            if (empty($item->email)) {
                $item->email = null;
            }

            if ($item->id_courses == '0' || !Courses::find($item->id_courses)) {
                $item->id_courses = null;
            }

            $item->object = trim(html_entity_decode($item->object));

            if (empty($item->object) || $item->object == '0') {
                $item->object = null;
            }

            $item->price_limit = intval($item->price_limit);
            $item->duration = intval($item->duration);

            switch ($item->id_orders_locations) {
                case 'у преподавателя':
                    $item->id_orders_locations = 2;
                    break;

                case 'у ученика':
                    $item->id_orders_locations = 1;
                    break;

                case 'дистанционно':
                    $item->id_orders_locations = 3;
                    break;

                default:
                    $item->id_orders_locations = 4;
            }

            $item->additional_information = trim(html_entity_decode($item->additional_information));

            if (empty($item->additional_information) || $item->additional_information == '0') {
                $item->additional_information = null;
            }

            $item->comment = trim(html_entity_decode($item->comment));

            if (empty($item->comment) || $item->comment == '0') {
                $item->comment = null;
            }

            if ($item->id_admins == 0) {
                $item->id_admins = 1;
            }

            if ($item->id_cities == 0) {
                $item->id_cities = null;
            }

            if ($item->id_users_referrer == 0 || !Users::find($item->id_users_referrer)) {
                $item->id_users_referrer = null;
            }

            if ($item->id_users == 0 || !Users::find($item->id_users)) {
                $item->id_users = null;
            }

            $item->p1 = intval($item->p1);
            $item->p2 = intval($item->p2);
            $item->p3 = intval($item->p3);

            if ($item->id_orders_admin_statuses < 10) {
                $item->id_orders_admin_statuses++;
            }

            $item->first_lesson_date = null;
            $item->time = trim($item->time);
            $item->id_orders_types++;

            if ((strlen($item->time) > 5) || empty($item->time)) {
                $item->time = '00:00';
            }

            $item->time .= ':00';

            $item->date = trim($item->date);

            if (empty($item->date)) {
                $item->date = Carbon::createFromFormat('Ymd', $item->kogda)->format('d.m.y');
            }

            switch ($item->date) {
                case '2810':
                    $item->date = '28.10.15';
                    break;

                case '05.03':
                    $item->date = '05.03.13';
                    break;

                case '11.02':
                    $item->date = '11.02.14';
                    break;

                case '14.01':
                    $item->date = '14.01.13';
                    break;

                case '8.9.12':
                    $item->date = '08.09.12';
                    break;

                case '211212':
                    $item->date = '21.12.12';
                    break;

                case '3.07.12':
                    $item->date = '03.07.12';
                    break;

                case '10.9.12':
                    $item->date = '10.09.12';
                    break;

                case '17.9.12':
                    $item->date = '17.09.12';
                    break;

                case '3.09.12':
                    $item->date = '03.09.12';
                    break;

                case '30.1112':
                    $item->date = '30.11.12';
                    break;

                case '1.07.13':
                    $item->date = '01.07.13';
                    break;

                case '6.11.14':
                    $item->date = '06.11.14';
                    break;

                case '1.11.15':
                    $item->date = '01.11.15';
                    break;

                case '02.11.12.':
                    $item->date = '02.11.12';
                    break;

                case '11..01.15':
                    $item->date = '11.01.15';
                    break;

                case '03,02.13':
                    $item->date = '03.02.13';
                    break;

                case '26.10.2011':
                    $item->date = '26.10.11';
                    break;

                case '29.08.2012':
                    $item->date = '29.08.12';
                    break;

                case '8-931-207-45-80':
                    $item->date = '13.03.13';
                    break;
            }

            $item->date = Carbon::createFromFormat('d.m.y', $item->date)->format('Y-m-d') . ' ' . $item->time;

            if ($item->id_metro == 0) {
                $item->id_metro = null;
            }

            if ($koef = KoefPrint::find($item->koef_print_id)) {
                $item->calculation = $koef->print;
            }

            $item->save();
        }

        DB::statement("ALTER TABLE `orders` MODIFY COLUMN `id_orders` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT");
        DB::statement("ALTER TABLE `orders` MODIFY COLUMN `id_orders_statuses` INT(10) UNSIGNED NOT NULL COMMENT 'Статус заявки' AFTER `id_orders`");
        DB::statement("ALTER TABLE `orders` MODIFY COLUMN `id_orders_admin_statuses` INT(10) UNSIGNED NOT NULL COMMENT 'Статус заявки для администратора' AFTER `id_orders_statuses`");
        DB::statement("ALTER TABLE `orders` MODIFY COLUMN `id_orders_types` INT(10) UNSIGNED NULL COMMENT 'Тип заявки' AFTER `id_orders_admin_statuses`");
        DB::statement("ALTER TABLE `orders` MODIFY COLUMN `id_admins` INT(10) UNSIGNED NULL COMMENT 'Администратор заявки' AFTER `id_orders_types`");
        DB::statement("ALTER TABLE `orders` MODIFY COLUMN `date` DATETIME NOT NULL COMMENT 'Дата заказа' AFTER `id_admins`");
        DB::statement("ALTER TABLE `orders` MODIFY COLUMN `name` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_unicode_ci' COMMENT 'Имя заказчика' AFTER `date`");
        DB::statement("ALTER TABLE `orders` MODIFY COLUMN `phone` VARCHAR(20) NULL COLLATE 'utf8mb4_unicode_ci' COMMENT 'Должно быть NOT NULL, но старые записи без указания телефона' AFTER `name`");
        DB::statement("ALTER TABLE `orders` MODIFY COLUMN `email` VARCHAR(255) NULL COLLATE 'utf8mb4_unicode_ci' AFTER `phone`");
        DB::statement("ALTER TABLE `orders` MODIFY COLUMN `id_cities` INT(10) UNSIGNED NULL COMMENT 'Город' AFTER `email`");
        DB::statement("ALTER TABLE `orders` MODIFY COLUMN `id_metro` INT(10) UNSIGNED NULL COMMENT 'Метро' AFTER `id_cities`");
        DB::statement("ALTER TABLE `orders` MODIFY COLUMN `id_courses` INT(10) UNSIGNED NULL COMMENT 'Предмет' AFTER `id_metro`");
        DB::statement("ALTER TABLE `orders` MODIFY COLUMN `id_orders_locations` INT(10) UNSIGNED NOT NULL COMMENT 'Место проведения занятия' AFTER `id_courses`");
        DB::statement("ALTER TABLE `orders` MODIFY COLUMN `id_users` INT(10) UNSIGNED NULL COMMENT 'Репетитор, который взял заказ' AFTER `id_orders_locations`");
        DB::statement("ALTER TABLE `orders` MODIFY COLUMN `id_users_referrer` INT(10) UNSIGNED NULL COMMENT 'Реферальный ID преподавателя, за которого начислится бонусная программа' AFTER `id_users`");
        DB::statement("ALTER TABLE `orders` MODIFY COLUMN `object` TEXT NULL COLLATE 'utf8mb4_unicode_ci' COMMENT 'Цель занятия' AFTER `id_users_referrer`");
        DB::statement("ALTER TABLE `orders` MODIFY COLUMN `additional_information` TEXT NULL COLLATE 'utf8mb4_unicode_ci' COMMENT 'Дополнительная информация' AFTER `object`");
        DB::statement("ALTER TABLE `orders` MODIFY COLUMN `duration` SMALLINT(5) UNSIGNED NOT NULL COMMENT 'Продолжительность занятия в минутах' DEFAULT 0 AFTER `additional_information`");
        DB::statement("ALTER TABLE `orders` MODIFY COLUMN `comment` TEXT NULL COLLATE 'utf8mb4_unicode_ci' COMMENT 'Комментарий от администратора' AFTER `duration`");
        DB::statement("ALTER TABLE `orders` MODIFY COLUMN `first_lesson_date` DATE NULL COMMENT 'Дата первого занятия' AFTER `comment`");
        DB::statement("ALTER TABLE `orders` MODIFY COLUMN `price_limit` DECIMAL(10,2) NOT NULL COMMENT 'Желаемая стоимость занятия от заказчика' DEFAULT 0.00 AFTER `first_lesson_date`");
        DB::statement("ALTER TABLE `orders` MODIFY COLUMN `price_per_user` DECIMAL(10,2) NOT NULL COMMENT 'Сколько должен заплатить преподаватель, если у него было менее 5 занятий' DEFAULT 0.00 AFTER `price_limit`");
        DB::statement("ALTER TABLE `orders` MODIFY COLUMN `how_much_paid` DECIMAL(10,2) NOT NULL COMMENT 'Сколько оплатил преподаватель из выставленной суммы' DEFAULT 0.00 AFTER `price_per_user`");
        DB::statement("ALTER TABLE `orders` MODIFY COLUMN `total_price` DECIMAL(10,2) NOT NULL COMMENT 'Общая стоимость заказа' DEFAULT 0.00 AFTER `how_much_paid`");
        DB::statement("ALTER TABLE `orders` MODIFY COLUMN `ps1` INT(10) UNSIGNED NOT NULL COMMENT 'Счетчик кол-ва смс с письмом 1' DEFAULT 0");
        DB::statement("ALTER TABLE `orders` MODIFY COLUMN `ps2` INT(10) UNSIGNED NOT NULL COMMENT 'Счетчик кол-ва смс с письмом 2' DEFAULT 0");
        DB::statement("ALTER TABLE `orders` MODIFY COLUMN `ps3` INT(10) UNSIGNED NOT NULL COMMENT 'Счетчик кол-ва смс с письмом 3' DEFAULT 0");
        DB::statement("ALTER TABLE `orders` MODIFY COLUMN `p1` INT(10) UNSIGNED NOT NULL COMMENT 'Счетчик кол-ва писем с письмом 1' DEFAULT 0");
        DB::statement("ALTER TABLE `orders` MODIFY COLUMN `p2` INT(10) UNSIGNED NOT NULL COMMENT 'Счетчик кол-ва писем с письмом 2' DEFAULT 0");
        DB::statement("ALTER TABLE `orders` MODIFY COLUMN `p3` INT(10) UNSIGNED NOT NULL COMMENT 'Счетчик кол-ва писем с письмом 3' DEFAULT 0");
        DB::statement("ALTER TABLE `orders` MODIFY COLUMN `data_od` MEDIUMTEXT NULL COMMENT '???'");
        DB::statement("ALTER TABLE `orders` MODIFY COLUMN `kogda` MEDIUMTEXT NULL COMMENT '???'");
        DB::statement("ALTER TABLE `orders` MODIFY COLUMN `god` MEDIUMTEXT NULL COMMENT '???'");
        DB::statement("ALTER TABLE `orders` MODIFY COLUMN `mes` MEDIUMTEXT NULL COMMENT '???'");
        DB::statement("ALTER TABLE `orders` MODIFY COLUMN `den` MEDIUMTEXT NULL COMMENT '???'");

        Schema::table('orders', function (Blueprint $table) {
            $table->dropIndex('id_user');
            $table->dropIndex('id_rep');

            $table->dropColumn('time');
            $table->dropColumn('koef_print_id');

            $table->index('id_orders_statuses', 'id_orders_statuses');
            $table->index('id_orders_admin_statuses', 'id_orders_admin_statuses');
            $table->index('id_courses', 'id_courses');
            $table->index('id_metro', 'id_metro');
            $table->index('id_orders_locations', 'id_orders_locations');
            $table->index('id_cities', 'id_cities');
            $table->index('id_orders_types', 'id_orders_types');
            $table->index('id_admins', 'id_admins');
            $table->index('id_users', 'id_users');
            $table->index('id_users_referrer', 'id_users_referrer');

            $table->foreign('id_cities', 'orders_cities')->references('id_cities')->on('cities')->onDelete('set null')->onUpdate('cascade');
            $table->foreign('id_orders_statuses', 'orders_orders_statuses')->references('id_orders_statuses')->on('orders_statuses')->onDelete('restrict')->onUpdate('cascade');
            $table->foreign('id_orders_admin_statuses', 'orders_orders_admin_statuses')->references('id_orders_admin_statuses')->on('orders_admin_statuses')->onDelete('restrict')->onUpdate('cascade');
            $table->foreign('id_orders_types', 'orders_orders_types')->references('id_orders_types')->on('orders_types')->onDelete('restrict')->onUpdate('cascade');
            $table->foreign('id_metro', 'orders_metro')->references('id_metro')->on('metro')->onDelete('set null')->onUpdate('cascade');
            $table->foreign('id_courses', 'orders_courses')->references('id_courses')->on('courses')->onDelete('set null')->onUpdate('cascade');
            $table->foreign('id_orders_locations', 'orders_orders_locations')->references('id_orders_locations')->on('orders_locations')->onDelete('restrict')->onUpdate('cascade');
            $table->foreign('id_admins', 'orders_admins')->references('id_admins')->on('admins')->onDelete('restrict')->onUpdate('cascade');
            $table->foreign('id_users_referrer', 'orders_users_referrer')->references('id_users')->on('users')->onDelete('restrict')->onUpdate('cascade');
            $table->foreign('id_users', 'orders_users')->references('id_users')->on('users')->onDelete('restrict')->onUpdate('cascade');
        });

        Schema::drop('koef_print');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
