<?php

use App\Models\Courses;
use App\Models\Ratings;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class NewRelations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $items = Ratings::all();

        foreach ($items ?? [] as $item) {
            if (!Courses::find($item->id_courses)) {
                $item->delete();
            }
        }

        Schema::table('ratings', function (Blueprint $table) {
            $table->foreign('id_courses', 'ratings_courses')->references('id_courses')->on('courses')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('sms', function (Blueprint $table) {
            $table->foreign('id_admins', 'sms_admins')->references('id_admins')->on('admins')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
