<?php

use App\Models\Sms;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE `sms` ENGINE = InnoDB');
        DB::statement('ALTER TABLE `sms` CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci');

        Schema::table('sms', function (Blueprint $table) {
            $table->renameColumn('id', 'id_sms');
            $table->renameColumn('admin', 'id_admins');
            $table->dropColumn('status');
            $table->timestamps();
        });

        Schema::table('sms', function (Blueprint $table) {
            $table->renameColumn('sms_id', 'id');
        });

        $sms = Sms::all();

        foreach ($sms ?? [] as $item) {
            $item->message = trim(str_replace('??', 'И', mb_convert_encoding($item->message, "windows-1251", "utf-8")));
            $item->save();
        }

        DB::statement("ALTER TABLE `sms` MODIFY COLUMN `id_sms` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT");
        DB::statement("ALTER TABLE `sms` MODIFY COLUMN `id` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_unicode_ci' AFTER `id_sms`");
        DB::statement("ALTER TABLE `sms` MODIFY COLUMN `date` DATETIME NOT NULL AFTER `id`");
        DB::statement("ALTER TABLE `sms` MODIFY COLUMN `phone` VARCHAR(20) NOT NULL COLLATE 'utf8mb4_unicode_ci' COMMENT 'Телефон получателя' AFTER `date`");
        DB::statement("ALTER TABLE `sms` MODIFY COLUMN `message` TEXT NOT NULL COMMENT 'Сообщение' AFTER `phone`");
        DB::statement("ALTER TABLE `sms` MODIFY COLUMN `id_admins` INT(10) UNSIGNED NULL AFTER `message`");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sms');
    }
}
