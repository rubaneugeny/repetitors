<?php

use App\Models\NewsUsers;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class NewsUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('news_rep')) {
            DB::statement('ALTER TABLE `news_rep` ENGINE = InnoDB');
            DB::statement('ALTER TABLE `news_rep` CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci');

            Schema::rename('news_rep', 'news_users');
        }

        Schema::table('news_users', function (Blueprint $table) {
            $table->renameColumn('date', 'year');
            $table->renameColumn('id_news_rep', 'id_news_users');
            $table->renameColumn('avto', 'for_car_instructors');
            $table->timestamps();
        });

        Schema::table('news_users', function (Blueprint $table) {
            $table->date('date')->nullable(false)->after('id_news_users');
        });

        $news = NewsUsers::all();

        foreach ($news ?? [] as $item) {
            $item->date = $item->year.'-'.$item->mes.'-'.$item->den;
            $item->for_car_instructors = $item->for_car_instructors == 1 ? 0 : 1;
            $item->title = trim($item->title);
            $item->text = trim($item->text);

            if (strlen($item->title) == 0) {
                $item->title = 'Нововведения';
            }

            $item->save();
        }

        Schema::table('news_users', function (Blueprint $table) {
            $table->dropColumn('year');
            $table->dropColumn('den');
            $table->dropColumn('mes');
        });

        DB::statement("ALTER TABLE `news_users` MODIFY COLUMN `id_news_users` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT");
        DB::statement("ALTER TABLE `news_users` MODIFY COLUMN `title` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_unicode_ci' AFTER `date`");
        DB::statement("ALTER TABLE `news_users` MODIFY COLUMN `text` TEXT NOT NULL COLLATE 'utf8mb4_unicode_ci' AFTER `title`");
        DB::statement("ALTER TABLE `news_users` MODIFY COLUMN `for_car_instructors` SMALLINT(5) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Новость для автоинструкторов' AFTER `text`");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news_users');
    }
}
