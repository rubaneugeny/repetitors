<?php

use App\Models\Users;
use App\Models\UsersDocuments;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UsersDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('doci')) {
            DB::statement('ALTER TABLE `doci` ENGINE = InnoDB');
            DB::statement('ALTER TABLE `doci` CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci');

            Schema::rename('doci', 'users_documents');
        }

        Schema::table('users_documents', function (Blueprint $table) {
            $table->renameColumn('id_post', 'id_users_documents');
            $table->renameColumn('id_rep', 'id_users');
            $table->renameColumn('doci', 'document');

            $table->dropIndex('id_rep');
            $table->index('id_users', 'id_users');
        });

        DB::statement("ALTER TABLE `users_documents` MODIFY COLUMN `id_users_documents` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT");
        DB::statement("ALTER TABLE `users_documents` MODIFY COLUMN `id_users` INT(10) UNSIGNED NOT NULL AFTER `id_users_documents`");
        DB::statement("ALTER TABLE `users_documents` MODIFY COLUMN `document` VARCHAR(255) NULL COLLATE 'utf8mb4_unicode_ci' AFTER `id_users`");

        $documents = UsersDocuments::all();

        foreach ($documents ?? [] as $document) {
            if (!Users::find($document->id_users)) {
                $document->delete();
            }
        }

        Schema::table('users_documents', function (Blueprint $table) {
            $table->foreign('id_users', 'users_documents_users')->references('id_users')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_documents');
    }
}
