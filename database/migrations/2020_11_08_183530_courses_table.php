<?php

use App\Models\Courses;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('courses');

        if (Schema::hasTable('lern')) {
            DB::statement('ALTER TABLE `lern` ENGINE = InnoDB');
            DB::statement('ALTER TABLE `lern` CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci');

            Schema::rename('lern', 'courses');
        }

        Schema::table('courses', function (Blueprint $table) {
            $table->renameColumn('id_lern', 'id_courses');
            $table->renameColumn('url', 'slug');
            $table->renameColumn('url2', 'slug_second');
            $table->renameColumn('category', 'id_courses_categories');
            $table->renameColumn('cat_metro', 'id_courses_categories_second');
            $table->renameColumn('link', 'recommend_name');
            $table->renameColumn('link2', 'recommend_name_second');
            $table->renameColumn('link3', 'recommend_name_third');
            $table->renameColumn('otzyv', 'has_review');
            $table->renameColumn('koef_dlit', 'multiplier_duration');
            $table->renameColumn('fltr', 'id_parent_course');
            $table->renameColumn('type', 'id_courses_types');
            $table->renameColumn('mgu', 'spbgu');

            $table->timestamps();

            $table->dropColumn('i_avg');
            $table->dropColumn('koef_zay');
        });

        DB::statement("ALTER TABLE `courses` MODIFY COLUMN `id_courses` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT");
        DB::statement("ALTER TABLE `courses` MODIFY COLUMN `id_courses_categories` INT(10) UNSIGNED NULL COMMENT '??? Основная категория, непонятное разделение и нет точного ответа на это' AFTER `id_courses`");
        DB::statement("ALTER TABLE `courses` MODIFY COLUMN `id_courses_categories_second` INT(10) UNSIGNED NULL COMMENT '??? Вторая категория для фильтрации, непонятное разделение и нет точного ответа на это' AFTER `id_courses_categories`");
        DB::statement("ALTER TABLE `courses` MODIFY COLUMN `id_courses_types` INT(10) NULL COMMENT 'Тип предмета' AFTER `id_courses_categories_second`");
        DB::statement("ALTER TABLE `courses` ADD COLUMN `id_title_categories` INT(10) UNSIGNED NULL COMMENT 'Шаблон заголовка предмета для генерации Title, Description' AFTER `id_courses_types`");
        DB::statement("ALTER TABLE `courses` ADD COLUMN `id_total_categories` INT(10) UNSIGNED NULL COMMENT 'Шаблон значения Всего преподавателей на странице, при отсутствии не показывается данная запись' AFTER `id_title_categories`");
        DB::statement("ALTER TABLE `courses` ADD COLUMN `id_prepend_text_categories` INT(10) UNSIGNED NULL COMMENT 'Шаблон текста после Всего преподавателей на странице' AFTER `id_total_categories`");
        DB::statement("ALTER TABLE `courses` ADD COLUMN `id_photo_text_categories` INT(10) UNSIGNED NULL COMMENT 'Шаблон текста в img alt преподавателя' AFTER `id_prepend_text_categories`");
        DB::statement("ALTER TABLE `courses` ADD COLUMN `id_comments_prepend_text_categories` INT(10) UNSIGNED NULL COMMENT 'Шаблон текста перед комментарием' AFTER `id_photo_text_categories`");
        DB::statement("ALTER TABLE `courses` ADD COLUMN `id_pages_append_text_categories` INT(10) UNSIGNED NULL COMMENT 'Шаблон текста после списка номеров страниц' AFTER `id_comments_prepend_text_categories`");
        DB::statement("ALTER TABLE `courses` MODIFY COLUMN `id_parent_course` INT(10) UNSIGNED NULL COMMENT 'Указывается основной предмет для объединения в группу с подкатегориями. Используется для генерации title' AFTER `id_pages_append_text_categories`");
        DB::statement("ALTER TABLE `courses` ADD COLUMN `id_courses_second` INT(10) UNSIGNED NULL COMMENT 'Второй предмет для выборки например биология и химия' AFTER `id_parent_course`");
        DB::statement("ALTER TABLE `courses` MODIFY COLUMN `name` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_unicode_ci' COMMENT 'Название курса' AFTER `id_courses_second`");
        DB::statement("ALTER TABLE `courses` MODIFY COLUMN `slug` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_unicode_ci' AFTER `name`");
        DB::statement("ALTER TABLE `courses` MODIFY COLUMN `slug_second` VARCHAR(255) NULL COLLATE 'utf8mb4_unicode_ci' AFTER `slug`");
        DB::statement("ALTER TABLE `courses` MODIFY COLUMN `recommend_name` VARCHAR(255) NULL COLLATE 'utf8mb4_unicode_ci' COMMENT 'Имя в рекомендованных ссылках, шаблонах' AFTER `slug_second`");
        DB::statement("ALTER TABLE `courses` MODIFY COLUMN `recommend_name_second` VARCHAR(255) NULL COLLATE 'utf8mb4_unicode_ci' COMMENT 'Имя в рекомендованных ссылках, шаблонах' AFTER `recommend_name`");
        DB::statement("ALTER TABLE `courses` MODIFY COLUMN `recommend_name_third` VARCHAR(255) NULL COLLATE 'utf8mb4_unicode_ci' COMMENT 'Имя в рекомендованных ссылках' AFTER `recommend_name_second`");
        DB::statement("ALTER TABLE `courses` MODIFY COLUMN `link4` VARCHAR(255) NULL COLLATE 'utf8mb4_unicode_ci' COMMENT '???' AFTER `recommend_name_third`");
        DB::statement("ALTER TABLE `courses` MODIFY COLUMN `link5` VARCHAR(255) NULL COLLATE 'utf8mb4_unicode_ci' COMMENT '???' AFTER `link4`");
        DB::statement("ALTER TABLE `courses` MODIFY COLUMN `link6` VARCHAR(255) NULL COLLATE 'utf8mb4_unicode_ci' COMMENT '???' AFTER `link5`");
        DB::statement("ALTER TABLE `courses` ADD COLUMN `recommend_name_seventh` VARCHAR(255) NULL COLLATE 'utf8mb4_unicode_ci' COMMENT 'Используется для автозамены в шаблонах' AFTER `link6`");
        DB::statement("ALTER TABLE `courses` ADD COLUMN `recommend_name_eighth` VARCHAR(255) NULL COLLATE 'utf8mb4_unicode_ci' COMMENT 'Используется для автозамены в шаблонах' AFTER `recommend_name_seventh`");
        DB::statement("ALTER TABLE `courses` ADD COLUMN `recommend_name_nineth` VARCHAR(255) NULL COLLATE 'utf8mb4_unicode_ci' COMMENT 'Шаблон mini_text' AFTER `recommend_name_eighth`");
        DB::statement("ALTER TABLE `courses` MODIFY COLUMN `has_review` TINYINT(1) UNSIGNED NOT NULL COMMENT 'Есть ли отзывы к предмету' DEFAULT 0 AFTER `recommend_name_nineth`");
        DB::statement("ALTER TABLE `courses` ADD COLUMN `class_menu` TINYINT(1) UNSIGNED NOT NULL COMMENT 'Включить позицию поиска по классам в фильтре поиска' DEFAULT 0 AFTER `has_review`");
        DB::statement("ALTER TABLE `courses` MODIFY COLUMN `skype` TINYINT(1) UNSIGNED NOT NULL COMMENT 'Возможно ли проводить онлайн занятие' DEFAULT 0 AFTER `class_menu`");
        DB::statement("ALTER TABLE `courses` MODIFY COLUMN `malo` INT(11) NOT NULL COMMENT '???' DEFAULT 1 AFTER `skype`");
        DB::statement("ALTER TABLE `courses` MODIFY COLUMN `spbgu` TINYINT(1) UNSIGNED NOT NULL COMMENT 'Флаг для фильтра на странице other_predmet_mgu.php' DEFAULT 0 AFTER `malo`");
        DB::statement("ALTER TABLE `courses` MODIFY COLUMN `mama` INT(11) NOT NULL COMMENT '???' AFTER `spbgu`");
        DB::statement("ALTER TABLE `courses` MODIFY COLUMN `multiplier_duration` DECIMAL(10,4) NULL COMMENT 'Коэффициент, изначально рассчитанный для каждого предмета на основании средней длительности занятий по данному предмету, используется для расчета цены при заявке' DEFAULT 1 AFTER `mama`");
        DB::statement("ALTER TABLE `courses` MODIFY COLUMN `koef_kol` INT(11) NULL COMMENT 'Количество репетиторов c aktive 2,3,5,6,8 по предметам начальный момент введения коэффициентов рассчитанное, на основании него прописывается необходимый коэффициент на странице расчета итогового коэффициента' AFTER `multiplier_duration`");
        DB::statement("ALTER TABLE `courses` MODIFY COLUMN `price_min` INT(11) NULL COMMENT '???' AFTER `koef_kol`");
        DB::statement("ALTER TABLE `courses` MODIFY COLUMN `price_max` INT(11) NULL COMMENT '???' AFTER `price_min`");

        Schema::table('courses', function (Blueprint $table) {
            $table->index('id_title_categories', 'id_title_categories');
            $table->index('id_total_categories', 'id_total_categories');
            $table->index('id_photo_text_categories', 'id_photo_text_categories');
            $table->index('id_comments_prepend_text_categories', 'id_comments_prepend_text_categories');
            $table->index('id_pages_append_text_categories', 'id_pages_append_text_categories');
            $table->index('id_parent_course', 'id_parent_course');
            $table->index('id_courses_types', 'id_courses_types');
            $table->index('id_courses_second', 'id_courses_second');
            $table->index('id_courses_categories_second', 'id_courses_categories_second');
            $table->index('id_prepend_text_categories', 'id_prepend_text_categories');
        });

        $courses = Courses::all();

        foreach ($courses ?? [] as $course) {
            if ($course->id_courses_categories == 5 && $course->dance == 5) {
                $course->id_courses_categories = 7;
                $course->id_courses_categories_second = 7;
            }

            if ($course->id_courses_categories == 6) {
                $course->id_courses_categories_second = 9;
            }

            if ($course->id_courses == 23) {
                $course->id_courses_categories = 8;
                $course->id_courses_categories_second = 8;
            }

            if ($course->id_courses == 4806) {
                $course->id_courses_categories = 5;
                $course->id_courses_categories_second = 5;
            }

            if (empty($course->price_min)) {
                $course->price_min = 0;
            }

            if (empty($course->price_max)) {
                $course->price_max = 0;
            }

            if (empty($course->skype)) {
                $course->skype = 0;
            }

            if (empty($course->has_review)) {
                $course->has_review = 0;
            }

            if (empty($course->spbgu)) {
                $course->spbgu = 0;
            }

            if (empty($course->id_courses_types)) {
                $course->id_courses_types = 1;
            } else {
                $course->id_courses_types = $course->id_courses_types + 2;
            }

            if (empty($course->link4)) {
                $course->link4 = null;
            }

            if (empty($course->link5)) {
                $course->link5 = null;
            }

            if (empty($course->link6)) {
                $course->link6 = null;
            }

            if (empty($course->slug_second)) {
                $course->slug_second = null;
            }

            if (empty($course->recommend_name_third)) {
                $course->recommend_name_third = null;
            }

            if ($course->id_parent_course == 0 || !Courses::find($course->id_parent_course)) {
                $course->id_parent_course = null;
            }

            $course->save();
        }

        DB::statement("ALTER TABLE `courses` MODIFY COLUMN `id_courses_types` INT(10) UNSIGNED NOT NULL COMMENT 'Тип предмета' AFTER `id_courses_categories_second`");

        if (($f = fopen(__DIR__.'/../dumps/courses.csv', 'r')) !== false) {
            while (($data = fgetcsv($f, 0, "\t")) !== false) {
                $course = Courses::find($data[0]);

                if ($course->id_courses_categories == 0) {
                    $course->id_courses_categories = null;
                }

                $course->id_title_categories = $data[4] != 'NULL' ? $data[4] : null;
                $course->id_total_categories = $data[5] != 'NULL' ? $data[5] : null;
                $course->id_prepend_text_categories = $data[6] != 'NULL' ? $data[6] : null;
                $course->id_photo_text_categories = $data[7] != 'NULL' ? $data[7] : null;
                $course->id_comments_prepend_text_categories = $data[8] != 'NULL' ? $data[8] : null;
                $course->id_pages_append_text_categories = $data[9] != 'NULL' ? $data[9] : null;
                $course->id_courses_second = $data[11] != 'NULL' ? $data[11] : null;
                $course->recommend_name_seventh = $data[21] != 'NULL' ? $data[21] : null;
                $course->recommend_name_eighth = $data[22] != 'NULL' ? $data[22] : null;
                $course->recommend_name_nineth = $data[23] != 'NULL' ? $data[23] : null;
                $course->class_menu = $data[25] != 'NULL' ? $data[25] : null;
                $course->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
