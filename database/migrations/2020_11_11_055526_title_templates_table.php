<?php

use App\Models\TitleTemplates;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TitleTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('title_templates');

        Schema::create('title_templates', function (Blueprint $table) {
            $table->integer('id_title_templates', true, true)->nullable(false);
            $table->integer('id_title_categories', false, true)->nullable(false)->index('id_title_categories')->comment('Категория заголовка');
            $table->integer('id_courses_categories', false, true)->nullable()->index('id_courses_categories')->comment('Категория предмета основная');
            $table->integer('id_courses_categories_second', false, true)->nullable()->index('id_courses_categories_second')->comment('Категория предмета дополнительная');
            $table->integer('id_courses_types', false, true)->nullable()->index('id_courses_types')->comment('Тип предмета');
            $table->integer('id_parent_course', false, true)->nullable()->index('id_parent_course')->comment('Основной предмет для группировки');
            $table->integer('id_courses', false, true)->nullable()->index('id_courses')->comment('Предмет');
            $table->smallInteger('page', false, true)->nullable(false)->comment('Номер страницы, на которой будет размещен, 0 - все страницы');
            $table->string('title')->nullable(false);
            $table->text('description')->nullable();

            $table->foreign('id_title_categories', 'title_templates_title_categories')
                ->references('id_title_categories')
                ->on('title_categories')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('id_courses_categories', 'title_templates_courses_categories')
                ->references('id_courses_categories')
                ->on('courses_categories')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('id_courses_categories_second', 'title_templates_courses_categories_second')
                ->references('id_courses_categories_second')
                ->on('courses_categories_second')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('id_courses_types', 'title_templates_courses_types')
                ->references('id_courses_types')
                ->on('courses_types')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('id_parent_course', 'title_templates_course')
                ->references('id_courses')
                ->on('courses')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('id_courses', 'title_templates_courses')
                ->references('id_courses')
                ->on('courses')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        if (($f = fopen(__DIR__.'/../dumps/title_templates.csv', 'r')) !== false) {
            while (($data = fgetcsv($f, 0, "\t")) !== false) {
                $item = new TitleTemplates();
                $item->id_title_templates = $data[0];
                $item->id_title_categories = $data[1];
                $item->id_courses_categories = $data[2] != 'NULL' ? $data[2] : null;
                $item->id_courses_categories_second = $data[3] != 'NULL' ? $data[3] : null;
                $item->id_courses_types = $data[4] != 'NULL' ? $data[4] : null;
                $item->id_parent_course = $data[5] != 'NULL' ? $data[5] : null;
                $item->id_courses = $data[6] != 'NULL' ? $data[6] : null;
                $item->page = $data[7];
                $item->title = $data[8];
                $item->description = $data[9] != 'NULL' ? $data[9] : null;
                $item->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('title_templates');
    }
}
