<?php

use App\Models\Courses;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FixCoursesTable3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Courses::whereIn('id_courses', [
            4750, 4751
        ])->update([
            'id_title_categories' => 1,
            'id_total_categories' => 8, // miniup
            'id_prepend_text_categories' => 15, // miniup
            'id_photo_text_categories' => 1,
            'id_pages_append_text_categories' => 14
        ]);
        Courses::whereIn('id_courses', [
            4001
        ])->update([
            'id_title_categories' => 1,
            'id_total_categories' => 6,
            'id_prepend_text_categories' => 16,
            'id_photo_text_categories' => 1,
            'id_pages_append_text_categories' => 18
        ]);
        Courses::whereIn('id_courses', [
            4751
        ])->update([
            'slug' => 'biologiya/pochvovedenie'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
