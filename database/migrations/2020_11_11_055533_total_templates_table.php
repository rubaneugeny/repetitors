<?php

use App\Models\TotalTemplates;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TotalTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('total_templates');

        Schema::create('total_templates', function (Blueprint $table) {
            $table->integer('id_total_templates', true, true)->nullable(false);
            $table->integer('id_total_categories', false, true)->nullable(false)->index('id_total_categories')->comment('Категория заголовка');
            $table->text('template')->nullable();
            $table->smallInteger('page', false, true)->nullable(false);

            $table->foreign('id_total_categories', 'total_templates_total_categories')
                ->references('id_total_categories')
                ->on('total_categories')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        if (($f = fopen(__DIR__.'/../dumps/total_templates.csv', 'r')) !== false) {
            while (($data = fgetcsv($f, 0, "\t")) !== false) {
                $item = new TotalTemplates();
                $item->id_total_templates = $data[0];
                $item->id_total_categories = $data[1];
                $item->template = $data[2] != 'NULL' ? $data[2] : null;
                $item->page = $data[3];
                $item->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('total_templates');
    }
}
