<?php

use App\Models\CommentsPrependTextTemplates;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CommentsPrependTextTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('comments_prepend_text_templates');

        Schema::create('comments_prepend_text_templates', function (Blueprint $table) {
            $table->integer('id_comments_prepend_text_templates', true, true)->nullable(false);
            $table->integer('id_comments_prepend_text_categories', false, true)->nullable(false)->index('id_comments_prepend_text_categories');
            $table->integer('id_courses', false, true)->nullable()->index('id_courses');
            $table->string('exclude_courses')->nullable();
            $table->text('template')->nullable(false);
            $table->smallInteger('page', false, true)->nullable(false);
            $table->smallInteger('row_index', false, true)->nullable(false);

            $table->foreign('id_comments_prepend_text_categories', 'comments_prepend_text_templates_comments_prepend_text_categories')
                ->references('id_comments_prepend_text_categories')
                ->on('comments_prepend_text_categories')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('id_courses', 'comments_prepend_text_templates_courses')
                ->references('id_courses')
                ->on('courses')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        if (($f = fopen(__DIR__.'/../dumps/comments_prepend_text_templates.csv', 'r')) !== false) {
            while (($data = fgetcsv($f, 0, "\t")) !== false) {
                $item = new CommentsPrependTextTemplates();
                $item->id_comments_prepend_text_templates = $data[0];
                $item->id_comments_prepend_text_categories = $data[1];
                $item->id_courses = $data[2] != 'NULL' ? $data[2] : null;
                $item->exclude_courses = $data[3] != 'NULL' ? $data[3] : null;
                $item->template = $data[4];
                $item->page = $data[5];
                $item->row_index = $data[6];
                $item->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments_prepend_text_templates');
    }
}
