<?php

use App\Models\PrependTextCategories;
use App\Models\PrependTextTemplates;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FixPrependText extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE `prepend_text_templates` ADD COLUMN `id_courses_categories_second` INT(10) UNSIGNED NULL AFTER `id_prepend_text_categories`");
        DB::statement("ALTER TABLE `prepend_text_templates` ADD COLUMN `id_courses` INT(10) UNSIGNED NULL AFTER `id_courses_categories_second`");

        Schema::table('prepend_text_templates', function (Blueprint $table) {
            $table->foreign('id_courses_categories_second', 'prepend_text_templates_courses_categories_second')
                ->references('id_courses_categories_second')
                ->on('courses_categories_second')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('id_courses', 'prepend_text_templates_courses')
                ->references('id_courses')
                ->on('courses')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        $category = PrependTextCategories::create(['name' => 'metro']);
        PrependTextTemplates::create([
            'id_prepend_text_categories' => $category->id_prepend_text_categories,
            'id_courses' => 1000,
            'template' => 'Если Вам нужны частные уроки по вождению автомобиля {IN_THE_DIRECTION}, но времени осуществлять подбор автоинструктора самостоятельно у Вас нет, Вы можете <a href="../postform.php">написать</a>, какой именно инструктор Вам нужен, и администратор бесплатно подберет подходящие для Вас варианты. Для корректного подбора администратору нужно знать Ваши требования к преподавателю - не так мало преподавателей приглашают на индивидуальные курсы вождения на метро {NAME}, но не все они Вам подойдут.',
            'page' => 1
        ]);
        PrependTextTemplates::create([
            'id_prepend_text_categories' => $category->id_prepend_text_categories,
            'id_courses' => 1001,
            'template' => 'Если Вам нужны частные уроки по вождению автомобиля {IN_THE_DIRECTION}, но времени осуществлять подбор автоинструктора самостоятельно у Вас нет, Вы можете <a href="../postform.php">написать</a>, какой именно инструктор Вам нужен, и администратор бесплатно подберет подходящие для Вас варианты. Для корректного подбора администратору нужно знать Ваши требования к преподавателю - не так мало преподавателей приглашают на индивидуальные курсы вождения на метро {NAME}, но не все они Вам подойдут.',
            'page' => 1
        ]);
        PrependTextTemplates::create([
            'id_prepend_text_categories' => $category->id_prepend_text_categories,
            'id_courses' => 1002,
            'template' => 'Если Вам нужны частные уроки по вождению мотоцикла {IN_THE_DIRECTION}, но времени осуществлять подбор мотоинструктора самостоятельно у Вас нет, Вы можете <a href="../postform.php">написать</a>, какой именно инструктор Вам нужен, и администратор бесплатно подберет подходящие для Вас варианты. Для корректного подбора администратору нужно знать Ваши требования к преподавателю - не так мало преподавателей приглашают на индивидуальные курсы вождения мотоцикла на метро {NAME}, но не все они Вам подойдут.',
            'page' => 1
        ]);
        PrependTextTemplates::create([
            'id_prepend_text_categories' => $category->id_prepend_text_categories,
            'id_courses' => 23,
            'template' => 'Если у Вас нет времени искать репетитора-логопеда самостоятельно, просматривая все анкеты, Вы можете <a href="../postform.php">написать</a>, какой именно Вам нужен логопед (для ребенка или для взрослого, например), и администратор бесплатно подберет подходящий для Вас вариант.',
            'page' => 1
        ]);
        PrependTextTemplates::create([
            'id_prepend_text_categories' => $category->id_prepend_text_categories,
            'id_courses' => 13,
            'template' => 'Если у Вам нужен репетитор начальной школы {IN_THE_DIRECTION} и у Вас нет времени искать его самостоятельно, просматривая все анкеты, Вы можете <a href="../postform.php">написать</a>, какой именно репетитор Вам нужен и администратор бесплатно подберет подходящий для Вас вариант.',
            'page' => 1
        ]);
        PrependTextTemplates::create([
            'id_prepend_text_categories' => $category->id_prepend_text_categories,
            'id_courses' => 63,
            'template' => 'Если у Вам нужен репетитор для подготовки ребенка к школе {IN_THE_DIRECTION} и у Вас нет времени искать его самостоятельно, просматривая все анкеты, Вы можете <a href="../postform.php">написать</a>, какой именно репетитор Вам нужен и администратор бесплатно подберет подходящий для Вас вариант.',
            'page' => 1
        ]);

        PrependTextTemplates::create([
            'id_prepend_text_categories' => $category->id_prepend_text_categories,
            'id_courses_categories_second' => 5,
            'template' => 'Если у Вас нет времени выбирать инструктора по {RECOMMEND_NAME} {IN_THE_DIRECTION} самостоятельно, просматривая все анкеты, Вы можете <a href="../postform.php">написать</a>, какой именно инструктор Вам нужен, и администратор бесплатно подберет Вам подходящие варианты.',
            'page' => 1
        ]);
        PrependTextTemplates::create([
            'id_prepend_text_categories' => $category->id_prepend_text_categories,
            'id_courses_categories_second' => 7,
            'template' => 'Если у Вас нет времени выбирать инструктора по {RECOMMEND_NAME} {IN_THE_DIRECTION} самостоятельно, просматривая все анкеты, Вы можете <a href="../postform.php">написать</a>, какой именно инструктор Вам нужен, и администратор бесплатно подберет Вам подходящие варианты.',
            'page' => 1
        ]);
        PrependTextTemplates::create([
            'id_prepend_text_categories' => $category->id_prepend_text_categories,
            'id_courses_categories_second' => 4,
            'template' => 'Если Вам нужен репетитор по {RECOMMEND_NAME} на м. {NAME} и у Вас нет времени искать его самостоятельно, просматривая все анкеты, Вы можете <a href="../postform.php">написать</a>, какой именно репетитор Вам нужен и администратор <strong>бесплатно</strong> подберет подходящий для Вас вариант.',
            'page' => 1
        ]);
        PrependTextTemplates::create([
            'id_prepend_text_categories' => $category->id_prepend_text_categories,
            'id_courses_categories_second' => 3,
            'template' => 'Если Вам нужен репетитор по {RECOMMEND_NAME_FOURTH} {IN_THE_DIRECTION} и у Вас нет времени искать его самостоятельно, просматривая все анкеты, Вы можете <a href="../postform.php">написать</a>, какой именно репетитор Вам нужен и администратор <strong>бесплатно</strong> подберет подходящий для Вас вариант.',
            'page' => 1
        ]);
        PrependTextTemplates::create([
            'id_prepend_text_categories' => $category->id_prepend_text_categories,
            'id_courses_categories_second' => 1,
            'template' => 'Если Вам нужен репетитор по {RECOMMEND_NAME} {IN_THE_DIRECTION} и у Вас нет времени искать его самостоятельно, просматривая все анкеты, Вы можете <a href="../postform.php">написать</a>, какой именно репетитор Вам нужен и администратор <strong>бесплатно</strong> подберет подходящий для Вас вариант.',
            'page' => 1
        ]);
        PrependTextTemplates::create([
            'id_prepend_text_categories' => $category->id_prepend_text_categories,
            'id_courses_categories_second' => 2,
            'template' => 'Если Вам нужен репетитор {RECOMMEND_NAME_SECOND} {IN_THE_DIRECTION} и у Вас нет времени искать его самостоятельно, просматривая все анкеты, Вы можете <a href="../postform.php">написать</a>, какой именно репетитор Вам нужен и администратор <strong>бесплатно</strong> подберет подходящий для Вас вариант.',
            'page' => 1
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
