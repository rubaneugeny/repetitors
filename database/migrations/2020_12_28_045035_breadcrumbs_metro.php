<?php

use App\Models\BreadcrumbsCategories;
use App\Models\BreadcrumbsTemplates;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BreadcrumbsMetro extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE `breadcrumbs_templates` ADD COLUMN `id_courses` INT(10) UNSIGNED NULL AFTER `id_breadcrumbs_categories`");

        $category = BreadcrumbsCategories::create(['name' => 'metro']);
        BreadcrumbsTemplates::create([
            'id_breadcrumbs_categories' => $category->id_breadcrumbs_categories,
            'id_courses' => 1000,
            'type' => 1,
            'last' => '{NAME}',
            'items' => json_encode([['url' => '/include/avtoinstruktor_obuchenie_vozhdeniju_v_spb.php', 'title' => 'Автоинструкторы']]),
            'page' => 1
        ]);
        BreadcrumbsTemplates::create([
            'id_breadcrumbs_categories' => $category->id_breadcrumbs_categories,
            'id_courses' => 1001,
            'type' => 1,
            'last' => '{NAME}',
            'items' => json_encode([['url' => '/include/avtoinstruktor_obuchenie_vozhdeniju_v_spb.php', 'title' => 'Автоинструкторы']]),
            'page' => 1
        ]);
        BreadcrumbsTemplates::create([
            'id_breadcrumbs_categories' => $category->id_breadcrumbs_categories,
            'id_courses' => 1002,
            'type' => 1,
            'last' => '{NAME}',
            'items' => json_encode([['url' => '/include/motoinstruktor.php', 'title' => 'Мотоинструкторы']]),
            'page' => 1
        ]);
        BreadcrumbsTemplates::create([
            'id_breadcrumbs_categories' => $category->id_breadcrumbs_categories,
            'type' => 1,
            'last' => '{NAME}',// Название метро
            'items' => json_encode([['url' => '/include/{COURSE_SLUG}.php', 'title' => '{COURSE_NAME}']]),
            'page' => 1
        ]);

        BreadcrumbsTemplates::create([
            'id_breadcrumbs_categories' => $category->id_breadcrumbs_categories,
            'id_courses' => 1000,
            'type' => 1,
            'last' => 'Анкеты c {FROM} по {TO}',
            'items' => json_encode([
                ['url' => '/include/avtoinstruktor_obuchenie_vozhdeniju_v_spb.php', 'title' => 'Автоинструкторы'],
                ['url' => '/include/{SLUG}.php', 'title' => '{NAME}']
            ]),
            'page' => 0
        ]);
        BreadcrumbsTemplates::create([
            'id_breadcrumbs_categories' => $category->id_breadcrumbs_categories,
            'id_courses' => 1001,
            'type' => 1,
            'last' => 'Анкеты c {FROM} по {TO}',
            'items' => json_encode([
                ['url' => '/include/avtoinstruktor_obuchenie_vozhdeniju_v_spb.php', 'title' => 'Автоинструкторы'],
                ['url' => '/include/{SLUG}.php', 'title' => '{NAME}']
            ]),
            'page' => 0
        ]);
        BreadcrumbsTemplates::create([
            'id_breadcrumbs_categories' => $category->id_breadcrumbs_categories,
            'id_courses' => 1002,
            'type' => 1,
            'last' => 'Анкеты c {FROM} по {TO}',
            'items' => json_encode([
                ['url' => '/include/motoinstruktor.php', 'title' => 'Мотоинструкторы'],
                ['url' => '/include/{SLUG}.php', 'title' => '{NAME}']
            ]),
            'page' => 0
        ]);
        BreadcrumbsTemplates::create([
            'id_breadcrumbs_categories' => $category->id_breadcrumbs_categories,
            'type' => 1,
            'last' => 'Анкеты c {FROM} по {TO}',
            'items' => json_encode([
                ['url' => '/include/{COURSE_SLUG}.php', 'title' => '{COURSE_NAME}'],
                ['url' => '/include/{SLUG}.php', 'title' => '{NAME}']
            ]),
            'page' => 0
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
