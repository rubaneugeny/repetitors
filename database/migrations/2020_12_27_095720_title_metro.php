<?php

use App\Models\TitleCategories;
use App\Models\TitleTemplates;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TitleMetro extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $category = TitleCategories::create(['name' => 'title_metro']);
        TitleTemplates::create([
            'id_title_categories' => $category->id_title_categories,
            'page' => 1,
            'id_courses' => 1000,
            'title' => 'Инструкторы по вождению &#128663; {IN_THE_DIRECTION}. Автоинструкторы {IN_THE_DIRECTION} - "СПбРепетитор"',
            'description' => 'Частные автоинструкторы по вождению {IN_THE_DIRECTION}. АКПП, МКПП. &#11088;Отзывы, цены, бесплатный и быстрый поиск!&#11088; Уроки вождения для учеников любого уровня. Обучение вождению {IN_THE_DIRECTION} на машине ученика или инструктора.'
        ]);
        TitleTemplates::create([
            'id_title_categories' => $category->id_title_categories,
            'page' => 0,
            'id_courses' => 1000,
            'title' => 'Автоинстукторы в районе метро {IN_THE_DIRECTION} (анкеты с {FROM} по {TO})',
            'description' => ''
        ]);

        TitleTemplates::create([
            'id_title_categories' => $category->id_title_categories,
            'page' => 1,
            'id_courses' => 1001,
            'title' => 'Инструкторы по вождению &#128663; {IN_THE_DIRECTION}. Автоинструкторы {IN_THE_DIRECTION} - "СПбРепетитор"',
            'description' => 'Частные автоинструкторы по вождению {IN_THE_DIRECTION}. АКПП, МКПП. &#11088;Отзывы, цены, бесплатный и быстрый поиск!&#11088; Уроки вождения для учеников любого уровня. Обучение вождению {IN_THE_DIRECTION} на машине ученика или инструктора.'
        ]);
        TitleTemplates::create([
            'id_title_categories' => $category->id_title_categories,
            'page' => 0,
            'id_courses' => 1001,
            'title' => 'Автоинстукторы в районе метро {IN_THE_DIRECTION} (анкеты с {FROM} по {TO})',
            'description' => ''
        ]);

        TitleTemplates::create([
            'id_title_categories' => $category->id_title_categories,
            'page' => 1,
            'id_courses' => 1002,
            'title' => 'Мотоинстукторы на м. {NAME}. Обучение вождению мотоцикла, уроки и курсы вождения мотоцикла на метро {NAME}',
            'description' => ''
        ]);
        TitleTemplates::create([
            'id_title_categories' => $category->id_title_categories,
            'page' => 0,
            'id_courses' => 1002,
            'title' => 'Мотоинструкторы в районе метро {NAME} (анкеты с {FROM} по {TO})',
            'description' => ''
        ]);

        TitleTemplates::create([
            'id_title_categories' => $category->id_title_categories,
            'page' => 1,
            'id_courses' => 23,
            'title' => 'Логопеды {IN_THE_DIRECTION}, &#128270; дефектологи. Занятия с логопедом {IN_THE_DIRECTION} - "СПбРепетитор"',
            'description' => 'Частные логопеды - метро {NAME}. &#11088;Отзывы, цены, бесплатный и удобный поиск!&#11088; Большой выбор специализаций. Логопеды для детей и взрослых.'
        ]);
        TitleTemplates::create([
            'id_title_categories' => $category->id_title_categories,
            'page' => 0,
            'id_courses' => 23,
            'title' => 'Логопеды в районе метро {NAME} (анкеты с {FROM} по {TO})',
            'description' => 'Преподаватели по {RECOMMEND_NAME} - {NAME} (анкета с {FROM} по {TO}).'
        ]);

        TitleTemplates::create([
            'id_title_categories' => $category->id_title_categories,
            'page' => 1,
            'id_courses' => 13,
            'title' => 'Репетиторы начальных классов {IN_THE_DIRECTION}. Репетиторы начальной школы на метро {NAME}',
            'description' => 'Лучшие репетиторы по начальной школе {IN_THE_DIRECTION}. Отзывы, цены. Преподаватели начальных классов {IN_THE_DIRECTION}.'
        ]);
        TitleTemplates::create([
            'id_title_categories' => $category->id_title_categories,
            'page' => 0,
            'id_courses' => 13,
            'title' => 'Репетиторы начальных классов {IN_THE_DIRECTION} (анкеты с {FROM} по {TO})',
            'description' => 'Преподаватели по {RECOMMEND_NAME} - {NAME} (анкеты с {FROM} по {TO}).'
        ]);

        TitleTemplates::create([
            'id_title_categories' => $category->id_title_categories,
            'page' => 1,
            'id_courses' => 63,
            'title' => 'Репетиторы для подготовки к школе {IN_THE_DIRECTION}. Репетиторы по подготовке ребенка к школе на метро {NAME}',
            'description' => 'Лучшие репетиторы по подготовке ребенка к школе {IN_THE_DIRECTION}. Отзывы, цены. Преподаватели по подготовке к школе {IN_THE_DIRECTION}.'
        ]);
        TitleTemplates::create([
            'id_title_categories' => $category->id_title_categories,
            'page' => 0,
            'id_courses' => 63,
            'title' => 'Репетиторы по подготовке к школе {IN_THE_DIRECTION} (анкеты с {FROM} по {TO})',
            'description' => 'Преподаватели по {RECOMMEND_NAME} - {NAME} (анкеты с {FROM} по {TO})'
        ]);

        TitleTemplates::create([
            'id_title_categories' => $category->id_title_categories,
            'id_courses_categories_second' => 5,
            'page' => 1,
            'title' => '{COURSE_NAME} {IN_THE_DIRECTION}, {SEO_EMOJI} занятия {RECOMMEND_NAME_THIRD}, тренеры по {RECOMMEND_NAME} {IN_THE_DIRECTION} - "СПбРепетитор"',
            'description' => 'Частные тренеры по {RECOMMEND_NAME} {IN_THE_DIRECTION}. &#11088;Отзывы, цены, бесплатный и быстрый поиск!&#11088; Занятия {RECOMMEND_NAME_THIRD} {IN_THE_DIRECTION}. Более 20 000 анкет в базе!'
        ]);
        TitleTemplates::create([
            'id_title_categories' => $category->id_title_categories,
            'id_courses_categories_second' => 5,
            'page' => 0,
            'title' => 'Тренеры по {RECOMMEND_NAME} в районе метро {NAME} (анкеты с {FROM} по {TO})',
            'description' => 'Преподаватели по {RECOMMEND_NAME} - {NAME} (анкеты с {FROM} по {TO}).'
        ]);
        TitleTemplates::create([
            'id_title_categories' => $category->id_title_categories,
            'id_courses_categories_second' => 7,
            'page' => 1,
            'title' => '{COURSE_NAME} {IN_THE_DIRECTION}, {SEO_EMOJI} занятия {RECOMMEND_NAME_THIRD}, тренеры по {RECOMMEND_NAME} {IN_THE_DIRECTION} - "СПбРепетитор"',
            'description' => 'Частные тренеры по {RECOMMEND_NAME} {IN_THE_DIRECTION}. &#11088;Отзывы, цены, бесплатный и быстрый поиск!&#11088; Занятия {RECOMMEND_NAME_THIRD} {IN_THE_DIRECTION}. Более 20 000 анкет в базе!'
        ]);
        TitleTemplates::create([
            'id_title_categories' => $category->id_title_categories,
            'id_courses_categories_second' => 7,
            'page' => 0,
            'title' => 'Тренеры по {RECOMMEND_NAME} в районе метро {NAME} (анкеты с {FROM} по {TO})',
            'description' => 'Преподаватели по {RECOMMEND_NAME} - {NAME} (анкеты с {FROM} по {TO}).'
        ]);

        TitleTemplates::create([
            'id_title_categories' => $category->id_title_categories,
            'id_courses_categories_second' => 3,
            'page' => 1,
            'title' => 'Репетиторы по {RECOMMEND_NAME} {SEO_EMOJI} на метро {NAME}. Уроки {RECOMMEND_NAME_SECOND} {IN_THE_DIRECTION}. Курсы {RECOMMEND_NAME_SECOND} {IN_THE_DIRECTION} - "СПбРепетитор"',
            'description' => 'Опытные репетиторы по {RECOMMEND_NAME} {IN_THE_DIRECTION}. &#11088;Отзывы, цены, бесплатный и удобный поиск!&#11088; Курсы и уроки {RECOMMEND_NAME_SECOND} {IN_THE_DIRECTION} для детей и взрослых с опытными преподавателями.'
        ]);
        TitleTemplates::create([
            'id_title_categories' => $category->id_title_categories,
            'id_courses_categories_second' => 3,
            'page' => 0,
            'title' => 'Репетиторы по {RECOMMEND_NAME} в районе метро {NAME} (анкеты с {FROM} по {TO})',
            'description' => 'Преподаватели по {RECOMMEND_NAME} - {NAME} (анкеты с {FROM} по {TO}).'
        ]);

        TitleTemplates::create([
            'id_title_categories' => $category->id_title_categories,
            'id_courses_categories_second' => 4,
            'page' => 1,
            'title' => 'Репетиторы по {RECOMMEND_NAME} {IN_THE_DIRECTION}',
            'description' => 'Опытные репетиторы по {RECOMMEND_NAME} {IN_THE_DIRECTION}. Отзывы, цены, бесплатный поиск и быстрый подбор преподавателя.'
        ]);
        TitleTemplates::create([
            'id_title_categories' => $category->id_title_categories,
            'id_courses_categories_second' => 4,
            'page' => 2,
            'title' => 'Преподаватели {RECOMMEND_NAME_SECOND} {IN_THE_DIRECTION}',
            'description' => ''
        ]);
        TitleTemplates::create([
            'id_title_categories' => $category->id_title_categories,
            'id_courses_categories_second' => 4,
            'page' => 3,
            'title' => 'Учителя по {RECOMMEND_NAME} {IN_THE_DIRECTION}',
            'description' => ''
        ]);
        TitleTemplates::create([
            'id_title_categories' => $category->id_title_categories,
            'id_courses_categories_second' => 4,
            'page' => 0,
            'title' => 'Репетиторы {RECOMMEND_NAME_SECOND} в районе метро {NAME} (анкеты с {FROM} по {TO})',
            'description' => 'Преподаватели по {RECOMMEND_NAME} - {NAME} (анкеты с {FROM} по {TO}).'
        ]);

        TitleTemplates::create([
            'id_title_categories' => $category->id_title_categories,
            'id_courses_categories_second' => 1,
            'page' => 1,
            'title' => 'Репетиторы по {RECOMMEND_NAME} {IN_THE_DIRECTION} {SEO_EMOJI} - "СПбРепетитор"',
            'description' => 'Опытные репетиторы по {RECOMMEND_NAME} {IN_THE_DIRECTION}. &#11088;Подготовка к ЕГЭ, ОГЭ по {RECOMMEND_NAME}!&#11088; Отзывы, цены, бесплатный поиск и быстрый подбор.'
        ]);
        TitleTemplates::create([
            'id_title_categories' => $category->id_title_categories,
            'id_courses_categories_second' => 1,
            'page' => 2,
            'title' => 'Преподаватели {RECOMMEND_NAME_SECOND} {IN_THE_DIRECTION}',
            'description' => ''
        ]);
        TitleTemplates::create([
            'id_title_categories' => $category->id_title_categories,
            'id_courses_categories_second' => 1,
            'page' => 3,
            'title' => 'Учителя по {RECOMMEND_NAME} {IN_THE_DIRECTION}',
            'description' => ''
        ]);
        TitleTemplates::create([
            'id_title_categories' => $category->id_title_categories,
            'id_courses_categories_second' => 1,
            'page' => 4,
            'title' => 'Индивидуальные занятия по {RECOMMEND_NAME} {IN_THE_DIRECTION}',
            'description' => ''
        ]);
        TitleTemplates::create([
            'id_title_categories' => $category->id_title_categories,
            'id_courses_categories_second' => 1,
            'page' => 5,
            'title' => 'Частные уроки {RECOMMEND_NAME_SECOND} {IN_THE_DIRECTION}',
            'description' => ''
        ]);
        TitleTemplates::create([
            'id_title_categories' => $category->id_title_categories,
            'id_courses_categories_second' => 1,
            'page' => 0,
            'title' => 'Репетиторы {RECOMMEND_NAME_SECOND} в районе метро {NAME} (анкеты с {FROM} по {TO})',
            'description' => 'Преподаватели по {RECOMMEND_NAME} - {NAME} (анкеты с {FROM} по {TO}).'
        ]);

        TitleTemplates::create([
            'id_title_categories' => $category->id_title_categories,
            'id_courses_categories_second' => 2,
            'page' => 1,
            'title' => 'Репетиторы по {RECOMMEND_NAME} на метро {NAME}{SEO_EMOJI}. Курсы {RECOMMEND_NAME_SECOND} {IN_THE_DIRECTION} - "СПбРепетитор"',
            'description' => 'Частные репетиторы {RECOMMEND_NAME_SECOND} {IN_THE_DIRECTION}. &#11088;Проверенные отзывы, адекватные цены!{SEO_EMOJI} Курсы {RECOMMEND_NAME_SECOND} {IN_THE_DIRECTION} для детей и взрослых с опытными преподавателями. Подготовка к ЕГЭ по {RECOMMEND_NAME}.'
        ]);
        TitleTemplates::create([
            'id_title_categories' => $category->id_title_categories,
            'id_courses_categories_second' => 2,
            'page' => 0,
            'title' => 'Репетиторы по {RECOMMEND_NAME} в районе метро {NAME} (анкеты с {FROM} по {TO})',
            'description' => 'Преподаватели по {RECOMMEND_NAME} - {NAME} (анкеты с {FROM} по {TO}).'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
