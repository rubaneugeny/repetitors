<?php

use App\Models\CountriesCities;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CountriesCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('countries_cities');

        Schema::create('countries_cities', function (Blueprint $table) {
            $table->integer('id_countries_cities', true, true)->nullable(false);
            $table->integer('id_countries', false, true)->nullable(false)->index('id_countries');
            $table->string('name')->nullable(false);
            $table->string('url')->nullable(false);

            $table->foreign('id_countries', 'countries_cities_countries')->references('id_countries')->on('countries')->onDelete('cascade')->onUpdate('cascade');
        });

        if (($f = fopen(__DIR__.'/../dumps/countries_cities.csv', 'r')) !== false) {
            while (($data = fgetcsv($f, 0, "\t")) !== false) {
                $city = new CountriesCities();
                $city->id_countries_cities = $data[0];
                $city->id_countries = $data[1];
                $city->name = $data[2];
                $city->url = $data[3];
                $city->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countries_cities');
    }
}
