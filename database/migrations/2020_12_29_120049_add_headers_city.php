<?php

use App\Models\HeadersCategories;
use App\Models\HeadersTemplates;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddHeadersCity extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $category = HeadersCategories::create(['name' => 'city']);
        HeadersTemplates::create([
            'id_headers_categories' => $category->id_headers_categories,
            'id_courses' => 1000,
            'template' => 'Инструкторы по вождению {IN_THE_DIRECTION}',
            'page' => 0
        ]);
        HeadersTemplates::create([
            'id_headers_categories' => $category->id_headers_categories,
            'id_courses' => 1001,
            'template' => 'Инструкторы по вождению {IN_THE_DIRECTION}',
            'page' => 0
        ]);
        HeadersTemplates::create([
            'id_headers_categories' => $category->id_headers_categories,
            'id_courses' => 1002,
            'template' => 'Мотоинструкторы {IN_THE_DIRECTION}',
            'page' => 0
        ]);
        HeadersTemplates::create([
            'id_headers_categories' => $category->id_headers_categories,
            'id_courses' => 23,
            'template' => 'Частные логопеды {IN_THE_DIRECTION}',
            'page' => 0
        ]);
        HeadersTemplates::create([
            'id_headers_categories' => $category->id_headers_categories,
            'id_courses' => 13,
            'template' => 'Репетиторы начальных классов в городе {NAME}',
            'page' => 0
        ]);
        HeadersTemplates::create([
            'id_headers_categories' => $category->id_headers_categories,
            'id_courses' => 63,
            'template' => 'Репетиторы для подготовки к школе в городе {NAME}',
            'page' => 0
        ]);

        HeadersTemplates::create([
            'id_headers_categories' => $category->id_headers_categories,
            'id_courses_categories_second' => 5,
            'template' => 'Частные тренеры по {RECOMMEND_NAME} {IN_THE_DIRECTION}',
            'page' => 0
        ]);
        HeadersTemplates::create([
            'id_headers_categories' => $category->id_headers_categories,
            'id_courses_categories_second' => 7,
            'template' => 'Частные тренеры по {RECOMMEND_NAME} {IN_THE_DIRECTION}',
            'page' => 0
        ]);

        foreach ([1, 4] as $value) {
            HeadersTemplates::create([
                'id_headers_categories' => $category->id_headers_categories,
                'id_courses_categories_second' => $value,
                'template' => 'Репетиторы по {RECOMMEND_NAME} в городе {NAME}',
                'page' => 1
            ]);
            HeadersTemplates::create([
                'id_headers_categories' => $category->id_headers_categories,
                'id_courses_categories_second' => $value,
                'template' => 'Преподаватели {RECOMMEND_NAME_SECOND} в городе {NAME}',
                'page' => 2
            ]);
            HeadersTemplates::create([
                'id_headers_categories' => $category->id_headers_categories,
                'id_courses_categories_second' => $value,
                'template' => 'Учителя {RECOMMEND_NAME_SECOND} в городе {NAME}',
                'page' => 3
            ]);
            HeadersTemplates::create([
                'id_headers_categories' => $category->id_headers_categories,
                'id_courses_categories_second' => $value,
                'template' => 'Репетиторы {RECOMMEND_NAME_SECOND} в городе {NAME}',
                'page' => 0
            ]);
        }

        HeadersTemplates::create([
            'id_headers_categories' => $category->id_headers_categories,
            'id_courses_categories_second' => 2,
            'template' => 'Репетиторы по {RECOMMEND_NAME} в городе {NAME}',
            'page' => 1
        ]);
        HeadersTemplates::create([
            'id_headers_categories' => $category->id_headers_categories,
            'id_courses_categories_second' => 2,
            'template' => 'Репетиторы {RECOMMEND_NAME_SECOND} в городе {NAME}',
            'page' => 0
        ]);

        HeadersTemplates::create([
            'id_headers_categories' => $category->id_headers_categories,
            'id_courses_categories_second' => 3,
            'template' => 'Репетиторы по {RECOMMEND_NAME} в городе {NAME}',
            'page' => 1
        ]);
        HeadersTemplates::create([
            'id_headers_categories' => $category->id_headers_categories,
            'id_courses_categories_second' => 3,
            'template' => 'Репетиторы по {RECOMMEND_NAME} в городе {NAME}',
            'page' => 0
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
