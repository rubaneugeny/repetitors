<?php

use App\Models\Blog;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Carbon\Carbon;

class BlogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('texty')) {
            DB::statement('ALTER TABLE `texty` ENGINE = InnoDB');
            DB::statement('ALTER TABLE `texty` CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci');

            Schema::rename('texty', 'blog');
        }

        Schema::table('blog', function (Blueprint $table) {
            $table->renameColumn('id_text', 'id_blog');
            $table->renameColumn('tip', 'column');
            $table->renameColumn('foto', 'photo');
            $table->renameColumn('descr', 'description');
            $table->renameColumn('url', 'slug');
            $table->renameColumn('kroha', 'breadcrumbs');
            $table->renameColumn('h1', 'header');
            $table->timestamps();
        });

        $blogs = Blog::all();

        foreach ($blogs ?? [] as $blog) {
            $blog->date = Carbon::createFromFormat('d.m.Y', $blog->date)->format('Y-m-d');

            $blog->description = trim($blog->description);
            $blog->breadcrumbs = trim($blog->breadcrumbs);
            $blog->header = trim($blog->header);
            $blog->title = trim($blog->title);
            $blog->title = trim($blog->title);
            $blog->text = trim($blog->text);

            if ($blog->photo == '0') {
                $blog->photo = null;
            } else {
                $blog->photo = str_replace('photo_news/', '', $blog->photo);
            }

            if ($blog->description == '') {
                $blog->description = null;
            }

            $blog->save();
        }

        DB::statement("ALTER TABLE `blog` MODIFY COLUMN `id_blog` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT");
        DB::statement("ALTER TABLE `blog` MODIFY COLUMN `column` SMALLINT(5) UNSIGNED NOT NULL COMMENT 'Разделение на колонки' AFTER `id_blog`");
        DB::statement("ALTER TABLE `blog` MODIFY COLUMN `date` DATE NOT NULL AFTER `column`");
        DB::statement("ALTER TABLE `blog` MODIFY COLUMN `slug` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_unicode_ci' AFTER `date`");
        DB::statement("ALTER TABLE `blog` MODIFY COLUMN `breadcrumbs` VARCHAR(255) NOT NULL COMMENT 'Хлебные крошки' COLLATE 'utf8mb4_unicode_ci' AFTER `slug`");
        DB::statement("ALTER TABLE `blog` MODIFY COLUMN `header` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_unicode_ci' AFTER `breadcrumbs`");
        DB::statement("ALTER TABLE `blog` MODIFY COLUMN `title` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_unicode_ci' AFTER `header`");
        DB::statement("ALTER TABLE `blog` MODIFY COLUMN `text` TEXT NOT NULL COLLATE 'utf8mb4_unicode_ci' AFTER `title`");
        DB::statement("ALTER TABLE `blog` MODIFY COLUMN `photo` VARCHAR(255) NULL COLLATE 'utf8mb4_unicode_ci' AFTER `text`");
        DB::statement("ALTER TABLE `blog` MODIFY COLUMN `description` TEXT NULL COLLATE 'utf8mb4_unicode_ci' AFTER `photo`");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blog');
    }
}
