<?php

use App\Models\Courses;
use App\Models\TitleCategories;
use App\Models\TitleTemplates;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FixAkpp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $category = TitleCategories::create(['name' => 'title_akpp']);
        TitleTemplates::create([
            'id_title_categories' => $category->id_title_categories,
            'page' => 1,
            'title' => 'Инструкторы по вождению на "автомате", &#128663; автоинструкторы на АКПП в Санкт-Петербурге - "СПбРепетитор"',
            'description' => 'Частные инструкторы по вождению с АКПП в Санкт-Петербурге и Области. Недорого. &#11088;Проверенные отзывы, адекватные цены, рейтинг!&#11088; База автоинструкторов на автомате.'
        ]);
        TitleTemplates::create([
            'id_title_categories' => $category->id_title_categories,
            'page' => 0,
            'title' => 'Автоинструкторы АКПП (анкеты с {FROM}-{TO}) - "СПбРепетитор"',
            'description' => 'Автоинструкторы на АКПП (анкеты с {FROM}-{TO})'
        ]);
        $course = Courses::find(Courses::AUTOMATIC_TRANSMISSION);
        $course->id_title_categories = $category->id_title_categories;
        $course->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
