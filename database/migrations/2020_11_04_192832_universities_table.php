<?php

use App\Models\Universities;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UniversitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('vuz')) {
            DB::statement('ALTER TABLE vuz ENGINE = InnoDB');
            DB::statement('ALTER TABLE vuz CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci');

            Schema::rename('vuz', 'universities');
        }

        Schema::table('universities', function (Blueprint $table) {
            $table->timestamps();
            $table->renameColumn('id_vuz', 'id_universities');
            $table->renameColumn('url', 'slug');
            $table->renameColumn('name', 'short_name');
            $table->renameColumn('long', 'name');
        });

        $universities = Universities::all();

        foreach ($universities ?? [] as $university) {
            $university->slug = trim($university->slug);
            $university->name = trim($university->name);
            $university->short_name = trim($university->short_name);
            $university->save();
        }

        DB::statement("ALTER TABLE `universities` MODIFY COLUMN `id_universities` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT");
        DB::statement("ALTER TABLE `universities` MODIFY COLUMN `slug` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_unicode_ci' AFTER `id_universities`");
        DB::statement("ALTER TABLE `universities` MODIFY COLUMN `name` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_unicode_ci' AFTER `slug`");
        DB::statement("ALTER TABLE `universities` MODIFY COLUMN `short_name` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_unicode_ci' AFTER `name`");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('universities');
    }
}
