<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FixBreadcrumbs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE `breadcrumbs_templates` MODIFY COLUMN `items` LONGTEXT NOT NULL COMMENT 'JSON'");
        DB::statement("ALTER TABLE `breadcrumbs_templates` MODIFY COLUMN `type` SMALLINT(6) NULL COMMENT 'Тип: 1 - репетитор, 2 - автоинструктор, null - убрать показ каталога'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
