<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Admins;

class AdminTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('admin')) {
            DB::statement('ALTER TABLE admin ENGINE = InnoDB');
            DB::statement('ALTER TABLE admin CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci');

            Schema::rename('admin', 'admins');
        }

        Schema::table('admins', function (Blueprint $table) {
            $table->timestamps();
            $table->renameColumn('id_admin', 'id_admins');
            $table->renameColumn('tel', 'phone');
            $table->renameColumn('mail', 'email');
            $table->dropIndex('name');
            $table->dropIndex('tel');
            $table->dropIndex( 'mail');
            $table->dropUnique('id_admin');
            $table->primary('id_admins');
        });

        Admins::where('id_admins', 0)->delete();
        $admins = Admins::all();

        foreach ($admins ?? [] as $admin) {
            $admin->name = trim($admin->name);
            $admin->phone = trim($admin->phone);
            $admin->email = trim($admin->email);

            if (empty($admin->email)) {
                $admin->email = null;
            }

            if (empty($admin->phone)) {
                $admin->phone = null;
            }

            $admin->save();
        }

        Schema::table('admins', function (Blueprint $table) {
            $table->integer('id_admins', true, true)->nullable(false)->default(null)->change();
            $table->string('name')->collation('utf8mb4_unicode_ci')->nullable(false)->change();
            $table->string('phone', 20)->collation('utf8mb4_unicode_ci')->comment('Телефон')->nullable()->change();
            $table->string('email', 40)->collation('utf8mb4_unicode_ci')->comment('E-mail')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins');
    }
}
