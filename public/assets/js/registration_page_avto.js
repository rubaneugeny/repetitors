$(document).ready(function () {

    $("#circle").circleProgress({
        value: 0,
        startAngle: -1.60,
        size: 133,
        reverse: true,
        thickness: 3,
        fill: {
            color: "#a24077"
        }
    }).on("circle-animation-progress", function (event, progress, stepValue) {
        var znach = stepValue * 100;
        $(this).find(".num").text(znach.toFixed(0));
    }).show();

    //Вывод фильтров предмета
    $(document).on("change", ".lern_select", function (e) {

        var id_lern = $(this).val(),
            $fltr_b = $(this).closest(".regist_form_col_lern").find(".regist_form_row_fltr");

        $fltr_b.html("<div class=\"form_preloader form_preloader_small\"><div></div></div>");

        $.ajax({
            type: "POST",
            url: "/h_regist_fltr_ajax.php",
            data: {id_lern: id_lern},
            success: function (data) {
                $fltr_b.html(data);
            },
            error: function (data) {
                $fltr_b.html("<div class=\"lern_select_refresh regist_link\">Ошибка подключения, <a href=\"javasctipt:;\" class=\"lern_select_refresh\">повротить.</a></div>");
            }
        });

    });
    $("#content").find(".lern_select").trigger("change");

    //Рефреш списка фильтров
    $(document).on("click", ".lern_select_refresh", function () {
        lern_b = $(this).closest(".regist_form_col_lern").find("select").change();
    });


    //Контроль изменения input[type="file"]
    $(".regist_file input[type=\"file\"]").change(function (e) {

        var fileName = "",
            block = $(this).closest(".regist_file");


        if (e.target.value) fileName = $(this)[0].files[0].name;

        var lls1 = fileName.length,
            lls2 = lls1 - fileName.lastIndexOf("."),
            lls3 = lls1 - lls2;

        if (lls3 > 11) {
            fileName = fileName.substr(0, 8) + "..." + fileName.substr(1 - lls2);
        }

        $(block).find("div").html(fileName);

    });


    //Показать еще
    $(document).on("click", ".regist_form_col_show", function () {

        var addlern = $(this).closest(".regist_form").find(".regist_form_col_hide");

        if (addlern.length > 0) {
            addlern.first().removeClass("regist_form_col_hide").addClass("last").prev().removeClass("last");

            if (addlern.length == 1)
                $(this).closest("div").hide();
        }

        return false;
    });


    //Данные для проверки формы
    var currentFullYear = new Date().getFullYear(),
        active_step = 1,
        fndubl = ["", "", ""],
        progress_value = 0,
        progress_step = {

            "1": {
                "step_summ": 0,
                "name": {"r1": 0.05, "s": 0, "t": "Введите имя", "ts": 0},
                "lname": {"r1": 0.05, "s": 0, "t": "Введите фамилию", "ts": 0},
                "oname": {"r1": 0.05, "s": 0, "t": "Введите отчество", "ts": 0},
                "pol": {"r1": 0.05, "s": 1, "ts": 0},
                "year": {
                    "r1": 0.05,
                    "s": 0,
                    "t": "Введите год рождения в диапазоне от 1900 до " + currentFullYear,
                    "ts": 0
                },
                "phone": {"r1": 0.05, "s": 0, "t": "Телефон введен некорректно", "ts": 0},
                "mail": {"r1": 0.05, "s": 0, "t": "Вы ввели некорректный e-mail", "ts": 0},
                "login_skype": {"r1": 0, "s": 0, "t": "", "ts": 0},
                "photo_profile": {"r1": 0.05, "s": 0, "t": "", "ts": 0},
                "repeat_fio": {"r1": 0, "s": 0, "t": "Фамилия, имя и отчество не могут совпадать.", "ts": 0}
            },
            "2": {
                "step_summ": 0,
                "korobka": {"r1": 0.05, "s": 0, "t": "Выберите коробку передач", "ts": 0},
                "rep_avto": {"r1": 0.05, "s": 0, "t": "Выберите автомобиль", "ts": 0},
                "nami1": {"r1": 0.05, "s": 0, "t": "", "ts": 0}
            },
            "3": {
                "step_summ": 0,
                "list_r_id": {"r1": 0.05, "s": 0, "t": "", "ts": 0},
                "price": {"r1": 0.05, "s": 0, "t": "Введите стоимость одного часа занятия", "ts": 0},
                "id_gorod": {"r1": 0.05, "s": 1, "t": "", "ts": 0}
            },
            "4": {
                "step_summ": 0,
                "stag_avto": {"r1": 0.05, "s": 0, "t": "Введите стаж вождения", "ts": 0},
                "lic": {"r1": 0.05, "s": 1, "t": "", "ts": 0},
                "vremya": {"r1": 0.05, "s": 0, "t": "", "ts": 0},
                "stag_lern": {"r1": 0.05, "s": 0, "t": "Введите преподавательский стаж", "ts": 0},
                "avto_pupil": {"r1": 0.05, "s": 1, "t": "", "ts": 0},
                "o_sebe": {"r1": 0.05, "s": 0, "t": "", "ts": 0}
            }
        };


    currentFullYear ? currentFullYear -= 13 : currentFullYear = 2019;


    //Контроль изменения элементов формы
    $(document).on("change", ".regist form input, .regist form textarea, .regist form select", function () {

        if ($(this).attr("name") == "select_search")
            return false;

        var this_n = $(this).attr("data-rch");
        checkregistrform(active_step, 0, this_n, $(this));
    });

    //Навигация по шагам
    $(document).on("click", ".regist_form_navigation a, .regist_name", function () {

        var $tjs = $("#content").find(".regist"),
            formData;

        if ($(this).hasClass("send")) {//Если send

            if (!checkregistrform(active_step, 0))
                return false;

            $(this).closest(".regist_active").removeClass(".regist_active");
            $tjs.prepend("<div class=\"regist_send_fon\"><div class=\"form_preloader\"><div></div></div></div>");

            var t_contentType;

            if (isIE() && isIE() <= 9) {
                formData = $tjs.find("form").serialize();
                t_contentType = "application/x-www-form-urlencoded; charset=UTF-8";
            } else {
                t_contentType = false;
                formData = new FormData($tjs.find("form")[0]);

            }

            $.ajax({
                type: "POST",
                url: "/include/add_avto_new.php",
                data: formData,
                dataType: "json",
                cache: false,
                contentType: t_contentType,
                processData: false,
                success: function (data) {

                    console.log(data);
                    $tjs.find(".regist_send_fon").remove(".regist_send_fon");

                    if (data.error) {

                        if (data.error == 1) {

                            active_block = $tjs.find(data.field).addClass("registr_error_input").closest(".regist_block");
                            active_step = Number($(active_block).attr("data-step"));
                            step_show_hide($tjs, active_step); //переключение на шаг

                            $(active_block).find(".callback_error").html("<ul><li>" + data.text + "</li></ul>");

                        } else if (data.error == 2) {

                            regist_error_send($tjs, data.text);

                        }

                    } else if (data.success) {
                        $("#content").find(".lead").find(".s_text_info, h4, .regist_link").remove();
                        $("#content").find(".main").html(data.text);
                    }

                },
                error: function (data) {
                    $tjs.find(".regist_send_fon").remove(".regist_send_fon");
                    regist_error_send($tjs, "Что-то пошло не так. Пожалуйста, попробуйте еще раз.");
                }
            });

        } else if ($(this).hasClass("prev")) {//Если prev

            active_step -= 1;

        } else if ($(this).hasClass("next")) {//Если next

            if (!checkregistrform(active_step, 0))
                return false;

            active_step += 1;

        } else if ($(this).hasClass("other")) { //Если otherlink

            return true;

        } else {//Если клик по шагу

            var this_step = Number($(this).closest(".regist_block").attr("data-step"));

            if (this_step == active_step)
                return false;
            else
                active_step = this_step;
        }

        checkregistrform(active_step, 1);

        if (active_step <= 0) active_step = 1;

        step_show_hide($tjs, active_step); //переключение на шаг


    });

    //Версия IE
    function isIE() {
        var myNav = navigator.userAgent.toLowerCase();
        return (myNav.indexOf("msie") != -1) ? parseInt(myNav.split("msie")[1]) : false;
    }

    //Ошибка 2 send ajax
    function regist_error_send(form, text) {
        form.find(".regist_block").last().find(".callback_error").html("<ul><li>" + text + "</li></ul>");
        step_show_hide(form, 5);
    }

    //Статус рейтинга
    function status_check(f, step, n, s) {

        progress_step[step][n]["s"] = s;

        if (progress_step[step][n]["t"]) {
            if (s) {
                progress_step[step][n]["ts"] = 0;
                f.removeClass("registr_error_input");
            } else {
                progress_step[step][n]["ts"] = 1;
                f.addClass("registr_error_input");
            }
        }

    }

    //Скрытие/открытие шага
    function step_show_hide(block, step) {

        block.find(".regist_block").each(function () {

            if ($(this).attr("data-step") == step) {

                $(this).addClass("regist_active");
                $(window).scrollTop($(this).offset().top - 50);
            } else
                $(this).removeClass("regist_active");

        });

    }

    //Проверка полей регистрации
    function checkregistrform(t_step, no_error, cn, ct) {

        var $tjs = $("#content").find(".regist"),
            scrollTop = $tjs.offset().top,
            error = "",
            f, //поле
            fname, //имя поля
            fval, //знач поля
            fp1 = /^\s*$/, //Фио
            fp2 = !/^\+7 \(\d{3}\) \d{3}\-\d{2}\-\d{2}$/, //Телефон
            fp3 = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,6}\.)?[a-z]{2,6}$/i, //Почта
            fs; //статус поля

        if (!cn) $tjs.find(".registr_error_input").removeClass("registr_error_input");

        //Первый шаг
        if (active_step == 1) {

            if (!no_error) {

                //START - Проверка ФИО
                fname = "name";
                if (!cn || cn == fname) {
                    cn ? f = ct : f = $tjs.find("[data-rch=" + fname + "]");
                    fval = f.val().toLowerCase();

                    fp1.test(fval) ? fs = 0 : fs = 1;
                    status_check(f, active_step, fname, fs);
                    fndubl[0] = fval;
                }

                fname = "lname";
                if (!cn || cn == fname) {
                    cn ? f = ct : f = $tjs.find("[data-rch=" + fname + "]");
                    fval = f.val().toLowerCase();

                    fp1.test(fval) ? fs = 0 : fs = 1;
                    status_check(f, active_step, fname, fs);
                    fndubl[1] = fval;
                }

                fname = "oname";
                if (!cn || cn == fname) {
                    cn ? f = ct : f = $tjs.find("[data-rch=" + fname + "]");
                    fval = f.val().toLowerCase();

                    fp1.test(fval) ? fs = 0 : fs = 1;
                    status_check(f, active_step, fname, fs);
                    fndubl[2] = fval;
                }


                if (!cn && fndubl[0]) {
                    if (fndubl[0] == fndubl[1] && fndubl[0] == fndubl[2])
                        fs = 0;
                    else
                        fs = 1;

                    f = $tjs.find("[data-rch=name], [data-rch=lname], [data-rch=oname]");
                    status_check(f, active_step, "repeat_fio", fs);
                }
                //END - Проверка ФИО


                //Year
                fname = "year";
                if (!cn || cn == fname) {
                    cn ? f = ct : f = $tjs.find("[data-rch=" + fname + "]");
                    fval = Number(f.val());

                    if (isNaN(fval) || fval < 1900 || fval > currentFullYear)
                        fs = 0;
                    else
                        fs = 1;

                    status_check(f, active_step, fname, fs);
                }

                //Phone
                fname = "phone";
                if (!cn || cn == fname) {
                    cn ? f = ct : f = $tjs.find("[data-rch=" + fname + "]");
                    fval = f.val();

                    !/^\+7 \(\d{3}\) \d{3}\-\d{2}\-\d{2}$/.test(fval) ? fs = 0 : fs = 1;
                    status_check(f, active_step, fname, fs);
                }

                //Email
                fname = "mail";
                if (!cn || cn == fname) {
                    cn ? f = ct : f = $tjs.find("[data-rch=" + fname + "]");
                    fval = f.val();


                    !fp3.test(fval) ? fs = 0 : fs = 1;
                    status_check(f, active_step, fname, fs);
                }

                //Skype
                fname = "login_skype";
                if (!cn || cn == fname) {
                    cn ? f = ct : f = $tjs.find("[data-rch=" + fname + "]");
                    fval = f.val().trim().length;

                    !fval ? fs = 0 : fs = 1;
                    status_check(f, active_step, fname, fs);
                }


                //Photo1
                fname = "photo_profile";
                if (!cn || cn == fname) {
                    cn ? f = ct : f = $tjs.find("[data-rch=" + fname + "]");
                    fval = f.val();

                    !fval ? fs = 0 : fs = 1;
                    status_check(f, active_step, fname, fs);
                }

            }

        }

        //Второй шаг
        if (active_step == 2) {

            if (!no_error) {

                //Комментарий
                fname = "korobka";
                if (!cn || cn == "akpp" || cn == "mkpp" || cn == "moto") {

                    f = $tjs.find("[data-rch=" + fname + "]");

                    if ($(f).find("[name=akpp]").prop("checked"))
                        fs = 1;
                    else if ($(f).find("[name=mkpp]").prop("checked"))
                        fs = 1;
                    else if ($(f).find("[name=moto]").prop("checked"))
                        fs = 1;
                    else
                        fs = 0;

                    status_check(f, active_step, fname, fs);
                }

                //id_avtomob
                fname = "rep_avto";
                if (!cn || cn == "id_avtomob" || cn == "avto1") {
                    f = $tjs.find("[data-rch=id_avtomob], [data-rch=avto1]");

                    var fval_1 = Number($tjs.find("[data-rch=id_avtomob]").val()),
                        fval_2 = $tjs.find("[data-rch=avto1]").val().trim().length;


                    if (fval_1 || fval_2)
                        fs = 1;
                    else
                        fs = 0;

                    status_check(f, active_step, fname, fs);
                }

                //Наличие дублирующих педалей
                fname = "nami1";
                if (!cn || cn == fname) {
                    cn ? f = ct : f = $tjs.find("[data-rch=" + fname + "]");
                    fval = f.prop("checked");

                    !fval ? fs = 0 : fs = 1;
                    status_check(f, active_step, fname, fs);
                }


            }
        }

        //Третий шаг
        if (active_step == 3) {

            if (!no_error) {

                //metro_map
                fname = "list_r_id";
                if (!cn || cn == fname) {
                    cn ? f = ct : f = $tjs.find("[data-rch=" + fname + "]");
                    fval = f.hasClass("list_r_id_active");

                    !fval ? fs = 0 : fs = 1;
                    status_check(f, active_step, fname, fs);
                }

                //Стоимость занятия
                fname = "price";
                if (!cn || cn == fname) {
                    cn ? f = ct : f = $tjs.find("[data-rch=" + fname + "]");
                    fval = Number(f.val());

                    !fval ? fs = 0 : fs = 1;
                    status_check(f, active_step, fname, fs);
                }

            }

        }

        //Четвертый шаг
        if (active_step == 4) {

            if (!no_error) {

                //Стаж вождения
                fname = "stag_avto";
                if (!cn || cn == fname) {
                    cn ? f = ct : f = $tjs.find("[data-rch=" + fname + "]");
                    fval = Number(f.val());

                    !fval ? fs = 0 : fs = 1;
                    status_check(f, active_step, fname, fs);
                }

                //Время проведения занятий:
                fname = "vremya";
                if (!cn || cn == fname) {
                    cn ? f = ct : f = $tjs.find("[data-rch=" + fname + "]");
                    fval = f.val().trim().length;

                    !fval ? fs = 0 : fs = 1;
                    status_check(f, active_step, fname, fs);
                }

                //Преподавательский стаж
                fname = "stag_lern";
                if (!cn || cn == fname) {
                    cn ? f = ct : f = $tjs.find("[data-rch=" + fname + "]");
                    fval = Number(f.val());

                    !fval ? fs = 0 : fs = 1;
                    status_check(f, active_step, fname, fs);
                }

                //Время проведения занятий:
                fname = "o_sebe";
                if (!cn || cn == fname) {
                    cn ? f = ct : f = $tjs.find("[data-rch=" + fname + "]");
                    fval = f.val().trim().length;

                    !fval ? fs = 0 : fs = 1;
                    status_check(f, active_step, fname, fs);
                }
            }
        }

        //Пятый шаг
        if (active_step == 5) {

            if (!no_error) {

                //Стаж
                fname = "stag";
                if (!cn || cn == fname) {
                    cn ? f = ct : f = $tjs.find("[data-rch=" + fname + "]");
                    fval = f.val().trim().length;

                    !fval ? fs = 0 : fs = 1;
                    status_check(f, active_step, fname, fs);
                }

                //Опыт работы
                fname = "stag_v";
                if (!cn || cn == fname) {
                    cn ? f = ct : f = $tjs.find("[data-rch=" + fname + "]");
                    fval = f.val().trim().length;

                    if (fval < 2)
                        fs = 0;
                    else
                        fs = 1;

                    status_check(f, active_step, fname, fs);
                }

                //Ваше образование
                fname = "obrazovanie";
                if (!cn || cn == fname) {
                    cn ? f = ct : f = $tjs.find("[data-rch=" + fname + "]");
                    fval = f.val().trim().length;

                    if (fval < 2)
                        fs = 0;
                    else
                        fs = 1;

                    status_check(f, active_step, fname, fs);
                }

                //Доп. информация
                fname = "dopinfo";
                if (!cn || cn == fname) {
                    cn ? f = ct : f = $tjs.find("[data-rch=" + fname + "]");
                    fval = f.val().trim().length;

                    if (fval < 2)
                        fs = 0;
                    else
                        fs = 1;

                    status_check(f, active_step, fname, fs);
                }


                //Фото димпломы или раб место
                fname = "photo_other_count";
                if (!cn || cn == fname) {
                    cn ? f = ct : f = $tjs.find("[data-rch=" + fname + "]");
                    fval = Number(f.val());

                    if (!fval)
                        fs = 0;
                    else
                        fs = 1;

                    status_check(f, active_step, fname, fs);
                }
            }
        }

        progress_value = 0;
        error = "";

        $.each(progress_step, function (k1, v1) {

            if (k1 == active_step) {

                $.each(v1, function (k2, v2) {

                    if (k2 == "step_summ")
                        v1["step_summ"] = 0;
                    else if (v2["s"]) {
                        v1["step_summ"] += v2["r" + v2["s"]];
                    } else if (v2["ts"] && v2["t"]) {
                        error = error + "<li>" + v2["t"] + "</li>";
                    }

                });
            }

            progress_value += v1["step_summ"];
        });

        if (error && !no_error) {
            $tjs.find(".callback_error").html("<ul>" + error + "</ul>");
            $(window).scrollTop(scrollTop);
        } else {
            $tjs.find(".callback_error").html("");
        }


        $("#circle").circleProgress("value", progress_value);
        var canvas = $("#circle").circleProgress("widget");

        if (error)
            return false;
        else
            return true;

    }

    checkregistrform(active_step, 1);


    //Фикзация прогресс бара
    $(window).on("scroll resize", function () {

        var $regist_right = $("#content").find(".regist_progressbar"),
            $regist_left = $("#content").find(".regist form"),
            regist_right_height = $regist_right.outerHeight(),
            regist_right_left = $(window).width() - $regist_left.offset().left - $regist_left.outerWidth() - $regist_right.outerWidth() - 40,
            regist_left_top = $regist_left.offset().top,
            regist_left_height = $regist_left.outerHeight(),
            regist_left_bottom = regist_left_height + regist_left_top - regist_right_height;

        var scroll_this = $(window).scrollTop();
        h = 0,
            hdop_1 = 0,
            t_regist_left_top = 0,
            t_regist_left_bottom = 0,
            t_regist_left_right = 0;


        t_regist_left_right = regist_right_left;
        t_regist_left_top = regist_left_top - hdop_1;
        t_regist_left_bottom = regist_left_bottom - hdop_1;

        if ($(window).width() <= 1000) {
            t_regist_left_right = 0;
            t_regist_left_top -= 61;
        } else if ($(window).width() <= 1000) {
            t_regist_left_right = 0;
            t_regist_left_top -= 37;
        } else {

        }

        if (scroll_this < t_regist_left_top) {
            $regist_right.removeClass("fixed").css({"right": "0px"});
        } else if (scroll_this > t_regist_left_top && scroll_this < t_regist_left_bottom) {
            $regist_right.addClass("fixed").css({"right": t_regist_left_right + "px"});
        } else {
            $regist_right.addClass("fixed").css({"right": t_regist_left_right + "px"});

        }


    });


    //Форма дропа файлов
    var globalFunctions = {};
    var file_array = new Array();
    globalFunctions.ddInput = function (elem) {

        var $fileupload = $(elem).find("input[type=\"file\"]"),
            $dropfield = $(elem).find(".droparea_upload"),
            $file_count = $(elem).find("input[type=\"hidden\"]"),
            file_list = $(elem).find(".droparea_preview_images"),
            last_index = 0;


        $fileupload.each(function () {
            var self = this;

            var isDropped = false;
            $(self).on("change", function (evt) {
                if ($(self).val() == "") {
                    //$file_list.find('li').remove();

                } else {
                    if (!isDropped) {
                        $dropfield.removeClass("hover");
                        $dropfield.addClass("loaded");
                        addfiles(evt.target.files);
                    }
                }
            });


            $dropfield.on("click", function (evt) {
                $fileupload.click();
            });


            $dropfield.on("dragover dragleave drop", function (evt) {

                evt.preventDefault();
                evt.stopPropagation();

                if (evt.type == "dragover") {
                    $dropfield.addClass("hover");
                } else if (evt.type == "dragleave") {
                    $dropfield.removeClass("hover");
                } else {
                    if (evt.type == "drop") {
                        evt.preventDefault();
                        isDropped = true;
                        $dropfield.removeClass("hover");
                        $dropfield.addClass("loaded");
                        addfiles(evt.originalEvent.dataTransfer.files);
                        isDropped = false;
                    }
                }

            });

            deletePreview = function (ele, i) {
                $(ele).parent().remove();
                file_array.splice(i, 1);
                $file_count.val(file_array.length).change();
            };

            function addfiles(f) {

                $.each(f, function (index, value) {
                    if (checkfiles(value)) {
                        file_array.push(value);
                        appendFile(last_index, value);
                        last_index++;
                    } else {
                        return false;
                    }
                });

                $file_count.val(file_array.length);

                $fileupload.val("");
            }

            function checkfiles(f) {

                if (file_array.length > 3) {

                    alert("Максимум 4 файла");
                    return false;

                } else {
                    var legalTypes = ["image/jpg", "image/jpeg", "image/png"];

                    if (legalTypes.indexOf(f.type) === -1) {

                        alert("Please only upload image files!");
                        return false;
                    }

                }

                return true;
            }

            function appendFile(i, img) {

                var reader = new FileReader(),
                    newElement = $("<div id='previewImg" + i + "' class='previewBox'><img /></div>"),
                    deleteBtn = $("<div class='delete' onClick='deletePreview(this, " + i + ")'><div class='fon'></div><div class='icon'></div></div>").prependTo(newElement),
                    preview = newElement.find("img");

                reader.onloadend = function () {
                    preview.attr("src", reader.result);
                    preview.attr("alt", img.name);
                };

                if (img) {
                    reader.readAsDataURL(img);
                } else {
                    preview.src = "";
                }

                $(file_list).append(newElement);


            }

            function traverseFiles(files) {
                if ($dropfield.hasClass("loaded")) {
                    $(file_list).html("");
                }
                if (typeof files !== "undefined") {
                    for (var i = 0, l = files.length; i < l; i++) {
                        appendFile(i, files[i]);
                    }
                } else {
                    alert("No support for the File API in this web browser");
                }
            }

        });
    };
    globalFunctions.ddInput(".droparea_files");

});
