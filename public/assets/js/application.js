'use strict';

let course_type, // Тип заявки, используется для метрики
    data_form_2 = [];

// Perfect Scrollbar
let scrollId = 0;
let scrollbars = [];

// Карта метро
let map;

// ---------------------------------------------------------------------------------------------------------------------
// Глобальные функции
// ---------------------------------------------------------------------------------------------------------------------

// Добавления иконки загрузки при ajax запросах
function preloader() {
    $('#content').find('.main').html('<div class="form_preloader"><div></div></div>');
}

// Показ скролла
function scrollShow(el = 'body') {
    if ($(window).width() >= 1000) {
        $(el).find('.scroll-custom').each(function (index, elem) {
            var $current = $(elem),
                id = $current.data('data-scroll-id');

            if (!id) {
                scrollbars[++scrollId] = new PerfectScrollbar(elem, {
                    suppressScrollX: true,
                    minScrollbarLength: 46,
                    maxScrollbarLength: 46
                });
                $current.data('data-scroll-id', scrollId);
            } else {
                scrollbars[id].update();
            }
        });
    }
}

// Функция отключения скролла
function stopScrollbar(enable) {
    if (enable) {
        $('body').addClass('stop-scrollbar');
    } else {
        $('body').removeClass('stop-scrollbar');
    }
}

// Кнопка читать полностью в каталоге репетитора и отзывах
$(document).on('click', '.s_text_link', function () {
    const $this = $(this),
        $text = $this.closest('.s_text_info'),
        $hiddenText = $text.find('span, div');

    // Добавляем отступ сверху для кнопки свернуть
    $text.toggleClass('hide');

    if ($hiddenText.is(':visible')) {
        $hiddenText.hide();
        $this.text('Читать полностью');
    } else {
        $hiddenText.show().css('display', 'inline');
        $this.text('Свернуть');
    }

    $text.find('i').toggle();
});

// ---------------------------------------------------------------------------------------------------------------------
// Мобильная версия сайта
// ---------------------------------------------------------------------------------------------------------------------

// Показ блока фильтров в мобильной версии в каталоге
$(document).on('click', '.resp_search_button', function () {
    const $this = $(this)
    const $help_buttons = $this.closest('.resp_help_buttons')

    if (!$this.hasClass('resp_show')) {
        $this.addClass('resp_show');
        $help_buttons.addClass('resp_help_buttons_active');

        // TODO: Проверить нужно или нет
        window.history.pushState('forward', null, '#menu');

        $('.aside_catalog').addClass('aside_catalog_active');
        stopScrollbar(true);
    } else {
        $this.removeClass('resp_show');
        $help_buttons.removeClass('resp_help_buttons_active');

        $('.aside_catalog').removeClass('aside_catalog_active');
        stopScrollbar();
    }
});

// Показ общего меню в мобильной версии (в левом верхнем углу кнопка)
$(document).on('click', '.h_resp_menu_link, .h_resp_menu_shadow, .h_resp_menu_close', function (event) {
    event.preventDefault();

    var head_menu = $('header').find('.head_menu');

    if (!$(head_menu).hasClass('h_resp_menu_open')) {
        // TODO: Проверить нужно или нет
        window.history.pushState('forward', null, '#menu');

        if (!$(head_menu).find('.h_resp_menu_shadow').length) {
            $(head_menu).prepend('<div class="h_resp_menu_shadow" onclick=""></div><div class="h_resp_menu_close" onclick=""></div>');
            $(head_menu).find('nav').prepend('<div class="h_resp_menu_close" onclick=""></div>');
            $(head_menu).find('.gr').first().children('.el').addClass('resp_menu_show');
        }

        $(head_menu).addClass('h_resp_menu_open');
        stopScrollbar(true);
    } else {
        // TODO: Проверить нужно или нет
        window.history.pushState('forward', null, '#menu');

        $(head_menu).removeClass('h_resp_menu_open');
        stopScrollbar();
    }
});

// Функция раскрытия списка в общем меню в мобильной версии
$(document).on('click', '.ls', function (event) {
    event.preventDefault()

    const $gr = $(this).closest('.gr')

    $(this).addClass('resp_menu_hide').next('.ct').addClass('resp_menu_show').find('.el').addClass('resp_menu_show');
    $(this).closest('.el').addClass('resp_menu_active');
    $gr.children('.el').not('.resp_menu_active').removeClass('resp_menu_show');

    if ($gr.hasClass('h_menu_ul')) {
        $('.h_resp_menu_open nav').addClass('h_resp_menu_padding').prepend('<a href="javascript:void(0);" class="h_resp_menu_back">Вернуться</a>');
    }
});

// Кнопка "Вернуться" назад в общем меню
$(document).on('click', '.h_resp_menu_back', function () {
    const $gr = $('.h_resp_menu_open').find('.resp_menu_active').last().closest('.gr');

    $gr.find('.ls').removeClass('resp_menu_hide').next('.ct').removeClass('resp_menu_show').find('.el').removeClass('resp_menu_show');
    $gr.find('.resp_menu_active').removeClass('resp_menu_active');
    $gr.children('.el').addClass('resp_menu_show');

    if ($gr.hasClass('h_menu_ul')) {
        $('.h_resp_menu_open nav').removeClass('h_resp_menu_padding').find('.h_resp_menu_back').remove();
    }
});

// Разворачивает список сортировки по популярности, цене, кол-ву отзывов в мобильной версии в каталоге репетиторов
$(document).on('click', '.sort_st_settings', function () {
    const $this = $(this)
    $this.toggleClass('sort_st_settings_show');
    $this.closest('.sort_st').toggleClass('resp_sort_st_show')
});

// ---------------------------------------------------------------------------------------------------------------------
// Формы заявок
// ---------------------------------------------------------------------------------------------------------------------

// Заявка репетитору или Поиск репетитора с нашей помощью
function callback_rep(id_users, type, selected_course_type, fav) {
    course_type = selected_course_type;

    $.fancybox.open({
        src: '/ajax/callback',
        type: 'ajax',
        opts: {
            touch: false,
            hash: 'fancybox-open',
            baseClass: 'fancybox-container-rpadd parent-title-change',
            ajax: {
                settings: {
                    type: 'GET',
                    data: {
                        id_users,
                        type,
                        fav
                    }
                }
            },
            afterShow: function () {
                $('.callback_js input[name="phone"]').inputmask({'mask': '+7 (999) 999-99-99'});
            }
        }
    });
}

// Отправка формы после заявки
$(document).on('submit', '.callback_js', function (event) {
    event.preventDefault();

    const $this = $(this),
        scrollTop = $this.offset().top,
        errors = [];

    $this.removeClass('.callback_error_input');

    // Проверка имени
    let field = $this.find('input[name="name"]');
    if (/^\s*$/.test(field.val())) {
        errors.push('Введите Имя');
        field.addClass('callback_error_input');
    }

    // Проверка телефона
    field = $this.find('input[name="phone"]');
    if (!/^\+7 \(\d{3}\) \d{3}-\d{2}-\d{2}$/.test(field.val())) {
        errors.push('Телефон введен некорректно');
        field.addClass('callback_error_input');
    }

    // Проверка коробки передач
    if ($this.find('.callback_korob').length) {
        if (!$this.find('.callback_korob').find('input[name="object"]:checked').val()) {
            errors.push('Выберите коробку передач');
        }
    }

    // В случае ошибок, выходим
    if (errors.length) {
        const $ul = $('<ul/>');
        $.each(errors, function (index, value) {
            $ul.append($('<li/>').text(value));
        });

        $this.find('.callback_error').html($ul);

        if ($this.hasClass('callback_postform')) {
            $(window).scrollTop(scrollTop);
        } else {
            $('.fancybox-content').scrollTop(0);
        }

        return;
    }

    // Скрываем кнопку отправить, если нет ошибок
    $this.find('input[type="submit"]').fadeOut();

    $.ajax({
        type: 'POST',
        url: $this.attr('action'),
        data: $this.serialize(),
        success: function (data) {
            if ($(document).width() < 805) $(document).scrollTop(scrollTop);

            $this.html(data);
            $this.closest('.parent-title-change').find('.callback-title-change').text('Спасибо за Ваш заказ!');

            if (course_type === 1) {
                ym(10151101, 'reachGoal', 'repetitor2');
                ga('send', 'event', 'ajax_post', 'repetitor2');
            } else if (course_type === 2) {
                ym(10151101, 'reachGoal', 'avto2');
                ga('send', 'event', 'ajax_post', 'avto2');
            } else if (course_type === 3) {
                ym(10151101, 'reachGoal', 'logoped2');
                ga('send', 'event', 'ajax_post', 'logoped2');
            } else if (course_type === 4) {
                ym(10151101, 'reachGoal', 'trener2');
                ga('send', 'event', 'ajax_post', 'trener2');
            }

            ym(10151101, 'reachGoal', '123456');
            gtag('event', 'ajax_post');
        }
    });
});

// ---------------------------------------------------------------------------------------------------------------------
// Фильтр поиска в каталоге репетиторов или автоинструкторов
// ---------------------------------------------------------------------------------------------------------------------

// Очистка выпадающего списка
function searchClear($list) {
    $('.search_clear', $list).remove();
    $('.select_search input', $list).val('');
    $('label.sh_i', $list).removeClass('sh_i');
    $list.removeClass('ss_a');

    //Обновление скролла
    const id = $list.find('.scroll-custom').data('data-scroll-id');

    if (id) {
        scrollbars[id].update();
    }
}

// changeForm(1, 1, this, id, value);
/*
function changeForm(a, tf, t, i, v) {
    if (v == 'undefined') v = 1;

    if (tf == 1) { //Если форма поиска
        if (a == 2) {
            $(t).removeClass('selected');
            delete window.data_form_2[i];
        } else {
            $(t).addClass('selected');
            window.data_form_2[i] = v;
        }

        if (window.type_rep != 3)
            ajaxForm();
        else {//Если главная

            var select = $(t).closest('.select'),
                select_input = $(select).find('.input_name div');

            if (a == 2) {
                $(select_input).find('[input-name=' + i + ']').remove();
            } else {
                $(select_input).append('<span input-name="' + i + '">' + $(t).text() + '<i></i></span>');
            }

            if ($(select_input).find('span').length > 0)
                $(select).addClass('current_hide');
            else
                $(select).removeClass('current_hide');

        }

    } else if (tf == 2) { //Если отдельный список заявки
        var select = $(t).closest('.select'),
            predmet_select_addpost = '',
            predmet_text = '';

        $(select).find('.selected').removeClass('selected');

        if (a != 2) {
            predmet_select_addpost = $(t).attr('fd-value');
            predmet_text = $(t).text();
            $(t).addClass('selected');
            $(t).closest('.list2').find('#predmet_select_addpost').prop('value', predmet_select_addpost);
        }

        $(select).find('#predmet_select_addpost').prop('value', predmet_select_addpost);
        $(select).find('.current').text(predmet_text);
    } else if (tf == 3) { //Если отдельный список фильтров
        var select = $(t).closest('.select'),
            select_input = $(select).find('.input_name div');

        if (a == 2) {
            $(t).removeClass('selected');
            $(t).find('input').prop('checked', false);
            $(select_input).find('[input-name=' + i + ']').remove();

        } else {
            $(t).addClass('selected');
            $(t).find('input').prop('checked', true);
            $(select_input).append('<span input-name="' + i + '">' + $(t).text() + '<i></i></span>');
        }

        if ($(select_input).find('span').length > 0)
            $(select).addClass('current_hide');
        else
            $(select).removeClass('current_hide');
    }
}*/

// Ajax загрузка данных по фильтру
function ajaxPage($form) {
    preloader();

    const url = $form.attr('action') + '?' + $form.serialize();

    $.get(url).done(function (data) {
        $('#content .rep_bc').first().replaceWith(data.breadcrumbs);
        $('#content h1.header-through').first().replaceWith(data.header);
        $('#content .rep_kol').first().replaceWith(data.totalText);
        $('#content .s_text_info').first().replaceWith(data.prependText);
        $('#content .main').first().replaceWith(data.content);

        // Добавляем в историю линк
        history.pushState(null, null, url)
    });

    // $form.submit();
    //$form.submit();

    // if (window.type_rep != 3)//поиск ajax
    //     ajaxFromPreloader();

    // var ajax_link_search = 'lernuser', fd = [];
    //
    // for (var key in window.data_form_2) {
    //     if (key)
    //         fd.push(key + '=' + window.data_form_2[key]);
    // }
    //
    // if (window.type_rep == 3) {//поиск переход главная
    //     window.open('/include/lernuser.php' + (fd.length > 0 ? '?' + fd.join('&') : ''), '_self');
    //     return false;
    // }
    //
    // if (window.type_rep == 2)
    //     ajax_link_search = 'avtouser';
    //
    //
    // $.get('/include/' + ajax_link_search + '_ajax.php' + (fd.length > 0 ? '?' + fd.join('&') : ''), {
    //     sort: sort,
    //     sort_type: sort_type
    // }).done(function (data) {
    //     $('#content .main').first().replaceWith(data);
    //     modCh();
    // });
}

// Кнопка завершить выбор в выпадающих списках в фильтре
$(document).on('click', '.select_cur_right', function (event) {
    event.stopPropagation();
    $(this).closest('.select').removeClass('open_slo');
});

// Очистка поля поиска в фильтре
$(document).on('click', '.search_clear', function () {
    searchClear($(this).closest('.list'));
});

// Выпадающий список с поиском в фильтре и на главной
$(document).on('click', 'div.select', function (event) {
    event.stopPropagation();

    const $this = $(this);
    const classList = event.target.classList;

    if (classList.contains('select') || classList.contains('current')) {
        const $item = classList.contains('current') ? $(event.target).parent() : $this;

        if (!$item.hasClass('open_slo')) {
            $item.addClass('open_slo');
            scrollShow(this);
            searchClear($('div.list', this));
        } else {
            $item.removeClass('open_slo');
        }

        return;
    }

    // Изменять ли текст в выпадающем списке
    if ((event.target instanceof HTMLInputElement) && (classList.contains('change-current'))) {
        const labels = $this.find('.list2 input:checked').map(function(index, item) {
            return $('label[for="' + $(item).attr('id') + '"]').text();
        }).get();
        const $current = $this.find('.current')
        $current.text(labels.length ? labels.join(', ') : $current.attr('data-text'));
    }

    const $form = $this.closest('form');

    if ((event.target instanceof HTMLInputElement) && ($form.attr('data-type') === 'filter')) {
        ajaxPage($form);
    }

    // var type_sel,
    //     $all_list_s = $(this).find('.list2'),
    //     fd_type = $all_list_s.attr('fd-type');
    //
    // if (fd_type == 2) //форма заказа
    //     type_sel = 2;
    // else if (fd_type == 3) //форма фильтров
    //     type_sel = 3;
    // else
    //     type_sel = 1;
    //
    // if (e.target.tagName.toLowerCase() == 'li') {
    //     var id = $(e.target).attr('fd');
    //
    //     if ($(e.target).hasClass('selected'))
    //         changeForm(2, type_sel, e.target, id);
    //     else
    //         changeForm(1, type_sel, e.target, id, 1);
    // }
});

// Радиокнопки в фильтре
$(document).on('click', '.radio-list-auto-submit input', function (event) {
    const $form = $(this).closest('form');

    if ((event.target instanceof HTMLInputElement) && ($form.attr('data-type') === 'filter')) {
        ajaxPage($form);
    }
});

// Кнопка закрыть на плашках с названием категории
$(document).on('click', '.searchPar .spDel', function () {
    const $this = $(this);
    const $searchPar = $this.parents('.searchPar');

    $this.parent().hide();

    if (!$searchPar.find('.spItem:visible').length) {
        $searchPar.hide();
    }

    // После кнопки удаления снимаем пометки с позиций в search_form
    const attributes = [
        'data-courses',
        'data-cities',
        'data-metro',
        'data-prices',
        'data-universities',
        'data-statuses',
        'data-places'
    ]

    for (const attribute of attributes) {
        if ($this.is('[' + attribute + ']')) {
            const value = $this.attr(attribute)
            $('.' + attribute + ' input[type=checkbox][value=' + value + ']').prop('checked', false)
        }
    }

    ajaxPage($('#search_form'));
});

// ---------------------------------------------------------------------------------------------------------------------
// Избранное
// ---------------------------------------------------------------------------------------------------------------------

// Добавление в избранное
function fav(id_users, type, action) {
    const count = Number($('.h_favorites_count_sa').first().text());

    const id = '.fav_' + id_users + '_' + type;

    $.post('/ajax/favourites', {
        id_users,
        type,
        action
    }).done(function (data) {
        data = $.parseJSON(data);

        switch (action) {
            case 'add':
                if (data.status === 'error') {
                    $('.fav_limit_error').remove();
                    $(id).before('<span class="fav_limit_error">Лимит 20 закладок</span>')
                        .delay(5000)
                        .fadeOut(300, function () {
                            $('.fav_limit_error').remove();
                            $(this).show();
                        });
                } else {
                    if (!count) {
                        $('.h_favorites').addClass('h_favorites_2');
                        $('#content').find('.no_user_fav').toggleClass('no_user_fav_show');
                    }

                    $('.h_favorites_count_sa').html(count + 1);
                    $(id).replaceWith('<a class="fav_' + id_users + '_' + type + ' del_fav" href="javascript:void(0);" onclick="fav(' + id_users + ', ' + type + ', \'del\'); return false;"></a>');
                }

                break;

            case 'del':
                const fav_page = $('#content').find('.no_user_fav');

                if (fav_page.length) {
                    $(id).closest('.anketa').fadeOut(300, function () {
                        $(this).hide();
                    });
                }

                if (count === 1) {
                    $('.h_favorites').removeClass('h_favorites_2');

                    if (fav_page.length) {
                        fav_page.fadeIn();
                        $('#content').find('.search-option-favorites').toggleClass('search-option-favorites-show');
                    }
                }

                $('.h_favorites_count_sa').html(count - 1);
                $(id).replaceWith('<a class="fav_' + id_users + '_' + type + ' add_fav" href="javascript:void(0);" onclick="fav(' + id_users + ', ' + type + ', \'add\'); return false;"></a>');

                break;

            case 'del_all':
                $('.h_favorites').removeClass('h_favorites_2');
                $('#content').find('.no_user_fav').toggleClass('no_user_fav_show');
                $('#content').find('.search-option-favorites').toggleClass('search-option-favorites-show');
                $('.h_favorites_count_sa').html(0);
                $('#tutors article.anketa').remove();

                break;
        }
    });
}

// ---------------------------------------------------------------------------------------------------------------------
// Карточка преподавателя
// ---------------------------------------------------------------------------------------------------------------------

// Ф-ия загрузки отзывов у профиля
function profile_reviews($this, $profile, id_users, count, start) {
    var $carousel = $profile.find('.owl-carousel');

    $.ajax({
        url: '/ajax/reviews',
        type: 'GET',
        data: {
            id_users,
            start
        },
        success: function (data) {
            if (!start) {
                $carousel.append(data);
                $carousel.owlCarousel({
                    loop: false,
                    margin: 0,
                    items: 1,
                    rewind: false,
                    nav: true,
                    dots: false,
                    checkVisible: true,
                    navText: '',
                    // onTranslate: function () {
                    //     $carousel.find('.hide a').click();
                    // },

                    // Отвечает за счетчик страниц
                    onTranslated: function (event) {
                        var next = event.item.count - 3;

                        $profile.find('.owl-count span').text(event.item.index + 1);

                        if (next <= event.item.index && event.item.count < count) {
                            profile_reviews($this, $profile, id_users, count, event.item.count);
                        }
                    }
                });

                $carousel.find('.owl-nav .owl-next').before('<div class="owl-count"><span>1</span> / ' + count + '</div>').after('<a href="/include/response.php?id_rep=' + id_users + '" class="button_3 button_all_response"><span>Все отзывы</span></a>');
                $profile.find('.form_preloader').remove();
                $carousel.show();
            } else {
                $carousel.trigger('add.owl.carousel', data, start).trigger('refresh.owl.carousel');
            }
        },
        error: function () {
            if (!start) {
                $profile.find('.form_preloader').html('').css({height: 'auto'}).html('Ошибка подключения, попробуйте еще раз...');
                $carousel.remove();
            }
        }
    });
}

// Загрузка отзывов у профиля
$(document).on('click', '.catalog-response_sh', function (event) {
    event.preventDefault();

    const $this = $(this),
        id_users = $this.attr('data-id_users'),
        count = $this.attr('data-count'),
        $profile = $this.closest('.anketa-response'),
        $carousel = $profile.find('.owl-carousel');

    $this.find('span').toggleClass('hide');
    $this.toggleClass('')

    if (!$this.hasClass('loaded')) {
        $this.addClass('loaded');

        if (!$this.hasClass('opened')) {
            $profile.find('.form_preloader, .owl-carousel').remove();
            $profile.append('<div class="form_preloader"><div></div></div><div class="owl-carousel owl-theme"></div>');

            $this.addClass('opened');

            profile_reviews($this, $profile, id_users, count, 0);
        } else {
            $carousel.show();
            $profile.find('.owl-count').fadeIn();
        }
    } else {
        $this.removeClass('loaded');
        $carousel.hide();
        $profile.find('.owl-count').fadeOut();
    }
});

// Карточка преподавателя, загрузка новых отзывов (Кнопка "Открыть еще")
$(document).on('click', '.anketa-response_sh', function (event) {
    event.preventDefault();

    const $this = $(this),
        id_users = Number($this.attr('data-user_id')),
        limit = Number($this.attr('data-limit')),
        pages_count = Number($this.attr('data-pages_count')),
        total_count = Number($this.attr('data-total_count')),
        $blocks = $this.closest('div.anketa-row_block'),
        $block = $blocks.find('.bl-response_block');

    let start = Number($this.attr('data-start'));

    $blocks.find('.form_preloader').remove();
    $this.hide().before('<div class="form_preloader"><div></div></div>');

    $.ajax({
        url: '/ajax/reviews',
        type: 'GET',
        data: {
            id_users,
            start,
            limit
        },
        success: function (data) {
            if (data) {
                $block.append(data);
                $blocks.find('.form_preloader').remove();

                if (start >= pages_count) {
                    $this.remove();
                } else {
                    start += 1;
                    $this.find('span').text(start < pages_count ? limit : total_count - (start - 1) * limit);
                    $this.attr('data-start', start).show();
                }
            } else {
                $this.remove();
            }
        },
        error: function () {
            $blocks.find('.form_preloader').html('').css({height: 'auto'}).html('Ошибка подключения, попробуйте еще раз...');
            $this.show();
        }
    });
});

// TODO: Загрузка фото, видео в карточке преподавателя
$(document).on('click', '.anketa-doci_sh', function () {
    const $this = $(this),
        user_id = Number($this.attr('data-user_id')),
        limit = Number($this.attr('data-limit')),
        start = Number($this.attr('data-start')),
        pages_count = Number($this.attr('data-pages_count')),
        total_count = Number($this.attr('data-total_count')),
        // doci_count = Number($this.attr("data-count")),
        // doci_start = Number($this.attr("data-start")),
        // doci_start_video = Number($this.attr("data-start_video")),
        blocks = $this.closest('div'),
        block = blocks.find('.anketa-row_block_doci .content');

    blocks.find('.form_preloader').remove();
    $this.hide().before('<div class="form_preloader"><div></div></div>');

    $.ajax({
        url: '/ajax/documents-video',
        type: 'GET',
        dataType: 'json',
        data: {
            user_id,
            start,
            limit
        },
        success: function (data) {
            if (data) {
                $.each(data.links, function (i, item) {
                    if (item.type === 2)
                        block.append('<a data-fancybox="images" href="//www.youtube.com/watch?v=' + item.value + '" style="background:url(//img.youtube.com/vi/' + item.value + '/mqdefault.jpg)" class="row_youtube"><span></span></a>');
                    else
                        block.append('<a data-fancybox="images" href="/include/photo_doc_mini/' + item.value + '" onclick="return hs.expand(this)"><img  src="/include/photo_doc_mini_icon/' + item.value + '"></a>');
                });

                blocks.find('.form_preloader').remove();

                if (data.status === 2)
                    $this.remove();
                else {
                    $this.find('span').html(data.count_link);
                    $this.attr('data-count', data.count).attr('data-start', data.start).attr('data-start_video', data.start_video).show();
                }
            }

            $(window).trigger('resize', [1]);
        },
        error: function () {
            blocks.find('.form_preloader').html('').css({height: 'auto'}).html('Ошибка подключения, попробуйте еще раз...');
            $this.show();
        }
    });
});

// Кнопка разделы анкеты в мобильной версии
$(document).on('click', '.resp_ankmenu_button', function () {
    const $this = $(this);

    $this.toggleClass('resp_show');

    if ($this.hasClass('resp_show')) {
        $this.closest('.resp_help_buttons').addClass('resp_help_buttons_active_ank');

        $('.aside_catalog').addClass('aside_catalog_active_ank').find('.left_block-rep-menu a').on('click', function () {
            $('.resp_help_buttons').removeClass('resp_help_buttons_active_ank').find('a').removeClass('resp_show');
            $('.aside_catalog').removeClass('aside_catalog_active_ank');
        });
    } else {
        $this.closest('.resp_help_buttons').removeClass('resp_help_buttons_active_ank');
        $('.aside_catalog').removeClass('aside_catalog_active_ank');
    }
});

// ---------------------------------------------------------------------------------------------------------------------
// Регионы
// ---------------------------------------------------------------------------------------------------------------------

// Подготовка текста для поиска в регионах
function prepareText(text) {
    return text.toLowerCase().replace(/ё/ig, 'е').trim();
}

// Регионы
$(document).on('click', '#gorod_link', function () {
    $.fancybox.open({
        src: '#gorod_form',
        opts: {
            touch: false,
            hash: 'fancybox-open',
            afterShow: function () {
                scrollShow('#gorod_form');
            },
            baseClass: 'fancybox-container-rpadd'
        }
    });
});

// Выбор региона
$(document).on('click', '.sel_gor_link a', function (event) {
    event.preventDefault();

    const $this = $(this);

    if (!$this.hasClass('active')) {
        $('.sel_gor_link a.active').removeClass('active');
        $this.addClass('active');

        const $content = $this.closest('.fancybox-body');

        $content.find('.sel_gor_con div').removeClass('active selected ss_i ss_a');
        $content.find('.sel_gor_search input').val('');

        $($this.attr('href')).addClass('active');
    }
});

// Открытие конкретного города в регионе в новом окне
$(document).on('click', '.sel_gor_con div.active div, .left_block-banner-2', function () {
    const $this = $(this);

    if (!$this.hasClass('selected')) {
        const link = $this.attr('data-link');

        if (link) {
            window.open(link, '_blank');
        }
    }
});

// Поиск по населенному пункту в выборе региона
$(document).on('keyup', '.sel_gor_search input', function () {
    const $this = $(this),
        $content = $this.closest('.fancybox-body'),
        $activeTab = $content.find('.sel_gor_con div.active'),
        text = prepareText($this.val());

    $activeTab.addClass('ss_a');

    if (text) {
        $activeTab.find('div').each(function (index, elem) {
            const $current = $(elem);

            if (prepareText($current.text()).indexOf(text) === 0) {
                $current.addClass('ss_i');
            } else {
                $current.removeClass('ss_i');
            }
        });
    } else {
        $activeTab.find('div.ss_i').removeClass('ss_i');
        $activeTab.removeClass('ss_a');
    }
});

// ---------------------------------------------------------------------------------------------------------------------
// Главная страница
// ---------------------------------------------------------------------------------------------------------------------

// Кнопка читать полностью на главной
$(document).on('click', '.show_main_text', function (event) {
    const $button = $('.hide_main_text_2');
    $(this).text($button.hasClass('hide_main_text_3') ? 'Читать полностью' : 'Скрыть');
    $button.toggleClass('hide_main_text_3');
});

// Табы на главной
$(document).on('click', '.tabs > ul > li', function (event) {
    let $target = $(event.target);

    if (!$target.is('li')) {
        $target = $target.closest('li');
    }

    $('.tabs > ul > li.active').removeClass('active');
    $target.addClass('active');

    const $tabs = $target.closest('ul').siblings('div');
    const id = $target.attr('id');

    $tabs.children('div:visible').hide();
    $('#tab-' + id).show();
});

//Поиск текста среди label
$(document).on('keyup', '.select_search input', function () {
    const $this = $(this),
        text = prepareText($this.val()),
        $parent = $this.parent(),
        $list = $this.closest('.list');

    if (text) {
        $list.addClass('ss_a');
        $list.find('.list2 label').each(function (index, elem) {
            const $current = $(elem);

            if (prepareText($current.text()).indexOf(text) === 0) {
                $current.addClass('ss_i');
            } else {
                $current.removeClass('ss_i');
            }
        });

        if (!$('.search_clear', $parent).length) {
            $parent.append('<div class="search_clear">x</div>');
        }
    } else {
        $('.search_clear', $parent).remove();
        $list.find('.list2 label').removeClass('ss_i');
        $list.removeClass('ss_a');
    }

    //Обновление скролла
    const id = $list.find('.scroll-custom').data('data-scroll-id');

    if (id) {
        scrollbars[id].update();
    }
});

// ---------------------------------------------------------------------------------------------------------------------
// Страница логина и регистрации
// ---------------------------------------------------------------------------------------------------------------------

//Вход для репетиторов
$(document).on('click', '#sign_in_link', function () {
    $.fancybox.open({
        src: '#sign_in_form',
        opts: {
            touch: false,
            hash: 'fancybox-open',
            afterLoad: function () {
                ym(10151101, 'reachGoal', 'entrance');
                gtag('event', 'ajax_post');
            }
        }
    });
});

// ---------------------------------------------------------------------------------------------------------------------
// Карта метро
// ---------------------------------------------------------------------------------------------------------------------

$(document).on('click', '.map_open_metro', function (event) {
    event.preventDefault();

    if (!map) {
        $.ajax({
            type: 'GET',
            url: '/assets/js/metro.svg?v=27',
            async: false,
            dataType: 'text',
            success: function (data) {
                map = $(data);
            }
        });
    }

    const id_users = $(this).attr('data-id_users');

    $.getJSON("/include/rep_metro_catalog.php?id_users=" + id_users, function (data) {
        if (Array.isArray(data)) {
            $(data).each(function (index, item) {
                map.find('.metro-station[id="s' + item + '"]').addClass('metro-active');
            })
        }
    });

    $.fancybox.open('<div id="h_metro" class="h_metro_ajax animated-modal clearfix"><div class="fancybox-title"><div>Карта метро<button type="button" data-fancybox-close="" class="fancybox-button fancybox-close-small" title="Close"><svg xmlns="http://www.w3.org/2000/svg" version="1" viewBox="0 0 24 24"><path d="M13 12l5-5-1-1-5 5-5-5-1 1 5 5-5 5 1 1 5-5 5 5 1-1z"></path></svg></button></div></div><div class="fancybox-body"><div id="lolo" style="width: 1200px;"></div></div></div>', {
        touch: false,
        hash: 'fancybox-open',
        baseClass: 'fancybox-container-hmetro',
        afterLoad: function () {
            $('#lolo').html(map);
        }
    });
});

// ---------------------------------------------------------------------------------------------------------------------
// Главная функция
// ---------------------------------------------------------------------------------------------------------------------

// Главная функция
$(function () {
    scrollShow();

    $('.hm_big-dropdown, .i_dropdown').hover(function () {
        scrollShow(this);
    });

    //Показать/скрыть предметы в /include/catalog.php
    $('.tep_list_m div b').click(function () {
        $(this).siblings('div').toggle();
    });

    // Удаление ajax загрузки
    $('.form_preloader').remove();

    // Закрытие выпадающего списка
    $(document).click(function () {
        $('div.select').removeClass('open_slo');
    });
});

// Дополнения для просмотра карты метро
(function ($) {
    $.fn.hasClassSVG = function (className) {
        return new RegExp('(\\s|^)' + className + '(\\s|$)').test(this.attr('class'));
    };

    $.fn.removeClassSVG = function (className) {
        return this.each(function () {
            const $this = $(this),
                removedClass = $this.attr('class').replace(new RegExp('(\\s|^)' + className + '(\\s|$)', 'g'), '$2');

            if ($this.hasClassSVG(className)) {
                $this.attr('class', removedClass);
            }
        });
    };

    $.fn.addClassSVG = function (className) {
        return this.each(function () {
            const $this = $(this);

            if (!$this.hasClassSVG(className)) {
                $this.attr('class', $this.attr('class') + ' ' + className);
            }
        });
    };

    $.fn.toggleClassSVG = function (className) {
        return this.each(function () {
            const $this = $(this);

            if ($this.hasClassSVG(className)) {
                $this.removeClassSVG(className);
            } else {
                $this.addClassSVG(className);
            }
        });
    };
})(jQuery);

/*
$(document).on('click', '.resp_search_block_shadow', function () {
    $('#content').find('.resp_help_buttons').removeClass('resp_help_buttons_active').find('.resp_search_button').removeClass('resp_show');
    $(this).closest('.aside_catalog').removeClass('aside_catalog_active');
    stop_scrollbar();
});

$(document).on('click', '.resp_search_dop_button', function () {
    if (!$(this).hasClass('resp_show')) {
        $(this).addClass('resp_show');
        $('.aside_catalog').find('.directions').addClass('resp_show');

    } else {
        $(this).removeClass('resp_show');
        $('.aside_catalog').find('.directions').removeClass('resp_show');
    }

    $($(this).closest('.resp_search_block')).scrollTop(2000);
});

if (window.history && window.history.pushState) {
    var head_menu = $('header').find('.head_menu');

    $(window).on('popstate', function () {
        $(head_menu).removeClass('h_resp_menu_open');
        stop_scrollbar();

        $('.resp_search_button').removeClass('resp_show');
        $('.resp_search_button').closest('.resp_help_buttons').removeClass('resp_help_buttons_active');
        $('.aside_catalog').removeClass('aside_catalog_active');
        stop_scrollbar();
    });
}
*/


/*$(function () {
    var targets = $('[rel~=tooltip]'),
        target = false,
        tooltip = false,
        title = false;

    targets.bind('mouseenter', function () {
        target = $(this);
        tip = target.attr('title');
        tooltip = $('<div id="tooltip"></div>');

        if (!tip || tip == '')
            return false;

        target.removeAttr('title');
        tooltip.css('opacity', 0)
            .html(tip)
            .appendTo('body');

        var init_tooltip = function () {
            if ($(window).width() < tooltip.outerWidth() * 1.5)
                tooltip.css('max-width', $(window).width() / 2);
            else
                tooltip.css('max-width', 340);

            var pos_left = target.offset().left + (target.outerWidth() / 2) - (tooltip.outerWidth() / 2),
                pos_top = target.offset().top - tooltip.outerHeight() - 20;
            if (pos_left < 0) {
                pos_left = target.offset().left + target.outerWidth() / 2 - 36;
                tooltip.addClass('left');
            } else
                tooltip.removeClass('left');

            if (pos_left + tooltip.outerWidth() > $(window).width()) {
                pos_left = target.offset().left - tooltip.outerWidth() + target.outerWidth() / 2 + 36;
                tooltip.addClass('right');
            } else
                tooltip.removeClass('right');

            if (pos_top < 0) {
                var pos_top = target.offset().top + target.outerHeight();
                tooltip.addClass('top');
            } else
                tooltip.removeClass('top');

            tooltip.css({left: pos_left, top: pos_top})
                .animate({top: '+=10', opacity: 1}, 50);
        };

        init_tooltip();
        $(window).resize(init_tooltip);

        var remove_tooltip = function () {
            tooltip.animate({top: '-=10', opacity: 0}, 50, function () {
                $(this).remove();
            });

            target.attr('title', tip);
        };

        target.bind('mouseleave', remove_tooltip);
        tooltip.bind('click', remove_tooltip);
    });
});*/


// function init() {
// if (!window.type_rep)
//     window.type_rep = $('#search_form').attr('type-rep');

//Очистка блока позиций
/*$(document).on('click', '.search_sbros_single', function () {
    var list_c = $(this).parents('.list');

    //Скрываем .search_sbros_single
    $(this).hide();

    //Сбрасываем checked
    list_c.find('.selected').removeClass('selected').find('input').removeAttr('checked').change();

    //Пишем 0 в "выбрано"
    $(this).parents('.select').find('.select_cur_count').text('0');

    //Скидываем спойлер
    list_c.find('.select_cur_left').removeClass('select_cur_show');

    //Вызываем функцию очистки
    search_clear(list_c);

    list_c.find('div.list2').removeClass('sbros_mobile only_selected');
    $(this).remove();
});*/

//Сброс всей формы
/*$(document).on('click', '.search_sbros', function () {
    //Очищаем блок активных checkbox
    $('#search').find('.input_name div').empty();

    //Сбрасываем checked
    var form = $(this).parents('form');
    form.find('li').removeClass('selected').find('input').removeAttr('checked');

    form.find('.select').removeClass('current_hide');

    form.find('.radio-list-form input[value=a], .radio-list-form input[value=0]').attr('checked', true);
    form.find('.radio-list-form input[name=kpp]').removeAttr('checked');
    form.change();
});

$(document).on('click', '.radio-list-form li', function (e) {
    var id = $(this).attr('fd'),
        value = $(this).attr('fd-value');

    if (!$(this).hasClass('selected')) {
        $(this).closest('ul').find('.selected').removeClass('selected');
        changeForm(1, 1, this, id, value);
    }
});*/

/* modCh();*/
// }

/*
function modCh() {
    var res = new Object();
    res['predmet'] = new Array();
    res['gorod'] = new Array();
    res['metro'] = new Array();
    res['stat'] = new Array();
    res['dom'] = new Array();
    res['sfx'] = new Array();
    res['nfv'] = new Array();
    res['price'] = new Array();
    res['vuz'] = new Array();
    res['dl'] = new Array();
    res['com'] = new Array();
    res['kpp'] = new Array(); //avto uniq

    var typeNames = {
        'predmet': {0: '', 1: ''},
        'gorod': {0: '', 1: ''},
        'metro': {0: '', 1: ''},
        'stat': {0: '', 1: ''},
        'dom': {0: '', 1: ''},
        'sfx': {0: '', 1: ''},
        'nfv': {0: '', 1: ''},
        'price': {0: '', 1: ' руб./час'},
        'vuz': {0: '', 1: ''},
        'dl': {0: '', 1: ''},
        'com': {0: '', 1: ''},
        'kpp': {0: '', 1: ''} //avto uniq
    };

    $('#search_form').find('li.selected').each(function () {
        var name = $.trim($(this).text()),
            type = $(this).attr('fd').replace(/_\d*//*, ''),
            id = $(this).attr('fd'),
            value = $(this).attr('fd-value'),
            text = $(this).attr('fd-text');

        list = new Object();

        list = {
            'name': name,
            'id': id,
            'text': text
        };

        if (!value)
            value = 1;

        if (typeof res[type] != 'undefined') {
            res[type].push(list);
            window.data_form_2[id] = value;
        }
    });

    var html = '<div class="searchPar">',
        text_item = '';

    for (var t in res) {

        var select_this = $('#search .select_' + t),
            input_name_add = '';

        if (res[t].length != 0 && res[t][0]['name'] != 'Не важно') {

            for (var i in res[t]) {

                if (res[t][i]['text'])
                    text_item = res[t][i]['text'];
                else
                    text_item = res[t][i]['name'];

                input_name_add += '<span input-name="' + res[t][i]['id'] + '">' + text_item + '<i></i></span>';

                html += '<div class="spItem" fd-id="' + res[t][i]['id'] + '">' + typeNames[t][0] + text_item + typeNames[t][1] + '<span class="spDel">x</span><i></i></div> ';

            }

            $(select_this).addClass('current_hide').find('.input_name div').html(input_name_add);

        } else {
            $(select_this).removeClass('current_hide').find('.input_name div').html(input_name_add);
        }
    }
    html += '</div>';
    $('.main .searchPar').replaceWith(html);

    $('.searchPar .spItem').click(function () {
        $(this).parent().hide();

        var id = $(this).attr('fd-id'),
            t = $('li[fd="' + id + '"]');

        changeForm(2, 1, t, id);
    });

}

// Поиск переход
$(document).on('click', '#search_form .btn', function () {
    ajaxForm();
});

function search_option_block(type) {
    var searchPar_callback = '',
        searchPar_new = '',
        search_option = $('#content').find('.search-option-empty');

    if (!type)
        type = 2;

    if (search_option.length > 0) {
        if (type == 1)
            searchPar_callback = '<a href="javascript:void(0);" onclick="callback_rep(null, 3, 0, 1); return false;" class="button_3"><span>Связаться с отложенными</span></a>';
        else
            searchPar_callback = '<a href="javascript:void(0);" onclick="callback_rep(null, 4, 0, 1); return false;" class="button_3"><span>Связаться с отложенными</span></a>';

        searchPar_new = '<div class="sort_st"><a id="sort_reit" class="active" href="javascript:void(0);" type_rep="' + type + '">По популярности<i></i></a><a id="sort_price" href="javascript:void(0);" type_rep="' + type + '">По цене<i></i></a><a id="sort_comment" href="javascript:void(0);" type_rep="' + type + '">По количеству отзывов<i></i></a><span class="sort_st_settings" onclick=""></span></div>' + searchPar_callback;
        $(search_option).html(searchPar_new);
    }
}*/
