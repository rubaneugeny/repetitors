$(document).ready(function () {

    var $element = '';

    $(document).on('click', '.map_open_raion', function () {

        if (!$element) {

            $.ajax({
                url: "/include/avto_raion.html?v=2",
                type: 'GET',
                async: false,
                dataType: 'text',
                success: function (data) {
                    $element = $(data);
                    $element.find('.edit_save_map').remove();
                }
            });


        }

        //Получаем название карты
        var map_name = '#' + $(this).attr('map-name');

        $.fancybox.open('<div id="h_metro" class="h_metro_ajax animated-modal clearfix"><div class="fancybox-title"><div>Карта метро<button type="button" data-fancybox-close="" class="fancybox-button fancybox-close-small" title="Close"><svg xmlns="http://www.w3.org/2000/svg" version="1" viewBox="0 0 24 24"><path d="M13 12l5-5-1-1-5 5-5-5-1 1 5 5-5 5 1 1 5-5 5 5 1-1z"></path></svg></button></div></div><div class="fancybox-body"><div id="lolo" style="width: 1600px;"></div></div></div>', {

            touch: false,
            hash: 'fancybox-open',
            baseClass: 'fancybox-container-hraion',

            afterLoad: function () {

                $('#lolo').prepend($element);

                //Выделяем на карте районы из анкеты или указаные
                $('#list_r_id input').each(function (i, elem) {

                    $('#' + $(elem).val()).data('maphilight', {"alwaysOn": true});

                });

                //Запускаем maphilight
                $(map_name + ' img[usemap]').maphilight({fillOpacity: 0.7});


                $(map_name + ' area').click(function (e) {
                    e.preventDefault();
                    var data = $(this).data('maphilight') || {};
                    data.alwaysOn = !data.alwaysOn;
                    $(this).data('maphilight', data).mouseout().trigger('alwaysOn.maphilight');
                });


                //Выделить или очистить весь округ
                $('.select_okrug input').click(function () {

                    var id = $(this).attr('name'),
                        id_check = $(this).is(':checked');

                    if (id == 'all')
                        id = map_name + ' area';
                    else
                        id = '.' + id;

                    if (id_check)
                        $(id).each(function (i, elem) {
                            $(elem).data('maphilight', {"alwaysOn": true});
                        });
                    else
                        $(id).each(function (i, elem) {
                            $(elem).data('maphilight', {"alwaysOn": false});
                        });

                    $(map_name + ' area').first().trigger('alwaysOn.maphilight');

                });


            },
            beforeClose: function () {

                var count = 1,
                    id_append = '#list_r_id';

                $(id_append).text('');

                var alw_id = 0,
                    alw_name_0 = '',
                    alw_name_1 = '',
                    alw_name_2 = '',
                    alw_name_3 = '';

                $('#map_moscow area').each(function (i, elem) {

                    var alw = $(elem).data('maphilight');

                    if (alw && alw.alwaysOn) {

                        alw_id = $(elem).attr('id');
                        alw_name_0 = $(elem).attr('data-name');

                        if (count == 1)
                            alw_name_1 = alw_name_1 + '<input type="hidden" name="r' + alw_id + '" value="' + alw_id + '" />' + alw_name_0;
                        else if (count <= 3)
                            alw_name_1 = alw_name_1 + ', <input type="hidden" name="r' + alw_id + '" value="' + alw_id + '" />' + alw_name_0;
                        else if (count >= 4)
                            alw_name_2 = alw_name_2 + ', <input type="hidden" name="r' + alw_id + '" value="' + alw_id + '" />' + alw_name_0;

                        count++;

                    }

                    if (count > 1) {

                        if (count <= 3)
                            alw_name_3 = alw_name_1;
                        else
                            alw_name_3 = alw_name_1 + '<span id="list_r_more">' + alw_name_2 + '</span>... <a span-name="list_r_more" class="list_l_more button_4" href="javascript:void(0);">читать далее</a>';

                        alw_name_3 = 'Выбрано: <input type="hidden" name="r_edit" value="1" />' + alw_name_3;
                        $(id_append).html(alw_name_3);

                        $(id_append).addClass('list_r_id_active');
                        $('#content').find('.map_open_raion').text('Изменить районы');

                    } else {

                        $(id_append).html('<input type="hidden" name="r_edit" value="0" />').removeClass('list_r_id_active');
                        $('#content').find('.map_open_raion').text('Выбрать районы');

                    }

                    $(id_append).find('input[name=r_edit]').trigger('change');

                });

            }
        });

        return false;
    });

    //Сохраняем изменения
    $(document).on("click", "#map_save", function () {
        $.fancybox.close();
    });

    $(document).on('click', '.list_l_more', function () {

        if ($(this).text() === "читать далее")
            $(this).text('скрыть');
        else
            $(this).text('читать далее');

        $(this).toggleClass('active_r');

        $('#' + $(this).attr('span-name')).toggleClass('active_r');

        return false;

    });

    $(document).on('click', '.open_block', function () {
        $($(this).attr('href')).slideToggle('slow');

        return false;

    });


    var num_avto = 2;

    $(document).on('click', '#plus_avto', function () {


        $('.avto_' + num_avto).show();

        if (num_avto == 4)
            $(this).remove();

        num_avto++;

        return false;

    });
});
