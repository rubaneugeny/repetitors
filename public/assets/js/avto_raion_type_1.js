$(document).ready(function () {

    $(document).on('click', '.map_open_raion', function () {

        //Получаем название карты
        var map_name = '#' + $(this).attr('map-name'),
            id = $(this).attr('avto-id'),
            t = 0;
        window.maphilight_nohover = 1;

        if (!$(this).hasClass('catalog'))
            t = $('.bread_ots').offset().top - 10;
        else
            t = $(this).parents('article').offset().top - 10;

        $.fancybox.open({
            src: "/include/avto_raion_catalog.php",
            type: "ajax",
            opts: {
                touch: false,
                hash: 'fancybox-open',
                baseClass: 'fancybox-container-hraion',
                ajax: {
                    settings: {
                        type: "POST",
                        data: {
                            id: id
                        }
                    }
                },
                afterLoad: function () {

                    //Выделяем на карте районы из анкеты или указаные
                    $('#list_r_id input').each(function (i, elem) {

                        $('#' + $(elem).val()).data('maphilight', {"alwaysOn": true});

                    });

                    //Запускаем maphilight
                    $(map_name + ' img[usemap]').maphilight({fillOpacity: 0.7});

                    $(map_name + ' area').click(function () {
                        return false;
                    });

                    $('#map_bottom_action').click(function () {
                        $.fancybox.close();
                    });

                },
                afterClose: function () {

                    $('html, body').animate({
                        scrollTop: t
                    }, 0);

                }
            }
        });

        return false;
    });

});
