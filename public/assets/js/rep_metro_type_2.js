
$(document).ready(function () {
    var $element = "";

    $(document).on("click", ".map_open_metro", function () {
        if (!$element) {
            $.ajax({
                url: "/js/metro_edit.svg?v=27",
                type: "GET",
                async: false,
                dataType: "text",
                success: function (data) {
                    $element = $(data);

                    //insert styles for svg
                    var svgCSS =
                        "" +
                        "metro:after{content:\"\";display:block;padding-bottom:120%}metro svg{position:absolute;left:0;top:0;width:100%}" +
                        ".st0{fill:none;stroke:#D8AC9D;stroke-width:7.9994;stroke-miterlimit:10}.st1{fill:none;stroke:#B59E99;stroke-width:7.9994;stroke-miterlimit:10}.st2{fill:none;stroke:#D3D3D5;stroke-width:7.9994;stroke-miterlimit:10}.st3{fill:none;stroke:#CFE58F;stroke-width:7.9994;stroke-miterlimit:10}.st4{fill:none;stroke:#95D59F;stroke-width:7.9994;stroke-miterlimit:10}.st5{fill:none;stroke:#C28FCC;stroke-width:7.9994;stroke-miterlimit:10}.st6{fill:none;stroke:#FABE94;stroke-width:7.9994;stroke-miterlimit:10}.st7{fill:none;stroke:#F58C84;stroke-width:7.9994;stroke-miterlimit:10}.st8{fill:none;stroke:#FFDF80;stroke-width:7.9994;stroke-miterlimit:10}.st9{fill:none;stroke:#62BFF8;stroke-width:7.9994;stroke-miterlimit:10}.st10{fill:none;stroke:#8FE7FF;stroke-width:7.9994;stroke-miterlimit:10}.st11{fill:none;stroke:#ACBFE3;stroke-width:7.9994;stroke-miterlimit:10}.st12{fill:none;stroke:#9CE8E8;stroke-width:7.9994;stroke-miterlimit:10}.st13{fill:none;stroke:#B59E99;stroke-width:9;stroke-miterlimit:10}.st14{fill:none;stroke:#FFF;stroke-width:2.6;stroke-miterlimit:10}.st15{fill:none;stroke:#D3D3D5;stroke-width:9;stroke-miterlimit:10}.st16{fill:none;stroke:#62BFF8;stroke-width:9;stroke-miterlimit:10}.st17{fill:none;stroke:#C28FCC;stroke-width:9;stroke-miterlimit:10}.st18{fill:none;stroke:#95D59F;stroke-width:9;stroke-miterlimit:10}.st19{fill:none;stroke:#FABE94;stroke-width:9;stroke-miterlimit:10}.st20{fill:none;stroke:#F58C84;stroke-width:9;stroke-miterlimit:10}.st21{fill:none;stroke:#FFDF80;stroke-width:9;stroke-miterlimit:10}.st22{fill:none;stroke:#8FE7FF;stroke-width:9;stroke-miterlimit:10}.st23{fill:none;stroke:#D8AC9D;stroke-width:9;stroke-miterlimit:10}.st24{fill:none;stroke:#CFE58F;stroke-width:9;stroke-miterlimit:10}.st25{fill:none;stroke:#FFCB32;stroke-width:9;stroke-miterlimit:10}.st26{font-family:'PT Sans', sans-serif;}.st27{font-size:20px}.st28{fill:none;stroke:#C28FCC;stroke-width:8;stroke-miterlimit:10}.st29{fill:#8E479B}.st30{display:none;enable-background:new}.st31{fill:#C28FCC}.st32{fill:#FFF}.st33{fill:#FABE94}.st34{fill:#F58232}.st35{fill:none;stroke:#95D59F;stroke-width:8;stroke-miterlimit:10}.st36{fill:#4CB85E}.st37{fill:#95D59F}.st38{fill:#D8AC9D}.st39{fill:#9D573E}.st40{fill:#F58C84}.st41{fill:#ED3326}.st42{fill:#B59E99}.st43{fill:#7A655F}.st44{fill:#ACBFE3}.st45{fill:none;stroke:#ACBFE2;stroke-width:8;stroke-miterlimit:10}.st46{display:none;fill:#ACBFE3}.st47{fill:#8FE7FF}.st48{fill:#00BEF0}.st49{fill:none;stroke:#8FE7FF;stroke-width:8;stroke-miterlimit:10}.st50{fill:none;stroke:#62BFF8;stroke-width:8;stroke-miterlimit:10}.st51{fill:#0078BF}.st52{fill:#62BFF8}.st53{fill:#FFDF80}.st54{fill:#FFCB35}.st55{fill:none;stroke:#FFDF80;stroke-width:8;stroke-miterlimit:10}.st56{fill:#CFE58F}.st57{fill:#B4D445}.st58{fill:none;stroke:#CFE58F;stroke-width:8;stroke-miterlimit:10}.st59{fill:#D3D3D5}.st60{fill:#A0A2A3}.st61{fill:#9CE8E8}.st62{fill:#79CDCD}.st63{fill:none;stroke:#9CE8E8;stroke-width:8;stroke-miterlimit:10}.st64{fill:none;stroke:#D3D3D5;stroke-width:8;stroke-miterlimit:10}.st65{fill:none;stroke:#FABE94;stroke-width:8;stroke-miterlimit:10}.st66{fill:none;stroke:#F58C84;stroke-width:8;stroke-miterlimit:10}.st67{fill:none;stroke:#B59E99;stroke-width:8;stroke-miterlimit:10}" +
                        "g.metro-station{cursor:pointer}g.metro-station.disabled{cursor:initial}g.metro-station.selected text,g.metro-station.selected tspan{font-weight:700!important}g.metro-station.disabled text,g.metro-station.disabled tspan{fill:gray;cursor:initial}g.metro-station text.style1{font-family:'PT Sans','sans-serif';font-weight:400;font-style:normal;font-stretch:normal;font-variant:normal;font-size:20px}g.metro-active .metro-point {display:block;fill:#4CB85E; stroke:#fff;}g.metro-station:hover .poly-yes {stroke: #4cb85e;}g.metro-active:hover .poly-yes {stroke: #fff;}g.vetka{cursor:pointer;}g.vetka_select_all:hover .st70,g.vetka_clear_all:hover .st70,g.vetka:hover .st70{fill: red;}";

                    var style = document.createElement("style");
                    style.id = "dvhb_metro_styles";
                    style.type = "text/css";
                    style.innerHTML = svgCSS;
                    document.getElementsByTagName("head")[0].appendChild(style);
                }
            });
        }

        var $metro_list = $("#list_r_id");

        $($element).find("[id^=\"s\"]").each(function () {
            var $this = $(this),
                $circle = $this.find("circle"),
                name = $this.find("text:first").text();

            $this
                .attr("class", $this.attr("class") + " metro-station")
                .attr("metro-station-name", name);

            if ($metro_list.find("input[name=\"" + $this.attr("id") + "\"]").val()) {
                $this.attr("class", $this.attr("class") + " metro-active");
            }

            $circle.each(function () {
                $(this).attr("class", $(this).attr("class") + " metro-point");
            });
        });

        $.fancybox.open("<div id=\"h_metro\" class=\"h_metro_ajax animated-modal clearfix\"><div class=\"fancybox-title\"><div>Карта метро<button type=\"button\" data-fancybox-close=\"\" class=\"fancybox-button fancybox-close-small\" title=\"Close\"><svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1\" viewBox=\"0 0 24 24\"><path d=\"M13 12l5-5-1-1-5 5-5-5-1 1 5 5-5 5 1 1 5-5 5 5 1-1z\"></path></svg></button></div></div><div class=\"fancybox-body\"><div id=\"lolo\" style=\"width: 1600px;\"></div></div></div>", {
            touch: false,
            hash: "fancybox-open",
            baseClass: "fancybox-container-hmetro",
            afterLoad: function () {
                $("#lolo").prepend($element);
                $(".vetka").click(function () {
                    var id_vetka = $(this).attr("id-vetka");

                    if (!$(this).hasClassSVG("vetka_selected"))
                        $(".vetka" + id_vetka).addClassSVG("metro-active");
                    else
                        $(".vetka" + id_vetka).removeClassSVG("metro-active");

                    $(this).toggleClassSVG("vetka_selected");
                });

                $(".vetka_select_all").click(function () {
                    $(".metro-station").addClassSVG("metro-active");
                });

                $(".vetka_clear_all").click(function () {
                    $(".metro-station").removeClassSVG("metro-active");
                });

                $(".metro-station").click(function () {
                    $(this).toggleClassSVG("metro-active");
                });
            },
            beforeClose: function () {
                var count = 1,
                    id_append = $("#list_r_id"),
                    metro_active = $("#h_metro").find(".metro-active"),
                    metro_active_count = metro_active.length;

                if (metro_active_count) {
                    var alw_id = 0,
                        alw_name_0 = "",
                        alw_name_1 = "",
                        alw_name_2 = "",
                        alw_name_3 = "";

                    metro_active.each(function (i, elem) {
                        alw_id = $(elem).attr("id-s");
                        alw_name_0 = $(elem).attr("metro-station-name");

                        if (count === 1)
                            alw_name_1 = alw_name_1 + "<input type=\"hidden\" name=\"s" + alw_id + "\" value=\"" + alw_id + "\" />" + alw_name_0;
                        else if (count <= 3)
                            alw_name_1 = alw_name_1 + ", <input type=\"hidden\" name=\"s" + alw_id + "\" value=\"" + alw_id + "\" />" + alw_name_0;
                        else if (count >= 4)
                            alw_name_2 = alw_name_2 + ", <input type=\"hidden\" name=\"s" + alw_id + "\" value=\"" + alw_id + "\" />" + alw_name_0;

                        count++;
                    });

                    if (metro_active_count <= 3)
                        alw_name_3 = alw_name_1;
                    else
                        alw_name_3 = alw_name_1 + "<span id=\"list_r_more\">" + alw_name_2 + "</span>... <a span-name=\"list_r_more\" class=\"list_l_more button_4\" href=\"#\">читать далее</a>";

                    alw_name_3 = "Выбрано: <input type=\"hidden\" name=\"r_edit\" value=\"1\" />" + alw_name_3;
                    $(id_append).html(alw_name_3);
                    $(id_append).addClass("list_r_id_active");
                    $("#content").find(".map_open_metro").text("Изменить районы");
                } else {
                    $(id_append).html("<input type=\"hidden\" name=\"r_edit\" value=\"0\" />").removeClass("list_r_id_active");
                    $("#content").find(".map_open_metro").text("Выбрать районы");
                }

                $(id_append).find("input[name=r_edit]").trigger("change");
            }
        });

    });

    //Сохраняем изменения
    $(document).on("click", "#map_save", function () {
        $.fancybox.close();
    });

    $(document).on("click", ".list_l_more", function () {
        if ($(this).text() === "читать далее")
            $(this).text("скрыть");
        else
            $(this).text("читать далее");

        $(this).toggleClass("active_r");

        $("#" + $(this).attr("span-name")).toggleClass("active_r");

        return false;
    });

    $(document).on("click", ".open_block", function () {
        $($(this).attr("href")).slideToggle("slow");

        return false;
    });

    var num_avto = 2;

    $(document).on("click", "#plus_avto", function () {
        $(".avto_" + num_avto).show();

        if (num_avto == 4)
            $(this).remove();

        num_avto++;

        return false;
    });
});
