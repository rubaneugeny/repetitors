<?php

namespace App\Http\Controllers;

use App\Extensions\CustomLengthAwarePaginator;
use App\Helpers\Helper;
use App\Models\BreadcrumbsCategories;
use App\Models\BreadcrumbsTemplates;
use App\Models\Cities;
use App\Models\CommentsPrependTextCategories;
use App\Models\CommentsPrependTextTemplates;
use App\Models\Districts;
use App\Models\HeadersCategories;
use App\Models\HeadersTemplates;
use App\Models\LinksCategories;
use App\Models\LinksTemplates;
use App\Models\Metro;
use App\Models\Multipliers;
use App\Models\PagesAppendTextCategories;
use App\Models\PagesAppendTextTemplates;
use App\Models\PrependTextCategories;
use App\Models\PrependTextTemplates;
use App\Models\TitleCategories;
use App\Models\TitleTemplates;
use App\Models\TotalCategories;
use App\Models\TotalTemplates;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Pagination\Paginator;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\View\View;
use Jenssegers\Agent\Agent;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
//        $agent = new Agent();
//
//        if ($agent->isMobile()) {
//            if (!Cookie::has('device')) {
//                Cookie::queue(Cookie::forever('device', 1, '/'));
//            } else {
//                Cookie::queue(Cookie::forever('device', 0, 1));
//            }
//        }

        if (!Cookie::has('fav')) {
            Cookie::queue(
                'fav',
                Str::uuid(),
                time() + (60 * 60 * 24 * 365),
                '/',
                null,
                false,
                false
            );
        }
    }

    public function paginate($request, $collection, $pageName = 'start')
    {
        $perPage = env('ITEMS_PER_PAGE', 10);
        $page = ceil(($request->get($pageName, 0) + $perPage) / $perPage);

        $results = ($total = $collection->toBase()->getCountForPagination())
            ? $collection->forPage($page, $perPage)->get()
            : new Collection();

        return new CustomLengthAwarePaginator($results, $total, $perPage, $page, [
            'path' => $request->url(),
            'query' => $request->query(),
            'pageName' => $pageName,
            'onEachSide' => 2,
        ]);
    }

    /******************************************************************************************************************/

    /**
     * Заполнение шаблона Title, Description
     *
     * @param Model $template
     * @param int|null $fromPage
     * @param int|null $toPage
     * @param Model|null $course
     * @param Model|null $metro_district_city
     *
     * @return array
     */
    private function fillTitleDescriptionByName(Model $template, ?int $fromPage, ?int $toPage, ?Model $course, ?Model $metro_district_city)
    {
        $values = [
            '{FROM}' => $fromPage,
            '{TO}' => $toPage,
            '{IN_THE_DIRECTION}' => $metro_district_city ? $metro_district_city->in_the_direction : '',
            '{NAME}' => $metro_district_city ? $metro_district_city->name : '',
            '{SEO_EMOJI}' => $course && $course->seo ? $course->seo->emoji : '',
            '{COURSE_NAME}' => $course ? $course->name : '',
            '{RECOMMEND_NAME}' => $course ? $course->recommend_name : '',
            '{RECOMMEND_NAME_SECOND}' => $course ? $course->recommend_name_second : '',
            '{RECOMMEND_NAME_THIRD}' => $course ? $course->recommend_name_third : '',
        ];

        return [
            'title' => strtr($template->title, $values),
            'description' => strtr($template->description, $values),
        ];
    }

    /**
     * Получение Title, Description для страницы по $name
     *
     * @param string $name
     * @param int $page
     * @param int|null $from
     * @param int|null $to
     * @param Model|null $course
     * @param Model|null $metro_district_city
     *
     * @return string[]
     */
    public function getTitleDescriptionByName(string $name, int $page, ?int $from, ?int $to, ?Model $course, ?Model $metro_district_city)
    {
        if ($category = TitleCategories::where(['name' => $name])->first()) {
            if ($course) {
                if ($template = TitleTemplates::where([
                        'id_title_categories' => $category->id_title_categories,
                        'id_courses' => $course->id_courses,
                        'page' => $page,
                    ])->first()) {
                    return $this->fillTitleDescriptionByName($template, $from, $to, $course, $metro_district_city);
                } elseif ($template = TitleTemplates::where([
                        'id_title_categories' => $category->id_title_categories,
                        'id_courses' => $course->id_courses,
                        'page' => 0,
                    ])->first()) {
                    return $this->fillTitleDescriptionByName($template, $from, $to, $course, $metro_district_city);
                } elseif ($template = TitleTemplates::where([
                        'id_title_categories' => $category->id_title_categories,
                        'id_courses_categories_second' => $course->id_courses_categories_second,
                        'id_courses' => null,
                        'page' => $page,
                    ])->first()) {
                    return $this->fillTitleDescriptionByName($template, $from, $to, $course, $metro_district_city);
                } elseif ($template = TitleTemplates::where([
                        'id_title_categories' => $category->id_title_categories,
                        'id_courses_categories_second' => $course->id_courses_categories_second,
                        'id_courses' => null,
                        'page' => 0,
                    ])->first()) {
                    return $this->fillTitleDescriptionByName($template, $from, $to, $course, $metro_district_city);
                }
            } else {
                if ($template = TitleTemplates::where([
                        'id_title_categories' => $category->id_title_categories,
                        'id_courses_categories_second' => null,
                        'id_courses' => null,
                        'page' => $page,
                    ])->first()) {
                    return $this->fillTitleDescriptionByName($template, $from, $to, $course, $metro_district_city);
                } elseif ($template = TitleTemplates::where([
                        'id_title_categories' => $category->id_title_categories,
                        'id_courses_categories_second' => null,
                        'id_courses' => null,
                        'page' => 0,
                    ])->first()) {
                    return $this->fillTitleDescriptionByName($template, $from, $to, $course, $metro_district_city);
                }
            }
        }

        return [
            'title' => '',
            'description' => ''
        ];
    }

    /******************************************************************************************************************/

    /**
     * Заполнение хлебных крошек
     *
     * @param Model $template
     * @param int|null $fromPage
     * @param int|null $toPage
     * @param Model|null $course
     * @param Model|null $metro_district_city
     *
     * @return Model
     */
    private function fillBreadcrumbsByName(Model $template, ?int $fromPage, ?int $toPage, ?Model $course, ?Model $metro_district_city)
    {
        $values = [
            '{FROM}' => $fromPage,
            '{TO}' => $toPage,
            '{NAME}' => $metro_district_city ? $metro_district_city->name : '',
            '{SLUG}' => $metro_district_city ? $metro_district_city->slug : '',
            '{COURSE_NAME}' => $course ? $course->name : '',
            '{COURSE_SLUG}' => $course ? $course->slug : ''
        ];

        $template->last = strtr($template->last, $values);
        $template->items = json_decode(strtr($template->items, $values));

        return $template;
    }

    /**
     * Получение хлебных крошек по $name
     *
     * @param string|null $name
     * @param int $page
     * @param int|null $from
     * @param int|null $to
     * @param Model|null $course
     * @param Model|null $metro_district_city
     *
     * @return Model|string
     */
    public function getBreadcrumbsByName(?string $name, int $page, ?int $from, ?int $to, ?Model $course, ?Model $metro_district_city)
    {
        if ($category = BreadcrumbsCategories::where(['name' => $name])->first()) {
            if ($course && $template = BreadcrumbsTemplates::where([
                    'id_breadcrumbs_categories' => $category->id_breadcrumbs_categories,
                    'id_courses' => $course->id_courses,
                    'page' => $page
                ])->first()) {
                return $this->fillBreadcrumbsByName($template, $from, $to, $course, $metro_district_city);
            } elseif ($course && $template = BreadcrumbsTemplates::where([
                    'id_breadcrumbs_categories' => $category->id_breadcrumbs_categories,
                    'id_courses' => $course->id_courses,
                    'page' => 0
                ])->first()) {
                return $this->fillBreadcrumbsByName($template, $from, $to, $course, $metro_district_city);
            } elseif ($template = BreadcrumbsTemplates::where([
                    'id_breadcrumbs_categories' => $category->id_breadcrumbs_categories,
                    'id_courses' => null,
                    'page' => $page
                ])->first()) {
                return $this->fillBreadcrumbsByName($template, $from, $to, $course, $metro_district_city);
            } elseif ($template = BreadcrumbsTemplates::where([
                    'id_breadcrumbs_categories' => $category->id_breadcrumbs_categories,
                    'id_courses' => null,
                    'page' => 0
                ])->first()) {
                return $this->fillBreadcrumbsByName($template, $from, $to, $course, $metro_district_city);
            }
        }

        return '';
    }

    /******************************************************************************************************************/

    /**
     * Заполнение полей заголовка по имени
     *
     * @param Model $template
     * @param Model|null $course
     * @param Model|null $metro_district_city
     *
     * @return string
     */
    private function fillHeaderByName(Model $template, ?Model $course, ?Model $metro_district_city)
    {
        $values = [
            '{IN_THE_DIRECTION}' => $metro_district_city ? $metro_district_city->in_the_direction : '',
            '{NAME}' => $metro_district_city ? $metro_district_city->name : '',
            '{RECOMMEND_NAME}' => $course ? $course->recommend_name : '',
            '{RECOMMEND_NAME_SECOND}' => $course ? $course->recommend_name_second : ''
        ];

        return strtr($template->template, $values);
    }

    /**
     * Поиск заголовка по имени
     *
     * @param string|null $name
     * @param int $page
     * @param Model|null $course
     * @param Model|null $metro_district_city
     *
     * @return string
     */
    public function getHeaderByName(?string $name, int $page, ?Model $course, ?Model $metro_district_city)
    {
        if ($category = HeadersCategories::where(['name' => $name])->first()) {
            if ($course && $template = HeadersTemplates::where([
                    'id_headers_categories' => $category->id_headers_categories,
                    'id_courses' => $course->id_courses,
                    'page' => $page
                ])->first()) {
                return $this->fillHeaderByName($template, $course, $metro_district_city);
            } elseif ($course && $template = HeadersTemplates::where([
                    'id_headers_categories' => $category->id_headers_categories,
                    'id_courses' => $course->id_courses,
                    'page' => 0
                ])->first()) {
                return $this->fillHeaderByName($template, $course, $metro_district_city);
            } elseif ($course && $template = HeadersTemplates::where([
                    'id_headers_categories' => $category->id_headers_categories,
                    'id_courses_categories_second' => $course->id_courses_categories_second,
                    'id_courses' => null,
                    'page' => $page
                ])->first()) {
                return $this->fillHeaderByName($template, $course, $metro_district_city);
            } elseif ($course && $template = HeadersTemplates::where([
                    'id_headers_categories' => $category->id_headers_categories,
                    'id_courses_categories_second' => $course->id_courses_categories_second,
                    'id_courses' => null,
                    'page' => 0
                ])->first()) {
                return $this->fillHeaderByName($template, $course, $metro_district_city);
            } elseif ($template = HeadersTemplates::where([
                    'id_headers_categories' => $category->id_headers_categories,
                    'id_courses_categories_second' => null,
                    'id_courses' => null,
                    'page' => $page
                ])->first()) {
                return $this->fillHeaderByName($template, $course, $metro_district_city);
            } elseif ($template = HeadersTemplates::where([
                    'id_headers_categories' => $category->id_headers_categories,
                    'id_courses_categories_second' => null,
                    'id_courses' => null,
                    'page' => 0
                ])->first()) {
                return $this->fillHeaderByName($template, $course, $metro_district_city);
            }
        }

        return '';
    }

    /******************************************************************************************************************/

    /**
     * Заполнение шаблона "Всего репетиторов на странице" по $name
     *
     * @param Model $template
     * @param int $count
     * @param Model|null $course
     * @param Model|null $metro_district_city
     *
     * @return string
     */
    private function fillTotalTextByName(Model $template, int $count, ?Model $course, ?Model $metro_district_city): string
    {
        $values = [
            '{TOTAL_COUNT}' => $count,
            '{IN_THE_DIRECTION}' => $metro_district_city ? $metro_district_city->in_the_direction : '',
            '{NAME}' => $metro_district_city ? $metro_district_city->name : '',
            '{RECOMMEND_NAME}' => $course ? $course->recommend_name : '',
            '{RECOMMEND_NAME_SECOND}' => $course ? $course->recommend_name_second : ''
        ];

        return strtr($template->template, $values);
    }

    /**
     * Получение шаблона для "Всего репетиторов на странице" на странице каталогов репетиторов по $name
     *
     * @param string|null $name
     * @param int $page
     * @param int $count
     * @param Model|null $course
     * @param Model|null $metro_district_city
     *
     * @return string
     */
    public function getTotalTextByName(?string $name, int $page, int $count, ?Model $course, ?Model $metro_district_city): string
    {
        if ($category = TotalCategories::where(['name' => $name])->first()) {
            if ($course && $template = TotalTemplates::where([
                    'id_total_categories' => $category->id_total_categories,
                    'id_courses' => $course->id_courses,
                    'page' => $page
                ])->first()) {
                return $this->fillTotalTextByName($template, $count, $course, $metro_district_city);
            } elseif ($course && $template = TotalTemplates::where([
                    'id_total_categories' => $category->id_total_categories,
                    'id_courses' => $course->id_courses,
                    'page' => 0
                ])->first()) {
                return $this->fillTotalTextByName($template, $count, $course, $metro_district_city);
            } elseif ($course && $template = TotalTemplates::where([
                    'id_total_categories' => $category->id_total_categories,
                    'id_courses_categories_second' => $course->id_courses_categories_second,
                    'id_courses' => null,
                    'page' => $page
                ])->first()) {
                return $this->fillTotalTextByName($template, $count, $course, $metro_district_city);
            } elseif ($course && $template = TotalTemplates::where([
                    'id_total_categories' => $category->id_total_categories,
                    'id_courses_categories_second' => $course->id_courses_categories_second,
                    'id_courses' => null,
                    'page' => 0
                ])->first()) {
                return $this->fillTotalTextByName($template, $count, $course, $metro_district_city);
            } elseif ($template = TotalTemplates::where([
                    'id_total_categories' => $category->id_total_categories,
                    'id_courses_categories_second' => null,
                    'id_courses' => null,
                    'page' => $page
                ])->first()) {
                return $this->fillTotalTextByName($template, $count, $course, $metro_district_city);
            } elseif ($template = TotalTemplates::where([
                    'id_total_categories' => $category->id_total_categories,
                    'id_courses_categories_second' => null,
                    'id_courses' => null,
                    'page' => 0
                ])->first()) {
                return $this->fillTotalTextByName($template, $count, $course, $metro_district_city);
            }
        }

        return '';
    }

    /******************************************************************************************************************/

    /**
     * Заполнение шаблона после "Всего репетиторов на странице"
     *
     * @param Model $template
     * @param Model|null $course
     * @param Model|null $metro_district_city
     *
     * @return Application|Factory|View|string
     */
    private function fillPrependTextByName(Model $template, ?Model $course, ?Model $metro_district_city)
    {
        $values = [
            '{IN_THE_DIRECTION}' => $metro_district_city ? $metro_district_city->in_the_direction : '',
            '{NAME}' => $metro_district_city ? $metro_district_city->name : '',
            '{RECOMMEND_NAME}' => $course ? $course->recommend_name : '',
            '{RECOMMEND_NAME_SECOND}' => $course ? $course->recommend_name_second : '',
            '{RECOMMEND_NAME_FOURTH}' => $course ? $course->recommend_name_fourth : ''
        ];

        $text = strtr($template->template, $values);

        if ($template->text_limit) {
            return $template->split
                ? Helper::splitSentence($text, $template->text_limit, $template->class)
                : Helper::cutUpToSentence($text, $template->text_limit, $template->class);
        }

        if ($template->wrap) {
            return view('components.helper.prepend-text', ['text' => $text]);
        }

        return $text;
    }

    /**
     * Получение шаблона для текста после "Всего репетиторов на странице" на странице каталогов репетиторов по $name
     *
     * @param string|null $name
     * @param int $page
     * @param Model|null $course
     * @param Model|null $metro_district_city
     *
     * @return string
     */
    public function getPrependTextByName(?string $name, int $page, ?Model $course, ?Model $metro_district_city): string
    {
        if ($category = PrependTextCategories::where(['name' => $name])->first()) {
            $id = null;

            // Определяем id для исключения текста в районах, метро или городах
            if ($metro_district_city) {
                if ($metro_district_city instanceof Metro) {
                    $id = $metro_district_city->id_metro;
                } elseif ($metro_district_city instanceof Districts) {
                    $id = $metro_district_city->id_districts;
                } elseif ($metro_district_city instanceof Cities) {
                    $id = $metro_district_city->id_cities;
                }
            }

            if ($course && $template = PrependTextTemplates::where([
                        'id_prepend_text_categories' => $category->id_prepend_text_categories,
                        'id_courses' => $course->id_courses,
                        'page' => $page
                    ])
                    ->when($id, function ($query) use ($id) {
                        $query->whereRaw('NOT FIND_IN_SET(?, excludes)', [$id]);
                    })
                    ->first()) {
                return $this->fillPrependTextByName($template, $course, $metro_district_city);
            } elseif ($course && $template = PrependTextTemplates::where([
                        'id_prepend_text_categories' => $category->id_prepend_text_categories,
                        'id_courses' => $course->id_courses,
                        'page' => 0
                    ])
                    ->when($id, function ($query) use ($id) {
                        $query->whereRaw('NOT FIND_IN_SET(?, excludes)', [$id]);
                    })
                    ->first()) {
                return $this->fillPrependTextByName($template, $course, $metro_district_city);
            } elseif ($course && $template = PrependTextTemplates::where([
                        'id_prepend_text_categories' => $category->id_prepend_text_categories,
                        'id_courses_categories_second' => $course->id_courses_categories_second,
                        'id_courses' => null,
                        'page' => $page
                    ])
                    ->when($id, function ($query) use ($id) {
                        $query->whereRaw('NOT FIND_IN_SET(?, excludes)', [$id]);
                    })
                    ->first()) {
                return $this->fillPrependTextByName($template, $course, $metro_district_city);
            } elseif ($course && $template = PrependTextTemplates::where([
                        'id_prepend_text_categories' => $category->id_prepend_text_categories,
                        'id_courses_categories_second' => $course->id_courses_categories_second,
                        'id_courses' => null,
                        'page' => 0
                    ])
                    ->when($id, function ($query) use ($id) {
                        $query->whereRaw('NOT FIND_IN_SET(?, excludes)', [$id]);
                    })
                    ->first()) {
                return $this->fillPrependTextByName($template, $course, $metro_district_city);
            } elseif ($template = PrependTextTemplates::where([
                    'id_prepend_text_categories' => $category->id_prepend_text_categories,
                    'id_courses_categories_second' => null,
                    'id_courses' => null,
                    'page' => $page
                ])->first()) {
                return $this->fillPrependTextByName($template, $course, $metro_district_city);
            } elseif ($template = PrependTextTemplates::where([
                    'id_prepend_text_categories' => $category->id_prepend_text_categories,
                    'id_courses_categories_second' => null,
                    'id_courses' => null,
                    'page' => 0
                ])->first()) {
                return $this->fillPrependTextByName($template, $course, $metro_district_city);
            }
        }

        return '';
    }

    /******************************************************************************************************************/

    /**
     * Заполнение заблона текста перед комментарием
     *
     * @param Collection $templates
     * @return array
     */
    private function fillCommentsPrependTemplatesByName(Collection $templates)
    {
        $result = [];

        $values = [
        ];

        foreach ($templates as $template) {
            $result[$template->row_index] = strtr($template->template, $values);
        }

        return $result;
    }

    /**
     * Получение текста перед комментарием по $name
     *
     * @param string|null $name
     * @param int $page
     *
     * @return array
     */
    public function getCommentsPrependTextByName(?string $name, int $page)
    {
        if ($category = CommentsPrependTextCategories::where(['name' => $name])->first()) {
            if (count($templates = CommentsPrependTextTemplates::where([
                'id_comments_prepend_text_categories' => $category->id_comments_prepend_text_categories,
                'page' => $page
            ])->get()) > 0) {
                return $this->fillCommentsPrependTemplatesByName($templates);
            }
        }

        return [];
    }

    /******************************************************************************************************************/

    /**
     * @param Model $template
     *
     * @return Application|Factory|View|string
     */
    private function fillPagesAppendTextByName(Model $template)
    {
        $values = [
        ];

        $text = strtr($template->template, $values);

        if ($template->text_limit) {
            return $template->split
                ? Helper::splitSentence($text, $template->text_limit, $template->class)
                : Helper::cutUpToSentence($text, $template->text_limit, $template->class);
        }

        if ($template->wrap) {
            return view('components.helper.prepend-text', ['text' => $text]);
        }

        return $text;
    }

    /**
     *
     * @param string $name
     * @param int $page
     *
     * @return string
     */
    public function getPagesAppendTextByName(string $name, int $page) {
        if ($category = PagesAppendTextCategories::where(['name' => $name])->first()) {
            if ($template = PagesAppendTextTemplates::where([
                'id_pages_append_text_categories' => $category->id_pages_append_text_categories,
                'page' => $page
            ])->first()) {
                return $this->fillPagesAppendTextByName($template);
            } elseif ($template = PagesAppendTextTemplates::where([
                'id_pages_append_text_categories' => $category->id_pages_append_text_categories,
                'page' => 0
            ])->first()) {
                return $this->fillPagesAppendTextByName($template);
            }
        }

        return '';
    }

    /**
     * @param string $name
     * @param int $page
     * @return \Illuminate\Database\Eloquent\HigherOrderBuilderProxy|mixed|string
     */
    public function getLinksByName(?string $name, int $page)
    {
        if ($category = LinksCategories::where(['name' => $name])->first()) {
            if ($template = LinksTemplates::where([
                'id_links_categories' => $category->id_links_categories,
                'page' => $page
            ])->first()) {
                return $template->template;
            } elseif ($template = LinksTemplates::where([
                'id_links_categories' => $category->id_links_categories,
                'page' => 0
            ])->first()) {
                return $template->template;
            }
        }

        return '';
    }

    /**
     * Установка сообщения об ошибке в сессию
     *
     * @param string $message
     */
    public function setErrorMessage(string $message)
    {
        session()->flash('error', $message);
    }

    /**
     * Установка успешного сообщения в сессию
     *
     * @param string $message
     */
    public function setSuccessMessage(string $message)
    {
        session()->flash('success', $message);
    }
}
