<?php

namespace App\Http\Controllers;

use App\Filters\UsersFilter;
use App\Filters\UsersWithRatingTableFilter;
use App\Helpers\Helper;
use App\Http\Requests\CarInstructorsSearchRequest;
use App\Models\Cars;
use App\Models\CarsManufactures;
use App\Models\Cities;
use App\Models\Courses;
use App\Models\Districts;
use App\Models\Metro;
use App\Models\Ratings;
use App\Models\Users;
use App\Models\UsersActiveStatuses;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Illuminate\Http\Request;

class CarInstructorsController extends Controller
{
    private $courses = [
        Courses::AUTOMATIC_TRANSMISSION,
        Courses::MANUAL_TRANSMISSION
    ];

    /**
     * Поиск автоинструктора по выбранным параметрам в фильтре
     *
     * @param Request $request
     */
    public function search(CarInstructorsSearchRequest $request)
    {
        $repetitors = Users::from(app(Users::class)->getTable(), 'u')
            ->where([
                'u.is_car_instructor' => true,
                'u.id_users_active_statuses' => UsersActiveStatuses::ACTIVE,
                'u.is_deleted' => 0
            ]);

        $repetitors = $this->paginate($request, (new UsersFilter($repetitors, $request))->apply(true));

        if ($request->ajax()) {
            Helper::makeFilterButtons($request);

            $breadcrumbs = view('components.breadcrumbs', [
                'type' => Helper::CAR_INSTRUCTOR,
                'last' => 'Поиск инструкторов по вождению'
            ])->render();

            $header = view('components.h1', [
                'name' => 'Поиск инструкторов по вождению'
            ])->render();

            $totalText = view('components.car-instructors.total-text', [
                'repetitors' => $repetitors
            ])->render();

            $prependText = view('components.car-instructors.prepend-text')->render();

            $content = view('components.ajax.repetitors', [
                'type' => Helper::CAR_INSTRUCTOR,
                'repetitors' => $repetitors->appends(request()->input())
            ])->render();

            return response()->json([
                'breadcrumbs' => $breadcrumbs,
                'header' => $header,
                'totalText' => $totalText,
                'prependText' => $prependText,
                'content' => $content
            ]);
        }

        return view('pages.car-instructors.search', [
            'repetitors' => $repetitors->appends(request()->input())
        ]);
    }

    /**
     * Поиск репетитора по автомобилю
     *
     * @return Factory|View
     */
    public function searchRepetitorByCar()
    {
        $cars = Cars::from(app(Cars::class)->getTable(), 'c')
            ->leftJoin(app(CarsManufactures::class)->getTable() . ' AS cm', 'cm.id_cars_manufactures', '=', 'c.id_cars_manufactures')
            ->selectRaw('cm.name_eng AS manufacturer, c.*')
            ->orderBy('cm.name_eng', 'ASC')
            ->orderBy('c.name_eng', 'ASC')
            ->get();

        return view('pages.car-instructors.search-by-car', [
            'cars' => $cars
        ]);
    }

    /**
     * Разделы:
     * avtoinstruktor_obuchenie_vozhdeniju_v_spb.php
     * instruktor_po_vozhdeniju_sankt-peterburg.php
     * instruktor_po_vozhdeniju_zhenshhina.php
     * motoinstruktor.php
     *
     * @param $slug
     * @param Request $request
     *
     * @return Application|Factory|View
     */
    public function searchCarInstructors($slug, Request $request)
    {
        $repetitors = Users::where([
                'u.is_car_instructor' => true,
                'u.id_users_active_statuses' => UsersActiveStatuses::ACTIVE,
                'u.is_deleted' => 0
            ])
            ->from(app(Users::class)->getTable(), 'u')
            ->when($slug == 'instruktor_po_vozhdeniju_zhenshhina', function ($query) {
                $query->where(['u.sex' => Users::SEX_WOMAN]);
            })
            ->when($slug == 'motoinstruktor', function ($query) {
                $query->where(function ($query) {
                    $query
                        ->where(['id_courses' => Courses::MOTORBIKES])
                        ->orWhere(['id_courses_second' => Courses::MOTORBIKES])
                        ->orWhere(['id_courses_third' => Courses::MOTORBIKES])
                        ->orWhere(['id_courses_fourth' => Courses::MOTORBIKES]);
                });
            });

        $repetitors = $this->paginate($request, (new UsersFilter($repetitors, $request))->apply());

        $seo = $this->getTitleDescriptionByName($slug, $repetitors->currentPage(), $repetitors->firstItem(), $repetitors->lastItem(), null, null);
        $breadcrumbs = $this->getBreadcrumbsByName($slug, $repetitors->currentPage(), $repetitors->firstItem(), $repetitors->lastItem(), null, null);
        $header = $this->getHeaderByName($slug, $repetitors->currentPage(), null, null);
        $totalText = $this->getTotalTextByName($slug, $repetitors->currentPage(), $repetitors->total(), null, null);
        $prependText = $this->getPrependTextByName($slug, $repetitors->currentPage(), null, null);
        $commentsPrependText = $this->getCommentsPrependTextByName($slug, $repetitors->currentPage());
        $pagesAppendText = $this->getPagesAppendTextByName($slug, $repetitors->currentPage());
        $link = $this->getLinksByName($slug, $repetitors->currentPage());

        return view('pages.car-instructors.main', [
            'slug' => $slug,
            'seo' => $seo,
            'breadcrumbs' => $breadcrumbs,
            'header' => $header,
            'totalText' => $totalText,
            'prependText' => $prependText,
            'commentsPrependText' => $commentsPrependText,
            'pagesAppendText' => $pagesAppendText,
            'link' => $link,
            'repetitors' => $repetitors
        ]);
    }

    /**
     * Поиск репетитора по выбранному автомобилю
     *
     * @param         $car
     * @param Request $request
     *
     * @return Factory|View
     */
    public function searchByCar($car, Request $request)
    {
        $car = Cars::with('manufacturer')
            ->where('slug', $car)
            ->firstOrFail();

        $repetitors = Users::with([
                'car',
                'car.manufacturer',
                'carSecond',
                'carSecond.manufacturer'
            ])
            ->from(app(Users::class)->getTable(), 'u')
            ->where(function ($query) use ($car) {
                $query
                    ->where(['u.id_cars' => $car->id_cars])
                    ->orWhere(['u.id_cars_second' => $car->id_cars]);
            })
            ->where([
                'u.id_users_active_statuses' => UsersActiveStatuses::ACTIVE,
                'u.is_deleted' => 0
            ]);

        $repetitors = $this->paginate($request, (new UsersFilter($repetitors, $request))->apply());

        $newCar = null;

        if ($repetitors->currentPage() == 1) {
            $newCar = Cars::with('manufacturer')
                ->where('id_cars', '>', $car->id_cars)
                ->orderBy('id_cars', 'ASC')
                ->first();

            if (!$newCar) {
                $newCar = Cars::with('manufacturer')
                    ->orderBy('id_cars', 'ASC')
                    ->first();
            }
        }

        return view('pages.car-instructors.car', [
            'car' => $car,
            'repetitors' => $repetitors,
            'newCar' => $newCar,
        ]);
    }

    /**
     * Поиск репетиторов в выбранном районе
     *
     * TODO: "../raion_all.php"
     *
     * @param $districts
     */
    public function searchByDistricts($districts, Request $request)
    {
        $course = Courses::where(['id_courses' => Courses::AUTOMATIC_TRANSMISSION])->firstOrFail();
        $district = Districts::where(['slug' => $districts])->firstOrFail();

        $repetitors = Users::from(app(Users::class)->getTable(), 'u')
            ->leftJoin(app(Ratings::class)->getTable() . ' AS rt', 'u.id_users', '=', 'rt.id_users')
            ->where([
                'u.id_districts' => $district->id_districts,
                'u.id_users_active_statuses' => UsersActiveStatuses::ACTIVE,
                'u.is_deleted' => 0
            ])
            ->where(function ($query) {
                $query
                    ->whereIn('u.id_courses', $this->courses)
                    ->orWhereIn('u.id_courses_second', $this->courses)
                    ->orWhereIn('u.id_courses_third', $this->courses)
                    ->orWhereIn('u.id_courses_fourth', $this->courses);
            });

        $repetitors = $this->paginate($request, (new UsersWithRatingTableFilter($repetitors, $request))->apply());

        $seo = $this->getTitleDescriptionByName('title_district', $repetitors->currentPage(), $repetitors->firstItem(), $repetitors->lastItem(), $course, $district);
        $breadcrumbs = $this->getBreadcrumbsByName('district', $repetitors->currentPage(), $repetitors->firstItem(), $repetitors->lastItem(), $course, $district);
        $header = $this->getHeaderByName('district', $repetitors->currentPage(), $course, $district);
        $totalText = $this->getTotalTextByName('district', $repetitors->currentPage(), $repetitors->total(), $course, $district);
        $prependText = $this->getPrependTextByName('district', $repetitors->currentPage(), $course, $district);

        return view('pages.car-instructors.districts', [
            'seo' => $seo,
            'breadcrumbs' => $breadcrumbs,
            'header' => $header,
            'totalText' => $totalText,
            'prependText' => $prependText,
            'repetitors' => $repetitors
        ]);
    }

    /**
     * Поиск инструкторов по метро
     *
     * TODO: "../metro_all.php"
     *
     * @param $metro
     */
    public function searchByMetro($metro, Request $request)
    {
        $metro = Metro::where(['slug' => $metro])->firstOrFail();

        $repetitors = Users::from(app(Users::class)->getTable(), 'u')
            ->leftJoin(app(Ratings::class)->getTable() . ' AS rt', 'u.id_users', '=', 'rt.id_users')
            ->where([
                'u.id_users_active_statuses' => UsersActiveStatuses::ACTIVE,
                'u.is_deleted' => 0
            ])
            ->where(function ($query) {
                $query
                    ->whereIn('u.id_courses', $this->courses)
                    ->orWhereIn('u.id_courses_second', $this->courses)
                    ->orWhereIn('u.id_courses_third', $this->courses)
                    ->orWhereIn('u.id_courses_fourth', $this->courses);
            })
            ->where(function ($query) use ($metro) {
                $query
                    ->where(['u.id_metro' => $metro->id_metro])
                    ->orWhere(['u.id_metro_second' => $metro->id_metro]);
            });

        $repetitors = $this->paginate($request, (new UsersWithRatingTableFilter($repetitors, $request))->apply());

        return view('pages.car-instructors.metro', [
            'repetitors' => $repetitors
        ]);
    }

    /**
     * Поиск инструкторов по области
     *
     * TODO: "../gorod_all.php"
     *
     * @param $city
     */
    public function searchByCity($city, Request $request)
    {
        $city = Cities::where(['slug' => $city])->firstOrFail();

        $repetitors = Users::from(app(Users::class)->getTable(), 'u')
            ->leftJoin(app(Ratings::class)->getTable() . ' AS rt', 'u.id_users', '=', 'rt.id_users')
            ->where([
                'u.id_users_active_statuses' => UsersActiveStatuses::ACTIVE,
                'u.is_deleted' => 0
            ])
            ->where(function ($query) use ($city) {
                $query
                    ->where(['u.id_cities' => $city->id_cities])
                    ->orWhere(['u.id_cities_second' => $city->id_cities])
                    ->orWhere(['u.id_cities_third' => $city->id_cities])
                    ->orWhere(['u.id_cities_fourth' => $city->id_cities]);
            })
            ->where(function ($query) {
                $query
                    ->whereIn('u.id_courses', $this->courses)
                    ->orWhereIn('u.id_courses_second', $this->courses)
                    ->orWhereIn('u.id_courses_third', $this->courses)
                    ->orWhereIn('u.id_courses_fourth', $this->courses);
            });

        $repetitors = $this->paginate($request, (new UsersWithRatingTableFilter($repetitors, $request))->apply());

        return view('pages.car-instructors.cities', [
            'repetitors' => $repetitors
        ]);
    }

    /**
     * Открытие выбранных страниц
     *
     * @param string $name Слаг страницы для поиска в папке pages.car-instructors.static
     *
     * @return Factory|View
     */
    public function pages($name)
    {
        $name = 'pages.car-instructors.static.' . $name;

        if (view()->exists($name)) {
            return view($name);
        }

        return view('errors.404');
    }
}
