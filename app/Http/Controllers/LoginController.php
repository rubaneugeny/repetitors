<?php

namespace App\Http\Controllers;

use App\Mail\PasswordRecovery;
use App\Models\Users;
use App\Models\UsersActiveStatuses;
use GuzzleHttp\Client;
use App\Http\Requests\RecoveryRequest;
use GuzzleHttp\Exception\GuzzleException;
use Mail;

class LoginController extends Controller
{
    public function remember()
    {
        return view('pages.login.remember');
    }

    public function recovery(RecoveryRequest $request)
    {
        $client = new Client();

        try {
            $response = $client->get("https://www.google.com/recaptcha/api/siteverify?secret=".env('RECAPTCHA_SECRET')."&response=".$request->get('g-recaptcha-response'));
            $json = json_decode($response->getBody());

            if ($json->success) {
                $user = Users::where(['email' => $request->email])->first();

                if (!$user) {
                    $this->setErrorMessage('Уважаемый репетитор! К сожалению, вы неправильно вводите адрес электронной почты.');
                } elseif ($user->id_users_active_statuses == UsersActiveStatuses::NOT_ACTIVE) {
                    $this->setErrorMessage('Уважаемый репетитор! Ваша анкета еще не активирована, пожалуйста, дождитесь звонка администратора.');
                } elseif ($user->id_users_active_statuses == UsersActiveStatuses::DELETED) {
                    $this->setErrorMessage('Уважаемый репетитор! К сожалению, ваша анкета удалена.');
                } else {
                    Mail::send(new PasswordRecovery($user));
                    $this->setSuccessMessage('Уважаемый репетитор! Пароль отправлен вам на указанную почту - '.$user->email);
                }
            } else {
                $this->setErrorMessage('Уважаемый репетитор! Вы не прошли проверку на робота.');
            }
        } catch (GuzzleException $e) {
            $this->setErrorMessage('Произошла ошибка проверки Google ReCaptcha.');
        }

        return view('pages.login.recovery');
    }
}
