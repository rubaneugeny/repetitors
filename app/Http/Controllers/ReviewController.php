<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Http\Requests\AddReviewRequest;
use App\Http\Requests\AllReviewsRequest;
use App\Http\Requests\SaveReviewRequest;
use App\Models\Courses;
use App\Models\CoursesCategoriesSecond;
use App\Models\Reviews;
use App\Models\ReviewsStatuses;
use App\Models\Users;
use App\Models\UsersActiveStatuses;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;
use Throwable;

class ReviewController extends Controller
{
    /**
     * Редирект на страницу добавления отзыва через смс
     *
     * @param Request $request
     *
     * @return RedirectResponse|Redirector
     */
    public function redirectToReview(Request $request)
    {
        $code = $request->get('c');
        $id_orders = Helper::getUniqueCode($code, true);

        if ($review = Reviews::where(['id_orders' => $id_orders])->first()) {
            return redirect('/include/response_add.php?id_rep='.$review->id_users.'&code='.$code);
        }

        return redirect('/');
    }

    /**
     * Страница добавления отзыва
     *
     * @param AddReviewRequest $request
     *
     * @return Factory|View
     */
    public function addReview(AddReviewRequest $request)
    {
        $id_users = $request->get('id_rep');
        $repetitor = Users::where(['id_users' => $id_users, 'is_deleted' => 0])->firstOrFail();

        if ($code = $request->get('code')) {
            $code = Helper::getUniqueCode($code, true);

            // Если пользователь уже добавлял комментарий
            if (Reviews::where(['id_users' => $id_users, 'id_orders' => $code])->count() > 0) {
                return view('components.reviews.incorrect-review');
            }
        }

        return view('pages.reviews.add-review', [
            'repetitor' => $repetitor,
        ]);
    }

    /**
     * Добавление отзыва в базу
     *
     * @param SaveReviewRequest $request
     *
     * @return Response
     * @throws Throwable
     */
    public function addReviewRecord(SaveReviewRequest $request)
    {
        $id_users = $request->query('id_rep');

        // Проверка на существование репетитора
        $repetitor = Users::where(['id_users' => $id_users, 'is_deleted' => 0])->firstOrFail();

        $code = $request->query('code');

        if ($code) {
            $code = Helper::getUniqueCode($code, true);
        }

        // Если комментарий по ссылке, то обновляем иначе добавляем
        $review = Reviews::where(['id_users' => $id_users, 'id_orders' => $code])->first();

        if ($review) {
            $review->rating = $request->get('rating');
            $review->comment = $request->get('comment');
        } else {
            $review = new Reviews($request->all());
            $review->id_users = $id_users;
            $review->id_orders = $code;
        }

        $review->save();
        session()->flash('success', view('components.reviews.success-review')->render());

        return response()
            ->view('pages.reviews.add-review', [
                'repetitor' => $repetitor,
            ])
            ->header('refresh', '5; url=/include/'.$repetitor->getLink());
    }

    /**
     * Отзывы
     *
     * @return Factory|View
     */
    public function reviews(Request $request)
    {
        // Общее кол-во отзывов
        $reviews = Reviews::with(
                'user',
                'user.course',
                'user.courseSecond',
                'user.courseThird',
                'user.courseFourth',
                'order'
            )
            ->where(['id_reviews_statuses' => ReviewsStatuses::APPROVED, 'is_deleted' => 0])
            ->orderBy('date', 'DESC');

        $reviews = $this->paginate($request, $reviews);

        return view('pages.reviews.reviews', [
            'reviews' => $reviews
        ]);
    }

    /**
     * Все отзывы
     *
     * @param AllReviewsRequest $request
     *
     * @return Factory|View
     */
    public function allReviews(AllReviewsRequest $request)
    {
        $repetitor = Users::where(['id_users' => $request->get('id_rep'), 'is_deleted' => 0])->firstOrFail();

        $type = !$repetitor->is_car_instructor
            ? Users::TYPE_REPETITOR
            : Users::TYPE_CAR_INSTRUCTOR;

        // Статистика по рейтингам
        $statistics = [5 => 0, 4 => 0, 3 => 0, 2 => 0, 1 => 0];
        $reviews = Reviews::where([
                'id_users' => $repetitor->id_users,
                'id_reviews_statuses' => ReviewsStatuses::APPROVED,
                'is_deleted' => 0,
            ])
            ->orderBy('rating', 'DESC')
            ->orderBy('date', 'DESC')
            ->get();

        foreach ($reviews ?? [] as $value) {
            $statistics[$value->rating]++;
        }

        $links = $this->recommendLinks($repetitor);

        return view('pages.reviews.reviews-all', [
            'type' => $type,
            'repetitor' => $repetitor,
            'statistics' => $statistics,
            'reviews' => $reviews,
            'links' => $links,
        ]);
    }

    /**
     * Отзывы:Поиск отзывов по категории
     *
     * @param string $category Слаг из таблицы Courses
     *
     * @return Factory|View
     */
    public function reviewsByCategory($category, Request $request)
    {
        $course = Courses::where(['slug' => $category])->firstOrFail();

        $reviews = Reviews::with([
                'user.course',
                'user.courseSecond',
                'user.courseThird',
                'user.courseFourth',
            ])
            ->from(app(Reviews::class)->getTable(), 'r')
            ->leftJoin(app(Users::class)->getTable() . ' AS u', 'r.id_users', '=', 'u.id_users')
            ->where(function ($query) use ($course) {
                $query
                    ->where(['u.id_courses' => $course->id_courses])
                    ->orWhere(['u.id_courses_second' => $course->id_courses])
                    ->orWhere(['u.id_courses_third' => $course->id_courses])
                    ->orWhere(['u.id_courses_fourth' => $course->id_courses]);
            })
            ->where([
                'u.is_deleted' => 0,
                'u.id_users_active_statuses' => UsersActiveStatuses::ACTIVE,
                'r.id_reviews_statuses' => ReviewsStatuses::APPROVED,
                'r.is_deleted' => 0
            ])
            ->orderBy('r.id_reviews', 'DESC')
            ->selectRaw('r.*');

        $reviews = $this->paginate($request, $reviews);

        $newCourse = null;

        if ($reviews->currentPage() == 1 && $course->has_review) {
            $newCourse = Courses::where('id_courses', '>', $course->id_courses)
                ->where(['has_review' => 1])
                ->orderBy('id_courses', 'ASC')
                ->first();

            if (!$newCourse) {
                $newCourse = Courses::where(['has_review' => 1])
                    ->orderBy('id_courses', 'ASC')
                    ->first();
            }
        }

        return view('pages.reviews.reviews-by-category', [
            'course' => $course,
            'reviews' => $reviews,
            'newCourse' => $newCourse,
        ]);
    }

    /**
     * Отзывы о репетиторах по общеобразовательным предметам
     *
     * @return Factory|View
     */
    public function reviewsAboutGeneral()
    {
        $courses = Courses::where(['has_review' => 1, 'id_courses_categories_second' => CoursesCategoriesSecond::GENERAL])
            ->orderBy('name', 'asc')
            ->get();

        return view('pages.reviews.reviews-categories', [
            'title' => 'Отзывы о репетиторах по общеобразовательным предметам - "Репетит-Центр"',
            'header' => 'Отзывы о репетиторах по общеобразовательным предметам',
            'canonical' => 'https://'.env('DOMAIN').'/include/otzyvy_regular.php',
            'courses' => $courses
        ]);
    }

    /**
     * Отзывы о репетиторах по иностранным языкам
     *
     * @return Factory|View
     */
    public function reviewsAboutLanguages()
    {
        $courses = Courses::where(['has_review' => 1, 'id_courses_categories_second' => CoursesCategoriesSecond::LANGUAGES])
            ->orderBy('name', 'asc')
            ->get();

        return view('pages.reviews.reviews-categories', [
            'title' => 'Отзывы о репетиторах по иностранным языкам - "Репетит-Центр"',
            'header' => 'Отзывы о репетиторах по иностранным языкам',
            'canonical' => 'https://'.env('DOMAIN').'/include/otzyvy_language.php',
            'courses' => $courses
        ]);
    }

    /**
     * Отзывы о преподавателях музыки
     *
     * @return Factory|View
     */
    public function reviewsAboutMusic()
    {
        $courses = Courses::where(['has_review' => 1, 'id_courses_categories_second' => CoursesCategoriesSecond::MUSIC])
            ->orderBy('name', 'asc')
            ->get();

        return view('pages.reviews.reviews-categories', [
            'title' => 'Отзывы о преподавателях музыки - "Репетит-Центр"',
            'header' => 'Отзывы о преподавателях музыки',
            'canonical' => 'https://'.env('DOMAIN').'/include/otzyvy_music.php',
            'courses' => $courses
        ]);
    }

    /**
     * Отзывы об инструкторах по танцам
     *
     * @return Factory|View
     */
    public function reviewsAboutDance()
    {
        $courses = Courses::where(['has_review' => 1, 'id_courses_categories_second' => CoursesCategoriesSecond::DANCE])
            ->orderBy('name', 'asc')
            ->get();

        return view('pages.reviews.reviews-categories', [
            'title' => 'Отзывы об инструкторах по танцам - "Репетит-Центр"',
            'header' => 'Отзывы об инструкторах по танцам',
            'canonical' => 'https://'.env('DOMAIN').'/include/otzyvy_dance.php',
            'courses' => $courses
        ]);
    }

    /**
     * Отзывы о тренерах
     *
     * @return Factory|View
     */
    public function reviewsAboutSport()
    {
        $courses = Courses::where(['has_review' => 1, 'id_courses_categories_second' => CoursesCategoriesSecond::SPORT_DISCIPLINES])
            ->orderBy('name', 'asc')
            ->get();

        return view('pages.reviews.reviews-categories', [
            'title' => 'Отзывы о тренерах - "Репетит-Центр"',
            'header' => 'Отзывы о тренерах',
            'canonical' => 'https://'.env('DOMAIN').'/include/otzyvy_sport.php',
            'courses' => $courses
        ]);
    }

    /**
     * Отзывы о тренерах
     *
     * @return Factory|View
     */
    public function reviewsAboutOther()
    {
        $courses = Courses::where(['has_review' => 1])
            ->whereIn('id_courses_categories_second', [CoursesCategoriesSecond::OTHER_SUBJECTS, CoursesCategoriesSecond::CHILDREN])
            ->orderBy('name', 'asc')
            ->get();

        return view('pages.reviews.reviews-categories', [
            'title' => 'Отзывы о репетиторах по разным дисциплинам - "Репетит-Центр"',
            'header' => 'Отзывы о репетиторах по разным дисциплинам',
            'canonical' => 'https://'.env('DOMAIN').'/include/otzyvy_other.php',
            'courses' => $courses
        ]);
    }

    /**
     * Отзывы об автоинструкторах
     *
     * @return Factory|View
     */
    public function reviewsAboutCar(Request $request)
    {
        $reviews = Reviews::with([
                'user.course',
                'user.courseSecond',
                'user.courseThird',
                'user.courseFourth',
            ])
            ->from(app(Reviews::class)->getTable(), 'r')
            ->leftJoin(app(Users::class)->getTable() . ' AS u', 'r.id_users', '=', 'u.id_users')
            ->where(['u.is_car_instructor' => 1, 'r.id_reviews_statuses' => ReviewsStatuses::APPROVED, 'r.is_deleted' => 0])
            ->orderBy('r.id_reviews', 'DESC')
            ->selectRaw('r.*');

        $reviews = $this->paginate($request, $reviews);

        return view('pages.reviews.reviews-car', [
            'reviews' => $reviews,
        ]);
    }

    /**
     * Отзывы о мотоинструкторах
     *
     * @return Factory|View
     */
    public function reviewsAboutMotor(Request $request)
    {
        $course = Courses::where(['id_courses' => Courses::MOTORBIKES])->firstOrFail();

        $reviews = Reviews::with([
                'user.course',
                'user.courseSecond',
                'user.courseThird',
                'user.courseFourth',
            ])
            ->from(app(Reviews::class)->getTable(), 'r')
            ->leftJoin(app(Users::class)->getTable() . ' AS u', 'r.id_users', '=', 'u.id_users')
            ->where(function ($query) use ($course) {
                $query->where(['u.id_courses' => $course->id_courses])
                    ->orWhere(['u.id_courses_second' => $course->id_courses])
                    ->orWhere(['u.id_courses_third' => $course->id_courses])
                    ->orWhere(['u.id_courses_fourth' => $course->id_courses]);
            })
            ->where(['r.id_reviews_statuses' => ReviewsStatuses::APPROVED, 'r.is_deleted' => 0])
            ->orderBy('r.id_reviews', 'DESC')
            ->selectRaw('r.*');

        $reviews = $this->paginate($request, $reviews);

        return view('pages.reviews.reviews-motor', [
            'course' => $course,
            'reviews' => $reviews,
        ]);
    }

    /**
     * @param $repetitor
     * @return string
     */
    private function recommendLinks(&$repetitor)
    {
        $result = '';

        return $result;
    }
}
