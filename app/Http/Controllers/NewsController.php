<?php

namespace App\Http\Controllers;

use App\Models\News;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;

class NewsController extends Controller
{
    /**
     * Новости:Показать новости
     *
     * @param Request $request
     *
     * @return Factory|View
     */
    public function showNews(Request $request)
    {
        $id = $request->query('id_news');
        $news = News::findOrFail($id);

        return view('pages.show-news', ['news' => $news]);
    }

    /**
     * Новости
     *
     * @return Factory|View
     */
    public function news()
    {
        $news = News::orderBy('id_news', 'DESC')->get();

        return view('pages.news', ['news' => $news]);
    }
}
