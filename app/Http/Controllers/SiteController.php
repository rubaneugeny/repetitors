<?php

namespace App\Http\Controllers;

use App\Filters\UsersFilter;
use App\Filters\UsersWithRatingTableFilter;
use App\Helpers\Helper;
use App\Http\Requests\OpenGraphRequest;
use App\Http\Requests\RepetitorsSearchRequest;
use App\Http\Requests\SearchRepetitorRequest;
use App\Models\Blog;
use App\Models\Cities;
use App\Models\CommentsPrependTextCategories;
use App\Models\CommentsPrependTextTemplates;
use App\Models\Courses;
use App\Models\CoursesCategories;
use App\Models\CoursesSubcourses;
use App\Models\CoursesTypes;
use App\Models\Districts;
use App\Models\Favourites;
use App\Models\Metro;
use App\Models\Orders;
use App\Models\OrdersLocations;
use App\Models\Ratings;
use App\Models\Reviews;
use App\Models\ReviewsStatuses;
use App\Models\ReviewsTeachers;
use App\Models\Universities;
use App\Models\Users;
use App\Models\UsersActiveStatuses;
use App\Models\UsersSchedule;
use App\Models\UsersScheduleTime;
use App\Models\UsersScheduleWeekDays;
use App\Models\UsersStatuses;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;

class SiteController extends Controller
{
    /**
     * Главная страница сайта
     *
     * @return Factory|View
     */
    public function index()
    {
        // ID Последней заявки
        $lastOrder = Orders::latest('id_orders')->first();
        $lastOrderId = $lastOrder->id_orders;

        // Количество предметов
        $courseCount = Courses::where('id_courses', '<', 4000)->count();

        // Количество репетиторов
        $usersCount = Users::whereIn('id_users_active_statuses', [
            UsersActiveStatuses::ACTIVE,
            UsersActiveStatuses::HIDDEN
        ])->count();

        // Отзывы
        $reviewsCount = Reviews::where([
            'id_reviews_statuses' => ReviewsStatuses::APPROVED,
            'is_deleted' => 0
        ])->count();

        // Соберем 5 положительных отзывов
        $reviews = Reviews::with(['user', 'order', 'order.course'])
            ->where(['rating' => 5, 'id_reviews_statuses' => ReviewsStatuses::APPROVED, 'is_deleted' => 0])
            ->orderBy('date', 'DESC')
            ->limit(5)
            ->get();

        return view('pages.static.index', [
            'lastOrderId' => $lastOrderId,
            'courseCount' => $courseCount,
            'usersCount' => $usersCount,
            'reviews' => $reviews,
            'reviewsCount' => $reviewsCount,
            'reviews_all_button' => view('components.reviews.reviews-all-button'),
        ]);
    }

    /******************************************************************************************************************/

    /**
     * Избранное
     *
     * @return Factory|View
     */
    public function favourites()
    {
        $ids = Favourites::where(['id_favourites' => Cookie::get('fav')])
            ->orderBy('created_at', 'DESC')
            ->pluck('id_users');

        $repetitors = Users::whereIn('id_users', $ids)
            ->where([
                'id_users_active_statuses' => UsersActiveStatuses::ACTIVE,
                'is_deleted' => 0
            ])
            ->get();

        return view('pages.favourites', [
            'repetitors' => $repetitors
        ]);
    }

    /**
     * Как это работает: Информация для учеников
     *
     * @return Factory|View
     */
    public function infoForStudents()
    {
        // Общеее кол-во и сумма рейтинга положительных отзывов
        $positiveReviews = Reviews::where('rating', '>', 3)
            ->selectRaw('COUNT(*) AS count, SUM(rating) AS sum')
            ->first();

        // Общеее кол-во и сумма рейтинга негативных отзывов
        $negativeReviews = Reviews::where('rating', '<=', 3)
            ->selectRaw('COUNT(*) AS count, SUM(rating) AS sum')
            ->first();

        // Общее кол-во и средняя оценка
        $reviewsCount = $positiveReviews->count + $negativeReviews->count;
        $reviewsSum = $positiveReviews->sum + $negativeReviews->sum;
        $reviewsRating = round($reviewsSum / $reviewsCount, 1);

        // Соберем 5 положительных отзывов
        $reviews = Reviews::with(['user', 'order', 'order.course'])
            ->where(['rating' => 5, 'id_reviews_statuses' => ReviewsStatuses::APPROVED, 'is_deleted' => 0])
            ->orderBy('id_reviews', 'DESC')
            ->limit(5)
            ->get();

        return view('pages.info_for_students', [
            'reviews' => $reviews,
            'reviewsCount' => $reviewsCount,
            'reviewsRating' => $reviewsRating,
        ]);
    }

    /**
     * Как это работает: Информация для репетиторов
     *
     * @return Factory|View
     */
    public function infoForTeachers()
    {
        $reviews = ReviewsTeachers::with('user')->where(['is_approved' => 1])->get();
        $orders = Orders::with('user')
            ->whereIn('id_orders', [220739, 220408, 257330, 257373])
            ->get();

        return view('pages.info_for_tutors', [
            'reviews' => $reviews,
            'orders' => $orders,
        ]);
    }

    /******************************************************************************************************************/

    /**
     * Блог
     *
     * @return Factory|View
     */
    public function blog()
    {
        $whenYouNeedRepetitor = Blog::where(['column' => Blog::TYPE_WHEN_YOU_NEED_REPETITOR])
            ->orderBy('id_blog', 'DESC')
            ->get();

        $internationalExam = Blog::where(['column' => Blog::TYPE_INTERNATIONAL_EXAM])
            ->orderBy('id_blog', 'DESC')
            ->get();

        return view('pages.blog', [
            'whenYouNeedRepetitor' => $whenYouNeedRepetitor,
            'internationalExam' => $internationalExam,
        ]);
    }

    /**
     * Блог: Показать блог
     *
     * @param string $slug Слаг из таблицы Blog
     *
     * @return Factory|View
     */
    public function showBlog($slug)
    {
        $blog = Blog::where(['slug' => $slug])->firstOrFail();
        $next = Blog::where(['column' => $blog->column])
            ->where('id_blog', '>', $blog->id_blog)
            ->select(['slug', 'breadcrumbs'])
            ->orderBy('id_blog', 'ASC')
            ->first();

        if (!$next) {
            $next = Blog::where(['column' => $blog->column])
                ->select(['slug', 'breadcrumbs'])
                ->orderBy('id_blog', 'ASC')
                ->first();
        }

        return view('pages.show-blog', [
            'blog' => $blog,
            'next' => $next,
        ]);
    }

    /******************************************************************************************************************/

    /**
     * Поиск
     *
     * @param RepetitorsSearchRequest $request
     *
     * @return Factory|JsonResponse|View
     * @throws \Throwable
     */
    public function search(RepetitorsSearchRequest $request)
    {
        $repetitors = Users::from(app(Users::class)->getTable(), 'u')
            ->where([
                'u.id_users_active_statuses' => UsersActiveStatuses::ACTIVE,
                'u.is_deleted' => 0
            ]);

        $repetitors = $this->paginate($request, (new UsersFilter($repetitors, $request))->apply());

        if ($request->ajax()) {
            Helper::makeFilterButtons($request);

            $breadcrumbs = view('components.breadcrumbs', [
                'type' => Helper::REPETITOR,
                'last' => 'Поиск репетиторов'
            ])->render();

            $header = view('components.h1', [
                'name' => 'Поиск репетиторов'
            ])->render();

            $totalText = view('components.search.total-text', [
                'repetitors' => $repetitors
            ])->render();

            $prependText = view('components.search.prepend-text')->render();

            $content = view('components.ajax.repetitors', [
                'type' => Helper::REPETITOR,
                'repetitors' => $repetitors->appends($request->input())
            ])->render();

            return response()->json([
                'breadcrumbs' => $breadcrumbs,
                'header' => $header,
                'totalText' => $totalText,
                'prependText' => $prependText,
                'content' => $content
            ]);
        }

        return view('pages.search', [
            'repetitors' => $repetitors->appends(request()->input())
        ]);
    }

    /******************************************************************************************************************/

    /**
     * Поиск по метро
     *
     * @param string  $metro Слаг из таблицы Metro
     * @param Request $request
     *
     * @return Factory|View
     */
    public function searchByMetro($metro, Request $request)
    {
        $metro = Metro::where(['slug' => $metro])->firstOrFail();

        $repetitorsCount = Users::where(function ($query) use ($metro) {
                $query
                    ->where(['id_metro' => $metro->id_metro])
                    ->orWhere(['id_metro_second' => $metro->id_metro]);
            })
            ->where(['id_users_active_statuses' => UsersActiveStatuses::ACTIVE, 'is_deleted' => 0])
            ->count();

        // Если не хватает до необходимого кол-ва, то дополняем из ближайших станций
        $needCount = intval(env('MIN_COUNT_OF_METRO'));

        if ($repetitorsCount < $needCount) {
            // Добавим в ближайшие станции текущую для полного списка
            $fullListOfStations = $metro->id_metro.','.$metro->nearest_stations;
            $nearestStations = array_map('intval', explode(',', $fullListOfStations));

            // Делаем реверс массива для правильной сортировки
            $nearestStations = array_reverse($nearestStations);

            $fullListOfStations = implode(',', $nearestStations);

            $repetitors = Users::fromSub(function ($query) use ($metro, $nearestStations, $fullListOfStations, $needCount) {
                    $query
                        ->selectRaw('u.*, FIELD(IF(IFNULL(FIND_IN_SET(u.id_metro,?),0) > IFNULL(FIND_IN_SET(u.id_metro_second,?),0),u.id_metro,u.id_metro_second),?) AS `compare`', [$fullListOfStations, $fullListOfStations, $fullListOfStations])
                        ->from(app(Users::class)->getTable(), 'u')
                        ->where(function ($query) use ($metro, $nearestStations) {
                            $query->whereIn('u.id_metro', $nearestStations)
                                ->orWhereIn('u.id_metro_second', $nearestStations)
                                ->orWhere('u.metro_list', 'regexp', '[[:<:]]('.str_replace(',', '|', $metro->nearest_stations).')[[:>:]]');
                        })
                        ->where([
                            'u.id_users_active_statuses' => UsersActiveStatuses::ACTIVE,
                            'u.is_deleted' => 0
                        ])
                        ->orderBy('compare', 'DESC')
                        ->take($needCount);
                }, 'u')
                ->orderBy('u.compare', 'DESC');
        } else {
            $repetitors = Users::from(app(Users::class)->getTable(), 'u')
                ->where(function ($query) use ($metro) {
                    $query
                        ->where(['u.id_metro' => $metro->id_metro])
                        ->orWhere(['u.id_metro_second' => $metro->id_metro]);
                })
                ->where([
                    'u.id_users_active_statuses' => UsersActiveStatuses::ACTIVE,
                    'u.is_deleted' => 0
                ]);
        }

        $repetitors = $this->paginate($request, (new UsersFilter($repetitors, $request))->apply());

        // Сохраняем в сессию для плашек и фильтров
        session()->flash('nametags', [
            'metro' => [$metro->id_metro => $metro->name]
        ]);

        return view('pages.search-by-metro', [
            'metro' => $metro,
            'repetitors' => $repetitors,
        ]);
    }

    /**
     * Поиск по предметам в готовых фильтрах поиска
     * other_predmet_metro.php
     *
     * @param Request $request
     *
     * @return Application|Factory|View
     */
    public function searchOtherCoursesByMetro(Request $request)
    {
        $metro = Metro::where(['id_metro' => $request->get('id_metro')])->firstOrFail();

        $courses = Courses::whereRaw('FIND_IN_SET(?, metro)', [$metro->id_metro])
            ->where('id_courses', '<', 1000)
            ->orderBy('name', 'ASC')
            ->get();

        $carInstructorsCount = Users::where(function ($query) use ($metro) {
                $query
                    ->where(['id_metro' => $metro->id_metro])
                    ->orWhere(['id_metro_second' => $metro->id_metro]);
            })
            ->where(function ($query) {
                $query
                    ->whereIn('id_courses', [Courses::AUTOMATIC_TRANSMISSION, Courses::MANUAL_TRANSMISSION])
                    ->orWhereIn('id_courses_second', [Courses::AUTOMATIC_TRANSMISSION, Courses::MANUAL_TRANSMISSION])
                    ->orWhereIn('id_courses_third', [Courses::AUTOMATIC_TRANSMISSION, Courses::MANUAL_TRANSMISSION])
                    ->orWhereIn('id_courses_fourth', [Courses::AUTOMATIC_TRANSMISSION, Courses::MANUAL_TRANSMISSION]);
            })
            ->where([
                'id_users_active_statuses' => UsersActiveStatuses::ACTIVE,
                'is_deleted' => 0
            ])
            ->count();

        $motorInstructorsCount = Users::where(function ($query) use ($metro) {
                $query
                    ->where(['id_metro' => $metro->id_metro])
                    ->orWhere(['id_metro_second' => $metro->id_metro]);
            })
            ->where(function ($query) {
                $query
                    ->where(['id_courses' => Courses::MOTORBIKES])
                    ->orWhere(['id_courses_second' => Courses::MOTORBIKES])
                    ->orWhere(['id_courses_third' => Courses::MOTORBIKES])
                    ->orWhere(['id_courses_fourth' => Courses::MOTORBIKES]);
            })
            ->where([
                'id_users_active_statuses' => UsersActiveStatuses::ACTIVE,
                'is_deleted' => 0
            ])
            ->count();

        return view('pages.search-other-courses-by-metro', [
            'metro' => $metro,
            'courses' => $courses,
            'carInstructorsCount' => $carInstructorsCount,
            'motorInstructorsCount' => $motorInstructorsCount
        ]);
    }

    /**
     * Поиск предметов по выбранному фильтру
     * metro_all.php
     *
     * @param string $course Слаг курса
     * @param string $metro Слаг метро
     * @param Request $request
     *
     * @return Application|Factory|View
     */
    public function searchByCourseAndMetro($course, $metro, Request $request)
    {
        $course = Courses::with([
                'seo',
                'parentCourse',
                'secondCourse',
            ])
            ->whereNotNull('id_title_categories')
            ->where(function ($query) use ($course) {
                $query
                    ->where(['slug' => $course])
                    ->orWhere(['slug_second' => $course]);
            })
            ->firstOrFail();

        $metro = Metro::where(['slug' => $metro])->firstOrFail();

        $repetitors = Users::from(app(Users::class)->getTable(), 'u')
            ->leftJoin(app(Ratings::class)->getTable().' AS rt', function ($join) use ($course) {
                $join
                    ->on('rt.id_users', '=', 'u.id_users')
                    ->where('rt.id_courses', '=', $course->id_courses);
            })
            ->where(function ($query) use ($course) {
                $query
                    ->where(['u.id_courses' => $course->id_courses])
                    ->orWhere(['u.id_courses_second' => $course->id_courses])
                    ->orWhere(['u.id_courses_third' => $course->id_courses])
                    ->orWhere(['u.id_courses_fourth' => $course->id_courses]);
            })
            ->where(function ($query) use ($metro) {
                $query
                    ->where(['u.id_metro' => $metro->id_metro])
                    ->orWhere(['u.id_metro_second' => $metro->id_metro]);
            })
            ->where([
                'u.id_users_active_statuses' => UsersActiveStatuses::ACTIVE,
                'u.is_deleted' => 0,
            ]);

        // Если не хватает до необходимого кол-ва, то дополняем из ближайших станций
        $needCount = intval(env('MIN_COUNT_OF_COURSE_AND_METRO'));

        if ($repetitors->count() < $needCount) {
            // Добавим в ближайшие станции текущую для полного списка
            $fullListOfStations = $metro->id_metro.','.$metro->nearest_stations;
            $nearestStations = array_map('intval', explode(',', $fullListOfStations));

            // Делаем реверс массива для правильной сортировки
            $nearestStations = array_reverse($nearestStations);

            $fullListOfStations = implode(',', $nearestStations);

            $repetitors = Users::fromSub(function ($query) use ($course, $metro, $nearestStations, $fullListOfStations, $needCount) {
                    $query
                        ->selectRaw('u.*, FIELD(IF(IFNULL(FIND_IN_SET(id_metro,"'.$fullListOfStations.'"),0) > IFNULL(FIND_IN_SET(id_metro_second, "'.$fullListOfStations.'"),0),id_metro,id_metro_second),'.$fullListOfStations.') AS `compare`')
                        ->from(app(Users::class)->getTable(), 'u')
                        ->where(function ($query) use ($course) {
                            $query
                                ->where(['u.id_courses' => $course->id_courses])
                                ->orWhere(['u.id_courses_second' => $course->id_courses])
                                ->orWhere(['u.id_courses_third' => $course->id_courses])
                                ->orWhere(['u.id_courses_fourth' => $course->id_courses]);
                        })
                        ->where(function ($query) use ($metro, $nearestStations) {
                            $query
                                ->whereIn('u.id_metro', $nearestStations)
                                ->orWhereIn('u.id_metro_second', $nearestStations)
                                ->orWhere('u.metro_list', 'regexp', '[[:<:]]('.str_replace(',', '|', $metro->nearest_stations).')[[:>:]]');
                        })
                        ->where([
                            'u.id_users_active_statuses' => UsersActiveStatuses::ACTIVE,
                            'u.is_deleted' => 0
                        ])
                        ->orderBy('compare', 'DESC')
                        ->take($needCount);
                }, 'u')
                ->leftJoin(app(Ratings::class)->getTable().' AS rt', function ($join) use ($course) {
                    $join
                        ->on('rt.id_users', '=', 'u.id_users')
                        ->where('rt.id_courses', '=', $course->id_courses);
                });
        }

        $repetitors = $this->paginate($request, (new UsersWithRatingTableFilter($repetitors, $request))->apply());

        $seo = $this->getTitleDescriptionByName('title_metro', $repetitors->currentPage(), $repetitors->firstItem(), $repetitors->lastItem(), $course, $metro);
        $breadcrumbs = $this->getBreadcrumbsByName('metro', $repetitors->currentPage(), $repetitors->firstItem(), $repetitors->lastItem(), $course, $metro);
        $header = $this->getHeaderByName('metro', $repetitors->currentPage(), $course, $metro);
        $totalText = $this->getTotalTextByName('metro', $repetitors->currentPage(), $repetitors->total(), $course, $metro);
        $prependText = $this->getPrependTextByName('metro', $repetitors->currentPage(), $course, $metro);

        // Записываем в сессию для плашек и статистики
        session()->flash('nametags', [
            'courses' => [$course->id_courses => $course->name],
            'metro' => [$metro->id_metro => $metro->name]
        ]);

        return view('pages.search-by-course-and-metro', [
            'seo' => $seo,
            'breadcrumbs' => $breadcrumbs,
            'header' => $header,
            'totalText' => $totalText,
            'prependText' => $prependText,
            'course' => $course,
            'metro' => $metro,
            'repetitors' => $repetitors
        ]);
    }

    /******************************************************************************************************************/

    /**
     * Поиск по городам Ленинградской области
     *
     * @param $slug
     *
     * @return Application|Factory|View
     */
    public function searchByCity(string $slug, Request $request)
    {
        $city = Cities::where(['slug' => $slug])->firstOrFail();

        $repetitors = Users::from(app(Users::class)->getTable(), 'u')
            ->leftJoin(app(Ratings::class)->getTable().' AS rt', 'u.id_users', '=', 'rt.id_users')
            ->where(function ($query) use ($city) {
                $query
                    ->where(['u.id_cities' => $city->id_cities])
                    ->orWhere(['u.id_cities_second' => $city->id_cities])
                    ->orWhere(['u.id_cities_third' => $city->id_cities])
                    ->orWhere(['u.id_cities_fourth' => $city->id_cities]);
            })
            ->where([
                'u.id_users_active_statuses' => UsersActiveStatuses::ACTIVE,
                'u.is_deleted' => 0
            ]);

        $repetitors = $this->paginate($request, (new UsersWithRatingTableFilter($repetitors, $request))->apply());

        // Записываем в сессию для плашек и статистики
        session()->flash('nametags', [
            'cities' => [$city->id_cities => $city->name],
        ]);

        return view('pages.search-by-city', [
            'city' => $city,
            'repetitors' => $repetitors
        ]);
    }

    /**
     * Поиск по предмету и выбранному городу
     * gorod_all.php
     *
     * @param $course
     * @param $city
     * @param Request $request
     *
     * @return Application|Factory|View
     */
    public function searchByCourseAndCity($course, $city, Request $request)
    {
        $course = Courses::with([
            'seo',
            'parentCourse',
            'secondCourse',
        ])
            ->whereNotNull('id_title_categories')
            ->where(function ($query) use ($course) {
                $query
                    ->where(['slug' => $course])
                    ->orWhere(['slug_second' => $course]);
            })
            ->firstOrFail();

        $city = Cities::where(['slug' => $city])->firstOrFail();

        $repetitors = Users::from(app(Users::class)->getTable(), 'u')
            ->leftJoin(app(Ratings::class)->getTable().' AS rt', function ($join) use ($course) {
                $join
                    ->on('rt.id_users', '=', 'u.id_users')
                    ->where('rt.id_courses', '=', $course->id_courses);
            })
            ->where(function ($query) use ($course) {
                $query
                    ->where(['u.id_courses' => $course->id_courses])
                    ->orWhere(['u.id_courses_second' => $course->id_courses])
                    ->orWhere(['u.id_courses_third' => $course->id_courses])
                    ->orWhere(['u.id_courses_fourth' => $course->id_courses]);
            })
            ->where(function ($query) use ($city) {
                $query
                    ->where(['u.id_cities' => $city->id_cities])
                    ->orWhere(['u.id_cities_second' => $city->id_cities])
                    ->orWhere(['u.id_cities_third' => $city->id_cities])
                    ->orWhere(['u.id_cities_fourth' => $city->id_cities]);
            })
            ->where([
                'u.id_users_active_statuses' => UsersActiveStatuses::ACTIVE,
                'u.is_deleted' => 0
            ]);

        $repetitors = $this->paginate($request, (new UsersWithRatingTableFilter($repetitors, $request))->apply());

        $seo = $this->getTitleDescriptionByName('title_city', $repetitors->currentPage(), $repetitors->firstItem(), $repetitors->lastItem(), $course, $city);
        $breadcrumbs = $this->getBreadcrumbsByName('city', $repetitors->currentPage(), $repetitors->firstItem(), $repetitors->lastItem(), $course, $city);
        $header = $this->getHeaderByName('city', $repetitors->currentPage(), $course, $city);
        $totalText = $this->getTotalTextByName('city', $repetitors->currentPage(), $repetitors->total(), $course, $city);
        $prependText = $this->getPrependTextByName('city', $repetitors->currentPage(), $course, $city);

        // Записываем в сессию для плашек и статистики
        session()->flash('nametags', [
            'courses' => [$course->id_courses => $course->name],
            'cities' => [$city->id_cities => $city->name],
        ]);

        return view('pages.search-by-course-and-city', [
            'seo' => $seo,
            'breadcrumbs' => $breadcrumbs,
            'header' => $header,
            'totalText' => $totalText,
            'prependText' => $prependText,
            'course' => $course,
            'city' => $city,
            'repetitors' => $repetitors
        ]);
    }

    /**
     * Поиск других предметов по городу
     * other_predmet_gorod.php
     *
     * @param Request $request
     *
     * @return Application|Factory|View
     */
    public function searchOtherCoursesByCity(Request $request)
    {
        $city = Cities::where(['id_cities' => $request->get('id_gorod')])->firstOrFail();

        $courses = Courses::whereRaw('FIND_IN_SET(?, cities)', [$city->id_cities])
            ->where('id_courses', '<', 1000)
            ->orderBy('name', 'ASC')->get();

        $carInstructorsCount = Users::where(function ($query) use ($city) {
            $query
                ->where(['id_cities' => $city->id_cities])
                ->orWhere(['id_cities_second' => $city->id_cities])
                ->orWhere(['id_cities_third' => $city->id_cities])
                ->orWhere(['id_cities_fourth' => $city->id_cities]);
        })
            ->where(function ($query) {
                $query
                    ->whereIn('id_courses', [Courses::AUTOMATIC_TRANSMISSION, Courses::MANUAL_TRANSMISSION])
                    ->orWhereIn('id_courses_second', [Courses::AUTOMATIC_TRANSMISSION, Courses::MANUAL_TRANSMISSION])
                    ->orWhereIn('id_courses_third', [Courses::AUTOMATIC_TRANSMISSION, Courses::MANUAL_TRANSMISSION])
                    ->orWhereIn('id_courses_fourth', [Courses::AUTOMATIC_TRANSMISSION, Courses::MANUAL_TRANSMISSION]);
            })
            ->where([
                'id_users_active_statuses' => UsersActiveStatuses::ACTIVE,
                'is_deleted' => 0
            ])
            ->count();

        $motorInstructorsCount = Users::where(function ($query) use ($city) {
            $query
                ->where(['id_cities' => $city->id_cities])
                ->orWhere(['id_cities_second' => $city->id_cities])
                ->orWhere(['id_cities_third' => $city->id_cities])
                ->orWhere(['id_cities_fourth' => $city->id_cities]);
        })
            ->where(function ($query) {
                $query
                    ->where(['id_courses' => Courses::MOTORBIKES])
                    ->orWhere(['id_courses_second' => Courses::MOTORBIKES])
                    ->orWhere(['id_courses_third' => Courses::MOTORBIKES])
                    ->orWhere(['id_courses_fourth' => Courses::MOTORBIKES]);
            })
            ->where([
                'id_users_active_statuses' => UsersActiveStatuses::ACTIVE,
                'is_deleted' => 0
            ])
            ->count();

        return view('pages.search-other-courses-by-city', [
            'city' => $city,
            'courses' => $courses,
            'carInstructorsCount' => $carInstructorsCount,
            'motorInstructorsCount' => $motorInstructorsCount
        ]);
    }

    /******************************************************************************************************************/

    /**
     * Поиск по районам
     *
     * @param $slug
     * @param Request $request
     *
     * @return Application|Factory|View
     */
    public function searchByDistricts($slug, Request $request)
    {
        $district = Districts::where(['slug' => $slug])->firstOrFail();

        $repetitors = Users::from(app(Users::class)->getTable(), 'u')
            ->leftJoin(app(Ratings::class)->getTable().' AS rt', 'u.id_users', '=', 'rt.id_users')
            ->where([
                'u.id_districts' => $district->id_districts,
                'u.id_users_active_statuses' => UsersActiveStatuses::ACTIVE,
                'u.is_deleted' => 0
            ]);

        $repetitors = $this->paginate($request, (new UsersWithRatingTableFilter($repetitors, $request))->apply());

        return view('pages.search-by-district', [
            'district' => $district,
            'repetitors' => $repetitors
        ]);
    }

    /**
     * Поиск по предмету и выбранному району
     * raion_all.php
     *
     * @param $course
     * @param $district
     * @param Request $request
     *
     * @return Application|Factory|View
     */
    public function searchByCourseAndDistrict($course, $district, Request $request)
    {
        $course = Courses::with([
                'seo',
                'parentCourse',
                'secondCourse',
            ])
            ->whereNotNull('id_title_categories')
            ->where(function ($query) use ($course) {
                $query
                    ->where(['slug' => $course])
                    ->orWhere(['slug_second' => $course]);
            })->firstOrFail();

        $district = Districts::where(['slug' => $district])->firstOrFail();

        $repetitors = Users::from(app(Users::class)->getTable(), 'u')
            ->leftJoin(app(Ratings::class)->getTable().' AS rt', function ($join) use ($course) {
                $join
                    ->on('rt.id_users', '=', 'u.id_users')
                    ->where('rt.id_courses', '=', $course->id_courses);
            })
            ->where(function ($query) use ($course) {
                $query
                    ->where(['u.id_courses' => $course->id_courses])
                    ->orWhere(['u.id_courses_second' => $course->id_courses])
                    ->orWhere(['u.id_courses_third' => $course->id_courses])
                    ->orWhere(['u.id_courses_fourth' => $course->id_courses]);
            })
            ->where([
                'u.id_districts' => $district->id_districts,
                'u.id_users_active_statuses' => UsersActiveStatuses::ACTIVE,
                'u.is_deleted' => 0
            ]);

        $repetitors = $this->paginate($request, (new UsersWithRatingTableFilter($repetitors, $request))->apply());

        $seo = $this->getTitleDescriptionByName('title_district', $repetitors->currentPage(), $repetitors->firstItem(), $repetitors->lastItem(), $course, $district);
        $breadcrumbs = $this->getBreadcrumbsByName('district', $repetitors->currentPage(), $repetitors->firstItem(), $repetitors->lastItem(), $course, $district);
        $header = $this->getHeaderByName('district', $repetitors->currentPage(), $course, $district);
        $totalText = $this->getTotalTextByName('district', $repetitors->currentPage(), $repetitors->total(), $course, $district);
        $prependText = $this->getPrependTextByName('district', $repetitors->currentPage(), $course, $district);

        session()->flash('nametags', [
            'courses' => [$course->id_courses => $course->name]
        ]);

        return view('pages.search-by-course-and-metro', [
            'seo' => $seo,
            'breadcrumbs' => $breadcrumbs,
            'header' => $header,
            'totalText' => $totalText,
            'prependText' => $prependText,
            'course' => $course,
            'district' => $district,
            'repetitors' => $repetitors
        ]);
    }

    /**
     * Поиск других предметов по району
     * other_predmet_raion.php
     *
     * @param Request $request
     *
     * @return Application|Factory|View
     */
    public function searchOtherCoursesByDistrict(Request $request)
    {
        $district = Districts::where(['id_districts' => $request->get('id_raion')])->firstOrFail();

        $courses = Courses::from(app(Courses::class)->getTable(), 'c')
            ->where('c.id_courses', '<', 1000)
            ->where('c.id_courses', '!=', Courses::OTHER_SUBJECTS)
            ->selectRaw('c.*, (SELECT COUNT(*) FROM `users` u WHERE (
                `u`.`id_courses`=`c`.`id_courses` OR
                `u`.`id_courses_second`=`c`.`id_courses` OR
                `u`.`id_courses_third`=`c`.`id_courses` OR
                `u`.`id_courses_fourth`=`c`.`id_courses`
            ) AND `u`.`is_deleted`=0 AND `u`.`id_users_active_statuses`=?) AS count', [UsersActiveStatuses::ACTIVE])
            ->orderBy('c.name', 'ASC')
            ->having('count', '>=', env('MIN_COUNT_OF_USERS'))
            ->get();

        $carInstructorsCount = Users::where(['id_districts' => $district->id_districts])
            ->where(function ($query) {
                $query
                    ->whereIn('id_courses', [Courses::AUTOMATIC_TRANSMISSION, Courses::MANUAL_TRANSMISSION])
                    ->orWhereIn('id_courses_second', [Courses::AUTOMATIC_TRANSMISSION, Courses::MANUAL_TRANSMISSION])
                    ->orWhereIn('id_courses_third', [Courses::AUTOMATIC_TRANSMISSION, Courses::MANUAL_TRANSMISSION])
                    ->orWhereIn('id_courses_fourth', [Courses::AUTOMATIC_TRANSMISSION, Courses::MANUAL_TRANSMISSION]);
            })
            ->where([
                'id_users_active_statuses' => UsersActiveStatuses::ACTIVE,
                'is_deleted' => 0
            ])
            ->count();

        $motorInstructorsCount = Users::where(['id_districts' => $district->id_districts])
            ->where(function ($query) {
                $query
                    ->where(['id_courses' => Courses::MOTORBIKES])
                    ->orWhere(['id_courses_second' => Courses::MOTORBIKES])
                    ->orWhere(['id_courses_third' => Courses::MOTORBIKES])
                    ->orWhere(['id_courses_fourth' => Courses::MOTORBIKES]);
            })
            ->where([
                'id_users_active_statuses' => UsersActiveStatuses::ACTIVE,
                'is_deleted' => 0
            ])
            ->count();

        return view('pages.search-other-courses-by-district', [
            'district' => $district,
            'courses' => $courses,
            'carInstructorsCount' => $carInstructorsCount,
            'motorInstructorsCount' => $motorInstructorsCount
        ]);
    }

    /******************************************************************************************************************/

    /**
     * Поиск по вузам
     *
     * @return Factory|View
     */
    public function searchByUniversity()
    {
        $universities = Universities::orderBy('name', 'asc')->get();

        return view('pages.search-by-university', [
            'universities' => $universities,
        ]);
    }

    /**
     * Поиск преподавателей по конкретному вузу
     *
     * @param string $slug Слаг из таблицы Universities
     *
     * @return Factory|View
     */
    public function searchBySelectedUniversity($slug, Request $request)
    {
        $university = Universities::where(['slug' => $slug])->firstOrFail();
        $repetitors = Users::with([
                'course',
                'courseSecond',
            ])
            ->from(app(Users::class)->getTable(), 'u')
            ->where([
                'u.id_universities' => $university->id_universities,
                'u.id_users_active_statuses' => UsersActiveStatuses::ACTIVE,
                'u.is_deleted' => 0
            ]);

        $repetitors = $this->paginate($request, (new UsersFilter($repetitors, $request))->apply());

        $commentsPrependText = [];

        if ($category = CommentsPrependTextCategories::where(['name' => 'search_by_selected_university'])->first()) {
            $commentsPrependText = CommentsPrependTextTemplates::where([
                'id_comments_prepend_text_categories' => $category->id_comments_prepend_text_categories,
                'page' => $repetitors->currentPage()
            ])->pluck('template');
        }

        $newUniversity = null;

        if ($repetitors->currentPage() == 1) {
            $newUniversity = Universities::where('id_universities', '>', $university->id_universities)
                ->orderBy('id_universities', 'ASC')
                ->first();

            if (!$newUniversity) {
                $newUniversity = Universities::orderBy('id_universities', 'ASC')->first();
            }
        }

        session()->flash('nametags', [
            'universities' => [$university->id_universities => $university->short_name]
        ]);

        return view('pages.search-by-selected-university', [
            'university' => $university,
            'repetitors' => $repetitors,
            'commentsPrependText' => $commentsPrependText,
            'newUniversity' => $newUniversity
        ]);
    }

    /******************************************************************************************************************/

    /**
     * @param Request $request
     *
     * @return Application|Factory|View
     */
    public function subCourses(Request $request)
    {
        $course = Courses::where(['id_courses' => $request->get('id_lern')])->firstOrFail();
        $courses = Courses::where(['id_parent_course' => $course->id_courses])->orderBy('name', 'asc')->get();

        return view('pages.sub-courses', [
            'course' => $course,
            'courses' => $courses
        ]);
    }

    /**
     * Поиск похожего репетитора в анкете репетитора
     *
     * @param                 $repetitor
     * @param bool            $firstRange   Будет использоваться первый ценовой диапазон или нет
     * @param bool            $includeMetro Будет включен поиск с учетом по метро
     * @param bool            $firstCourse  Будет ли использоваться первый или второй предмет от преподавателя
     * @param Collection|null $usersList    Список репетиторов для исключения дублей
     *
     * @return Collection
     */
    private function getSimilarUsers($repetitor, bool $firstRange = true, bool $includeMetro = true, bool $firstCourse = true, Collection $usersList = null) {
        $priceRance = $firstRange
            ? [$repetitor->price_per_hour * 0.7, $repetitor->price_per_hour * 1.3]
            : [$repetitor->price_per_hour * 0.6, $repetitor->price_per_hour * 1.4];

        $users = Users::with([
                'course',
                'courseSecond',
            ])
            ->from(app(Users::class)->getTable(), 'u');

        if ($includeMetro) {
            $users = $users
                ->leftJoin(app(Metro::class)->getTable()." AS m", 'm.id_metro', '=', 'u.id_metro')
                ->where('m.nearest_stations', 'regexp', '^(\d+,){0,2}'.$repetitor->id_metro);
        }

        if ($usersList) {
            $usersIdList = $usersList->map(function ($user) {
                return $user->id_users;
            });
            $users = $users->whereNotIn('u.id_users', $usersIdList);
        }

        return $users
            ->select('u.*')
            ->where('u.id_users', '!=', $repetitor->id_users)
            ->where([
                'u.id_users_active_statuses' => UsersActiveStatuses::ACTIVE,
                'u.is_deleted' => 0,
                'u.id_courses' => $firstCourse ? $repetitor->id_courses : $repetitor->id_courses_second,
            ])
            ->whereBetween('u.price_per_hour', $priceRance)
            ->orderBy('u.rating', 'DESC')
            ->limit(4)
            ->get();
    }

    /**
     * Анкета репетитора
     *
     * @param Request     $request
     * @param string|null $account
     *
     * @return Factory|View
     */
    public function account(Request $request, ?string $account = null)
    {
        $repetitor = null;

        if (!$account) {
            $id = $request->get('id_user');

            $repetitor = Users::where(['id_users' => $id])->firstOrFail();
        } else {
            $tmp = explode('-', $account);
            $id = array_pop($tmp);
            $slug = implode('-', $tmp);

            $repetitor = Users::where(['id_users' => $id, 'slug' => $slug])->firstOrFail();
        }

        // Отзывы
        $reviews = $reviews = Reviews::with([
                'order',
                'order.course',
            ])
            ->where([
                'id_users' => $repetitor->id_users,
                'id_reviews_statuses' => ReviewsStatuses::APPROVED,
                'is_deleted' => 0,
            ])
            ->orderBy('date', 'DESC')
            ->limit(env('ACCOUNT_REVIEWS_PER_PAGE'))
            ->get();

        // График занятости
        $schedule = UsersSchedule::where(['id_users' => $id])
            ->orderBy('id_users_schedule_week_days', 'asc')
            ->orderBy('id_users_schedule_time', 'asc')
            ->get()
            ->mapToGroups(function ($item, $key) {
                return [$item['id_users_schedule_time'] => $item['id_users_schedule_week_days']];
            })
            ->toArray();

        $weekDays = UsersScheduleWeekDays::all();
        $timeSheet = UsersScheduleTime::all();

        // Блок документы и видео
        $videoDocuments = $repetitor->documentsVideo()->slice(0, 1);

        // Блок похожие преподаватели
        $newUsers = $this->getSimilarUsers($repetitor);

        if (!$newUsers) {
            $newUsers = $this->getSimilarUsers($repetitor, false);
        }

        // Если кол-во похожих преподавателей меньше 4, то ищем без выборки по метро
        if (count($newUsers) < 4) {
            $usersWithoutMetro = $this->getSimilarUsers($repetitor, true, false);

            if (!$usersWithoutMetro) {
                $usersWithoutMetro = $this->getSimilarUsers($repetitor, false, false);
            }

            $newUsers->merge($usersWithoutMetro);
        }

        // Если указан второй предмет у репетитора
        $usersWithSecondCourse = [];

        if ($repetitor->id_courses_second) {
            $usersWithSecondCourse = $this->getSimilarUsers($repetitor, true, true, false, $newUsers);

            if (!$usersWithSecondCourse) {
                $usersWithSecondCourse = $this->getSimilarUsers($repetitor, false, true, false, $newUsers);
            }

            // Если кол-во похожих преподавателей меньше 4, то ищем без выборки по метро
            if (count($usersWithSecondCourse) < 4) {
                $usersWithoutMetro = $this->getSimilarUsers($repetitor, true, false, false, $newUsers);

                if (!$usersWithoutMetro) {
                    $usersWithoutMetro = $this->getSimilarUsers($repetitor, false, false, false, $newUsers);
                }

                $usersWithSecondCourse->merge($usersWithoutMetro);
            }
        }

        if (count($newUsers) >= 2 && count($usersWithSecondCourse) >= 2) {
            $newUsersTemp = $newUsers->slice(0, 2);
            $usersWithSecondCourseTemp = $usersWithSecondCourse->slice(0, 2);
            $newUsers = $newUsersTemp->merge($usersWithSecondCourseTemp);
        } else {
            if (count($newUsers) <= count($usersWithSecondCourse)) {
                $newUsers = $usersWithSecondCourse;
            }

            // Забираем 4 репетитора из коллекции
            // TODO: Ошибка http://repetitors.local/include/profrep.php?id_user=20379
            // $newUsers = $newUsers->slice(0, 4);
        }

        return view('pages.account', [
            'repetitor' => $repetitor,
            'reviews' => $reviews,
            'schedule' => $schedule,
            'weekDays' => $weekDays,
            'timeSheet' => $timeSheet,
            'newUsers' => $newUsers,
            'videoDocuments' => $videoDocuments,
        ]);
    }

    public function searchForm()
    {
        $searchCoursesList = Courses::whereIn('id_courses_types', [CoursesTypes::RARE_COURSES_3, CoursesTypes::RARE_COURSES_4])
            ->orderBy('name', 'ASC')
            ->get();

        $coursesList = Courses::where(['id_courses_types' => CoursesTypes::NORMAL_POPULAR_COURSES])
            ->orderByRaw("FIELD(id_courses,?) ASC, id_courses_types DESC, name ASC", [Courses::OTHER_SUBJECTS])
            ->get();

        $cities = Cities::orderByRaw('FIELD(id_cities,1) DESC, name ASC')->get();
        $metro = Metro::orderBy('name', 'asc')->get();
        $locations = OrdersLocations::all();

        return view('pages.search-form', [
            'searchCoursesList' => $searchCoursesList,
            'coursesList' => $coursesList,
            'locations' => $locations,
            'cities' => $cities,
            'metro' => $metro,
        ]);
    }

    /**
     * Поиск репетитора по номеру анкеты
     *
     * @param SearchRepetitorRequest $request
     *
     * @return Factory|View
     */
    public function searchRepetitorByID(SearchRepetitorRequest $request)
    {
        $id = $request->get('account_number');

        $repetitor = Users::with([
                'course',
                'courseSecond',
                'courseThird',
                'courseFourth',
                'metro',
                'metroSecond',
                'city',
                'citySecond',
                'cityThird',
                'cityFourth',
                'university',
            ])
            ->where(['id_users' => $id, 'is_deleted' => 0])
            ->whereNotIn('id_users_active_statuses', [
                UsersActiveStatuses::NOT_ACTIVE,
                UsersActiveStatuses::DELETED
            ])
            ->first();

        return view('pages.search-by-repetitor-id', [
            'id' => $id,
            'repetitor' => $repetitor,
        ]);
    }

    /**
     * Поиск онлайн курса
     *
     * @param $course
     *
     * @return Factory|View
     */
    public function searchOnlineCourseBySlug(string $course, Request $request)
    {
        $course = Courses::with('seo')
            ->where([
                'slug' => $course,
                'skype' => 1
            ])
            ->firstOrFail();

        $repetitors = Users::from(app(Users::class)->getTable(), 'u')
            ->leftJoin(app(Ratings::class)->getTable().' AS rt', function ($join) use ($course) {
                $join
                    ->on('rt.id_users', '=', 'u.id_users')
                    ->where('rt.id_courses', '=', $course->id_courses);
            })
            ->where(function ($query) use ($course) {
                $query
                    ->where(['u.id_courses' => $course->id_courses])
                    ->orWhere(['u.id_courses_second' => $course->id_courses])
                    ->orWhere(['u.id_courses_third' => $course->id_courses])
                    ->orWhere(['u.id_courses_fourth' => $course->id_courses]);
            })
            ->where([
                'u.id_users_active_statuses' => UsersActiveStatuses::ACTIVE,
                'u.is_deleted' => 0,
                'u.skype' => 1
            ]);

        $repetitors = $this->paginate($request, (new UsersWithRatingTableFilter($repetitors, $request))->apply());

        $newCourse = null;

        if ($repetitors->currentPage() == 1) {
            $newCourse = Courses::where('id_courses', '>', $course->id_courses)
                ->where(['skype' => 1])
                ->orderBy('id_courses', 'ASC')
                ->first();

            if (!$newCourse) {
                $newCourse = Courses::where(['skype' => 1])
                    ->orderBy('id_courses', 'ASC')
                    ->first();
            }
        }

        // Сохраняем данные в сессию для вывода в плашках и фильтре
        session()->flash('nametags', [
            'courses' => [$course->id_courses => $course->name]
        ]);

        return view('pages.online-course', [
            'course' => $course,
            'repetitors' => $repetitors,
            'newCourse' => $newCourse,
        ]);
    }

    /**
     * Каталог репетиторов. Поиск по предметам.
     *
     * @param $slug
     *
     * @return Factory|View
     */
    public function searchByCourse(string $slug, Request $request)
    {
        $course = Courses::with([
                'seo',
                'parentCourse',
                'secondCourse',
            ])
            ->whereNotNull('id_title_categories')
            ->where(function ($query) use ($slug) {
                $query
                    ->where(['slug' => $slug])
                    ->orWhere(['slug_second' => $slug]);
            })->firstOrFail();

        $repetitors = Users::from(app(Users::class)->getTable(), 'u')
            ->leftJoin(app(Ratings::class)->getTable().' AS rt', function ($join) use ($course) {
                $join
                    ->on('rt.id_users', '=', 'u.id_users')
                    ->where('rt.id_courses', '=', $course->id_courses);
            })
            ->where(function ($query) use ($course) {
                $query
                    ->where(['u.id_courses' => $course->id_courses])
                    ->orWhere(['u.id_courses_second' => $course->id_courses])
                    ->orWhere(['u.id_courses_third' => $course->id_courses])
                    ->orWhere(['u.id_courses_fourth' => $course->id_courses]);
            })
            ->where([
                'u.id_users_active_statuses' => UsersActiveStatuses::ACTIVE,
                'u.is_deleted' => 0
            ]);

        $repetitors = $this->paginate($request, (new UsersWithRatingTableFilter($repetitors, $request))->apply());

        $seo = Helper::getCourseTitleDescription($course, $repetitors->currentPage(), $repetitors->firstItem(), $repetitors->lastItem());
        $totalText = Helper::getCourseTotalText($course, $repetitors->currentPage(), $repetitors->total());
        $prependText = Helper::getPrependText($course, $repetitors->currentPage());
        $altText = Helper::getPhotoAltText($course, $repetitors->currentPage());
        $commentsPrependText = Helper::getCommentsPrependText($course, $repetitors->currentPage());
        $pagesAppendText = Helper::getPagesAppendText($course, $repetitors->currentPage(), $repetitors->total());

        // Сохраняем данные в сессию для вывода в плашках и фильтре
        session()->flash('nametags', [
            'courses' => [$course->id_courses => $course->name]
        ]);

        return view('pages.search-by-course', [
            'seo' => $seo,
            'totalText' => $totalText,
            'prependText' => $prependText,
            'altText' => $altText,
            'commentsPrependText' => $commentsPrependText,
            'pagesAppendText' => $pagesAppendText,
            'course' => $course,
            'repetitors' => $repetitors,
        ]);
    }

    /**
     * Подкатегории предмета
     * TODO: Проверить
     *
     * @param string $course
     * @param string $subcourse
     */
    public function searchBySubCourse(string $subcourseSlug, Request $request)
    {
        $page = $request->get('page', 0);

        // Поиск выбранного подраздела
        $subcourse = Courses::with([
                'seo',
                'parentCourse',
                'secondCourse',
            ])
            ->whereNotNull('id_title_categories')
            ->where(function ($query) use ($subcourseSlug) {
                $query
                    ->where(['slug' => $subcourseSlug])
                    ->orWhere(['slug_second' => $subcourseSlug]);
            })
            ->firstOrFail();

        $slug = substr($subcourse->slug, strpos($subcourse->slug, '/') + 1);

        $limitUsersOnCourses = [
            'repetitor-expert-ege',
            'repetitor-olimpiada',
            'repetitor-dlya-doshkolnikov',
            'repetitor-student',
            'repetitor-podgotovka-k-EGE',
            'repetitor-podgotovka-k-OGE',
            'repetitor-profilnyj-uroven-ege'
        ];

//        $ratings = app(Ratings::class)->getTable();
//        $cs = app(CoursesSubcourses::class)->getTable();
//        dump($subcourse->attributesToArray());

        $repetitors = Users::from(app(Users::class)->getTable(), 'u')
            ->leftJoin(app(Ratings::class)->getTable().' AS rt', function ($join) use ($subcourse) {
                $join
                    ->on('rt.id_users', '=', 'u.id_users')
                    ->whereIn('rt.id_courses', [
                        $subcourse->id_parent_course,
//                        $subcourse->id_courses_second,
//                        $subcourse->id_courses_third
                    ]);
            })
            ->where(function ($query) use ($subcourse) {
                $query
                    ->where(function ($query) use ($subcourse) {
                        $query
                            ->where(['u.id_courses' => $subcourse->id_courses_second])
                            ->orWhere(['u.id_courses_second' => $subcourse->id_courses_second])
                            ->orWhere(['u.id_courses_third' => $subcourse->id_courses_second])
                            ->orWhere(['u.id_courses_fourth' => $subcourse->id_courses_second]);
                    })
                    ->where(function ($query) use ($subcourse) {
                        $query
                            ->where(['u.id_courses' => $subcourse->id_parent_course])
                            ->orWhere(['u.id_courses_second' => $subcourse->id_parent_course])
                            ->orWhere(['u.id_courses_third' => $subcourse->id_parent_course])
                            ->orWhere(['u.id_courses_fourth' => $subcourse->id_parent_course]);
                    });
            })
            ->where([
                'u.id_users_active_statuses' => UsersActiveStatuses::ACTIVE,
                'u.is_deleted' => 0,
            ])
            //!in_array($slug, $limitUsersOnCourses) && empty($subcourse->id_courses_second)
            ->when(!in_array($subcourse->id_courses_types, [CoursesTypes::TWO_COURSES, CoursesTypes::THREE_COURSES]), function ($query) use ($subcourse) {
                $users = CoursesSubcourses::where(['id_courses' => $subcourse->id_courses])->pluck('id_users');

                return $query->whereIn('u.id_users', $users);
            })
            ->when($slug == 'repetitor-expert-ege', function ($query) {
                return $query
                    ->where(function ($query) {
                        $query
                            ->where('u.comment', 'like', '%эксперт%')
                            ->orWhere('u.comment', 'like', '%ЕГЭ%');
                    })
                    ->whereRaw('u.id_users % 3 = 0'); // Чтобы выборка отличалась от выборки других страниц, была уникальна и Яндекс не банил страницы в выдаче
            })
            ->when(in_array($slug, ['repetitor-podgotovka-k-EGE', 'repetitor-profilnyj-uroven-ege']), function ($query) {
                return $query->where('u.comment', 'like', '%ЕГЭ%');
            })
            ->when($slug == 'repetitor-podgotovka-k-OGE', function ($query) {
                return $query->where('u.comment', 'like', '%ОГЭ%');
            })
            ->when($slug == 'repetitor-olimpiada', function ($query) {
                return $query->where('u.comment', 'like', '%олимпиада%');
            })
            ->when($slug == 'repetitor-dlya-doshkolnikov', function ($query) {
                return $query->where('u.comment', 'like', '%дошкольник%');
            })
            ->when($slug == 'repetitor-student', function ($query) {
                return $query->where(['u.id_users_statuses' => UsersStatuses::STUDENT]);
            })
            ->when(($subcourse->id_courses == 4530) && ($page == 0), function ($query) {
                return $query->whereRaw('u.id_users % 2 = 0');
            })
            ->when(($subcourse->id_courses == 4531) && ($page == 0), function ($query) {
                return $query->whereRaw('u.id_users % 3 = 0');
            })
            ->when(($subcourse->id_courses == 4532) && ($page == 0), function ($query) {
                return $query->whereRaw('u.id_users % 4 = 0');
            })
            ->when(($subcourse->id_courses == 4533) && ($page == 0), function ($query) {
                return $query->whereRaw('u.id_users % 5 = 0');
            })
            ->when(($subcourse->id_courses == 4534) && ($page == 0), function ($query) {
                return $query->whereRaw('u.id_users % 6 = 0');
            })
            ->when(($subcourse->id_courses == 4535) && ($page == 0), function ($query) {
                return $query->whereRaw('u.id_users % 7 = 0');
            })
            ->when(($subcourse->id_courses == 4536) && ($page == 0), function ($query) {
                return $query->whereRaw('u.id_users % 8 = 0');
            });

        $repetitors = $this->paginate($request, (new UsersWithRatingTableFilter($repetitors, $request))->apply());

        $seo = Helper::getCourseTitleDescription($subcourse, $repetitors->currentPage(), $repetitors->firstItem(), $repetitors->lastItem());
        $totalText = Helper::getCourseTotalText($subcourse, $repetitors->currentPage(), $repetitors->total());
        $prependText = Helper::getPrependText($subcourse, $repetitors->currentPage());
        $altText = Helper::getPhotoAltText($subcourse, $repetitors->currentPage());
        $commentsPrependText = Helper::getCommentsPrependText($subcourse, $repetitors->currentPage());
        $pagesAppendText = Helper::getPagesAppendText($subcourse, $repetitors->currentPage(), $repetitors->total());

        session()->flash('nametags', [
            'courses' => [$subcourse->parentCourse->id_courses => $subcourse->parentCourse->name]
        ]);

        return view('pages.search-by-course', [
            'seo' => $seo,
            'totalText' => $totalText,
            'prependText' => $prependText,
            'altText' => $altText,
            'commentsPrependText' => $commentsPrependText,
            'pagesAppendText' => $pagesAppendText,
            'course' => $subcourse,
            'repetitors' => $repetitors
        ]);
    }

    /******************************************************************************************************************/

    /**
     * Поиск по классам
     *
     * @param Request $request
     */
    public function searchByClass(Request $request)
    {
        $course = Courses::where(['id_courses' => $request->get('predmet'), 'class_menu' => 1])->firstOrFail();

        return view('pages.search-by-class', [
            'course' => $course
        ]);
    }

    /**
     * Поиск по выбранному предмету и классу
     *
     * @param string $course fizika|himiya|matematika|russkij_yazyk
     * @param string $class
     * @param Request $request
     *
     * @return Application|Factory|View
     */
    public function searchByCourseAndClass(string $course, string $class)
    {
        $course = Courses::where(['slug' => $course, 'class_menu' => 1])->firstOrFail();

//            ->when($course == 'fizika', function ($query) use ($class) {
//                switch ($class) {
//
//                }
//            })
//            ->when($course == 'himiya', function ($query) {
//
//            })
//            ->when($course == 'matematika', function ($query) {
//
//            })
//            ->when($course == 'russkij_yazyk', function ($query) {
//
//            })


        return view('pages.search-by-course-and-class', [
            'course' => $course
        ]);
    }

    /******************************************************************************************************************/

    /**
     * Поиск предметов в СПбГУ
     *
     * @return Application|Factory|View
     */
    public function searchBySpbgu()
    {
        $courses = Courses::where(['spbgu' => 1])
            ->where('id_courses', '!=', Courses::OTHER_SUBJECTS)
            ->get();

        return view('pages.search-by-spbgu', [
            'courses' => $courses
        ]);
    }

    /**
     * Поиск преподавателей по предмету и университету СПбГУ
     *
     * @param string $slug
     * @param Request $request
     *
     * @return Application|Factory|View
     */
    public function searchByCourseAndSpbgu(string $slug, Request $request)
    {
        $course = Courses::with([
                'seo',
                'parentCourse',
                'secondCourse',
            ])
            ->whereNotNull('id_title_categories')
            ->where(['spbgu' => 1])
            ->where(function ($query) use ($slug) {
                $query
                    ->where(['slug' => $slug])
                    ->orWhere(['slug_second' => $slug]);
            })->firstOrFail();
        $university = Universities::where(['id_universities' => Universities::SPBGU])->first();

        $repetitors = Users::from(app(Users::class)->getTable(), 'u')
            ->leftJoin(app(Ratings::class)->getTable().' AS rt', function ($join) use ($course) {
                $join
                    ->on('rt.id_users', '=', 'u.id_users')
                    ->where('rt.id_courses', '=', $course->id_courses);
            })
            ->where(function ($query) use ($course) {
                $query
                    ->where(['u.id_courses' => $course->id_courses])
                    ->orWhere(['u.id_courses_second' => $course->id_courses])
                    ->orWhere(['u.id_courses_third' => $course->id_courses])
                    ->orWhere(['u.id_courses_fourth' => $course->id_courses]);
            })
            ->where([
                'u.id_universities' => Universities::SPBGU,
                'u.id_users_active_statuses' => UsersActiveStatuses::ACTIVE,
                'u.is_deleted' => 0
            ]);

        $repetitors = $this->paginate($request, (new UsersWithRatingTableFilter($repetitors, $request))->apply());

        $newCourse = null;

        if ($repetitors->currentPage() == 1) {
            $newCourse = Courses::where('id_courses', '>', $course->id_courses)
                ->where(['spbgu' => 1])
                ->orderBy('id_courses', 'ASC')
                ->first();

            if (!$newCourse) {
                $newCourse = Courses::where(['spbgu' => 1])
                    ->orderBy('id_courses', 'ASC')
                    ->first();
            }
        }

        // Сохраняем данные в сессию для вывода в плашках и фильтре
        session()->flash('nametags', [
            'courses' => [$course->id_courses => $course->name],
            'universities' => [$university->id_universities => $university->short_name]
        ]);

        return view('pages.search-by-course-and-spbgu', [
            'course' => $course,
            'repetitors' => $repetitors,
            'newCourse' => $newCourse
        ]);
    }

    /******************************************************************************************************************/

    /**
     * Открытие выбранных страниц
     *
     * @param string $name Слаг страницы для поиска в папке pages
     *
     * @return Factory|View
     */
    public function pages($name)
    {
        $name = 'pages.static.' . $name;

        if (view()->exists($name)) {
            return view($name);
        }

        return view('errors.404');
    }

    /**
     * @return RedirectResponse
     */
    public function changeDevice()
    {
        return Redirect::back();
    }

    /**
     * Изображения для соцсетей
     *
     * @param OpenGraphRequest $request
     */
    public function openGraph(OpenGraphRequest $request)
    {
        $id = $request->get('id');

        $user = Users::where([
            'id_users' => $id,
            'id_users_active_statuses' => UsersActiveStatuses::ACTIVE,
            'is_deleted' => 0
        ])
            ->select('photo')
            ->first();

        $path = env('PHOTOS_USERS_DIR');

        header('Content-Type: image/png');

        if ($user && !empty($user->photo) && file_exists(public_path($path . $user->photo))) {
            // Фото репетитора
            $img = imagecreatefromstring(file_get_contents(public_path($path . $user->photo)));
            $fix_size = 200;

            // Щирина изображения
            $width = imagesx($img);

            // Высота изображения
            $height = imagesy($img);

            // Коэффициент
            $k = $fix_size / $width;
            $new_w = $fix_size;
            $new_h = round($height * $k);

            $new_image = imagecreatetruecolor($new_w * 2, $new_h);
            imagefill($new_image, 0, 0, 0xffffff);

            $border = imagecolorallocate($new_image, 0, 0, 0x6a6a6a);
            imagerectangle($new_image, 0, -1, $new_w * 2 - 1, $new_h, $border);

            // Накладываем фото
            imagecopyresampled($new_image, $img, 100, 0, 0, 0, $new_w, $new_h, $width, $height);

            // Отображаем изображение
            imagepng($new_image);

            // Очищаем память
            imagedestroy($img);
            imagedestroy($new_image);
        } else {
            //Лого сайта
            $img = imagecreatefrompng(public_path(env('ASSETS_DIR') . '/img/og_img.png'));

            // Отображаем изображение
            imagepng($img);

            // Очищаем память
            imagedestroy($img);
        }
    }
}
