<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Http\Requests\AddOrderRequest;
use App\Http\Requests\AjaxRequest;
use App\Http\Requests\FavouritesRequest;
use App\Http\Requests\MetroRequest;
use App\Http\Requests\TeacherFormRequest;
use App\Models\Cities;
use App\Models\Courses;
use App\Models\CoursesTypes;
use App\Models\Favourites;
use App\Models\Metro;
use App\Models\Orders;
use App\Models\OrdersLocations;
use App\Models\Reviews;
use App\Models\ReviewsStatuses;
use App\Models\Users;
use App\Models\UsersActiveStatuses;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Support\Facades\Cookie;
use Illuminate\View\View;
use Throwable;

class AjaxController extends Controller
{
    /**
     * Получение списка отзывов в разделе поиска репетитора по ID
     *
     * @param AjaxRequest $request
     *
     * @return string
     */
    public function ajax(AjaxRequest $request)
    {
        $id = $request->get('id_users');
        $start = $request->get('start', 1);

        if ($request->get('limit')) {
            $limit = $request->get('limit');
        } else {
            $limit = !$start ? 20 : 1;
        }

        $repetitor = Users::where(['id_users' => $id, 'is_deleted' => 0])->firstOrFail();

        $result = '';

        if ($repetitor && ($repetitor->reviewsCount() > $start)) {
            $reviews = Reviews::with([
                    'order',
                    'order.course',
                ])
                ->where([
                    'id_users' => $repetitor->id_users,
                    'id_reviews_statuses' => ReviewsStatuses::APPROVED,
                    'is_deleted' => 0,
                ])
                ->orderBy('date', 'DESC')
                ->paginate($limit, ['*'], 'start');

            foreach ($reviews ?? [] as $review) {
                $result .= view('components.ajax.reviews-account', [
                    'repetitor' => $repetitor,
                    'review' => $review,
                ]);
            }
        }

        return $result;
    }

    /**
     * @param MetroRequest $request
     *
     * @return false|mixed|string
     */
    public function metro(MetroRequest $request)
    {
        try {
            $metro = Users::where([
                    'id_users' => $request->get('id_users'),
                    'is_deleted' => 0
                ])
                ->firstOrFail();

            return json_encode(explode(',', $metro->metro_list));
        } catch (Exception $e) {
            return 'Ошибка';
        }
    }

    /**
     * Форма заявки, универсальная, как для общей формы, так и для конкретного репетитора
     *
     * @param TeacherFormRequest $request
     *
     * @return Factory|View
     */
    public function requestTeacherForm(TeacherFormRequest $request)
    {
        $id = $request->get('id_users');
        $type = $request->get('type');

        $title = '';

        if (in_array($type, [1, 2])) {
            $repetitor = Users::with([
                    'course',
                    'courseSecond',
                    'courseThird',
                    'courseFourth',
                ])
                ->where(['id_users' => $id, 'is_deleted' => 0])
                ->firstOrFail();

            // Исключим список предметов преподавателя из общего списка
            $repetitorSubjects = array_filter([
                $repetitor->id_courses,
                $repetitor->id_courses_second,
                $repetitor->id_courses_third,
                $repetitor->id_courses_fourth,
            ], function (?int $value) {
                return !is_null($value);
            });

            $courses = Courses::where(['id_courses_types' => CoursesTypes::NORMAL_POPULAR_COURSES])
                ->whereNotIn('id_courses', $repetitorSubjects)
                ->orderByRaw("FIELD(id_courses,?) ASC, name ASC", [Courses::OTHER_SUBJECTS])
                ->get();
        } else {
            // Формирование заголовка для репетиторов из избранного
            if (Cookie::has('fav')) {
                $favorites = Favourites::where(['id_favourites' => Cookie::get('fav')])->pluck('id_users');

                $users = Users::whereIn('id_users', $favorites)->get();

                $items = [];

                foreach ($users ?? [] as $user) {
                    $items[] = '№ '.$user->id_users.' ('.trim($user->first_name.' '.$user->middle_name).')';
                }

                // Макс 4 репетитора в заголовке
                $title = count($items) > 4 ? implode(', ', array_slice($items, 0, 4)).'...' : implode(', ', $items);
            }

            $searchCoursesList = Courses::whereIn('id_courses_types', [CoursesTypes::RARE_COURSES_3, CoursesTypes::RARE_COURSES_4])
                ->orderBy('name', 'ASC')
                ->get();

            $coursesList = Courses::where(['id_courses_types' => CoursesTypes::NORMAL_POPULAR_COURSES])
                ->orderByRaw("FIELD(id_courses,?) ASC, id_courses_types DESC, name ASC", [Courses::OTHER_SUBJECTS])
                ->get();
        }

        $cities = Cities::orderByRaw('FIELD(id_cities,1) DESC, name ASC')->get();
        $metro = Metro::orderBy('name', 'asc')->get();
        $locations = OrdersLocations::all();

        if (in_array($type, [3, 4])) {
            return view('components.ajax.teacher-form-common', [
                'title' => $title,
                'type' => $type,
                'searchCoursesList' => $searchCoursesList,
                'coursesList' => $coursesList,
                'locations' => $locations,
                'cities' => $cities,
                'metro' => $metro,
            ]);
        }

        return view('components.ajax.teacher-form', [
            'repetitor' => $repetitor,
            'courses' => $courses,
            'locations' => $locations,
            'cities' => $cities,
            'metro' => $metro,
        ]);
    }

    /**
     * Функция создания заявки, универсальная, как для общей формы, так и для конкретного преподавателя
     *
     * @param AddOrderRequest $request
     *
     * @return Factory|View|string
     * @throws Throwable
     */
    public function addOrder(AddOrderRequest $request)
    {
        $id_users = $request->get('id_users', []);
        $sex = $request->get('sex');

        try {
            $order = new Orders($request->all());

            if ($id_users) {
                $repetitor = Users::where([
                        'id_users' => $id_users,
                        'id_users_active_statuses' => UsersActiveStatuses::ACTIVE,
                        'is_deleted' => 0,
                    ])
                    ->firstOrFail();

                $order->price_limit = round(($repetitor->price_per_hour * 3) / 2, -2);
            } else {
                $order->additional_information .= "\r\n".$order->additional_information_second;

                if (!is_null($sex)) {
                    $order->additional_information .= "\r\nЖелаемый пол репетитора: ".($sex ? 'Жен.' : 'Муж.');
                }

                $order->additional_information = trim($order->additional_information);
            }

            // Расчет цены и вывод подробной калькуляции
            $calculation = Helper::calculatePrice(
                null,
                $id_users,
                $request->get('id_courses'),
                $request->get('id_cities'),
                $request->get('duration', 90),
                $request->get('price_limit', 0)
            );

            $order->price_per_user = isset($calculation['price']) ? $calculation['price'] : 0;
            $order->calculation = isset($calculation['calculation']) ? $calculation['calculation'] : 0;

            if ($order->save()) {
                return view('components.ajax.success-order', [
                    'order' => $order,
                ]);
            }

            return 'Не вышло. Проверьте правильность заполнения полей.';
        } catch (Exception $e) {
            return view('components.ajax.incorrect-order');
        }
    }

    /**
     *
     */
    public function documentsVideo()
    {
//        $id_users = $request->get('id_users');
    }

    /**
     * Управление избранным: добавление, удаление, полное удаление записей
     *
     * @param FavouritesRequest $request
     * @return false|string
     */
    public function favourites(FavouritesRequest $request)
    {
        $id_users = $request->get('id_users');
        $action = $request->get('action');

        $id = Cookie::get('fav');

        $result = ['status' => 'ok'];

        switch ($action) {
            case 'add':
                try {
                    Users::where(['id_users' => $id_users])->firstOrFail();

                    $count = Favourites::where(['id_favourites' => $id])->count();

                    if ($count < 20) {
                        (new Favourites([
                            'id_favourites' => $id,
                            'id_users' => $id_users
                        ]))->save();
                    } else {
                        $result['status'] = 'error';
                    }
                }
                catch (Exception $e) {
                    $result['status'] = 'error';
                }

                break;

            case 'del':
                try {
                    Users::where(['id_users' => $id_users])->firstOrFail();

                    Favourites::where(['id_favourites' => $id, 'id_users' => $id_users])->delete();
                }
                catch (Exception $e) {
                    $result['status'] = 'error';
                }

                break;

            case 'del_all':
                Favourites::where(['id_favourites' => $id])->delete();

                break;
        }

        return json_encode($result);
    }
}
