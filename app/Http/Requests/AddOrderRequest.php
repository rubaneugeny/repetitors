<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id_users' => !isset($this->type) ? 'required|numeric' : 'nullable',
            'name' => 'required',
            'phone' => 'required:regex:/\+\d\s\(\d{3}\)\s\d{3}-\d{2}-\d{2}/',
        ];
    }
}
