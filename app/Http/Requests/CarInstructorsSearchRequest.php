<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CarInstructorsSearchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cities' => 'array',
            'cities.*' => 'integer|distinct',
            'metro' => 'array',
            'metro.*' => 'integer|distinct',
            'price' => 'array',
            'price.*' => Rule::in(['<500', '500-750', '750-1000', '1000-1500', '>1500']),
            'transmission' => Rule::in([null, 'a', 'm', 'c']),
            'sex' => Rule::in([null, 'm', 'w']),
        ];
    }
}
