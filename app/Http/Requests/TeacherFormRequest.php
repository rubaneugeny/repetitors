<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class TeacherFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        /*
         * types:
         * 1 - репетиторы индивидуальная форма
         * 2 - автоинструкторы индивидуальная форма
         * 3 - репетиторы общая форма
         * 4 - автоинструкторы общая форма
         */

        return [
            'id_users' => in_array($this->type, [3, 4]) ? 'nullable' : 'required|numeric',
            'fav' => 'nullable|numeric',
            'type' => 'required|numeric|between:1,4',
        ];
    }
}
