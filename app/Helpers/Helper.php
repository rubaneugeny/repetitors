<?php
/*
 * Web-studio: Chameleon
 * Url: https://chameleonweb.ru
 *
 * Created by Ruban Eugeny
 * E-mail: schiller2k@gmail.com
 *
 * Date: 10.03.2020
 * Time: 23:35
 */

namespace App\Helpers;

use App\Models\Cities;
use App\Models\CommentsPrependTextTemplates;
use App\Models\Countries;
use App\Models\Courses;
use App\Models\CoursesCategories;
use App\Models\CoursesCategoriesSecond;
use App\Models\CoursesTypes;
use App\Models\Favourites;
use App\Models\Metro;
use App\Models\Multipliers;
use App\Models\PagesAppendTextTemplates;
use App\Models\PhotoTextTemplates;
use App\Models\PrependTextTemplates;
use App\Models\TitleTemplates;
use App\Models\TotalTemplates;
use App\Models\Universities;
use App\Models\Users;
use App\Models\UsersActiveStatuses;
use App\Models\UsersStatuses;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Cookie;
use Illuminate\View\View;
use Illuminate\Support\Str;

class Helper
{
    public const REPETITOR = 1;
    public const CAR_INSTRUCTOR = 2;

    /**
     * @param int $id_users
     * @param int $type
     *
     * @return Factory|View
     */
    public static function callback($id_users, int $type)
    {
        return view('components.helper.callback', [
            'id_users' => $id_users,
            'type' => $type,
        ]);
    }

    /**
     * @return Factory|View
     */
    public static function countries()
    {
        $countries = Countries::with('cities')->get();

        return view('components.helper.countries', [
            'countries' => $countries,
        ]);
    }

    /**
     * Кнопка помощи на мобильном
     *
     * @param int $type
     *
     * @return Factory|View
     */
    public static function helpButtons(int $type)
    {
        return view('components.helper.help-button', [
            'type' => $type == self::REPETITOR ? 3 : 4,
        ]);
    }

    /**
     * Записывает в сессию текущие плашки выбранных фильтров с левой панели
     *
     * @param Request $request
     */
    public static function makeFilterButtons(Request $request)
    {
        $nametags = [
            'courses' => null,
            'cities' => null,
            'metro' => null,
            'transmission' => null,
            'sex' => null
        ];

        foreach ($request->input() as $key => $values) {
            switch ($key) {
                case 'cities':
                    $nametags[$key] = Cities::whereIn('id_cities', $values)->pluck('name', 'id_cities');
                    break;

                case 'metro':
                    $nametags[$key] = Metro::whereIn('id_metro', $values)->pluck('name', 'id_metro');
                    break;

                case 'sex':
                    switch ($values) {
                        case 'm':
                            $nametags[$key] = ['m' => 'Мужской'];
                            break;

                        case 'w':
                            $nametags[$key] = ['w' => 'Женский'];
                            break;
                    }
                    break;

                case 'transmission':
                    switch ($values) {
                        case 'a':
                            $nametags[$key] = Courses::where(['id_courses' => Courses::AUTOMATIC_TRANSMISSION])->pluck('name', 'id_courses');
                            break;

                        case 'm':
                            $nametags[$key] = Courses::where(['id_courses' => Courses::MANUAL_TRANSMISSION])->pluck('name', 'id_courses');
                            break;

                        case 'c':
                            $nametags[$key] = Courses::where(['id_courses' => Courses::MOTORBIKES])->pluck('name', 'id_courses');
                            break;
                    }
                    break;

                case 'courses':
                    $nametags[$key] = Courses::whereIn('id_courses', $values)->pluck('name', 'id_courses');
                    break;
            }
        }

        session()->flash('nametags', $nametags);
    }

    /**
     * @param array $items
     *
     * @return Factory|View
     */
    public static function filterButtons()
    {
        return view('components.helper.filter-buttons');
    }

    /**
     * Кнопки сортировки
     *
     * @param int  $type            Тип страницы (1 - Репетиторы, 2 - Автоинструкторы)
     * @param int  $count           Общее кол-во преподавателей в выборке
     *
     * @return Factory|View
     */
    public static function sortButtons(int $type, int $count)
    {
        if ($count > 0) {
            return view('components.helper.sort-buttons', [
                'type' => $type,
            ]);
        }

        if ($type == self::REPETITOR) {
            $title = 'Репетиторов не найдено';
            $type = 'репетитора';
        } else {
            $title = 'Автоинструктов не найдено';
            $type = 'автоинструктора';
        }

        return view('components.helper.register', [
            'title' => $title,
            'type' => $type,
        ]);
    }

    /**
     * Получение уникального кода для отправки через смс или почту
     *
     * @param string $str
     * @param bool   $convertToDigits
     *
     * @return string
     */
    public static function getUniqueCode(string $str, bool $convertToDigits = false)
    {
        $encoding = [
            0 => "w",
            1 => "u",
            2 => "t",
            3 => "j",
            4 => "x",
            5 => "n",
            6 => "s",
            7 => "k",
            8 => "q",
            9 => "p",
        ];

        if ($convertToDigits) {
            $encoding = array_flip($encoding);
        }

        $result = '';
        $length = mb_strlen($str);

        for ($i = 0; $i < $length; $i++) {
            $result .= $encoding[$str[$i]];
        }

        return $result;
    }

    /**
     * Функция обрезания строки до знаков препинания. Уменьшает текущую строку.
     *
     * @param string $text        Основной текст
     * @param int    $offsetIndex Индекс начальной обрезки
     * @param string $class       CSS класс добавляется к выводу
     *
     * @return Factory|View
     */
    public static function cutUpToSentence(
        string $text,
        int $offsetIndex,
        string $class = ''
    )
    {
        $symbols = ['.', '!', '?'];

        // Обрезаем текст с начала до указанного индекса
        $result = mb_substr($text, 0, $offsetIndex);

        // Установим начальную позицию
        $maxIndex = 0;

        // Перебираем все символы и находим максимальный индекс
        foreach ($symbols ?? [] as $symbol) {
            $index = mb_strrpos($result, $symbol);

            if ($index !== false) {
                $maxIndex = max($maxIndex, $index + 1);
            }
        }

        $snippet = '';

        // Если нашли знак препинания
        if ($maxIndex != 0) {
            $result = mb_substr($text, 0, $maxIndex);

            // Добавляем остаток к тексту
            $snippet = mb_substr($text, $maxIndex);
        } else {
            $result = $text;
        }

        return view('components.helper.cut-up-to-sentence', [
            'result' => $result,
            'snippet' => $snippet,
            'class' => $class,
        ]);
    }

    /**
     * Функция разрезания текста. Оборачивает все в div блоки и добавляет кнопку Читать полностью
     *
     * @param string $text        Основной текст
     * @param int    $offsetIndex Индекс обрезки текста
     * @param string $class       CSS класс добавляется к выводу
     *
     * @return Factory|View
     */
    public static function splitSentence(string $text, int $offsetIndex, string $class = '')
    {
        $result = mb_substr($text, 0, $offsetIndex);
        $snippet = trim(mb_substr($text, $offsetIndex));

        return view('components.helper.split-sentence', [
            'result' => $result,
            'snippet' => $snippet,
            'class' => $class,
        ]);
    }

    /**
     * Кнопка отзывы
     *
     * @param Users $repetitor Запись преподавателя из таблицы Users
     *
     * @return Factory|View
     */
    public static function reviewsButton(Users $repetitor)
    {
        if ($repetitor && ($repetitor->reviewsCount() > 0)) {
            return view('components.helper.reviews-button', [
                'id_users' => $repetitor->id_users,
                'count' => $repetitor->reviewsCount(),
            ]);
        }

        return null;
    }

    /**
     * Вывод станций метро в анкете преподавателя, если заполнен metro_list
     *
     * @param Users $repetitor Запись преподавателя из таблицы Users
     * @param int   $type      Тип страницы (2 - Страница каталога)
     *
     * @return array|string
     */
    public static function metroList(Users $repetitor, int $type)
    {
        if ($repetitor && !empty($repetitor->metro_list)) {
            $list = explode(',', $repetitor->metro_list);
            $metro = Metro::with('branch')->whereIn('id_metro', $list)->get();

            $result = [];

            foreach ($metro ?? [] as $item) {
                // Инициализация
                if (!isset($result[$item->id_metro_branches])) {
                    $result[$item->id_metro_branches]['count'] = 0;
                    $result[$item->id_metro_branches]['name'] = [];
                }

                if ($result[$item->id_metro_branches]['count'] < 3) {
                    $result[$item->id_metro_branches]['count']++;

                    if ($result[$item->id_metro_branches]['count'] != 3) {
                        // Название станции
                        $result[$item->id_metro_branches]['name'][] = $item->name;
                    } else {
                        // Если больше 3х станций, то в качестве имени используем название ветки
                        $result[$item->id_metro_branches]['name'] = [$item->branch->name];
                    }
                }
            }

            $flatten = Arr::flatten(array_column($result, 'name'));
            $result = implode(', ', $flatten);

            // Страница каталога
            if ($type == 2) {
                // Показываем максимально 6 станций
                if (count($flatten) > 6) {
                    $result = implode(', ', array_slice($flatten, 0, 6));
                    $result .= view('components.helper.metro-button', [
                        'id_users' => $repetitor->id_users,
                        'prefix' => ' и ',
                        'text' => 'другие районы',
                    ]);
                } else {
                    $result .= view('components.helper.metro-button', [
                        'id_users' => $repetitor->id_users,
                        'prefix' => ', ',
                        'text' => 'См. карту',
                    ]);
                }
            } else {
                $space = '';
                $class = '';

                if ($type == 2) {
                    $space = '&ensp;&ensp;';
                } else {
                    $class = 'catalog';
                }

                $result = view('components.helper.metro-button-extended', [
                    'class' => $class,
                    'space' => $space,
                    'result' => $result,
                    'id_users' => $repetitor->id_users,
                    'prefix' => ', ',
                    'text' => 'См. карту',
                ]);
            }

            return $result;
        }

        return '';
    }

    /**
     * @param int|null       $id_orders   Номер заказа
     * @param int|array|null $id_users    Преподаватель
     * @param int            $id_courses  Предмет
     * @param int            $id_cities   Город
     * @param int            $duration    Продолжительность занятия
     * @param float          $price_limit Желаемая стоимость от заказчика
     * @param int            $type        Тип заявки: 1 - одиночная заявка, 2 - заявка из закладок
     * @param int            $type_page   Тип страницы: 1 (добавление с сайта) / 2 (редактирование адм) / 3 (добавление адм) / 4 (добавление дубля адм)
     * @param array          $info
     *
     * @return array
     */
    public static function calculatePrice(?int $id_orders, $id_users, ?int $id_courses, int $id_cities, int $duration, ?float $price_limit, int $type = 1, int $type_page = 1, array $info = [])
    {
        // Сохранять информацию о расчете или нет
        $calculation = true;

        // ------------------------------------------

        // Значения по-умолчанию
        // Город Санкт-Петербург, меньше это регионы
        $multiplierCity = 1;

        // Коэффициент длительности занятия
        $multiplierDuration = 1;

        // Итоговый результат
        $result = [];

        // ------------------------------------------

        // TODO:       // Проверяем, распределена ли заявка репетитору
        //        if ($koef_post_page == 2) {
        //            if ($koef_array['id_user_old'] <> 2 AND $koef_array['id_user'] == 2 AND $koef_array['phone'] AND $koef_post_id_lern AND $koef_post_id_rep_l AND $koef_post_id) {
        //                $koef_array['phone'] = mysql_real_escape_string($koef_array['phone']);
        //
        //                $koef_query_post_dubl = "SELECT * FROM post WHERE phone = '{$koef_array['phone']}' AND id_lern = $koef_post_id_lern AND id_rep = '{$koef_post_id_rep_l}' AND id_post <> $koef_post_id AND id_user = 2 ORDER BY `id_post` ASC LIMIT 1";
        //
        //                $koef_result_post_dubl = mysql_query($koef_query_post_dubl);
        //                $koef_row_post_dubl = mysql_fetch_array($koef_result_post_dubl);
        //
        //                if ($koef_row_post_dubl['id_post']) { //если у репетитора уже есть похожие заявки
        //
        //                    if (!$koef_array['raspr']) //Если распределение со страницы редактирования заявки
        //                    {
        //                        $dop_info_edit_dubl = '<br><br><font style="color: red; font-size: 20px;">Внимание! Поля, которые вы изменяли, сбросились. ';
        //                    }
        //
        //                    exit('<div style="margin: 100px auto; max-width: 600px; width: 100%; text-align: center;"><font style="color: red; font-size: 20px;">Ошибка:</font><br>Этому репетитору уже распределена заявка от этого клиента(предмет, телефон).'.$dop_info_edit_dubl.'<br><br></font><a onclick="window.history.go(-1); return false;" href="javascript:void(0);">Вернуться назад!</a></div>');
        //                }
        //            }
        //        }
        //
        //        // только когда указан $koef_array
        //        // Проверяем открытые заявки на дубли, если они есть копируем utime, pprice, cenazakaza, koef_print_id
        //        if (!$koef_array['id_post_dubl_yes'] AND $koef_array['id_user_old'] != 1 AND $koef_array['id_user'] == 1 AND $koef_array['name'] AND $koef_array['phone'] AND $koef_post_id_lern) {
        //            $koef_array['name'] = mysql_real_escape_string($koef_array['name']);
        //            $koef_array['phone'] = mysql_real_escape_string($koef_array['phone']);
        //
        //            $koef_query_post_dubl = "SELECT * FROM post WHERE name = '{$koef_array['name']}' AND id_lern = $koef_post_id_lern AND phone = '{$koef_array['phone']}' AND id_user = 1 AND id_post <> {$koef_post_id} ORDER BY `id_post` ASC";
        //            $koef_result_post_dubl = mysql_query($koef_query_post_dubl);
        //
        //            while ($row = mysql_fetch_array($koef_result_post_dubl)) {
        //                $koef_row_post_dubl[] = $row['id_post'];
        //            }
        //
        //            if ($koef_row_post_dubl) { //если есть дубли копируем utime, pprice, cenazakaza, koef_print_id
        //                $_POST['id_user'] = $koef_array['id_user_old'];
        //
        //                $value['error']['dubl_post_id_user_1']['status'] = 1; //Добавляем ошибку дубля, похожие заявки уже есть в открытом доступе
        //                $value['error']['dubl_post_id_user_1']['utime'] = $koef_row_post_dubl['utime'];
        //                $value['error']['dubl_post_id_user_1']['pprice'] = $koef_row_post_dubl['pprice'];
        //                $value['error']['dubl_post_id_user_1']['cenazakaza'] = $koef_row_post_dubl['cenazakaza'];
        //                $value['error']['dubl_post_id_user_1']['id_post_dubl'] = $koef_row_post_dubl;
        //
        //                $value['error']['id_admin'] = $koef_array['id_admin'];
        //
        //                return $value;
        //            }
        //        }
        //
        //        // только когда указан $koef_array
        //        if ($koef_array['id_user_old'] >= 2 AND ($koef_array['id_user'] == 1 OR !$koef_array['id_user'])) {//Если статус заявки с >= 2 изменился на 1(открытые) или 0(новые), убираем список репетиторов
        //            $koef_post_id_rep_l = 0;
        //            $koef_open_or_new = 1;
        //        }

        // TODO:       if ($koef_post_page == 2) {//Если страница редактирования в админке, проверяем стоимость и длительность, выдаем ошибки
        //            if ($koef_array['id_lern_old'] AND (!$koef_post_id_lern OR $koef_post_id_lern == 9999)) {//Если старый предмет есть(т.е реп), а новый 0 или 9999, то заявка для реп
        //                $value['error'] = 1;
        //                $value['text'] .= '<br> - Заполните предмет, заявка для репетитора';
        //            }
        //
        //            if (!$koef_post_pprice) {//Если стоимость 0
        //                $value['error'] = 1;
        //                $value['text'] .= '<br> - Стоимость занятия должна быть больше 0';
        //            }
        //
        //            if (!$koef_post_utime) {//Если продолжительность занятия 0
        //                $value['error'] = 1;
        //                $value['text'] .= '<br> - Продолжительность занятия должна быть больше 0';
        //            }
        //
        //            if ($value['error']) {
        //                $value['text'] = '<div style="margin: 100px auto; max-width: 600px; width: 100%; text-align: center;"><font style="color: red; font-size: 20px;">Заполните обязательные поля:</font><br>'.$value['text'].'<br><br><font style="color: red; font-size: 20px;">Внимание! Поля, которые вы изменяли, сбросились. <br><br></font><a onclick="window.history.go(-1); return false;" href="javascript:void(0);">Вернуться назад!</a></div>';
        //
        //                exit($value['text']);
        //            }
        //        }

        // Проверка на ошибки (обязательные поля продолжительность занятия, желаемая стоимость и ID предмета при type = 1)
        if (!$duration || !$price_limit || ($type == 1 && !$id_courses)) {
            $result['error'] = 1;

            if ($type == 1) {
                $result['price'] = 0;
                $result['calculation'] = '';
            } else {
                //  TODO: foreach ($koef_post_id_rep_l as $val) {
                //           $value[$val]['cenazakaza'] = 0;
                //           $value[$val]['print_id'] = 0;
                //        }
            }

            return $result;
        }

        // TODO:       //Если заказ есть в базе, и если статус не изменился на 1(открытые) или 0(новые)
        //        if ($koef_post_id AND !$koef_open_or_new) {
        //            $koef_query_post_check = "SELECT * FROM post WHERE id_post = {$koef_post_id}";
        //            $koef_result_post_check = mysql_query($koef_query_post_check);
        //            $koef_row_post_check = mysql_fetch_array($koef_result_post_check);
        //
        //            //Проверяем изменились ли: стоимость, длительность, предмет, город, cenazakaza_type_post(Доп параметры: подготовка к экзамену и т.д)
        //            if ($koef_row_post_check['id_post'] AND $koef_row_post_check['pprice'] == $koef_post_pprice AND $koef_row_post_check['utime'] == $koef_post_utime AND $koef_row_post_check['id_lern'] == $koef_post_id_lern AND $koef_row_post_check['id_gorod'] == $koef_post_id_gorod AND $koef_row_post_check['cenazakaza_type_post'] == $koef_array['cenazakaza_type_post']) {
        //                $value['cenazakaza'] = $koef_row_post_check['cenazakaza'];
        //                $value['print_id'] = $koef_row_post_check['koef_print_id'];
        //
        //                return $value;
        //            }
        //        }

        // ------------------------------------------

        // Количество репетиторов по данному предмету, где id_users_active_statuses = 2 (Анкета видна в базе)
        $repetitorsActiveCount = Users::where([
                'id_users_active_statuses' => UsersActiveStatuses::ACTIVE,
                'is_deleted' => 0,
            ])
            ->where(function ($query) use ($id_courses) {
                $query->where(['id_courses' => $id_courses])
                    ->orWhere(['id_courses_second' => $id_courses])
                    ->orWhere(['id_courses_third' => $id_courses])
                    ->orWhere(['id_courses_fourth' => $id_courses]);
            })
            ->count();

        // Расчет коэффициента по указанному предмету и найденному кол-ву
        switch (true) {
            case $repetitorsActiveCount >= 50 && $repetitorsActiveCount < 100:
                $multiplierActiveUsers = 1.05;
                break;

            case $repetitorsActiveCount >= 100 && $repetitorsActiveCount < 150:
                $multiplierActiveUsers = 1.1;
                break;

            case $repetitorsActiveCount >= 150 && $repetitorsActiveCount < 200:
                $multiplierActiveUsers = 1.15;
                break;

            case $repetitorsActiveCount >= 200 && $repetitorsActiveCount < 500:
                $multiplierActiveUsers = 1.2;
                break;

            case $repetitorsActiveCount >= 500 && $repetitorsActiveCount < 1000:
                $multiplierActiveUsers = 1.25;
                break;

            case $repetitorsActiveCount >= 1000:
                $multiplierActiveUsers = 1.3;
                break;

            default:
                $multiplierActiveUsers = 1;
        }
        $repetitorsList = is_array($id_users) ? $id_users : [$id_users];

        // Если указан предмет, забираем коэффициент из базы
        if ($id_courses) {
            $course = Courses::where(['id_courses' => $id_courses])->first();

            if ($course) {
                $multiplierDuration = $course->multiplier_duration;
            }
        }

        // Если выбрана область, уменьшаем коэффициент города
        if ($id_cities > 1) {
            $multiplierCity = 0.86;
        }

        // Общий множитель для всей суммы
        $multiplier = Multipliers::orderBy('date', 'DESC')->first()->value('multiplier');

        // Дополнительная цена, которая зависит от типа заказа
        $multiplierByType = 1;

        // TODO:       //Доп параметры, которые зависят от cenazakaza_type_post
        //        $koef_dop_cenazakaza_type_post_2 = 1.00; //Поумолчанию
        //
        //        if ($koef_array['cenazakaza_type_post'] == 1) //Если подготовка к экзамену, цена заявки домножается на 1.16
        //        {
        //            $koef_dop_cenazakaza_type_post_2 = 1.16;
        //        } elseif ($koef_array['cenazakaza_type_post'] == 2) //Если Занятия с детьми, $koef_dlit = 1
        //        {
        //            $koef_dlit = 1;
        //        } elseif ($koef_array['cenazakaza_type_post'] == 3) //Если Занятия с 2 учениками
        //        {
        //            $koef_dop_cenazakaza_type_post_2 = 1.86;
        //        }

        // ------------------------------------------------

        // Расчет цены
        $price = $price_limit / $duration * 60;

        // Расчет второго множителя
        $multiplierSecond = round($multiplierDuration * $multiplierActiveUsers * $multiplierCity * $multiplier, 2);

        // Расчет индивидуального среднего коэффициента по текущему предмету
        // TODO: Проверить
        $multiplierAvg = Users::where(function ($query) use ($id_courses) {
                $query
                    ->where(['id_courses' => $id_courses])
                    ->orWhere(['id_courses_second' => $id_courses])
                    ->orWhere(['id_courses_third' => $id_courses])
                    ->orWhere(['id_courses_fourth' => $id_courses]);
            })
            ->where(['is_deleted' => 0])
            ->where('multiplier_personal', '>', 0)
            ->avg('multiplier_personal');

        // Расчет третьего множителя
        $multiplierThird = !$multiplierAvg ? $multiplierSecond : round(($multiplierSecond + $multiplierAvg) / 2, 2);

        if ($multiplierThird > 2) {
            $multiplierThird = 2;
        }

        $repetitorsListCount = count($repetitorsList);

        foreach ($repetitorsList as $id_user) {
            if ($repetitorsListCount == 1) {
                if ($repetitor = Users::where(['id_users' => $id_user])->first()) {
                    $result['price'] = round($multiplierThird * $price * $multiplierByType);

                    $result['calculation'] = $calculation ? view('components.helper.calculation', [
                        'id_courses' => $id_courses,
                        'id_users' => $id_users,
                        'id_cities' => $id_cities,
                        'duration' => $duration,
                        'price_limit' => $price_limit,
                        'multiplier' => $multiplier,
                        'multiplierCity' => $multiplierCity,
                        'multiplierDuration' => $multiplierDuration,
                        'multiplierActiveUsers' => $multiplierActiveUsers,
                        'multiplierAvg' => $multiplierAvg,
                        'price' => $price,
                        'multiplierSecond' => $multiplierSecond,
                        'multiplierThird' => $multiplierThird,
                        'totalPrice' => $result['price'],
                    ]) : '';
                }
            }
        }

        return $result;
    }

    /**
     * Избранное
     */
    public static function favourites()
    {
        $favouritesCount = Favourites::where(['id_favourites' => Cookie::get('fav')])->count();

        return view('components.helper.favourites', [
            'favouritesCount' => $favouritesCount
        ]);
    }

    public static function favouritesButton($id_users, $type, $action = null)
    {
        $id = Cookie::get('fav');
        $count = Favourites::where(['id_favourites' => $id, 'id_users' => $id_users])->count();

        return view('components.helper.favourites-button', [
            'id_users' => $id_users,
            'type' => $type,
            'action' => $action,
            'count' => $count
        ]);
    }

    public static function searchIndex()
    {
        $metroList = Metro::orderBy('name', 'ASC')->get();
        $citiesList = Cities::orderByRaw("FIELD(id_cities,'1') DESC, name ASC")->get();
        $searchCoursesList = Courses::whereIn('id_courses_types', [
                CoursesTypes::RARE_COURSES_3,
                CoursesTypes::RARE_COURSES_4
            ])
            ->orderBy('name', 'ASC')
            ->get();

        $coursesList = Courses::where(['id_courses_types' => CoursesTypes::NORMAL_POPULAR_COURSES])
            ->orderByRaw("FIELD(id_courses,?) ASC, id_courses_types DESC, name ASC", [Courses::OTHER_SUBJECTS])
            ->get();

        return view('components.helper.search-index', [
            'metroList' => $metroList,
            'citiesList' => $citiesList,
            'searchCoursesList' => $searchCoursesList,
            'coursesList' => $coursesList,
        ]);
    }

    /**
     * Боковое меню с фильтрами в поиске
     *
     * @param Courses|null $course
     * @param bool $is_car_instructor
     * @param array $filter
     *
     * @return Factory|View
     */
    public static function searchSidebar(Courses $course = null, bool $is_car_instructor = false, array $filter = [])
    {
        $metroList = Metro::orderBy('name', 'ASC')->get();
        $citiesList = Cities::orderByRaw("FIELD(id_cities,'1') DESC, name ASC")->get();

        if ($is_car_instructor || (!is_null($course) && in_array($course->id_courses, [
                Courses::AUTOMATIC_TRANSMISSION,
                Courses::MANUAL_TRANSMISSION
            ]))) {
                return view('components.helper.search-sidebar-car', [
                    'course' => $course,
                    'citiesList' => $citiesList,
                    'metroList' => $metroList
                ]);
            }

        $universitiesList = Universities::orderBy('name', 'asc')->get();
        $statusesList = UsersStatuses::all();
        $searchCoursesList = Courses::whereIn('id_courses_types', [
                CoursesTypes::RARE_COURSES_3,
                CoursesTypes::RARE_COURSES_4
            ])
            ->orderBy('name', 'ASC')
            ->get();

        $coursesList = Courses::where(['id_courses_types' => CoursesTypes::NORMAL_POPULAR_COURSES])
            ->orderByRaw("FIELD(id_courses,?) ASC, id_courses_types DESC, name ASC", [Courses::OTHER_SUBJECTS])
            ->get();

        // Определяем, есть ли подразделы
        $showSubCourses = false;

        if (!is_null($course)) {
            $showSubCourses = Courses::where(['id_parent_course' => $course->id_courses])->count() > 0 || !empty($course->id_parent_course);
        }

        return view('components.helper.search-sidebar', [
            'filter' => $filter,
            'course' => $course,
            'citiesList' => $citiesList,
            'metroList' => $metroList,
            'universitiesList' => $universitiesList,
            'statusesList' => $statusesList,
            'searchCoursesList' => $searchCoursesList,
            'coursesList' => $coursesList,
            'showSubCourses' => $showSubCourses
        ]);
    }

    /**
     * Используется только в разделе каталога репетиторов
     * Функция заполнения данными шаблона заголовков getCourseTitleDescription
     *
     * @param Model $template
     * @param Model $course
     * @param int|null $fromPage
     * @param int|null $toPage
     *
     * @return array
     */
    private static function fillTemplate(Model $template, Model $course, ?int $fromPage, ?int $toPage)
    {
        // Иконка
        $emoji = '';

        if (!$course->id_courses_second) {
            if (in_array($course->id_courses, [Courses::CHINESE, Courses::DRAWING])) {
                $emoji = '&#128216;';
            } else {
                if (in_array($course->id_courses, [
                        Courses::BOXING,
                        Courses::YOGA,
                        Courses::SWIMMING,
                    ]) || $course->id_courses_categories_second == CoursesCategoriesSecond::LANGUAGES) {
                    $emoji = '&#11088;';
                }
            }
        }

        // Уникальное имя
        $name = 'Репетиторы по ' . $course->name;

        if (in_array($course->id_courses_categories_second,
            [CoursesCategoriesSecond::SPORT_DISCIPLINES, CoursesCategoriesSecond::DANCE])) {
            $name = 'Тренеры по ' . $course->name;
        } else {
            if ($course->id_courses_categories_second == CoursesCategoriesSecond::LOGOPEDICS) {
                $name = 'Логопеды';
            }
        }

        $values = [
            '{NAME}' => $name,
            '{FROM}' => $fromPage,
            '{TO}' => $toPage,
            '{PARENT_RECOMMEND_NAME}' => $course->id_parent_course ? $course->parentCourse->recommend_name : '',
            '{PARENT_RECOMMEND_NAME_SECOND}' => $course->id_parent_course ? $course->parentCourse->recommend_name_second : '',
            '{RECOMMEND_NAME}' => $course->recommend_name,
            '{RECOMMEND_NAME_SECOND}' => $course->recommend_name_second,
            '{SEO_TITLE}' => ucfirst($course->seo ? $course->seo->title : ''),
            '{SEO_EMOJI}' => $course->seo ? $course->seo->emoji : '',
            '{EMOJI}' => $emoji,
        ];

        return [
            'title' => strtr($template->title, $values),
            'description' => strtr($template->description, $values),
        ];
    }

    /**
     * Используется только в разделе каталога репетиторов
     * Получение Title, Description для списка курсов
     *
     * @param Model $course
     * @param int $page
     * @param int|null $from
     * @param int|null $to
     *
     * @return string[]
     */
    public static function getCourseTitleDescription(Model $course, int $page, ?int $from, ?int $to)
    {
        if ($course->id_parent_course && ($template = TitleTemplates::where([
                'id_title_categories' => $course->id_title_categories,
                'page' => $page,
                'id_parent_course' => $course->id_parent_course,
                'id_courses' => $course->id_courses,
            ])->first())) {
            return self::fillTemplate($template, $course, $from, $to);
        } else {
            if ($course->id_parent_course && ($template = TitleTemplates::where([
                    'id_title_categories' => $course->id_title_categories,
                    'page' => $page,
                    'id_parent_course' => $course->id_parent_course,
                    'id_courses_types' => $course->id_courses_types,
                ])->first())) {
                return self::fillTemplate($template, $course, $from, $to);
            } else {
                if ($course->id_parent_course && ($template = TitleTemplates::where([
                        'id_title_categories' => $course->id_title_categories,
                        'page' => $page,
                        'id_parent_course' => $course->id_parent_course,
                    ])->first())) {
                    return self::fillTemplate($template, $course, $from, $to);
                } else {
                    if ($template = TitleTemplates::where([
                            'id_title_categories' => $course->id_title_categories,
                            'page' => $page,
                            'id_courses_types' => $course->id_courses_types,
                        ])->first()) {
                        return self::fillTemplate($template, $course, $from, $to);
                    } else {
                        if ($template = TitleTemplates::where([
                                'id_title_categories' => $course->id_title_categories,
                                'page' => $page,
                                'id_courses' => $course->id_courses,
                            ])->first()) {
                            return self::fillTemplate($template, $course, $from, $to);
                        } else {
                            if ($template = TitleTemplates::where([
                                    'id_title_categories' => $course->id_title_categories,
                                    'id_courses' => null,
                                    'page' => $page,
                                ])->first()) {
                                return self::fillTemplate($template, $course, $from, $to);
                            } else {
                                if ($template = TitleTemplates::where([
                                        'id_title_categories' => $course->id_title_categories,
                                        'page' => $page,
                                        'id_courses_categories_second' => $course->id_courses_categories_second,
                                    ])->first()) {
                                    return self::fillTemplate($template, $course, $from, $to);
                                } else {
                                    if ($template = TitleTemplates::where([
                                            'id_title_categories' => $course->id_title_categories,
                                            'page' => $page,
                                            'id_courses_categories' => $course->id_courses_categories,
                                        ])->first()) {
                                        return self::fillTemplate($template, $course, $from, $to);
                                    } else {
                                        if ($page > 1 && ($template = TitleTemplates::where([
                                                'id_title_categories' => $course->id_title_categories,
                                                'page' => 0,
                                            ])->first())) {
                                            return self::fillTemplate($template, $course, $from, $to);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return [
            'title' => '',
            'description' => '',
        ];
    }

    /**
     * Используется только в разделе каталога репетиторов
     * Заполнение шаблона "Всего репетиторов на странице"
     *
     * @param Model $template
     * @param Model $course
     * @param int   $count
     *
     * @return string
     */
    private static function fillTotalTemplate(Model $template, Model $course, int $count)
    {
        if (in_array($course->id_courses_categories,
            [CoursesCategories::SPORT_DISCIPLINES, CoursesCategories::DANCE])) {
            if (in_array($course->id_courses, [Courses::YOGA, Courses::SKIES, Courses::SNOWBOARD])) {
                $text = 'инструкторов по ' . $course->recommend_name . ' в Санкт-Петербурге';
            } else {
                $text = 'тренеров по ' . $course->recommend_name . ' в Санкт-Петербурге';
            }
        } else {
            $text = 'репетиторов по ' . $course->recommend_name . ' в Санкт-Петербурге';
        }

        $values = [
            '{TEXT}' => $text,
            '{TOTAL_COUNT}' => $count,
            '{SEO_TOTAL_COUNT}' => $course->seo ? $course->seo->total_count : 'Всего: ',
            '{RECOMMEND_NAME}' => $course->recommend_name,
        ];

        return strtr($template->template, $values);
    }

    /**
     * Используется только в разделе каталога репетиторов
     * Получение шаблона для "Всего репетиторов на странице" на странице каталогов репетиторов
     *
     * @param Model $course
     * @param int   $page
     * @param int   $count
     *
     * @return string
     */
    public static function getCourseTotalText(Model $course, int $page, int $count)
    {
        if ($template = TotalTemplates::where([
            'id_total_categories' => $course->id_total_categories,
            'page' => $page,
        ])->first()) {
            return self::fillTotalTemplate($template, $course, $count);
        } elseif ($template = TotalTemplates::where([
            'id_total_categories' => $course->id_total_categories,
            'page' => 0,
        ])->first()) {
            return self::fillTotalTemplate($template, $course, $count);
        }

        return '';
    }

    /**
     * Используется только в разделе каталога репетиторов
     * Заполнение шаблона для текста после "Всего репетиторов на странице"
     * TODO: Возможно надо будет добавить 4 параметр это закрытие тега P
     *
     * @param Model $template
     * @param Model $course
     *
     * @return string
     */
    private static function fillPrependTemplate(Model $template, Model $course)
    {
        $values = [
            '{RECOMMEND_NAME}' => $course->recommend_name,
            '{RECOMMEND_NAME_SECOND}' => $course->recommend_name_second,
            '{RECOMMEND_NAME_SEVENTH}' => $course->recommend_name_seventh,
            '{RECOMMEND_NAME_NINETH}' => $course->recommend_name_nineth,
            '{SEO_PREPEND_TEXT}' => $course->seo
                ? view('components.helper.prepend-text', ['text' => $course->seo->prepend_text])
                : '',
        ];

        if (empty($course->recommend_name_nineth)) {
            switch ($course->id_courses_categories) {
                case CoursesCategories::GENERAL:
                case CoursesCategories::OTHER_SUBJECTS:
                    $values['{RECOMMEND_NAME_NINETH}'] = 'репетитора по ' . $course->recommend_name;
                    break;

                case CoursesCategories::LANGUAGES:
                    $values['{RECOMMEND_NAME_NINETH}'] = 'репетитора ' . $course->recommend_name_second;
                    break;

                case CoursesCategories::SPORT_DISCIPLINES:
                case CoursesCategories::DANCE:
                    $values['{RECOMMEND_NAME_NINETH}'] = 'тренера по ' . $course->recommend_name;
                    break;

                default:
                    $values['{RECOMMEND_NAME_NINETH}'] = 'репетитора';
            }
        }

        $text = strtr($template->template, $values);

        if ($template->text_limit) {
            return $template->split
                ? self::splitSentence($text, $template->text_limit, $template->class)
                : self::cutUpToSentence($text, $template->text_limit, $template->class);
        }

        // Установлен флаг, то оборачивается текст в шаблон prepend-text
        if ($template->wrap) {
            return view('components.helper.prepend-text', ['text' => $text]);
        }

        return $text;
    }

    /**
     * Используется только в разделе каталога репетиторов
     * Получение шаблона для текста после "Всего репетиторов на странице" на странице каталогов репетиторов
     *
     * @param Model $course
     * @param int   $page
     *
     * @return string
     */
    public static function getPrependText(Model $course, int $page)
    {
        if ($template = PrependTextTemplates::where([
            'id_prepend_text_categories' => $course->id_prepend_text_categories,
            'page' => $page,
        ])->first()) {
            return self::fillPrependTemplate($template, $course);
        } elseif ($template = PrependTextTemplates::where([
            'id_prepend_text_categories' => $course->id_prepend_text_categories,
            'page' => 0,
        ])->first()) {
            return self::fillPrependTemplate($template, $course);
        }

        return '';
    }

    /**
     * Используется только в разделе каталога репетиторов
     * Заполнение шаблона текста перед комментарием
     * {COURSE_SECOND_RECOMMEND_NAME}
     * {COURSE_SECOND_RECOMMEND_NAME_SECOND}
     * заполняются прямо при выдаче, иначе невозможно проверить, есть ли второй предмет у преподавателя
     *
     * @param Collection $templates
     * @param Model      $course
     *
     * @return array
     */
    private static function fillCommentsPrependTemplates(Collection $templates, Model $course)
    {
        $result = [];

        $values = [
            '{RECOMMEND_NAME}' => $course->recommend_name,
            '{RECOMMEND_NAME_SECOND}' => $course->recommend_name_second,
            '{RECOMMEND_NAME_THIRD}' => $course->recommend_name_third,
        ];

        foreach ($templates as $template) {
            $result[$template->row_index] = strtr($template->template, $values);
        }

        return $result;
    }

    /**
     * Используется только в разделе каталога репетиторов
     * Шаблон текста перед комментарием
     *
     * @param Model $course
     * @param int   $page
     *
     * @return array
     */
    public static function getCommentsPrependText(Model $course, int $page)
    {
        if (count($templates = CommentsPrependTextTemplates::where([
                    'id_comments_prepend_text_categories' => $course->id_comments_prepend_text_categories,
                    'page' => $page,
                ])
                ->where(function ($query) use ($course) {
                    $query
                        ->whereRaw('NOT FIND_IN_SET(?, exclude_courses)', [$course->id_courses])
                        ->orWhereNull('exclude_courses');
                })
                ->where(function ($query) use ($course) {
                    $query
                        ->where(['id_courses' => $course->id_courses])
                        ->orWhereNull('id_courses');
                })
                ->get()) > 0) {
            return self::fillCommentsPrependTemplates($templates, $course);
        }

        return [];
    }

    /**
     * Используется только в разделе каталога репетиторов
     * Заполнение шаблона для alt текста в фотографии преподавателя
     * {FIRST_NAME}
     * {MIDDLE_NAME}
     * Меняем прямо в шаблоне, так как выводится текст под конкретного преподавателя
     *
     * @param Collection $templates
     * @param Model      $course
     *
     * @return array
     */
    private static function fillPhotoAltTextTemplates(Collection $templates, Model $course)
    {
        $result = [];

        $values = [
            '{RECOMMEND_NAME}' => $course->recommend_name,
            '{RECOMMEND_NAME_SECOND}' => $course->recommend_name_second,
        ];

        foreach ($templates as $template) {
            $result[$template->row_index] = strtr($template->template, $values);
        }

        return $result;
    }

    /**
     * Используется только в разделе каталога репетиторов
     * Шаблон для alt текста в фотографии репетитора
     *
     * @param Model $course
     * @param int   $page
     *
     * @return array
     */
    public static function getPhotoAltText(Model $course, int $page)
    {
        if (count($templates = PhotoTextTemplates::where([
                'id_photo_text_categories' => $course->id_photo_text_categories,
                'id_courses' => $course->id_courses,
                'page' => $page,
            ])->get()) > 0) {
            return self::fillPhotoAltTextTemplates($templates, $course);
        } elseif (count($templates = PhotoTextTemplates::where([
                'id_photo_text_categories' => $course->id_photo_text_categories,
                'id_courses_categories_second' => $course->id_courses_categories_second,
                'page' => $page,
            ])->get()) > 0) {
            return self::fillPhotoAltTextTemplates($templates, $course);
        }

        return [];
    }

    /**
     * Используется только в разделе каталога репетиторов
     * Функция подготовки текста для размещения с лимитами, добавлением классов и тд
     *
     * @param Model $template        Основной шаблон
     * @param Model $course          Основной предмет
     * @param int   $repetitorsCount Количество репетиторов
     *
     * @return mixed
     */
    private static function preparePagesAppendTextTemplate(Model $template, Model $course, int $repetitorsCount)
    {
        $text = $template->template;

        if ($template->seo && !empty($course->seo->text)) {
            $text = $course->seo->text;
        } elseif ($template->repetitors_count_limit > 0 && $repetitorsCount > $template->repetitors_count_limit) {
            $text = '';
        }

        $values = [
            '{RECOMMEND_NAME}' => $course->recommend_name,
            '{RECOMMEND_NAME_SECOND}' => $course->recommend_name_second,
            '{RECOMMEND_NAME_THIRD}' => $course->recommend_name_third,
            '{RECOMMEND_NAME_EIGHTH}' => $course->recommend_name_eighth,
        ];

        $text = strtr($text, $values);

        if ($template->text_limit) {
            if ($template->split) {
                return self::splitSentence($text, $template->text_limit, $template->class);
            }

            return self::cutUpToSentence($text, $template->text_limit, $template->class);
        }

        return view('components.helper.pages-append-text', ['text' => $text]);
    }

    /**
     * Используется только в разделе каталога репетиторов
     * Функция добавления текста после пагинации
     *
     * @param Model $course          Основной предмет
     * @param int   $page            Номер страницы
     * @param int   $repetitorsCount Количество репетиторов
     *
     * @return string
     */
    public static function getPagesAppendText(Model $course, int $page, int $repetitorsCount)
    {
        if ($template = PagesAppendTextTemplates::where([
            'id_pages_append_text_categories' => $course->id_pages_append_text_categories,
            'id_courses_categories' => $course->id_courses_categories,
            'page' => $page,
        ])->first()) {
            return self::preparePagesAppendTextTemplate($template, $course, $repetitorsCount);
        } elseif ($template = PagesAppendTextTemplates::where([
            'id_pages_append_text_categories' => $course->id_pages_append_text_categories,
            'id_courses' => $course->id_courses,
            'page' => $page,
        ])->first()) {
            return self::preparePagesAppendTextTemplate($template, $course, $repetitorsCount);
        } elseif ($template = PagesAppendTextTemplates::where([
            'id_pages_append_text_categories' => $course->id_pages_append_text_categories,
            'page' => $page,
        ])->first()) {
            return self::preparePagesAppendTextTemplate($template, $course, $repetitorsCount);
        }

        return '';
    }
}
