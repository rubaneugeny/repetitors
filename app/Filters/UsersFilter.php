<?php

namespace App\Filters;

use App\Models\Cities;
use App\Models\Courses;
use App\Models\Reviews;
use Illuminate\Http\Request;

class UsersFilter
{
    protected $builder;
    protected $request;
    protected $direction;
    protected $courses = [
        Courses::AUTOMATIC_TRANSMISSION,
        Courses::MANUAL_TRANSMISSION,
        //Courses::MOTORBIKES
    ];

    public function __construct($builder, Request $request)
    {
        $this->builder = $builder;
        $this->request = $request;
        $this->direction = $this->request->get('sort_type', 'desc') == 'asc' ? 'ASC' : 'DESC';
    }

    public function apply(bool $set_default_transmission = false)
    {
        foreach ($this->filters() as $filter => $value) {
            if (method_exists($this, $filter)) {
                $this->$filter($value);
            }
        }

        // Установка типа транспортного средства по-умолчанию для фильтра transmission в разделе автоинструкторы
        if ($set_default_transmission) {
            $this
                ->builder
                ->where(function ($query) {
                    $query
                        ->whereIn('u.id_courses', $this->courses)
                        ->orWhereIn('u.id_courses_second', $this->courses)
                        ->orWhereIn('u.id_courses_third', $this->courses)
                        ->orWhereIn('u.id_courses_fourth', $this->courses);
                });
        }

        if (!$this->request->has('sort')) {
            $this->defaultSort();
        }

        return $this->builder;
    }

    // Сортировка по-умолчанию
    public function defaultSort()
    {
        $this
            ->builder
            ->selectRaw('u.*')
            ->orderBy('u.rating', 'DESC');
    }

    // Сортировка
    public function sort($value)
    {
        switch ($value) {
            case 'price':
                $this
                    ->builder
                    ->selectRaw('u.*')
                    ->orderBy('u.price_per_hour', $this->direction);
                break;

            case 'comment':
                $table = app(Reviews::class)->getTable();

                $this
                    ->builder
                    ->selectRaw('u.*, COUNT(r.id_reviews) AS reviews_count')
                    ->leftJoin("{$table} AS r", 'u.id_users', '=', 'r.id_users')
                    ->where(function ($query) {
                        $query
                            ->where(['r.is_deleted' => 0])
                            ->orWhereNull('r.id_reviews');
                    })
                    ->groupBy('u.id_users')
                    ->orderBy('reviews_count', $this->direction);
                break;

            default:
                $this
                    ->builder
                    ->selectRaw('u.*')
                    ->orderBy('u.rating', $this->direction);
        }
    }

    // Фильтр по предметам
    public function courses($values)
    {
        foreach ($values ?? [] as $value) {
            $this
                ->builder
                ->where(function ($query) use ($value) {
                    $query
                        ->where('id_courses', $value)
                        ->orWhere('id_courses_second', $value)
                        ->orWhere('id_courses_third', $value)
                        ->orWhere('id_courses_fourth', $value);
                });
        }
    }

    // Фильтр по городам
    public function cities($value)
    {
        $this
            ->builder
            ->when(in_array(Cities::SAINTS_PETERSBURG, $value), function ($query) {
                $query->whereNotNull('u.id_metro');
            })
            ->when(!in_array(Cities::SAINTS_PETERSBURG, $value), function ($query) use ($value) {
                $query
                    ->where(function ($query) use ($value) {
                        $query
                            ->whereIn('u.id_cities', $value)
                            ->orWhereIn('u.id_cities_second', $value)
                            ->orWhereIn('u.id_cities_third', $value)
                            ->orWhereIn('u.id_cities_fourth', $value);
                    });
            });

    }

    // Фильтр по метро
    public function metro($value)
    {
        $this
            ->builder
            ->where(function ($query) use ($value) {
                $query
                    ->whereIn('u.id_metro', $value)
                    ->orWhereIn('u.id_metro_second', $value);
            });
    }

    // Фильтр по цене
    public function price($value)
    {
        $this
            ->builder
            ->where(function ($query) use ($value) {
                foreach ($value as $price) {
                    switch ($price) {
                        case '<500':
                            $query->orWhere('u.price_per_hour', '<', 500);
                            break;

                        case '500-750':
                            $query->orWhere(function ($query) {
                                $query
                                    ->where('u.price_per_hour', '>=', 500)
                                    ->where('u.price_per_hour', '<=', 750);
                            });
                            break;

                        case '750-1000':
                            $query->orWhere(function ($query) {
                                $query
                                    ->where('u.price_per_hour', '>=', 750)
                                    ->where('u.price_per_hour', '<=', 1000);
                            });
                            break;

                        case '1000-1500':
                            $query->orWhere(function ($query) {
                                $query
                                    ->where('u.price_per_hour', '>=', 1000)
                                    ->where('u.price_per_hour', '<=', 1500);
                            });
                            break;

                        case '>1500':
                            $query->orWhere('u.price_per_hour', '>', 1500);
                            break;
                    }
                }
            });
    }

    // Фильтр по Вузу
    public function universities($value)
    {
        $this
            ->builder
            ->whereIn('u.id_universities', $value);
    }

    // Фильтр по статусам преподавателя
    public function statuses($value)
    {
        $this
            ->builder
            ->whereIn('u.id_users_statuses', $value);
    }

    // Фильтр по местам
    public function places($value)
    {
        if (in_array('students-home', $value)) {
            $this
                ->builder
                ->where('u.students_home', 1);
        }

        if (in_array('teachers-home', $value)) {
            $this
                ->builder
                ->where('u.teachers_home', 1);
        }
    }

    // Фильтр по носителю языка
    public function native($value)
    {
        switch ($value) {
            case 'y':
                $this
                    ->builder
                    ->whereNotNull('u.id_courses_native');
                break;

            case 'n':
                $this
                    ->builder
                    ->whereNull('u.id_courses_native');
                break;
        }
    }

    // Фильтр по дистанционным занятиям
    public function distance($value)
    {
        switch ($value) {
            case 'y':
                $this
                    ->builder
                    ->where('u.skype', 1);
                break;

            case 'n':
                $this
                    ->builder
                    ->where('u.skype', 0);
                break;
        }
    }

    // Фильтр по типу транспортного средства
    public function transmission($value)
    {
        switch ($value) {
            case 'a':
                $this->courses = [Courses::AUTOMATIC_TRANSMISSION];
                break;

            case 'm':
                $this->courses = [Courses::MANUAL_TRANSMISSION];
                break;

            case 'c':
                $this->courses = [Courses::MOTORBIKES];
                break;
        }
    }

    // Фильтр по наличию отзывов
    public function reviews($value)
    {
        switch ($value) {
            case 'y':
                $this
                    ->builder
                    ->whereRaw('EXISTS (SELECT 1 FROM `' . app(Reviews::class)->getTable() . '` r WHERE u.`id_users` = r.`id_users`)');
                break;

            case 'n':
                $this
                    ->builder
                    ->whereRaw('NOT EXISTS (SELECT 1 FROM `' . app(Reviews::class)->getTable() . '` r WHERE u.`id_users` = r.`id_users`)');
                break;
        }
    }

    // Фильтр по полу
    public function sex($value)
    {
        if (in_array($value, ['m', 'w'])) {
            $isMan = $value == 'm' ? 0 : 1;
            $this->builder->where(['u.sex' => $isMan]);
        }
    }

    // Список всех фильтров
    public function filters()
    {
        return $this->request->all();
    }
}
