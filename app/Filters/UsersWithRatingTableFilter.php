<?php

namespace App\Filters;

use App\Models\Reviews;

class UsersWithRatingTableFilter extends UsersFilter
{
    public function defaultSort()
    {
        $this
            ->builder
            ->selectRaw('u.*, rt.rating AS ratings_rating')
            ->orderBy('ratings_rating', 'DESC')
            ->orderBy('u.rating', 'DESC')
            ->groupBy('u.id_users');
    }

    public function sort($value)
    {
        switch ($value) {
            case 'price':
                $this
                    ->builder
                    ->selectRaw('u.*, rt.rating AS ratings_rating')
                    ->groupBy('u.id_users')
                    ->orderBy('u.price_per_hour', $this->direction);
                break;

            case 'comment':
                $table = app(Reviews::class)->getTable();

                $this
                    ->builder
                    ->leftJoin("{$table} AS r", 'u.id_users', '=', 'r.id_users')
                    ->selectRaw('u.*, rt.rating AS ratings_rating, COUNT(r.id_reviews) AS reviews_count')
                    ->where(function ($query) {
                        $query
                            ->where(['r.is_deleted' => 0])
                            ->orWhereNull('r.id_reviews');
                    })
                    ->groupBy('u.id_users')
                    ->orderBy('reviews_count', $this->direction);
                break;

            default:
                $this
                    ->builder
                    ->selectRaw('u.*, rt.rating AS ratings_rating')
                    ->orderBy('ratings_rating', $this->direction)
                    ->orderBy('u.rating', $this->direction)
                    ->groupBy('u.id_users');
        }
    }

}
