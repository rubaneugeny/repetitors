<?php

namespace App\Mail;

use App\Models\Users;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PasswordRecovery extends Mailable
{
    use Queueable, SerializesModels;

    public $repetitor;

    /**
     * Create a new message instance.
     *
     * @param Users $repetitor
     */
    public function __construct(\Illuminate\Database\Eloquent\Model $repetitor)
    {
        $this->repetitor = $repetitor;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject('Восстановление пароля СПбРепетитор')
            ->view('emails.recovery-password');
    }
}
