<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * PhotoTextTemplates
 *
 * @mixin Eloquent
 */
class PhotoTextTemplates extends Model
{
    protected $table = 'photo_text_templates';
    protected $primaryKey = 'id_photo_text_templates';
    public $timestamps = false;
}
