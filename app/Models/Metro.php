<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * Metro
 *
 * @mixin Eloquent
 */
class Metro extends Model
{
    protected $table = 'metro';
    protected $primaryKey = 'id_metro';

    public function branch()
    {
        return $this->hasOne('App\Models\MetroBranches', 'id_metro_branches', 'id_metro_branches');
    }
}
