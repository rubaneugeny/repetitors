<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * AdminsReviews
 *
 * @mixin Eloquent
 */
class Admins extends Model
{
    protected $table = 'admins';
    protected $primaryKey = 'id_admins';
    protected $attributes = [
        'phone' => null,
        'email' => null
    ];
}
