<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * UsersScheduleTime
 *
 * @mixin Eloquent
 */
class UsersScheduleTime extends Model
{
    protected $table = 'users_schedule_time';
    protected $primaryKey = 'id_users_schedule_time';
    public $timestamps = false;
}
