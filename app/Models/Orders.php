<?php

namespace App\Models;

use Carbon\Carbon;
use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * Orders
 *
 * @mixin Eloquent
 */
class Orders extends Model
{
    protected $table = 'orders';
    protected $primaryKey = 'id_orders';
    protected $fillable = [
        'id_users',
        'name',
        'phone',
        'email',
        'id_courses',
        'object',
        'id_orders_locations',
        'id_cities',
        'id_metro',
        'price_limit',
        'duration',
        'additional_information'
    ];
    protected $attributes = [
        'id_orders_statuses' => OrdersStatuses::NEW,
        'id_orders_admin_statuses' => OrdersAdminStatuses::NEW
    ];

    public function user()
    {
        return $this->hasOne('App\Models\Users', 'id_users', 'id_users');
    }

    public function course()
    {
        return $this->hasOne('App\Models\Courses', 'id_courses', 'id_courses');
    }

    public function review()
    {
        return $this->hasOne('App\Models\Reviews', 'id_orders', 'id_orders');
    }

    public function save(array $options = [])
    {
        if (!$this->exists) {
            $this->date = Carbon::now();
            $this->id_orders_statuses = OrdersStatuses::NEW;
        }

        return parent::save($options);
    }
}
