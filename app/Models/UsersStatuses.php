<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * UsersStatuses
 *
 * @mixin Eloquent
 */
class UsersStatuses extends Model
{
    const STUDENT = 3;

    protected $table = 'users_statuses';
    protected $primaryKey = 'id_users_statuses';
    public $timestamps = false;
}
