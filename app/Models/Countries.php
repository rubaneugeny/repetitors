<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * Countries
 *
 * @mixin Eloquent
 */
class Countries extends Model
{
    protected $table = 'countries';
    protected $primaryKey = 'id_countries';
    public $timestamps = false;

    public function cities()
    {
        return $this->hasMany('App\Models\CountriesCities', 'id_countries', 'id_countries');
    }
}
