<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * OrdersLocations
 *
 * @mixin Eloquent
 */
class OrdersLocations extends Model
{
    protected $table = 'orders_locations';
    protected $primaryKey = 'id_orders_locations';
    public $timestamps = false;
}
