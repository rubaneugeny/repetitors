<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * CommentsAppendTextTemplates
 *
 * @mixin Eloquent
 */
class CommentsAppendTextTemplates extends Model
{
    protected $table = 'comments_append_text_templates';
    protected $primaryKey = 'id_comments_append_text_templates';
    public $timestamps = false;
}
