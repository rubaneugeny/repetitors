<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * TotalCategories
 *
 * @mixin Eloquent
 */
class TotalCategories extends Model
{
    protected $table = 'total_categories';
    protected $primaryKey = 'id_total_categories';
    public $timestamps = false;
    protected $fillable = ['name'];
}
