<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * HeadersTemplates
 *
 * @mixin Eloquent
 */
class HeadersTemplates extends Model
{
    protected $table = 'headers_templates';
    protected $primaryKey = 'id_headers_templates';
    public $timestamps = false;
    protected $fillable = [
        'id_headers_categories',
        'id_courses_categories_second',
        'id_courses',
        'template',
        'page'
    ];
}
