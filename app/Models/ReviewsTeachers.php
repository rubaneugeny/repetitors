<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * ReviewsTeachers
 *
 * @mixin Eloquent
 */
class ReviewsTeachers extends Model
{
    protected $table = 'reviews_teachers';
    protected $primaryKey = 'id_reviews_teachers';
    public $timestamps = false;

    public function user()
    {
        return $this->hasOne('App\Models\Users', 'id_users', 'id_users');
    }
}
