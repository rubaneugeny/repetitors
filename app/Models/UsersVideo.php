<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * UsersVideo
 *
 * @mixin Eloquent
 */
class UsersVideo extends Model
{
    protected $table = 'users_video';
    protected $primaryKey = 'id_users_video';
    public $timestamps = false;

    public function getVideoId()
    {
        if (preg_match('#/embed/([^?]+)#', $this->video, $matches)) {
            return $matches[1];
        }

        return false;
    }
}
