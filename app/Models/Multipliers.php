<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * MetroBranches
 *
 * @mixin Eloquent
 */
class Multipliers extends Model
{
    protected $table = 'multipliers';
    protected $primaryKey = 'id_multipliers';
    public $timestamps = false;
}
