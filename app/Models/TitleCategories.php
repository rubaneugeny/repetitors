<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * TitleCategories
 *
 * @mixin Eloquent
 */
class TitleCategories extends Model
{
    protected $table = 'title_categories';
    protected $primaryKey = 'id_title_categories';
    protected $fillable = ['name'];
    public $timestamps = false;
}
