<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * Districts
 *
 * @mixin Eloquent
 */
class Favourites extends Model
{
    protected $table = 'favourites';
    protected $primaryKey = 'id_favourites';
    protected $fillable = ['id_favourites', 'id_users'];
}
