<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * CommentsPrependTextTemplates
 *
 * @mixin Eloquent
 */
class CommentsPrependTextTemplates extends Model
{
    protected $table = 'comments_prepend_text_templates';
    protected $primaryKey = 'id_comments_prepend_text_templates';
    public $timestamps = false;
    protected $attributes = [
        'id_courses' => null,
        'exclude_courses' => null
    ];
}
