<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * PhotoTextCategories
 *
 * @mixin Eloquent
 */
class PhotoTextCategories extends Model
{
    protected $table = 'photo_text_categories';
    protected $primaryKey = 'id_photo_text_categories';
    public $timestamps = false;
}
