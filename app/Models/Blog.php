<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * Blog
 *
 * @mixin Eloquent
 */
class Blog extends Model
{
    const TYPE_INTERNATIONAL_EXAM = 1;
    const TYPE_WHEN_YOU_NEED_REPETITOR = 2;

    protected $table = 'blog';
    protected $primaryKey = 'id_blog';
    protected $attributes = [
        'photo' => null,
        'description' => null,
    ];
}
