<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * Phonebook
 *
 * @mixin Eloquent
 */
class Phonebook extends Model
{
    protected $table = 'phonebook';
    protected $primaryKey = 'id_phonebook';
    protected $attributes = [
        'last_name' => null,
        'first_name' => null,
        'middle_name' => null,
        'email' => null
    ];
}
