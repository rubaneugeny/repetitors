<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * Cars Manufactures
 *
 * @mixin Eloquent
 */
class CarsManufactures extends Model
{
    protected $table = 'cars_manufactures';
    protected $primaryKey = 'id_cars_manufactures';
}
