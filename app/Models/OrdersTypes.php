<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * OrdersTypes
 *
 * @mixin Eloquent
 */
class OrdersTypes extends Model
{
    protected $primaryKey = 'id_orders_types';
    protected $table = 'orders_types';
    public $timestamps = false;
}
