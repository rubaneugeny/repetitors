<?php

namespace App\Models;

use App\Traits\HasCompositePrimaryKey;
use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * Ratings
 *
 * @mixin Eloquent
 */
class Ratings extends Model
{
    use HasCompositePrimaryKey;

    protected $table = 'ratings';
    protected $primaryKey = ['id_users', 'id_courses'];
    public $incrementing = false;
    public $timestamps = false;
}
