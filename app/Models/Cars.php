<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * Cars
 *
 * @mixin Eloquent
 */
class Cars extends Model
{
    protected $table = 'cars';
    protected $primaryKey = 'id_cars';

    public function manufacturer()
    {
        return $this->hasOne('App\Models\CarsManufactures', 'id_cars_manufactures', 'id_cars_manufactures');
    }
}
