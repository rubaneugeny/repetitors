<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * ReviewsStatuses
 *
 * @mixin Eloquent
 */
class ReviewsStatuses extends Model
{
    const NEW = 1;
    const APPROVED = 2;
    const DECLINED = 2;

    protected $table = 'reviews_statuses';
    protected $primaryKey = 'id_reviews_statuses';
    public $timestamps = false;
}
