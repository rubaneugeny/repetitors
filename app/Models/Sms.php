<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * Sms
 *
 * @mixin Eloquent
 */
class Sms extends Model
{
    protected $table = 'sms';
    protected $primaryKey = 'id_sms';
}
