<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * PrependTextCategories
 *
 * @mixin Eloquent
 */
class PrependTextCategories extends Model
{
    protected $table = 'prepend_text_categories';
    protected $primaryKey = 'id_prepend_text_categories';
    protected $fillable = ['name'];
    public $timestamps = false;
}
