<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * LinksCategories
 *
 * @mixin Eloquent
 */
class LinksCategories extends Model
{
    protected $table = 'links_categories';
    protected $primaryKey = 'id_links_categories';
    public $timestamps = false;
}
