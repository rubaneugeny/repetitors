<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * CoursesCategoriesSecond
 *
 * @mixin Eloquent
 */
class CoursesCategoriesSecond extends Model
{
    const GENERAL = 1;
    const LANGUAGES = 2;
    const MUSIC = 3;
    const OTHER_SUBJECTS = 4;
    const SPORT_DISCIPLINES = 5;
    const CHILDREN = 6;
    const DANCE = 7;
    const LOGOPEDICS = 8;
    const CAR_INSTRUCTORS = 9;

    protected $table = 'courses_categories_second';
    protected $primaryKey = 'id_courses_categories_second';
    public $timestamps = false;
}
