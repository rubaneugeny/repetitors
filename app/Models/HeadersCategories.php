<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * HeadersCategories
 *
 * @mixin Eloquent
 */
class HeadersCategories extends Model
{
    protected $table = 'headers_categories';
    protected $primaryKey = 'id_headers_categories';
    public $timestamps = false;
    protected $fillable = ['name'];
}
