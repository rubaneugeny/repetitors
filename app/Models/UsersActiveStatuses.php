<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * UsersActiveStatuses
 *
 * @mixin Eloquent
 */
class UsersActiveStatuses extends Model
{
    const NOT_ACTIVE = 1;
    const ACTIVE = 2;
    const HIDDEN = 3;
    const BLACK_LIST = 4;
    const DELETED = 7;

    protected $table = 'users_active_statuses';
    protected $primaryKey = 'id_users_active_statuses';
    public $timestamps = false;
}
