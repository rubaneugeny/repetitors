<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * OrdersStatuses
 *
 * @mixin Eloquent
 */
class OrdersStatuses extends Model
{
    const NEW = 1; // Новая заявка

    protected $table = 'orders_statuses';
    protected $primaryKey = 'id_orders_statuses';
    public $timestamps = false;
}
