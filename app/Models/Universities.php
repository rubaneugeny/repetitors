<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * Universities
 *
 * @mixin Eloquent
 */
class Universities extends Model
{
    const SPBGU = 1;

    protected $table = 'universities';
    protected $primaryKey = 'id_universities';
}
