<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * PrependTextTemplates
 *
 * @mixin Eloquent
 */
class PrependTextTemplates extends Model
{
    protected $table = 'prepend_text_templates';
    protected $primaryKey = 'id_prepend_text_templates';
    protected $fillable = [
        'id_prepend_text_categories',
        'id_courses_caterogires_second',
        'id_courses',
        'excludes',
        'template',
        'page',
        'wrap',
        'split',
        'text_limit',
        'class'
    ];
    public $timestamps = false;
}
