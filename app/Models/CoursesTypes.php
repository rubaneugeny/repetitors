<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * CoursesTypes
 *
 * @mixin Eloquent
 */
class CoursesTypes extends Model
{
    const NORMAL_POPULAR_COURSES = 1;   // Репетиторы по нормальным, популярным предметам
    const RARE_COURSES_1 = 2;           // Репетиторы по супер редким предметам
    const RARE_COURSES_2 = 3;           // Репетиторы по супер редким предметам
    const RARE_COURSES_3 = 4;           // Репетиторы по супер редким предметам
    const RARE_COURSES_4 = 5;           // Репетиторы по супер редким предметам
    const TWO_COURSES = 7;              // Репетитора по двум предметам
    CONST THREE_COURSES = 8;            // Репетиторы по трем предметам

    protected $table = 'courses_types';
    protected $primaryKey = 'id_courses_types';
    public $timestamps = false;
}
