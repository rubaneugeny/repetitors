<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * Districts
 *
 * @mixin Eloquent
 */
class Districts extends Model
{
    protected $table = 'districts';
    protected $primaryKey = 'id_districts';
}
