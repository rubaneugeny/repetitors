<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * CountriesCities
 *
 * @mixin Eloquent
 */
class CountriesCities extends Model
{
    protected $table = 'countries_cities';
    protected $primaryKey = 'id_countries_cities';
    public $timestamps = false;
}
