<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * AdminsReviews
 *
 * @mixin Eloquent
 */
class AdminsReviews extends Model
{
    protected $table = 'admins_reviews';
    protected $primaryKey = 'id_admins_reviews';
}
