<?php

namespace App\Models;

use Carbon\Carbon;
use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * News
 *
 * @mixin Eloquent
 */
class News extends Model
{
    protected $table = 'news';
    protected $primaryKey = 'id_news';
    protected $attributes = [
        'photo' => null,
        'description' => null,
    ];

    public function save(array $options = [])
    {
        if (empty($this->date)) {
            $this->date = Carbon::now();
        }

        return parent::save($options);
    }
}
