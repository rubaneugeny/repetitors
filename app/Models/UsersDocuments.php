<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * UsersDocuments
 *
 * @mixin Eloquent
 */
class UsersDocuments extends Model
{
    protected $table = 'users_documents';
    protected $primaryKey = 'id_users_documents';
}
