<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * CommentsPrependTextCategories
 *
 * @mixin Eloquent
 */
class CommentsPrependTextCategories extends Model
{
    protected $table = 'comments_prepend_text_categories';
    protected $primaryKey = 'id_comments_prepend_text_categories';
    public $timestamps = false;
}
