<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * NewsUsers
 *
 * @mixin Eloquent
 */
class NewsUsers extends Model
{
    protected $table = 'news_users';
    protected $primaryKey = 'id_news_users';
}
