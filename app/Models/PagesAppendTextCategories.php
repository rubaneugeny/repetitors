<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * PagesAppendTextCategories
 *
 * @mixin Eloquent
 */
class PagesAppendTextCategories extends Model
{
    protected $table = 'pages_append_text_categories';
    protected $primaryKey = 'id_pages_append_text_categories';
    protected $fillable = ['name'];
    public $timestamps = false;
}
