<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * CoursesCategories
 *
 * @mixin Eloquent
 */
class CoursesCategories extends Model
{
    const GENERAL = 1;
    const LANGUAGES = 2;
    const MUSIC = 3;
    const OTHER_SUBJECTS = 4;
    const SPORT_DISCIPLINES = 5;
    const DANCE = 7;
    const LOGOPEDICS = 8;

    protected $table = 'courses_categories';
    protected $primaryKey = 'id_courses_categories';
    public $timestamps = false;
}
