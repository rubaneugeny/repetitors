<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * TitleTemplates
 *
 * @mixin Eloquent
 */
class TitleTemplates extends Model
{
    protected $table = 'title_templates';
    protected $primaryKey = 'id_title_templates';
    protected $fillable = [
        'id_title_categories',
        'id_courses_categories',
        'id_courses_categories_second',
        'id_courses_types',
        'id_parent_course',
        'id_courses',
        'page',
        'title',
        'description'
    ];
    public $timestamps = false;
}
