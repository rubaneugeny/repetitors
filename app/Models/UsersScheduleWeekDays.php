<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * UsersScheduleWeekDays
 *
 * @mixin Eloquent
 */
class UsersScheduleWeekDays extends Model
{
    protected $table = 'users_schedule_week_days';
    protected $primaryKey = 'id_users_schedule_week_days';
    public $timestamps = false;
}
