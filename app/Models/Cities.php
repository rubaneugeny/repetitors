<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * Cities
 *
 * @mixin Eloquent
 */
class Cities extends Model
{
    const SAINTS_PETERSBURG = 1;

    protected $table = 'cities';
    protected $primaryKey = 'id_cities';
}
