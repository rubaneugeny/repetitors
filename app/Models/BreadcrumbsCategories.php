<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * BreadcrumbsCategories
 *
 * @mixin Eloquent
 */
class BreadcrumbsCategories extends Model
{
    protected $table = 'breadcrumbs_categories';
    protected $primaryKey = 'id_breadcrumbs_categories';
    public $timestamps = false;
    protected $fillable = ['name'];
}
