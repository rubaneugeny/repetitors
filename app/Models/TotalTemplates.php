<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * TotalTemplates
 *
 * @mixin Eloquent
 */
class TotalTemplates extends Model
{
    protected $table = 'total_templates';
    protected $primaryKey = 'id_total_templates';
    public $timestamps = false;
    protected $fillable = [
        'id_total_categories',
        'id_courses_categories_second',
        'id_courses',
        'template',
        'page'
    ];
}
