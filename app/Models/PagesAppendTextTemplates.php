<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * PagesAppendTextTemplates
 *
 * @mixin Eloquent
 */
class PagesAppendTextTemplates extends Model
{
    protected $table = 'pages_append_text_templates';
    protected $primaryKey = 'id_pages_append_templates';
    protected $fillable = ['id_pages_append_text_categories', 'id_courses_categories', 'id_courses', 'template', 'page', 'split', 'seo', 'text_limit', 'repetitors_count_limit', 'class'];
    protected $attributes = [
        'id_courses_categories' => null,
        'id_courses' => null,
        'split' => 0,
        'seo' => 0,
        'repetitors_count_limit' => 0
    ];
    public $timestamps = false;
}
