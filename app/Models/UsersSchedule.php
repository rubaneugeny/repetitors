<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * UsersSchedule
 *
 * @mixin Eloquent
 */
class UsersSchedule extends Model
{
    protected $table = 'users_schedule';
    protected $primaryKey = 'id_users_schedule';
}
