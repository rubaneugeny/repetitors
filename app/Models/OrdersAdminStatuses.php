<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * OrdersAdminStatuses
 *
 * @mixin Eloquent
 */
class OrdersAdminStatuses extends Model
{
    const NEW = 1;

    protected $table = 'orders_admin_statuses';
    protected $primaryKey = 'id_orders_admin_statuses';
    public $timestamps = false;
}
