<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * BreadcrumbsTemplates
 *
 * @mixin Eloquent
 */
class BreadcrumbsTemplates extends Model
{
    protected $table = 'breadcrumbs_templates';
    protected $primaryKey = 'id_breadcrumbs_templates';
    public $timestamps = false;
    protected $fillable = [
        'id_breadcrumbs_categories',
        'id_courses',
        'type',
        'last',
        'items',
        'page'
    ];
    protected $attributes = [
        'id_courses' => null,
        'type' => null
    ];
}
