<?php

namespace App\Models;

use Eloquent;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

/**
 * Users
 *
 * @mixin Eloquent
 */
class Users extends Authenticatable
{
    use Notifiable;

    const SEX_MAN = 0;
    const SEX_WOMAN = 1;

    const TYPE_REPETITOR = 1;
    const TYPE_CAR_INSTRUCTOR = 2;

    protected $table = 'users';
    protected $primaryKey = 'id_users';
    protected $attributes = [
        'slug' => null,
        'email' => null,
        'last_name' => null,
        'middle_name' => null,
        'phone' => null,
        'landline_phone' => null,
        'year_of_birth' => null,
        'education' => null,
        'work_experience' => null,
        'work_experience_info' => null,
        'graduation_year' => null,
        'id_courses' => null,
        'id_courses_second' => null,
        'id_courses_third' => null,
        'id_courses_fourth' => null,
        'id_cities' => null,
        'id_cities_second' => null,
        'id_cities_third' => null,
        'id_cities_fourth' => null,
        'metro_list' => null,
        'id_metro' => null,
        'id_universities' => null,
        'reg_date' => null,
    ];

    public function getPrefixName($exclude_car_instructors = false)
    {
        if ($this->is_car_instructor && !$exclude_car_instructors) {
            return 'Инструктор по вождению ';
        } elseif (($this->course && $this->course->id_courses_categories == CoursesCategories::SPORT_DISCIPLINES) ||
            ($this->courseSecond && $this->courseSecond->id_courses_categories == CoursesCategories::SPORT_DISCIPLINES) ||
            ($this->courseThird && $this->courseThird->id_courses_categories == CoursesCategories::SPORT_DISCIPLINES) ||
            ($this->courseFourth && $this->courseFourth->id_courses_categories == CoursesCategories::SPORT_DISCIPLINES)) {

            return 'Тренер ';
        } elseif (($this->course && $this->course->id_courses_categories == CoursesCategories::LOGOPEDICS) ||
            ($this->courseSecond && $this->courseSecond->id_courses_categories == CoursesCategories::LOGOPEDICS) ||
            ($this->courseThird && $this->courseThird->id_courses_categories == CoursesCategories::LOGOPEDICS) ||
            ($this->courseFourth && $this->courseFourth->id_courses_categories == CoursesCategories::LOGOPEDICS)) {

            return 'Логопед ';
        }

        return 'Репетитор ';
    }

    public function getOrdersCount()
    {
        return $this->hasMany('App\Models\Orders', 'id_users', 'id_users')->whereIn('id_admins', [29, 3, 33, 34, 35, 4, 41, 42, 43])->count();
    }

    public function checkAutomaticTransmissionCourse()
    {
        return $this->id_courses == Courses::AUTOMATIC_TRANSMISSION
            || $this->id_courses_second == Courses::AUTOMATIC_TRANSMISSION
            || $this->id_courses_third == Courses::AUTOMATIC_TRANSMISSION
            || $this->id_courses_fourth == Courses::AUTOMATIC_TRANSMISSION;
    }

    public function checkManualTransmissionCourse()
    {
        return $this->id_courses == Courses::MANUAL_TRANSMISSION
            || $this->id_courses_second == Courses::MANUAL_TRANSMISSION
            || $this->id_courses_third == Courses::MANUAL_TRANSMISSION
            || $this->id_courses_fourth == Courses::MANUAL_TRANSMISSION;
    }

    public function isWoman()
    {
        return $this->sex == self::SEX_WOMAN;
    }

    public function getLink()
    {
        return !empty($this->slug) ? $this->slug . '-' . $this->id_users . '.php' : 'profrep.php?id_user=' . $this->id_users;
    }

    public function getLastName()
    {
        return $this->show_last_name ? $this->last_name . ' ' : null;
    }

    public function getPhoto($changeEmptyToLogo = false)
    {
        $path = env('PHOTOS_USERS_DIR');

        if (!empty($this->photo) && file_exists(public_path($path . $this->photo))) {
            return $path . $this->photo;
        }

        if ($changeEmptyToLogo) {
            return '/img/logo.png';
        }

        return $path . (!$this->isWoman() ? 'teacher0.jpg' : 'teacher.jpg');
    }

    public function getRating()
    {
        return str_replace('.0', '', $this->reviews_rating);
    }

    public function course()
    {
        return $this->hasOne('App\Models\Courses', 'id_courses', 'id_courses');
    }

    public function courseSecond()
    {
        return $this->hasOne('App\Models\Courses', 'id_courses', 'id_courses_second');
    }

    public function courseThird()
    {
        return $this->hasOne('App\Models\Courses', 'id_courses', 'id_courses_third');
    }

    public function courseFourth()
    {
        return $this->hasOne('App\Models\Courses', 'id_courses', 'id_courses_fourth');
    }

    public function metro()
    {
        return $this->hasOne('App\Models\Metro', 'id_metro', 'id_metro');
    }

    public function metroSecond()
    {
        return $this->hasOne('App\Models\Metro', 'id_metro', 'id_metro_second');
    }

    public function city()
    {
        return $this->hasOne('App\Models\Cities', 'id_cities', 'id_cities');
    }

    public function citySecond()
    {
        return $this->hasOne('App\Models\Cities', 'id_cities', 'id_cities_second');
    }

    public function cityThird()
    {
        return $this->hasOne('App\Models\Cities', 'id_cities', 'id_cities_third');
    }

    public function cityFourth()
    {
        return $this->hasOne('App\Models\Cities', 'id_cities', 'id_cities_fourth');
    }

    public function university()
    {
        return $this->hasOne('App\Models\Universities', 'id_universities', 'id_universities');
    }

    public function district()
    {
        return $this->hasOne('App\Models\Districts', 'id_districts', 'id_districts');
    }

    public function car()
    {
        return $this->hasOne('App\Models\Cars', 'id_cars', 'id_cars');
    }

    public function carSecond()
    {
        return $this->hasOne('App\Models\Cars', 'id_cars', 'id_cars_second');
    }

    public function usersActiveStatuses()
    {
        return $this->hasOne('App\Models\UsersActiveStatuses', 'id_users_active_statuses', 'id_users_active_statuses');
    }

    public function status()
    {
        return $this->hasOne('App\Models\UsersStatuses', 'id_users_statuses', 'id_users_statuses');
    }

    public function documents()
    {
        return $this->hasMany('App\Models\UsersDocuments', 'id_users', 'id_users');
    }

    public function documentsCount()
    {
        return $this->hasMany('App\Models\UsersDocuments', 'id_users', 'id_users')->count();
    }

    public function video()
    {
        return $this->hasMany('App\Models\UsersVideo', 'id_users', 'id_users');
    }

    public function videoCount()
    {
        return $this->hasMany('App\Models\UsersVideo', 'id_users', 'id_users')->count();
    }

    // Объединение видео и документов, так как выводятся в одном блоке
    public function documentsVideoCount()
    {
        return $this->documentsCount() + $this->videoCount();
    }

    public function documentsVideo()
    {
        return $this->documents->merge($this->video);
    }

    // Общее кол-во отзывов
    public function reviewsCount()
    {
        return $this->belongsTo('App\Models\Reviews', 'id_users', 'id_users')
            ->where(['id_reviews_statuses' => ReviewsStatuses::APPROVED, 'is_deleted' => 0])
            ->count();
    }

    // Количество положительных отзывов
    public function reviewsPositiveCount()
    {
        return $this->belongsTo('App\Models\Reviews', 'id_users', 'id_users')
            ->where(['id_reviews_statuses' => ReviewsStatuses::APPROVED, 'is_deleted' => 0])
            ->where('rating', '>', 3)
            ->count();
    }

    // Количество нейтральных отзывов
    public function reviewsNeutralCount()
    {
        return $this->belongsTo('App\Models\Reviews', 'id_users', 'id_users')
            ->where(['id_reviews_statuses' => ReviewsStatuses::APPROVED, 'is_deleted' => 0, 'rating' => 3])
            ->count();
    }

    // Количество негативных отзывов
    public function reviewsNegativeCount()
    {
        return $this->belongsTo('App\Models\Reviews', 'id_users', 'id_users')
            ->where(['id_reviews_statuses' => ReviewsStatuses::APPROVED, 'is_deleted' => 0])
            ->where('rating', '<', 3)
            ->count();
    }

    public function getTotalRating()
    {
        $reviews = $this->belongsTo('App\Models\Reviews', 'id_users', 'id_users')
            ->where(['id_reviews_statuses' => ReviewsStatuses::APPROVED, 'is_deleted' => 0])
            ->selectRaw('SUM(rating) AS s, COUNT(*) AS c')
            ->first();

        return $reviews->c > 0 ? round($reviews->s / $reviews->c, 1) : 0;
    }

//    protected $fillable = [
//        'name', 'email', 'password',
//    ];
//    protected $hidden = [
//        'password', 'remember_token',
//    ];

//    protected $casts = [
//        'email_verified_at' => 'datetime',
//    ];
}
