<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * MetroBranches
 *
 * @mixin Eloquent
 */
class MetroBranches extends Model
{
    protected $table = 'metro_branches';
    protected $primaryKey = 'id_metro_branches';
    public $timestamps = false;
}
