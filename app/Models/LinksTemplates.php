<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * LinksTemplates
 *
 * @mixin Eloquent
 */
class LinksTemplates extends Model
{
    protected $table = 'links_templates';
    protected $primaryKey = 'id_links_templates';
    public $timestamps = false;
}
