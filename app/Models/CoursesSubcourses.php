<?php

namespace App\Models;

use Eloquent;
use App\Traits\HasCompositePrimaryKey;
use Illuminate\Database\Eloquent\Model;

/**
 * CoursesSubcourses
 *
 * @mixin Eloquent
 */
class CoursesSubcourses extends Model
{
    use HasCompositePrimaryKey;

    protected $table = 'courses_subcourses';
    protected $primaryKey = ['id_courses', 'id_users'];
    public $incrementing = false;
    public $timestamps = false;
}
