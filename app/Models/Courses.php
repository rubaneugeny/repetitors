<?php

namespace App\Models;

use Eloquent;

/**
 * Courses
 *
 * @mixin Eloquent
 */
class Courses extends Eloquent
{
    const MATHEMATICS = 1;      // Математика
    const RUSSIAN = 3;          // Русский язык
    const CHINESE = 19;         // Китайский язык
    const OTHER_SUBJECTS = 26;  // Другие предметы
    const DRAWING = 64;         // Черчение
    const BOXING = 108;         // Бокс
    const YOGA = 111;           // Йога
    const SKIES = 116;          // Горные лыжи
    const SNOWBOARD = 117;      // Сноуборд
    const SWIMMING = 112;       // Плавание
    const AUTOMATIC_TRANSMISSION = 1000; // Автоматическая коробка передач
    const MANUAL_TRANSMISSION = 1001;    // Механическая коробка передач
    const MOTORBIKES = 1002;    // Мотоциклы

    protected $table = 'courses';
    protected $primaryKey = 'id_courses';
    protected $attributes = [
        'id_courses_categories_second' => null,
        'id_title_categories' => null,
        'id_total_categories' => null,
        'id_prepend_text_categories' => null,
        'id_photo_text_categories' => null,
        'id_comments_prepend_text_categories' => null,
        'id_pages_append_text_categories' => null,
        'id_parent_course' => null,
        'id_courses_second' => null,
        'id_courses_third' => null,
        'recommend_name' => null,
        'recommend_name_second' => null,
        'recommend_name_third' => null,
        'recommend_name_seventh' => null,
        'recommend_name_eighth' => null,
        'recommend_name_nineth' => null,
        'cities' => null
    ];

    public function seo()
    {
        return $this->hasOne('App\Models\Seo', 'id_seo', 'id_courses');
    }

    public function parentCourse()
    {
        return $this->hasOne('App\Models\Courses', 'id_courses', 'id_parent_course');
    }

    public function secondCourse()
    {
        return $this->hasOne('App\Models\Courses', 'id_courses', 'id_courses_second');
    }
}
