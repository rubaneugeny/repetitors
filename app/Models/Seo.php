<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * Seo
 *
 * @mixin Eloquent
 */
class Seo extends Model
{
    protected $table = 'seo';
    protected $primaryKey = 'id_seo';
    public $timestamps = false;
}
