<?php

namespace App\Models;

use Carbon\Carbon;
use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * Reviews
 *
 * @mixin Eloquent
 */
class Reviews extends Model
{
    protected $table = 'reviews';
    protected $primaryKey = 'id_reviews';
    protected $fillable = [
        'comment',
        'rating',
        'name',
        'phone'
    ];
    protected $attributes = [
        'id_reviews_statuses' => ReviewsStatuses::NEW
    ];

    public function status()
    {
        return $this->hasOne('App\Models\ReviewsStatuses', 'id_reviews_statuses', 'id_reviews_statuses');
    }

    public function user()
    {
        return $this->hasOne('App\Models\Users', 'id_users', 'id_users');
    }

    public function course()
    {
        return $this->hasOne('App\Models\Courses', 'id_courses', 'id_courses');
    }

    public function order()
    {
        return $this->hasOne('App\Models\Orders', 'id_orders', 'id_orders');
    }

    public function save(array $options = [])
    {
        if (empty($this->date)) {
            $this->date = Carbon::now();
        }

        return parent::save($options);
    }
}
