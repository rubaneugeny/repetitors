<?php

namespace App\Garbage;

use Illuminate\Database\Eloquent\Model;

/**
 * @mixin \Eloquent
 */
class OtzyvDel extends Model
{
    protected $table = 'otzyv_del';
    protected $primaryKey = 'id_post';
    public $timestamps = false;
}
