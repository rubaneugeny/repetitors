<?php

namespace App\Garbage;

use Illuminate\Database\Eloquent\Model;

class LernGeo extends Model
{
    protected $table = 'lerngeo';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
