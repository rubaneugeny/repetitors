<?php

namespace App\Garbage;

use Illuminate\Database\Eloquent\Model;

/**
 * @mixin \Eloquent
 */
class OtzyvNegDel extends Model
{
    protected $table = 'otzyv_neg_del';
    protected $primaryKey = 'id_post';
    public $timestamps = false;
}
