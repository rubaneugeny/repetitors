<?php

namespace App\Garbage;

use Illuminate\Database\Eloquent\Model;

/**
 * @mixin \Eloquent
 */
class OtzyvAdd extends Model
{
    protected $table = 'otzyv_add';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
