<?php

namespace App\Garbage;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * Headers
 *
 * @mixin Eloquent
 */
class Headers extends Model
{
    protected $table = 'headers';
    protected $primaryKey = 'id_headers';
    public $timestamps = false;
}
