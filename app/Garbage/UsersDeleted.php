<?php

namespace App\Garbage;

use Illuminate\Database\Eloquent\Model;

class UsersDeleted extends Model
{
    protected $table = 'users_deleted';
    protected $primaryKey = 'id_users_deleted';
}
