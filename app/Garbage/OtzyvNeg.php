<?php

namespace App\Garbage;

use Illuminate\Database\Eloquent\Model;

/**
 * @mixin \Eloquent
 */
class OtzyvNeg extends Model
{
    protected $table = 'otzyv_neg';
    protected $primaryKey = 'id_post';
    public $timestamps = false;
}
