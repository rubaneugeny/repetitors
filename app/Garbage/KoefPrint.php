<?php

namespace App\Garbage;

use Illuminate\Database\Eloquent\Model;

/**
 * @mixin \Eloquent
 */
class KoefPrint extends Model
{
    protected $table = 'koef_print';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
