<?php

namespace App\Console\Commands;

use App\Models\Courses;
use Illuminate\Console\Command;

class CreateCoursesList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:CreateCoursesList';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $slug = Courses::whereNotNull('slug')->where('slug', '<>', '')->whereNull('id_parent_course')->pluck('slug')->toArray();
        $slugSecond = Courses::whereNotNull('slug_second')->where('slug_second', '<>', '')->whereNull('id_parent_course')->pluck('slug_second')->toArray();

        $merged = array_unique(array_merge($slug, $slugSecond));
        unset($merged['muzyka']);

        file_put_contents('links.txt', implode('|', $merged));

        /**************************************************************************************************************/

        $slug = Courses::whereNotNull('slug')->where('slug', '<>', '')->whereNotNull('id_parent_course')->pluck('slug')->toArray();
        $slugSecond = Courses::whereNotNull('slug_second')->where('slug_second', '<>', '')->whereNotNull('id_parent_course')->pluck('slug_second')->toArray();

        $merged = array_unique(array_merge($slug, $slugSecond));

        file_put_contents('links_subcourses.txt', implode('|', $merged));
    }
}
