<?php

namespace App\Extensions;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class CustomLengthAwarePaginator extends LengthAwarePaginator
{
    public function previousPageUrl()
    {
        if ($this->currentPage() > 0) {
            return $this->url(($this->currentPage() - 1) * $this->perPage - $this->perPage);
        }
    }

    public function nextPageUrl()
    {
        if ($this->hasMorePages()) {
            return $this->url(($this->currentPage() + 1) * $this->perPage - $this->perPage);
        }
    }

    protected function isValidPageNumber($page)
    {
        return $page >= 0 && filter_var($page, FILTER_VALIDATE_INT) !== false;
    }

    public function getUrlRange($start, $end)
    {
        return collect(range($start, $end))->mapWithKeys(function ($page) {
            return [$page => $this->url($page * $this->perPage - $this->perPage)];
        })->all();
    }

    protected function setCurrentPage($currentPage, $pageName)
    {
        $currentPage = $currentPage ?: static::resolveCurrentPage($pageName);

        return $this->isValidPageNumber($currentPage) ? (int) $currentPage : 0;
    }

    public function url($page)
    {
        if ($page <= 0) {
            $page = null;
        }

        // If we have any extra query string key / value pairs that need to be added
        // onto the URL, we will put them in query string form and then attach it
        // to the URL. This allows for extra information like sortings storage.
        $parameters = [$this->pageName => $page];

        if (count($this->query) > 0) {
            $parameters = array_merge($this->query, $parameters);
        }

        $parameters = Arr::query($parameters);

        return $this->path()
            .(!empty($parameters) ? (Str::contains($this->path(), '?') ? '&' : '?').$parameters : '')
            .$this->buildFragment();
    }

//    public function toArray()
//    {
//        return [
//            'current_page' => $this->currentPage(),
//            'data' => $this->items->toArray(),
//            'first_page_url' => $this->url(0),
//            'from' => $this->firstItem(),
//            'last_page' => $this->lastPage(),
//            'last_page_url' => $this->url($this->lastPage()),
//            'next_page_url' => $this->nextPageUrl(),
//            'path' => $this->path(),
//            'per_page' => $this->perPage(),
//            'prev_page_url' => $this->previousPageUrl(),
//            'to' => $this->lastItem(),
//            'total' => $this->total(),
//        ];
//    }
}
