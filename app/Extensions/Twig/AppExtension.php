<?php
/*
 * Web-studio: Chameleon
 * Url: https://chameleonweb.ru
 *
 * Created by Ruban Eugeny
 * E-mail: schiller2k@gmail.com
 *
 * Date: 12.03.2020
 * Time: 12:02
 */

namespace App\Extensions\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class AppExtension extends AbstractExtension
{
    /**
     * @return array|TwigFilter[]
     */
    public function getFilters()
    {
        return [
            new TwigFilter('cut_up_to_marks', [$this, 'cutUpToMarks']),
            new TwigFilter('cut_up_to_sentence', [$this, 'cutUpToSentence']),
            new TwigFilter('plural_word', [$this, 'pluralWord']),
            new TwigFilter('plural_text', [$this, 'pluralWord']),
            new TwigFilter('unique', [$this, 'unique']),
            new TwigFilter('ucfirst', [$this, 'ucfirst']),
            new TwigFilter('nlbr', [$this, 'nlbr']),
            new TwigFilter('quotes', [$this, 'quotes']),
        ];
    }

    /**
     * Example usage: 'черновик'|plural_word(2, '|а|ов', ''); # Return: черновика
     *
     * @param string $word
     * @param number $count
     * @param string $endings
     * @param string $delimiter
     *
     * @return string
     */
    public function pluralWord(?string $word, $count, string $endings, string $delimiter = '')
    {
        $endings = preg_split('/[,\|-]/', $endings);
        $cases = [2, 0, 1, 1, 1, 2];
        $ending = sprintf($endings[($count % 100 > 4 && $count % 100 < 20) ? 2 : $cases[min($count % 10, 5)]], $count);

        return $word.$delimiter.$ending;
    }

    /**
     * Example usage: plural_text('Перенесли 2 черновик%s в товары.', '|а|ов');
     *
     * @param string $text
     * @param string $endings
     *
     * @return string
     */
    public function pluralText(string $text, string $endings)
    {
        if (!preg_match('/(\d+)/', $text, $match)) {
            return $text;
        }

        $count = $match[1];
        $endings = preg_split('/[,\|-]/', $endings);
        $cases = [2, 0, 1, 1, 1, 2];
        $ending = sprintf($endings[($count % 100 > 4 && $count % 100 < 20) ? 2 : $cases[min($count % 10, 5)]], $count);
        $text = str_replace('%s', $ending, $text);

        return $text;
    }

    /**
     * Обрезание текса до ближайшего знака препинания или пробела
     *
     * @param string|null $text           Исходная строка
     * @param int    $offsetIndex    Исходный идекс, до которого нужно обрезать
     * @param bool   $showHideButton Показать/скрыть кнопку читать далее
     * @param bool   $nlbr           Включить автозамену \n на <br>
     *
     * @return false|string|string[]|null
     */
    public function cutUpToMarks($text, int $offsetIndex, bool $showHideButton = false, bool $nlbr = true)
    {
        $text = htmlspecialchars($text);
        $length = mb_strlen($text);
        $symbols = ['.', '!', '?', ' ', ',', ';'];

        // Установим максимальный индекс в позицию обрезки
        $maxIndex = $offsetIndex;

        // Проверим, что позиция обрезки меньше длины самого текста
        if ($offsetIndex <= $length) {
            // Обрежим текст с указанной позиции до конца строки
            $temp = mb_substr($text, $maxIndex);

            // Рассчитаем длину строки
            $cutLength = mb_strlen($temp);

            // Установим начальную позицию проверки индекса на полную длину строки
            $minIndex = $cutLength;

            // Ищем самый ближайший символ к позиции обрезки
            foreach ($symbols ?? [] as $symbol) {
                $index = mb_strpos($temp, $symbol);

                if ($index !== false) {
                    $minIndex = min($minIndex, $index);
                }
            }

            // Если нашли ближайший знак, то добавляем его к максимальному индексу
            if ($minIndex != $cutLength) {
                $maxIndex += $minIndex;
            }
        }

        // Проверим, что знак находится не в конце всей строки
        if (($length - $maxIndex) > 4) {
            // Обрезаем текст до необходимого индекса
            $result = $nlbr ? self::nlbr(mb_substr($text, 0, $maxIndex)) : mb_substr($text, 0, $maxIndex);

            // Если включен аргумент $showHideButton, то к результату добавляем кнопку читать полностью с оставшимся текстом
            if ($showHideButton) {
                $result .= ' <span>'.($nlbr
                        ? self::nlbr(mb_substr($text, $maxIndex + 1))
                        : mb_substr($text, $maxIndex + 1)).
                    '</span><i>...</i><br><a href="javascript:void(0);" class="btn button_3 s_text_link">Читать полностью</a>';
            }

            return $result;
        }

        return $nlbr ? self::nlbr($text) : $text;
    }

    /**
     * Функция обрезания текста и добавления кнопки читать полностью
     *
     * @param string $text
     * @param int    $offsetIndex
     * @param string $class
     */
    public function cutUpToSentence(string $text, int $offsetIndex, string $class = '')
    {
        $length = mb_strlen($text);
        $symbols = ['.', '!', '?'];

        // Обрезаем строку до необходимого кол-ва
        $temp = mb_substr($text, 0, $offsetIndex);

        // Находим максимальный индекс в строке
        $maxIndex = $length;

        if ($offsetIndex <= $length) {
            $maxIndex = 0;

            // Определяем кол-во символов и ищем максимальный
            $lengthSymbols = count($symbols);
            for ($i = 0; $i < $lengthSymbols; $i++) {
                $maxIndex = max($maxIndex, mb_strpos($temp, $symbols[$i], $offsetIndex));
            }
        }
    }

    /**
     * @param string|null $text
     *
     * @return string|string[]|null
     */
    public function nlbr(?string $text)
    {
        if (is_null($text)) {
            return $text;
        }

        return str_replace("\r\n", '<br>&ensp;&ensp;', $text);
    }

    /**
     * @param string $text
     *
     * @return string|string[]|null
     */
    public function quotes(string $text)
    {
        return preg_replace('/&quot;/', '"', $text);
    }

    /**
     * @param array $items
     *
     * @return array
     */
    public function unique(array $items)
    {
        return array_unique($items);
    }

    /**
     * @param string $text
     *
     * @return string
     */
    public function ucfirst(string $text)
    {
        return mb_strtoupper(mb_substr($text, 0, 1)) . mb_substr($text, 1);
    }
}
